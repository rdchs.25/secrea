<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/* Socialite */
Route::get('login/{rs}', 'Auth\LoginController@redirectToProvider')->name('login.redsocial');
Route::get('login/{rs}/callback', 'Auth\LoginController@handleProviderCallback')->name('callback.redsocial');

/* Verification correo*/
Route::get('/register/verify/{code}','Auth\RegisterController@verification');

Route::group(['middleware' => 'auth'], function () {


		Route::get('/get-user', function (Request $request) {
			return \Auth::user();
		});

		Route::get('/home', 'HomeController@index')->name('home');

		Route::get('/user/{id}/confirmed','UserController@confirmed');

	Route::group(['middleware' => 'has.permission:comment'], function(){
	/* Comments */
		Route::post('comment', 'CommentController@store');	
		Route::get('announcement/{slug}/comment/{id}', 'CommentController@show');	
	});

	/* Payment */
	Route::group(['middleware' => 'has.permission:payment'], function(){
		Route::resource('payment', 'PayStudentController',['only'=>['store']]);
	});
		
		Route::get('announcement/{slug}/payment/{id}', 'PayStudentController@show');

	    Route::resource('voucher', 'VoucherController',['only'=>['store']]);

		Route::get('my-payment/{slug}', 'PayStudentController@mypayment');
	
	/* Perfil */

		Route::resource('profile', 'ProfileController');

	/* Documentos */

		Route::resource('document', 'DocumentController');

	/* Notificaciones */

		Route::get('notification', 'NotificationController@index');		


	/* Mensajes al Soporte (Estudiante) */

		Route::resource('support-message', 'SupportMessageController', ['only' => [
			'store', 'create'
		]]);

	/* sms */
	Route::group(['middleware' => 'has.permission:messages-management'], function(){
		Route::resource('sms','SmsController');	
		Route::get('/sms/{id}/delete', ['uses' => 'SmsController@destroy']);
	});

	Route::group(['middleware' => 'has.permission:messages'], function(){
		Route::post('/sms/send','SmsController@send');
	});
	
	/* Usuarios */

	Route::group(['middleware' => 'has.permission:user-management'], function(){

		Route::get('/user', 'UserController@index');
		Route::resource('user', 'UserController');
		Route::get('user/{slug}/delete', ['uses' => 'UserController@destroy']);
		//Route::get('user/{id}/documents', ['uses' => 'UserController@documents']);
		Route::get('user/change-status/{id}', ['uses' => 'UserController@changeStatusUser']);
		Route::get('user-registrado/{id}/delete', ['uses' => 'UserController@destroyRegistrado']);

	});

	/* Roles */

	Route::group(['middleware' => 'has.permission:rol-management'], function(){

		Route::resource('role', 'RoleController');
		Route::get('role/{id}/delete', ['uses' => 'RoleController@destroy']);
		Route::get('role/{id}/permissions', ['uses' => 'RoleController@editPermissions']);
		Route::post('role/assign/{id}', 'RoleController@addPermissions');
		Route::get('role/remove/{id}/{permission_id}', 'RoleController@removePermissions');

	});

	/* Subject */

	Route::group(['middleware' => 'has.permission:subject-management'], function(){
		
		Route::resource('subject', 'SubjectController');
		Route::get('subject/{slug}/delete', ['uses' => 'SubjectController@destroy']);

	});

	/* Niveles Academicos */

	Route::group(['middleware' => 'has.permission:academic-level-management'], function(){

		Route::resource('academic-level', 'AcademicLevelController');
		Route::get('academic-level/{slug}/delete', ['uses' => 'AcademicLevelController@destroy',]);

	});

	/* Asignaciones de Docentes */

	Route::group(['middleware' => 'has.permission:assignment-teacher-management'], function(){


		Route::resource('assignment-teacher', 'AssignmentTeacherController');
		Route::get('assignment-teacher/{id}/delete', ['uses' => 'AssignmentTeacherController@destroy',]);

		Route::get('userassignment/{user}/announcement/{slug}', 'UserAssignmentController@store');
		Route::get('userassignment/{user}/announcement/{slug}/delete', 'UserAssignmentController@destroy');
		Route::get('userassignment/{user}/announcement/{slug}/analyze', 'UserAssignmentController@analyze');
		Route::get('userassignment/{id}/content', 'UserAssignmentController@content');
		Route::post('changeContent/{id}','UserAssignmentController@changeContent');
	});


	/* Asignaciones de Gestores de Contenido */

	Route::group(['middleware' => 'has.permission:assignment-content-manager-management'], function(){

		Route::resource('content-manager-assignment', 'ContentManagerAssignmentController');
		Route::get('content-manager-assignment/{id}/delete', ['uses' => 'ContentManagerAssignmentController@destroy',]);

	});

	/* Grados */

	Route::group(['middleware' => 'has.permission:grade-management'], function(){

		Route::resource('grade', 'GradeController');
		Route::get('grade/{slug}/delete', ['uses' => 'GradeController@destroy']);

	});

	/* Convocatorias */

	Route::group(['middleware' => 'has.permission:announcement-management'], function(){

		Route::resource('announcement', 'AnnouncementController');
		Route::get('announcement/{slug}/delete', ['uses' => 'AnnouncementController@destroy']);
		Route::get('announcement/{slug}/enrolled','AnnouncementController@enrolled');

	});

	/* Convenios y Empresas Asociadas*/


	Route::group(['middleware' => 'has.permission:agreement-management'], function(){

		Route::resource('agreement', 'AgreementController');
		Route::get('agreement/{slug}/delete', ['uses' => 'AgreementController@destroy']);

		Route::resource('company', 'CompanyController');
		Route::get('company/{slug}/delete', ['uses' => 'CompanyController@destroy']);
		
	});

	/* Pre Inscripciones */

	Route::group(['middleware' => 'has.permission:pre-enrollment-management'], function(){

		Route::get('pre-enrollment', 'PreEnrollmentController@index')->name('pre-enrollment.index');
		Route::get('pre-enrollment/{id}', 'PreEnrollmentController@preEnrollment');

	});

	/* Mensajes al Soporte (Admin) */

	Route::group(['middleware' => 'has.permission:support-message-management'], function(){

		Route::get('support-message', 'SupportMessageController@index');
		Route::get('support-message/{id}/attend', 'SupportMessageController@attend');

	});


	/* Mis Cursos */

	Route::group(['middleware' => 'has.permission:my-subjects-management'], function(){

		Route::get('my-subjects','UserSubjectController@subjectByStudent');
		Route::get('my-subjects/{id}/','UserSubjectController@getCourseAssigned');
		Route::get('my-evaluations/{slug}','UserSubjectController@studentByAssignmentMyProgress');

		
	});

	Route::get('course/{id}','UserSubjectController@getCourse');

	Route::get('/api/course/{id}', 'CourseBuilderController@getCourseInfoStudent');
	Route::get('/api/course-preview/{id}', 'CourseBuilderController@getCoursePreview');

	/* Tiempos Academicos */

	Route::group(['middleware' => 'has.permission:academic-times-management'], function(){
	
		Route::resource('academic-time', 'AcademicTimeController');
		Route::get('academic-time/{slug}/delete', ['uses' => 'AcademicTimeController@destroy']);

	});

	/* Evaluation Type */

	Route::group(['middleware' => 'has.permission:evaluation-type-management'], function(){
	
		Route::resource('evaluation-type', 'EvaluationTypeController');
		Route::get('evaluation-type/{slug}/delete', ['uses' => 'EvaluationTypeController@destroy']);

	});
	Route::get('my-student/{slug}','UserSubjectController@studentByAssignment');
	/* Mis asignaciones */

	Route::group(['middleware' => 'has.permission:my-assignments-management'], function(){

		Route::get('my-assignment','UserSubjectController@subjectByTeacher');
		
	});

	Route::get('assignment','UserSubjectController@subjectOnAnnActive');

	/* Monitor general de evaluaciones */

	Route::group(['middleware' => 'has.permission:evaluation-monitor'], function(){

		Route::get('evaluation-monitor','UserSubjectController@studentByAssignmentProgressMonitor');
		
		Route::get('my-student-progress/{slug}/{id}','UserSubjectController@studentByAssignmentProgress');
		
	});

	/* Generador de Formularios */

	Route::group(['middleware' => 'has.permission:form-generator'], function(){

		Route::get('form-generator','UserSubjectController@subjectByContentManager');
		Route::resource('gallery', 'GalleryController');
		
	});

	/* Gestión de Notificaciones */

	Route::group(['middleware' => 'has.permission:notifications-management'], function(){

		Route::get('notification-management','NotificationManagementController@index');
		Route::get('notification-management/inactivity-mail','NotificationManagementController@inactivityMail');
		
	});

	/* Reportes */

	Route::group(['middleware' => 'has.permission:reports-management'], function(){

		Route::get('constancy','ConstancyController@create');
		Route::post('constancy/generate','ConstancyController@generate');

		Route::get('report','ReportController@index');

		/* Listado Usuarios */
		Route::get('report/users/excel','Report\UserReportController@userListExcel');		
		Route::get('report/users/pdf','Report\UserReportController@userListPDF');
		/* Listo Pre-Inscritos */
		Route::get('report/pre-enrolled-users/excel','Report\UserReportController@userPreEnrolledListExcel');
		Route::get('report/pre-enrolled-users/pdf','Report\UserReportController@userPreEnrolledListPDF');
		/* Listado Matriculados */
		Route::get('report/enrolled-users/excel','Report\UserReportController@userEnrolledListExcel');	
		Route::get('report/enrolled-users/pdf','Report\UserReportController@userEnrolledListPDF');
		/* Listado Usuarios - Documentos */
		Route::get('report/document-users/pdf','Report\UserReportController@userDocumentsListPDF');
		/* Listo de Usuarios - Convenio */
		Route::get('report/agreement-users/pdf','Report\UserReportController@userAgreementsListPDF');
		
		Route::get('report/enroll/{grade}','ReportController@enroll');
				
	});

});

	/* Chat */

	Route::group(['middleware' => 'has.permission:chat'], function(){

		Route::post('/setMessage','ChatMessageController@setMessage');
		
		Route::get('/conversations','ChatMessageController@conversations');

		Route::post('/getConversation','ChatMessageController@getConversation');

		Route::get('/getCountSmsChatIcon','ChatMessageController@getCountSmsChatIcon');

		Route::get('/getNotyChat','ChatMessageController@getNotyChat');

		Route::get('/chat','ChatMessageController@index');

	});

	/* Seguimiento Documental */

	Route::group(['middleware' => 'has.permission:documentary-management'], function(){
		
		Route::get('/documentary-management','DocumentaryManagementController@index');
		Route::get('/documentary-management/{slug}','DocumentaryManagementController@enrolled');
		Route::get('/documentary-management/change-status/{id}','DocumentaryManagementController@changeStatusUser');
		Route::get('user/{id}/documents', ['uses' => 'UserController@documents']);

	});


	/* Demo Cursos */
	
	Route::get('/course-demo','CourseDemoController@index');


	/* Gestión Multimedia */

	Route::group(['middleware' => 'has.permission:media-management'], function(){
		
		Route::get('/form-generator-media-management','UserSubjectController@subjectByMediaManager');
		Route::get('/form-generator-media-management/report/{id}','UserSubjectController@reportPdfSubject');
		Route::resource('gallery', 'GalleryController');

	});

	/* Gestor de Formularios (Mover luego al permiso) */

	Route::get('course-builder/{id}', 'CourseBuilderController@index');
	Route::get('course-preview/{id}', 'CourseBuilderController@showPreview');
	Route::post('/course-log', 'CourseBuilderLogController@startCourseEditSession');
	Route::post('/course-log/{id}', 'CourseBuilderLogController@endCourseEditSession');
	Route::post('/course-builder/{id}', 'SubjectController@updateContent');
	Route::post('/course-preview/{id}', 'SubjectController@updateContentPreview');

	/* Backups */
	
	Route::group(['middleware' => 'has.permission:backups-management'], function(){
	
		Route::get('/backup', 'BackupController@index');
		Route::get('/backup-export', 'BackupController@export');

	});
	/* Drag */

	Route::get('/drag', function(){
		return view('drag');
	});

	Route::get('/test','Report\UserReportController@test');
	Route::get('my-student/{slug}','UserSubjectController@studentByAssignment');
	Route::get('my-student-progress/{slug}/{id}','UserSubjectController@studentByAssignmentProgress');