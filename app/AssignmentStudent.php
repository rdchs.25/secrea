<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Announcement;
use App\Subject;
use App\Grade;
use App\User;

class AssignmentStudent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id', 'grade_id','announcement_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

       /**
     * Relationship with subjects
     *
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    /**
     * Relationship with grades
     *
     */
    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }
    
    /**
     * Relationship with users
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with announcement
     *
     */
    public function announcement()
    {
        return $this->belongsTo(Announcement::class);
    }
}
