<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;


class Agreement extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $fillable = [
        'id','name', 'partner_company_id', 'sales_company_id', 'amount'
    ];		
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function partnerCompanies()
    {
      return $this->belongsTo(Company::class, 'partner_company_id');
    }

    public function saleCompanies()
    {
      return $this->belongsTo(Company::class, 'sales_company_id');
    }

}
