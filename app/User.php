<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Profile;
use App\SupportMessage;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, ShinobiTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email', 'password','confirmed','confirmed_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profiles()
    {
        return $this->hasOne(Profile::class);
    }

    public function preEnrollments()
    {
        return $this->hasMany(PreEnrollment::class);
    }

    public function documents()
    {
        return $this->hasOne(Document::class);
    }    

    public function userAgreements()
    {
        return $this->hasOne(UserAgreement::class);
    }

    public function supportMessages()
    {
        return $this->hasOne(SupportMessage::class);
    }
    
    public function notifications()
    {
      return $this->hasMany(Notification::class);
    } 
    
    public function assignmentStudent()
    {
        return $this->hasMany(AssignmentStudent::class);
    }

}
