<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Announcement extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $fillable = [
		'id','name','slug','size_time','current_time_active','status','academic_time_id'
    ];		
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
   
    public function preEnrollments()
    {
        return $this->hasMany(PreEnrollment::class);
    }

    public function academicTime()
    {
        return $this->belongsTo(AcademicTime::class, 'academic_time_id');
    }
    public function AnnouncementEvaluationType()
    {
        return $this->belongsTo(AnnouncementEvaluationType::class);
    }

}
