<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['id','path_voucher','announcement_id','user_id'];
}
