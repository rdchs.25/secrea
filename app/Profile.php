<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'dni', 'name', 'address', 'phone', 'path_profile_image','birth_date','user_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
	

}
