<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','message','emitter_user_id','receiver_user_id','state','assignment_teacher_id','created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

     /**
     * Relationship with users
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class,'emitter_user_id');
    }

}
