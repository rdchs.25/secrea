<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'week','evaluation_type_id', 'user_assignment_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    
    /**
     * Relationship with User Assignment
     *
     */
    public function userAssignment()
    {
        return $this->belongsTo(UserAssignment::class);
    }
    public function evaluationType()
    {
        return $this->belongsTo(EvaluationType::class);
    }

}
