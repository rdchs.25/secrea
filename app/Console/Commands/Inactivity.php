<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Notification;
use App\Mail\InactivityMail;
use Illuminate\Support\Facades\Mail;


class Inactivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:inactivity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia un correo a los usuarios que presentan una inactividad de siete días o más';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $now = \Carbon\Carbon::now();
        
	
		$users = User::whereHas('roles', function($q){
			$q->where('slug', 'student');
		})->get();

        $count = 0;

        foreach( $users as $user ) {

            $lastLogin = \Carbon\Carbon::parse($user->last_login);

            $length = $lastLogin->diffInDays($now);

            if($length < 6) {
                
                /*Mail::to($user->email)->send(new InactivityMail($user));*/
                $count++;

                 Notification::create([
                    'title'     =>  'No te pierdas de vista '.$user->name,
                    'message'   =>  'Terminar tu secundaria será un gran paso en tu vida ¡Retoma tus lecciones desde donde te quedaste!',
                    'user_id'   =>  $user->id

                ]);

            }

        }

         $this->info( $count.' correos han sido enviado(s) correctamente!');
    }
}
