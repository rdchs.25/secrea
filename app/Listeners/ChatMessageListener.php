<?php
	
namespace App\Listeners;

use App\Events\ChatMessageWasReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChatMessageListener implements ShouldQueue
{
	public function __construct()
	{

	}

	public function handle(ChatMessageWasReceived $event)
	{
		return true;
	} 

}

?>