<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayAnnouncement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','concept', 'rode', 'cuotas','announcement_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function announcement()
    {
        return $this->hasOne(Announcement::class,'id','announcement_id');
    }
}
