<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreEnrollment extends Model
{
    protected $fillable = [
        'user_id', 'announcement_id'
    ];

    public function users()
    {
      return $this->belongsTo(User::class,'user_id');
    }

    public function announcements()
    {
      return $this->belongsTo(Announcement::class, 'announcement_id');
    }	
    public function documents()
    {
        return $this->hasOne(Document::class,'user_id','user_id');
    }  		
}
