<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
		'id','title','message', 'attend', 'user_id'
    ];

    public function users()
    {
      return $this->belongsTo(User::class,'user_id');
    }		
}
