<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AverageAcademicTime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'current_academic_time_active','user_assigment_id','evaluation_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
