<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayStudent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','pay_announcement_id', 'pre_enrollment_id','voucher_id','count'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function payannouncement()
    {
        return $this->hasOne(PayAnnouncement::class,'id','pay_announcement_id');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class,'id','voucher_id');
    }
}
