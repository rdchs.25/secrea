<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class CorreoAgreement extends Mailable
{
    use Queueable, SerializesModels;

    public $user; 
    public $pass;  
    public $partner_company;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$pass,$partner_company)
    {
        $this->user = $user;
        $this->pass = $pass;
        $this->partner_company = $partner_company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.correo-agreement')
                    ->attach( storage_path('app/public').'/secrea/Informes.pdf');
    }
}
