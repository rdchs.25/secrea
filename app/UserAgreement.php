<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAgreement extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id', 'agreement_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

     /**
     * Relationship with users
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

     /**
     * Relationship with agreements
     *
     */
    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }
}
