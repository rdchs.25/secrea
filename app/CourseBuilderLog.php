<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseBuilderLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id','user_id', 'subject_id', 'started_at', 'finished_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public $timestamps = false;

    /**
     * Relationship with users
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Relationship with subjects
     *
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
