<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\AssignmentTeacher;
use App\AssignmentStudent;
use App\Agreement;
use App\ChatMessage;

class UserAssignment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','content','user_id','final_score','assignment_teacher_id','assignment_student_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    /**
     * Relationship with agreement
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function agreement()
    {
    	return $this->belongsTo(Agreement::class);
    }

    public function assignmentTeacher()
    {
        return $this->belongsTo(AssignmentTeacher::class);
    }

    public function assignmentStudent()
    {
        return $this->belongsTo(AssignmentStudent::class);
    }
    
}
