<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAssignment;
use App\AssignmentStudent;
use App\AssignmentTeacher;
use App\Announcement;
use App\User;
use Toastr;

class UserAssignmentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($user,$slug)
    {
        #CONVOCATORIA POR SLUG
        $announcement = Announcement::findBySlug($slug);
        #CHECKED MATRICULA (verificar si el alumno ya está matriculado)
        $userassignment = UserAssignment::with('user','assignmentStudent')->where('user_id',$user)->get();

            #FILTRAR LA MATRICULA POR CONVOCATORIA
            $filtered = $userassignment->filter(function($value) use ($slug){
                return $value->assignmentStudent->announcement->slug == $slug ;
            });

        if(!$filtered->isNotEmpty()){
           
            $assignment_student   = AssignmentStudent::where('user_id',$user)->where('announcement_id',$announcement->id)->first();
            $grade_id = $assignment_student->grade_id;
            #OBTENER LAS ASIGNATURAS DE LA CONVOCATORIA
            $assignments_teachers = AssignmentTeacher::with('subject','grade')->where('announcement_id',$announcement->id)->where('grade_id',$grade_id)->get();
            
            if($assignments_teachers->isEmpty()){
                Toastr::warning('La convocatoria '.$announcement->name.' no tiene asignaciones a los docentes. Verificar las asignaciones al grado del alumno.');
            }else{
                #OBTENER ASSIGNMENT STUDENT
                if(is_null($assignment_student)){
                    Toastr::error('Este alumno no se encuentra en esta convocatoria.');
                }else{
                    #GUARDAR USERASSIGNMENT
                    foreach ($assignments_teachers as $id => $assignment_teacher) {
                        UserAssignment::create([
                            'user_id'               => $user,
                            'assignment_teacher_id' => $assignment_teacher->id,
                            'assignment_student_id' => $assignment_student->id,
                            'content'               => $assignment_teacher->subject->content,
                        ]);
                    }
                    $usuario = User::find($user);
                    $usuario->revokeAllRoles();
                    $usuario->assignRole(3);

                    Toastr::success('Alumno Matriculado Exitosamente.');
                }   
            }
                

        }else{
            Toastr::warning('Este Alumno ya se encuentra matriculado.');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user,$slug)
    {
        $announcement = Announcement::findBySlug($slug);
        #BUSCAMOS TODAS LAS USERASSIGNMENT
        //$UserAssignment = UserAssignment::with('user','assignmentStudent.announcement')->get();
        $userAssignment = UserAssignment::select('id','user_id','assignment_student_id')
                                        ->with('user:id','assignmentStudent:id,announcement_id')
                                        ->where('user_id',$user)
                                        ->get();
            #FILTRAMOS LAS USERASSIGMENT
            $id = $announcement->id;
            $filtered = $userAssignment->filter(function($value) use ($id,$user){
                if( $value->assignmentStudent->announcement_id == $id && $value->user->id == $user){
                    return true;
                }else{
                    return false;
                }
            });
            
        #RECORREMOS Y ELMINAMOS LAS USERASSIGNMENT
        foreach ($filtered as $key => $value) { UserAssignment::destroy($value->id);}
        #MENSAJE
        Toastr::success('Matricula Eliminada Exitosamente');
        #RESPUESTA
        return back();
    }
    public function content($id){
        $user_assignement = UserAssignment::select('id','content')->find($id);
        
        return view('configviews.userassignment.content',compact('user_assignement'));
    }

    public function changeContent(Request $request, $id){

        $ua = UserAssignment::find($id);
        $ua->update(['content'=>$request->input('content')]);
    
        return "true";
    }

    public function analyze($user,$slug)
    {
        $announcement = Announcement::findBySlug($slug);
        #BUSCAMOS TODAS LAS USERASSIGNMENT
        $userAssignment = UserAssignment::select('id','user_id','assignment_student_id','assignment_teacher_id','content')
                                        ->with('user:id','assignmentStudent:id,announcement_id')
                                        ->where('user_id',$user)
                                        ->get();
            #FILTRAMOS LAS USERASSIGMENT
            $id = $announcement->id;
            $filtered = $userAssignment->filter(function($value) use ($id,$user){
                if( $value->assignmentStudent->announcement_id == $id && $value->user->id == $user){
                    return true;
                }else{
                    return false;
                }
            });
        #RECORREMOS Y ELMINAMOS LAS USERASSIGNMENT
        foreach ($filtered as $key => $value) { 
            if(!isset($value->content) || $value->content == 'null'){
                $assignment_teacher = AssignmentTeacher::with('subject:id,content')->where('id',$value->assignment_teacher_id)->first();
                $value->update(
                    [
                        'content'  => $assignment_teacher->subject->content,
                    ]
                );
            }
        }
        
        #MENSAJE
        Toastr::success('Matrícula analizada');
        #RESPUESTA
        return back();
    }
}
