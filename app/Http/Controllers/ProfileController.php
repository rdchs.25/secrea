<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use Auth;
use App\Document;
use App\Profile;
use App\Announcement;
use App\UserAssignment;
use App\Assignment;
use App\User;
use Toastr;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $announcement = Announcement::where('status',1)->first();
        // $id = $announcement->id;

        //     $subjectByUser = UserAssignment::with('user','assignment.subject')
        //         ->where('user_id', Auth::id())
        //         ->get();

        //             $filtered = $subjectByUser->filter(function($value) use ($id){
        //                 return $value->assignment->announcement_id == $id ;
        //             });

        //     $subjectByTeacher = Assignment::with('subject','grade.academicLevels')
        //         ->where('user_id', Auth::id())
        //         ->where('announcement_id',$id)
        //         ->get();

                        
        //     $countSubjectByUser     = $filtered->count();
        //     $countSubjectByTeacher  = $subjectByTeacher->count();

        // return view('configviews.profile.index', compact('countSubjectByUser', 'countSubjectByTeacher'));
        return view('configviews.profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {  

        $user_id =  Auth::user()->id;
        Profile::create([
            'dni'                   =>  $request->input('dni'),
            'name'                  =>  $request->input('name'),
            'address'               =>  $request->input('address'),
            'phone'                 =>  $request->input('phone'),
            'birth_date'            =>  $request->input('birth_date'),
            'path_profile_image'    =>  $request->file('path_profile_image')->store('profile', 'public'),
            'user_id'               =>  $user_id
        ]);

        $user = User::find($user_id);
        $user->name = $request->input('name');
        $user->save();

        Toastr::success('Perfil Editado Exitosamente');

        return redirect('/profile/'.Auth::user()->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #Check user authenticado
        if(Auth::user()->id == $id){
            $profile  = Profile::where('user_id',$id)->first();
            return view('configviews.profile.edit', compact("profile"));
        }
        else{
            return redirect('/profile');
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Profile $profile, ProfileRequest $request)
    {
        #Obtener nuevos datos
        $new_data = $request->all();
        #verificar ingreso de image profile
        if(!empty($request->file('path_profile_image'))){
            $new_data['path_profile_image']  =  $request->file('path_profile_image')->store('profile', 'public');
            \Storage::delete($profile->path_profile_image);
        }else{
            unset($new_data['path_profile_image']);
        }   
        #Update profile
        $profile->update($new_data);
        #Update user
        $user = User::find($profile->user_id);
        $user->name = $request->input('name');
        $user->save();
        #Mensaje
        Toastr::success('Perfil Editado Exitosamente');
        #Response
        return redirect('/profile/'.Auth::user()->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
