<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use App\Announcement;
use App\PreEnrollment;
use App\Document;
use Auth;
use Toastr; 

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $help = explode('/',$request->server('HTTP_REFERER')); // case: auth == admin
        $this->validate($request, [
            'path_voucher'                  =>  'mimes:jpg,png,jpeg,gif,svg,pdf',
        ]);  
        $auth = Auth::user()->isRole('registrado') || Auth::user()->isRole('student')? Auth::id() : end($help);
        $pe = PreEnrollment::with('announcements')->where('user_id',$auth)->orderBy('created_at','DESC')->first();
        $ann = $pe->announcements;
        
        if(!empty($request->file('path_voucher'))){
            Voucher::create([
                'path_voucher'      => $request->file('path_voucher')->store('voucher', 'public'),
                'status'            => 1,
                'announcement_id'   => $ann->id,
                'user_id'           => $auth
            ]);

            Document::updateOrCreate(
                ['user_id'      => $auth],
                ['path_voucher' => $request->file('path_voucher')->store('voucher', 'public')]
            );

            Toastr::success('Voucher registrado con éxito');
        }else{
            Toastr::error('Ingresa el voucher');
        }

        return redirect()->back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
