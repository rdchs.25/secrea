<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\PreEnrollment;
use App\Document;
use App\Voucher;
use Toastr;

class DocumentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'path_dni'                  =>  'mimes:jpg,png,jpeg,gif,svg,pdf',
            'path_study_certificate'    =>  'mimes:jpg,png,jpeg,gif,svg,pdf',
            'path_voucher'              =>  'mimes:jpg,png,jpeg,pdf'
        ]);  

        $help = explode('/',$request->server('HTTP_REFERER')); 
        $auth = Auth::user()->isRole('registrado') || Auth::user()->isRole('student') ? Auth::id() : end($help);

       $data = [];
       if(!empty($request->file('path_dni'))){
           $data['path_dni']  =  $request->file('path_dni')->store('dni', 'public');
       }else{
          unset($data['path_dni']);
       }
       if(!empty($request->file('path_study_certificate'))){
           $data['path_study_certificate']  =  $request->file('path_study_certificate')->store('study_certicate', 'public');
       }else{
          unset($data['path_study_certificate']);
       }
       if(!empty($request->file('path_voucher'))){
            $data['path_voucher']  =  $request->file('path_voucher')->store('voucher', 'public');
            $pe = PreEnrollment::with('announcements')->where('user_id',$auth)->orderBy('created_at','DESC')->first();
            Voucher::create([
                'path_voucher'      => $request->file('path_voucher')->store('voucher', 'public'),
                'status'            => 1,
                'announcement_id'   => $pe->announcements->id,
                'user_id'           => $auth,
            ]);
       }else{
          unset($data['path_voucher']);
       }

       if(empty($data)){
           Toastr::warning('Usted no ha ingresado ningún documento');
       }else{
           $data['user_id'] = $auth;
           Document::create($data);
           Toastr::success('Documentos Registrados Exitosamente');
       }

       return redirect()->back();
       //return redirect('/document/'.Auth::user()->id.'/edit');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->id == $id){
            $document = Document::where('user_id',Auth::user()->id)->first();
            return view('configviews.document.index',compact("document"));
        }
        else{
            return redirect('/home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Document $document, Request $request)
    { 
        $help = explode('/',$request->server('HTTP_REFERER')); 
        $auth = Auth::user()->isRole('registrado') || Auth::user()->isRole('student') ? Auth::id() : end($help);

        $new_data = [];
        #Obtener nuevos datos
        if(!empty($request->file('path_dni'))){
            $new_data['path_dni']  =  $request->file('path_dni')->store('dni', 'public');
            \Storage::delete($document->path_dni);
        }
        else{
           unset($new_data['path_dni']);
        }
        if(!empty($request->file('path_study_certificate'))){
            $new_data['path_study_certificate']  =  $request->file('path_study_certificate')->store('study_certicate', 'public');
            \Storage::delete($document->path_study_certificate);
        }
        else{
           unset($new_data['path_study_certificate']);
        }
        if(!empty($request->file('path_voucher'))){
            $new_data['path_voucher']  =  $request->file('path_voucher')->store('voucher', 'public');
            $pe = PreEnrollment::with('announcements')->where('user_id',$auth)->orderBy('created_at','DESC')->first();
            Voucher::create([
                'path_voucher'      => $request->file('path_voucher')->store('voucher', 'public'),
                'status'            => 1,
                'announcement_id'   => $pe->announcements->id,
                'user_id'           => $auth,
            ]);
        }
        else{
           unset($new_data['path_voucher']);
        }
                
        #Update subject
        $document->update($new_data);
        #Mensaje
        Toastr::success('Documentos Registrados Exitosamente'); 
        #Response
        return redirect()->back();
        //return redirect('/document/'.Auth::user()->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
