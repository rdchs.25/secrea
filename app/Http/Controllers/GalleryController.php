<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Storage;
use File;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store an image resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeFile(Request $request)
    {
        $ext  = '.' . $request->file('image_path')->extension();
        $name = $request->name . $ext;
        $courseName = '/' . $request->course . ' (' . strtoupper(substr($request->course, 0, 3) . ')') . '-' . $request->course_id;
        $weekName = '/Semana ' . $request->week;
        return Gallery::create([
            'path' =>  $request->file('image_path')->storeAs('gallery'. $courseName . $weekName . '/img', $name ,'public'),
            'type' => 'image'
        ]);
    }

    /**
     * Store an audio resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAudio(Request $request)
    {
        $ext = $request->audio->extension() == 'mpga' ? 'mp3' : $request->audio->extension();
        $ext  = '.' . $ext;
        $name = $request->name . $ext;
        $courseName = '/' . $request->course . ' (' . strtoupper(substr($request->course, 0, 3) . ')') . '-' . $request->course_id;
        $weekName = '/Semana ' . $request->week;
        return Gallery::create([
            'path' =>  $request->file('audio')->storeAs('gallery'. $courseName . $weekName . '/audio', $name ,'public'),
            'type' => 'audio'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::find($id);

        \Storage::delete($gallery->path_image);

        $gallery->delete();

        return true;
    }
}
