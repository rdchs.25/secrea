<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;
use Toastr;

class NotificationManagementController extends Controller
{
	public function index()
    {   
        return view('configviews.notification-management.index');
    }	

    public function inactivityMail()
    {   
    	Artisan::call('mail:inactivity');

        Toastr::success('Correos enviados exitosamente');
        return view('configviews.notification-management.index');
    }    

    public function missingDocumentsMail()
    {   
    	//Artisan::call('mail:missing-documents');


		$announcement = Announcement::where('status','1')->first();

		if(!$announcement){
			Toastr::error('No hay convocatoria activa');
			return redirect('/report');
		}

		$slug = $announcement->slug;

        $userAssignment = userAssignment::with('user.profiles','assignmentStudent.announcement','assignmentStudent.grade')->get();
        $unique = $userAssignment->unique('user_id');

        $data = $unique->filter(function($value) use ($slug){
            return $value->assignmentStudent->announcement->slug == $slug;
        });

		if($userassignment->user->documents){
          //Si
		}
        else{
          //No
        }
        Toastr::success('Correos enviados exitosamente');
        return view('configviews.notification-management.index');
    }




}
