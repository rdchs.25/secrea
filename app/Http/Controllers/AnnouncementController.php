<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Requests\AnnouncementRequest;
use App\AnnouncementEvaluationType;
use App\Announcement;
use App\AcademicTime;
use App\PreEnrollment;
use App\UserAssignment;
use App\EvaluationType;
use App\Comment;
use App\Voucher;
use App\PayAnnouncement;
use App\Sms;
use Toastr;
use Auth;

class AnnouncementController extends Controller
{
    
    /**
     * see list of enrolled for announcement.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function enrolled($slug)
    {
        #OBTENER CONVOCATORIA
        $announcement = Announcement::findBySlug($slug);
        #OBTENER USUARIOS REGISTRADOS MATRICULADOS EN CONVOCATORIA
        $userAssignment = UserAssignment::select('id','user_id','assignment_student_id')->with('user:id,name,email,confirmed','user.profiles:id,phone,user_id','assignmentStudent:id,announcement_id,grade_id','assignmentStudent.grade:id,name')->get();
        
        $unique = $userAssignment->unique('user_id');
        $id = $announcement->id;
        $filtered = $unique->filter(function($value) use ($id){
            return $value->assignmentStudent->announcement_id == $id;
        });

        return view('configviews.announcement.enrolled',compact('filtered','announcement'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fannouncements=Announcement::with('academicTime')
            ->orderBy('id', 'desc')
            ->get();

        if(Auth::user()->isRole('asesor-comercial')){
            $announcements = $fannouncements->filter(function($value){
                if($value->status == 0  || $value->status == 1 ){
                    return true;
                }
                return false;
            });
        }else{
            $announcements = $fannouncements;
        }

        return view('configviews.announcement.index', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {

        $tiemposacademicos = AcademicTime::all();
        $evaluationTypes = EvaluationType::all();
        if($evaluationTypes->isEmpty() || $tiemposacademicos->isEmpty() ){
            Toastr::error('Verificar registro de Tiempos Académicos y Tipos de Evaluación.');
            return redirect('/announcement');            
        }
        return view('configviews.announcement.create',compact('tiemposacademicos','evaluationTypes'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {
    
        if( $this->validationEvaluationType($request) ){

            if($this->announcementActiveOrEnInscripcion($request)){
                if($request->input('status') == 1){
                    Toastr::warning('Existe una convocatoria ACTIVA.'); 
                }else{
                    Toastr::warning('Existe convocatorias En Inscripción.');
                }
            }else{
                // Store announcement
                $announcement = Announcement::create($request->only('name','size_time', 'academic_time_id','status'));
                
                // Store percent evaluation type
                $evaluationTypes = EvaluationType::all();
                foreach ($evaluationTypes as $key => $evaluationType){
                   
                    AnnouncementEvaluationType::create([
                        'percent'               =>  $request->input("percent_eval_type_$evaluationType->id"),
                        'announcement_id'       =>  $announcement->id,
                        'evaluation_types_id'   =>  $evaluationType->id
                    ]);
                    
                }
                //registrar pagos
                PayAnnouncement::create([
                    'concept'           => 'ma',
                    'rode'              => $request->input("rode_ma"),
                    'cuotas'            => 1,
                    'announcement_id'   => $announcement->id
                ]);
                PayAnnouncement::create([
                    'concept'           => 'cu',
                    'rode'              => $request->input("rode_pe"),
                    'cuotas'            => $request->input("count_pe"),
                    'announcement_id'   => $announcement->id
                ]);
                PayAnnouncement::create([
                    'concept'           => 'ce',
                    'rode'              => $request->input("rode_ce"),
                    'cuotas'            => 1,
                    'announcement_id'   => $announcement->id
                ]);
               
                Toastr::success('Convocatoria Creada Exitosamente');
            }    
        }else{
            Toastr::warning('Los porcentajes de tipos de evaluación deben sumar 100%.');
        }
        
       
        return redirect('/announcement');

    }

    public function validationEvaluationType(Request $request)
    {
        $data  = $request->except('name','size_time', 'academic_time_id','status','_token','rode_ma','rode_pe','count_pe','rode_ce');
        return  (array_sum($data) == 100 )?  true :  false;
    }

    public function announcementActiveOrEnInscripcion(Request $request){
        if($request->input('status') == 1){

            return (Announcement::where('status','1')->count() == 1)? true : false; 

        }
        if($request->input('status') == 0){
            
            return (Announcement::where('status','0')->count() == 2)? true : false; 
            
        }

        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
    
        $announcement = Announcement::findBySlug($slug);
        if(! $announcement){
            Toastr::warning('Convocatoria inexistente');
            return redirect('/announcement');
        }
        #PRE ENROLLMENT
        $preEnrollments = PreEnrollment::select('id','user_id','announcement_id','created_at')->with('users:name,email,confirmed,id','users.profiles:name,phone,user_id,birth_date','users.assignmentStudent.grade:id,name,section', 'users.documents:path_study_certificate,path_voucher,path_dni,user_id')
            ->where('announcement_id',$announcement->id)
            ->orderBy('created_at','desc')
            ->get();


        foreach ($preEnrollments as $key => $value) {
            #CONVOCATORIA POR SLUG
            $announcement = Announcement::findBySlug($slug);
            #CHECKED MATRICULA (verificar si el alumno ya está matriculado)
            $userassignment = UserAssignment::select('user_id','assignment_student_id')->with('user','assignmentStudent:id,announcement_id')->where('user_id',$value->users->id)->get();
            #FILTRAR LA MATRICULA POR CONVOCATORIA
            $id = $announcement->id;
            $filtered = $userassignment->filter(function($value) use ($id){
                return $value->assignmentStudent->announcement_id == $id ;
            });

            if(!$filtered->isNotEmpty()){
                $preEnrollments[$key]['enrollment'] = false; 
            }else{
                $preEnrollments[$key]['enrollment'] = true;
            }
            $preEnrollments[$key]['tag'] = Comment::select('status')->where('user_id',$value->users->id)->where('status','<>','SMS')->orderBy('created_at','desc')->first() ;    
            
            $preEnrollments[$key]['voucher'] = Voucher::where(['user_id'=>$value->users->id,'announcement_id'=>$announcement->id])->first();

        }

        $sms = Sms::all();

        return view('configviews.announcement.show', compact('preEnrollments','announcement','sms'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($slug)
    {
        $announcement               = Announcement::findBySlug($slug);

        if(! $announcement){
            Toastr::warning('Convocatoria inexistente');
            return redirect('/announcement');
        }

        $tiemposacademicos          = AcademicTime::all();
        $evaluationTypes            = EvaluationType::all();
        $announcementEvaluationType = AnnouncementEvaluationType::where('announcement_id',$announcement->id)->get();
        
        foreach ($evaluationTypes as $key => $evaluationType) {

            $a = $announcementEvaluationType->where('evaluation_types_id',$evaluationType->id)->first();
            if(empty($a)){
                $evaluationTypes[$key]['percent'] = 0;
            }else{
                $evaluationTypes[$key]['percent'] = $a->percent;
            }
            

        }

        foreach(PayAnnouncement::where('announcement_id',$announcement->id)->get() as $value){
            if($value->concept == 'ma'){
                $announcement['rode_ma'] = $value->rode;
            }
            if($value->concept == 'cu'){
                $announcement['rode_pe'] = $value->rode;
                $announcement['count_pe'] = $value->cuotas;
            }
            if($value->concept == 'ce'){
                $announcement['rode_ce'] = $value->rode;
            }
        }

        return view('configviews.announcement.edit', compact('announcement','tiemposacademicos','evaluationTypes'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Announcement $announcement , AnnouncementRequest $request)
    {  
        if($request->input('status')=='1'){
            if( empty(Announcement::where('status','0')->where('id','<>',$announcement->id)->first()) ){
                Toastr::warning('Antes de activar una convocatoria, registra una convocatoria de inscripción.');
                return redirect('/announcement/'.$announcement->slug.'/edit');
            }
        }
        $announcement->update($request->only('name','size_time', 'academic_time_id','status'));
        
        $evaluationTypes = EvaluationType::all();
        foreach ($evaluationTypes as $evaluationType) {

            $announcementEvaluationType = AnnouncementEvaluationType::where('announcement_id',$announcement->id)
                ->where('evaluation_types_id',$evaluationType->id)
                ->first();

        
                AnnouncementEvaluationType::UpdateOrCreate(
                    [
                        'evaluation_types_id'   =>  $evaluationType->id,
                        'announcement_id'       =>  $announcement->id
                    ],

                    [
                        'percent'              =>  $request->input("percent_eval_type_$evaluationType->id"),
                    ]
                );
            
            
        }

        PayAnnouncement::where('announcement_id',$announcement->id)
                        ->where('concept','ma')
                        ->update(['rode'=>$request->input('rode_ma')]);
        
        PayAnnouncement::where('announcement_id',$announcement->id)
                        ->where('concept','cu')
                        ->update(['rode'=>$request->input('rode_pe'),'cuotas'=>$request->input('count_pe')]);
        
        PayAnnouncement::where('announcement_id',$announcement->id)
                        ->where('concept','ce')
                        ->update(['rode'=>$request->input('rode_ce')]);
           
        Toastr::success('Convocatoria Actualizada Exitosamente');
        return redirect('/announcement/'.$announcement->slug.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $announcement = Announcement::findBySlug($slug);       

        try{
            $announcement->delete();
            Toastr::success('Convocatoria Eliminada Exitosamente');
        }catch(\PDOException $e){
           
            Toastr::warning('Lo sentimos, la convocatoria \"'.$announcement->name.'\" tiene alumnos pre inscritos y matriculados.');
        
        }

        return redirect('/announcement');
    }
}
