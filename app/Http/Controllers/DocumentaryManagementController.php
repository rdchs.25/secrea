<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\UserAssignment;
use App\User;
use Toastr;

class DocumentaryManagementController extends Controller
{
    public function index()
    {
		$announcements=Announcement::with('academicTime')
		->orderBy('name', 'asc')
		->get();
     
        return view('configviews.documentary-management.index', compact('announcements'));
    }    

    public function enrolled($slug)
    {
        #OBTENER CONVOCATORIA
        $announcement = Announcement::findBySlug($slug);
        #OBTENER USUARIOS REGISTRADOS MATRICULADOS EN CONVOCATORIA
        //$userAssignment = userAssignment::with('user.profiles','assignmentStudent.announcement','assignmentStudent.grade')->get();
        $userAssignment = UserAssignment::select('id','user_id','assignment_student_id')
                            ->with('user:id,name,email,status','user.profiles:id,phone,user_id','user.documents')
                            ->get();
        
        $unique = $userAssignment->unique('user_id');

        $filtered = $unique->filter(function($value) use ($slug){
            return $value->assignmentStudent->announcement->slug == $slug;
        });
     
        return view('configviews.documentary-management.enrolled', compact('filtered','announcement'));
    }

    public function changeStatusUser($id)
    {
        $user = User::find($id);

         if(! $user){
            Toastr::warning('Usuario inexistente');
            return redirect('/configviews.documentary-management.index');
         }

        $user->status == 1 ? $user->status = 0 : $user->status = 1;

        $user->save();

        Toastr::success('Usuario cambiado de estado exitosamente');
        
        $announcements=Announcement::with('academicTime')
            ->orderBy('name', 'asc')
            ->get();

        return view('configviews.documentary-management.index', compact('announcements'));
    }
}
