<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Announcement;
use App\UserAssignment;
use App\PreEnrollment;
use App\UserAgreement;
use Toastr;

class UserReportController extends Controller
{
	 /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function userListExcel()
	 {
	 
	    Excel::create('Reporte Usuarios', function($excel) {

	        $excel->sheet('Usuarios', function($sheet) {

				$users = User::select('name','email','last_login','created_at')->get();

	            $sheet->fromArray($users);

				$sheet->row(1, array(
	 				'Nombre', 'Correo', 'Ultima Conexión', 'Resgistro'
				));

	        });
	    })->export('xls');
	 
	 }

    public function userListPDF() 
    {

        $data =  User::select('name','email','last_login','created_at')->get();
        $date 	= date('Y-m-d');
        $view 	= \View::make('reports/users', compact('data', 'date'))->render();
        $pdf 	= \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('usuarios');
    }

   	public function userEnrolledListExcel()
	{
	 
	    Excel::create('Reporte Usuarios Matriculados', function($excel) {

	        $excel->sheet('Usuarios', function($sheet) {

    			$announcement = Announcement::where('status','1')->first();
    			$slug = $announcement->slug;

		        $userAssignment = userAssignment::with('user.profiles','assignmentStudent.announcement','assignmentStudent.grade')->get();
		        $unique = $userAssignment->unique('user_id');

		        $filtered = $unique->filter(function($value) use ($slug){
		            return $value->assignmentStudent->announcement->slug == $slug;
		        });

		        //dd($filtered);

	            $sheet->fromArray($filtered);

				/*$sheet->row(1, array(
	 				'Nombre', 'Correo', 'Ultima Conexión', 'Resgistro'
				));*/

	        });
	    })->export('xls');
	 
	}

	public function userEnrolledListPDF() 
    {
		$announcement = Announcement::where('status','1')->first();
		
		if(!$announcement){
			Toastr::error('No hay convocatoria activa');
			return redirect('/report');
		}		$slug = $announcement->slug;

        $userAssignment = userAssignment::with('user.profiles','assignmentStudent.announcement','assignmentStudent.grade')->get();
        $unique = $userAssignment->unique('user_id');

        $data = $unique->filter(function($value) use ($slug){
            return $value->assignmentStudent->announcement->slug == $slug;
        });

        $date 	= date('Y-m-d');
        $view 	= \View::make('reports/enrolled-users', compact('data', 'date'))->render();
        $pdf 	= \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('usuarios-maticulados');
    }

    public function userPreEnrolledListExcel()
	{
	 
	    Excel::create('Reporte Usuarios Matriculados', function($excel) {

	        $excel->sheet('Usuarios', function($sheet) {

    			$announcement = Announcement::where('status','0')->first();
    			$slug = $announcement->slug;

        		$preEnrollments = PreEnrollment::with('users.profiles', 'users.documents')
            		->where('announcement_id',$announcement->id)
            		->get();

	            $sheet->fromArray($preEnrollments);

				/*$sheet->row(1, array(
	 				'Nombre', 'Correo', 'Ultima Conexión', 'Resgistro'
				));*/

	        });
	    })->export('xls');
	 
	}

	public function userPreEnrolledListPDF() 
    {
		$announcement = Announcement::where('status','0')->first();

		if(!$announcement){
			Toastr::error('No hay convocatoria activa');
			return redirect('/report');
		}

		$slug = $announcement->slug;

		$data = PreEnrollment::with('users.profiles', 'users.documents')
			->where('announcement_id',$announcement->id)
			->get();

        $date 	= date('Y-m-d');
        $view 	= \View::make('reports/pre-enrolled-users', compact('data', 'date'))->render();
        $pdf 	= \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('usuarios-pre-inscritos');
    }

    public function userDocumentsListPDF() 
    {
		$announcement = Announcement::where('status','1')->first();

		if(!$announcement){
			Toastr::error('No hay convocatoria activa');
			return redirect('/report');
		}

		$slug = $announcement->slug;

        $userAssignment = userAssignment::with('user.profiles','assignmentStudent.announcement','assignmentStudent.grade')->get();
        $unique = $userAssignment->unique('user_id');

        $data = $unique->filter(function($value) use ($slug){
            return $value->assignmentStudent->announcement->slug == $slug;
        });

        $date 	= date('Y-m-d');
        $view 	= \View::make('reports/document-users', compact('data', 'date'))->render();
        $pdf 	= \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('usuarios-pre-inscritos');
    }

    public function userAgreementsListPDF() 
    {
		$announcement = Announcement::where('status','1')->first();

		if(!$announcement){
			Toastr::error('No hay convocatoria activa');
			return redirect('/report');
		}

		$slug = $announcement->slug;

		$data = UserAgreement::with(array('user', 'agreement' => function($query) {
        		$query->orderBy('name', 'DESC');
    			}))
    			->get();

        $date 	= date('Y-m-d');
        $view 	= \View::make('reports/agreement-users', compact('data', 'date'))->render();
        $pdf 	= \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('usuarios-pre-inscritos');
    }

 
}
