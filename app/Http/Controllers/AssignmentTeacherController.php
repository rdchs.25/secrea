<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AssignmentTeacherRequest;
use App\Grade;
use App\Subject;
use App\User;
use App\AssignmentTeacher;
use App\Announcement;
use Toastr;

class AssignmentTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #OBTENER CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status', 1 )->first();
        
        if(is_null($announcement)){
            Toastr::warning('No existe un ciclo académico activo.');
            return  back();
        }else{
            #OBTENER ASIGNACIONES DE CONVOCATORIA ACTIVA
            $assignments = AssignmentTeacher::with('user','grade.academicLevels','subject')
                                            ->where('announcement_id',$announcement->id)
                                            ->orderBy('id', 'desc')
                                            ->get();
            return view('configviews.assignment-teacher.index',compact('assignments'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #DOCENTES
        $docentes = User::with('roles')->get();

        #FILTRAR SOLO DOCENTES
        $filtered = $docentes->filter(function($value){
            return $value->roles[0]->slug == 'teacher' ;
        });

        $docentes = $filtered;
        #GRADOS
        $grades= Grade::with('academicLevels')->orderBy('name', 'asc')->get();
        #ASIGNATURAS
        $subjects= Subject::all();
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status','1')->first();
        
        if($docentes->isEmpty() || $grades->isEmpty() || $subjects->isEmpty() || empty($announcement) ){
            Toastr::error("Verificar registros de Docentes, Grados, Asignaturas o Estado de Convocatoria.");
            return redirect('/assignment-teacher');
        }
       
        return view('configviews.assignment-teacher.create',compact('docentes','grades','subjects'));                
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentTeacherRequest $request)
    {
        #OBTENER CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status','1')->first();
        #VERIFICR QUE ASIGNATURA YA ESTA ASIGNADA
        $verify = AssignmentTeacher::where('announcement_id',$announcement->id)
                        ->where('subject_id',$request->input('subject_id'))
                        ->count();
        if($verify > 0 ){
            Toastr::error('Lo sentimos, las asignatura elegida ya se encuentra asignada.');
        }else{
            #STORE
            $data = $request->all();
            $data['announcement_id'] = $announcement->id;
            AssignmentTeacher::create($data);

            Toastr::success('Asignación registrada');
        }                

        return redirect('/assignment-teacher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #DOCENTES
        $docentes = User::all();
        #GRADOS
        $grades= Grade::with('academicLevels')->orderBy('name', 'asc')->get();
        #ASIGNATURAS
        $subjects= Subject::all();
        #ASIGNACION ELEGIDA
        $assignment = AssignmentTeacher::find($id);
         if(! $assignment){
            Toastr::warning('Asignación inexistente');
            return redirect('/assignment-teacher');
         }

        return view('configviews.assignment-teacher.edit', compact('assignment','docentes','grades','subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AssignmentTeacherRequest $request, AssignmentTeacher $assignment)
    {
        $assignment->update($request->all());
        Toastr::success('Asignación Actualizada Exitosamente');
        
        return redirect('/assignment-teacher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignment = AssignmentTeacher::find($id);
        try{
            $assignment->delete();
            Toastr::success('Asignación Eliminada Exitosamente');
        }catch(\PDOException $e){
           
            Toastr::warning('Ya hay alumnos matriculados con esta asignación.');
        
        }
        return redirect('/assignment-teacher');
    }
}
