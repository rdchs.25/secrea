<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreEnrollment;
use App\Announcement;
use Auth;
use Toastr;

class PreEnrollmentController extends Controller
{

    public function index()
    {   
        $announcementsByUser =  PreEnrollment::with('announcements')
        ->where('user_id', Auth::user()->id)
        ->get();

        $announcements=Announcement::where('status','0')
        ->orderBy('name', 'asc')
        ->get();

        return view('managementviews.pre-enrollment.index', compact('announcements', 'announcementsByUser'));
    }

    public function preEnrollment($id)
    {
        if(Auth::user()->isRole('registrado') || Auth::user()->isRole('student')){
            $announcement_active = Announcement::where('status',1)->first();
            if($announcement_active){
                $preEnrollment = PreEnrollment::where('announcement_id', $announcement_active->id )
                    ->where('user_id', Auth::user()->id)
                    ->get();
                if($preEnrollment->isNotEmpty()){
                    Toastr::error('Usted está cursando estudios en una convocatoria activa.');
                    return redirect('/pre-enrollment');            
                }
            }
            $preEnrollment = PreEnrollment::where('announcement_id', $id )
                ->where('user_id', Auth::user()->id)
                ->get();
    
            if($preEnrollment->isNotEmpty()){
                Toastr::error('Ya estas pre-inscrito en esta convocatoria');
                return redirect('/pre-enrollment');            
            }else{
                PreEnrollment::firstOrCreate([
                    'user_id'               =>  Auth::user()->id,
                    'announcement_id'       =>  $id,
                ]); 
                Toastr::success('Pre-Inscripción Registrada Exitosamente');
                return redirect('/pre-enrollment');
            }
     
        }else{
            Toastr::warning('Usted no se puede matricular.');
            return redirect('/home');
        }
    
    }


}
