<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Toastr;
use Backup;
use File;

class BackupController extends Controller
{
    public function index()
    {   
        $backups = [];
        $directory = 'storage/backup/';

        $filesInFolder = File::files($directory );

        foreach($filesInFolder as $path)
        {
            $backups[] = pathinfo($path);
        }

        return view('configviews.backup.index', compact('backups'));
    }
    public function export()
    {   
        Backup::export(); 

        $backups = [];
        $directory = 'storage/backup/';

        $filesInFolder = File::files($directory );

        foreach($filesInFolder as $path)
        {
            $backups[] = pathinfo($path);
        }

        Toastr::success('Respaldo realizado Exitosamente');
        return view('configviews.backup.index', compact('backups'));
    }
}
