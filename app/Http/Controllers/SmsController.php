<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SmsGateway;
use App\Sms;
use App\User;
use App\Comment;
use Toastr;
use Auth;

class SmsController extends Controller
{
    
    public function create()
    {
        return view('configviews.sms.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'tag'        =>  'required',
            'content'    =>  'required|max:230',
        ]); 
        Sms::create($request->all());
        Toastr::success('Mensaje registrado');
        return redirect('/sms');
    }
    
    public function index()
    {
        $sms = Sms::all();
        return view('configviews.sms.index',compact('sms'));
    }

    public function edit($id)
    {
         $sms = Sms::find($id);
         if(! $sms){
            Toastr::success('Mensaje inexistente');
            return redirect('/sms');
         }

         return view('configviews.sms.edit', compact('sms'));
    }

    public function update(Request $request,$id)
    {   
        $this->validate($request, [
            'tag'        =>  'required',
            'content'    =>  'required|max:230',
        ]); 
        $sms = Sms::find($id);
        $sms->update($request->all());
        Toastr::success('Mensaje Actualizado Exitosamente');
        return redirect('/sms');
    }

    public function destroy($id)
    {
        $sms = Sms::find($id);
        $sms->delete();
        Toastr::success('Mensaje Eliminado Exitosamente');
        return redirect('/sms');
    }

    public function send(Request $request)
    {
        
        try{
            $mensaje = Sms::find($request->input('action_sms'));
            if(empty($request->input('users'))){
                Toastr::warning("No seleccionaste usuarios");
            }else{
                
                $objSms = new SmsGateway(env('SMS_USER'),env('SMS_KEY'));
                $users = User::with('profiles')->whereIn('id', $request->input('users'))->get();
                $phones = [];
                foreach( $users as $id => $user){
                    Comment::create([
                        'body'          => 'Mensaje de texto enviado: '.$mensaje->content,
                        'author_id'     => Auth::id(),
                        'user_id'       => $user->id,
                        'status'        => 'SMS',
                    ]);
                    $phones[$id] = $user->profiles->phone;
                }
                $resultado = $objSms->sendMessageToManyNumbers($phones,$mensaje->content,env('SMS_ID'));
                    
                if($resultado['response']['success']){
                    Toastr::success("Mensajes enviados");
                }else{
                    Toastr::error("Error al enviar mensajes");
                }
            }
            
        }catch(\Exception $e){
            Toastr::error('Eres un infiltrado.');
        }

        

        return back();
        
    }
    
}

