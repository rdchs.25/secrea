<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\UserAssignment;
use App\CourseBuilderLog;
use App\User;
use Auth;
use Toastr;

class CourseBuilderController extends Controller
{
    public function index(Request $request, $id)
    {

        
        $courseInfo = Subject::find($id);
        if(!$courseInfo || ! json_decode($courseInfo->content)){
            Toastr::error('No existe este curso.');
            return back();
        }

        $log = CourseBuilderLog::where([
            ['subject_id', '=' ,$id],
        ])->orderBy('started_at', 'DESC')
        ->first();  
        
        if($log){
            if(is_null($log->finished_at)){
                if($log->user_id != $request->user()->id){
                    $current_user = User::find($log->user_id);
                    Toastr::error($current_user->name.' esta editando el curso, por favor espere.');
                    return back();
                }
            }
        }

    	return view('managementviews.course-builder.index');
    }

    public function showPreview($id)
    {
     
        $courseInfo = Subject::find($id);
        if(!$courseInfo || ! json_decode($courseInfo->content_preview)){

            Toastr::error('No hay vista previa de este curso.');
            return redirect('/home');
        }
    	return view('managementviews.course-builder.preview');
    }

    public function getCourseInfo($id)
    {
    	$courseInfo = Subject::find($id);
    	return response()->json([
    			'id' => $courseInfo->id,
    			'title' => $courseInfo->name,
    			'grade' => $courseInfo->identify,
                'content' => json_decode($courseInfo->content)
    		]);
    }


    public function getCourseInfoStudent($id)
    {
        $assignment = UserAssignment::where('id', $id)
        ->first();

        $courseInfo = UserAssignment::where('id',$id)
        ->where('user_id', Auth::id())
        ->first()
        ->assignmentTeacher->subject; //


    	return response()->json([
    			'id' => $assignment->id,
    			'title' => $courseInfo->name,
    			'grade' => $courseInfo->identify,
                'content' => json_decode($assignment->content)
    		]);
    }


    public function getCoursePreview($id)
    {
    	$courseInfo = Subject::find($id);
    	return response()->json([
    			'id' => $courseInfo->id,
    			'title' => $courseInfo->name,
    			'grade' => $courseInfo->identify,
                'content' => json_decode($courseInfo->content_preview)
    		]);
    }
}
