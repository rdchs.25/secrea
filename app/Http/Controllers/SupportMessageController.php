<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SupportMessageRequest;
use App\SupportMessage;
use Auth;
use Toastr;

class SupportMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = SupportMessage::with('users.profiles')
        ->orderBy('attend', 'asc')
        ->orderBy('created_at', 'desc')
        ->get();

        return view('configviews.support-messages.index', compact('messages'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('managementviews.support-messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupportMessageRequest $request)
    {
        SupportMessage::create([
            'message'               =>  $request->input('message'),
            'user_id'               =>  Auth::user()->id
        ]);

        Toastr::success('Mensaje enviado al Soporte Exitosamente');

        return redirect('/support-message/create');        
    }

    public function attend($id){

        $message = SupportMessage::find($id);

        $data = [
            'attend' =>  true,
        ];

        $message->update($data);

        Toastr::success('Mensaje Atendido Exitosamente');
        #Response
        return redirect('/support-message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
