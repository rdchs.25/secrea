<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAssignment;
use App\AssignmentTeacher;
use App\Subject;
use App\Announcement;
use App\Evaluation;
use App\ContentManagerAssignment;
use App\User;
use Auth;
use Toastr;


class UserSubjectController extends Controller
{
    public function subjectByStudent()
    {   
        if(Auth::user()->status == false){

            return view('errors.block-user');
        }

    	#CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status',1)->first();
        $id = $announcement->id;
        #ASIGNATURAS POR ESTUDIANTE AUTHENTICADO
        $subjectByUser = UserAssignment::with('user','assignmentTeacher.subject')
    		->where('user_id', Auth::id())
    		->get();

        #FILTRAR ASIGNATURAS SEGUN CONVOCATORIA ACTIVA
        $filtered = $subjectByUser->filter(function($value) use ($id){
            return $value->assignmentTeacher->announcement_id == $id ;
        });
        #RESPONSE
		return view('managementviews.subject.student.index', compact('filtered'));    
	}

    public function subjectOnAnnActive(){
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status',1)->first();
        $id = $announcement->id;
        #ASIGNATURAS ASIGNADAS A UN PROFESOR
    	$subjectByTeacher = AssignmentTeacher::with('subject','grade.academicLevels')
            ->where('announcement_id',$id)
    		->get();
        #RESPONSE
		return view('managementviews.subject.teacher.index', compact('subjectByTeacher'));
    }


	public function subjectByTeacher()
    {   
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status',1)->first();
        $id = $announcement->id;
        #ASIGNATURAS ASIGNADAS A UN PROFESOR
    	$subjectByTeacher = AssignmentTeacher::with('subject','grade.academicLevels')
    		->where('user_id', Auth::id())
            ->where('announcement_id',$id)
    		->get();
        #RESPONSE
		return view('managementviews.subject.teacher.index', compact('subjectByTeacher'));    
	}



    public function subjectByContentManager()
    {   

        #ASIGNATURAS ASIGNADAS A UN GESTOR DE CONTENIDO
        $subjectByContentManager = ContentManagerAssignment::with('subject')
            ->where('user_id', Auth::id())
            ->get();
        #RESPONSE
        return view('managementviews.subject.content-manager.index', compact('subjectByContentManager'));    
    }



    public function studentByAssignment($slug)
    {
        #BUSCAR ASIGNATURA POR SLUG
        $subject = Subject::findBySlug($slug);
        $announcement = Announcement::where('status','1')->first();
        #BUSCAR ASIGNACION
        $assignment = AssignmentTeacher::where('subject_id', $subject->id )->where('announcement_id',$announcement->id)->first();
        #BUSCAR ALUMNOS 
        $studentsByAssignment = UserAssignment::with('user')
            ->where('assignment_teacher_id', $assignment->id)
            ->get();

        #RESPONSE
        return view('managementviews.subject.teacher.student', compact('studentsByAssignment','subject')); 
    }



    public function studentByAssignmentProgress($slug, $id)
    {
        #BUSCAR ASIGNATURA POR SLUG
        $subject = Subject::findBySlug($slug);
        #DATOS ESTUDIANTE
        $student = User::with('profiles')->find($id);
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::with('academicTime')->where('status',1)->first();
        #BUSCAR ASIGNACION
        $assignment = AssignmentTeacher::where('subject_id', $subject->id )
            ->where('announcement_id',$announcement->id)
            ->first();
        #BUSCAR ALUMNO
        $studentsByAssignment = UserAssignment::with('user')
            ->where('assignment_teacher_id', $assignment->id)
            ->where('user_id', $id)
            ->first();

        $evaluations = Evaluation::with('userAssignment.assignmentTeacher.subject', 'evaluationType')->whereHas('userAssignment', function($query) use ($assignment, $id) {
            return $query->where('user_assignments.assignment_teacher_id', $assignment->id)->where('user_assignments.user_id', $id);
        })->get();
        
        $week_num = $announcement->academicTime->number_weeks*$announcement->size_time;
        $progress = $evaluations->count()*100/$week_num;

        #Evaluaciones Cuestionarios
        $Quizevaluations = Evaluation::with('userAssignment.assignmentTeacher.subject', 'evaluationType')->whereHas('userAssignment', function($query) use ($assignment, $id) {
            return $query->where('user_assignments.assignment_teacher_id', $assignment->id)->where('user_assignments.user_id', $id);
        })
        ->where('evaluation_type_id', 1)
        ->get();

        #Evaluaciones Examenes
        $Evalevaluations = Evaluation::with('userAssignment.assignmentTeacher.subject', 'evaluationType')->whereHas('userAssignment', function($query) use ($assignment, $id) {
            return $query->where('user_assignments.assignment_teacher_id', $assignment->id)->where('user_assignments.user_id', $id);
        })
        ->where('evaluation_type_id', 2)
        ->get();

        #Evaluaciones Promedio
        $Quizaverage = $Quizevaluations->avg('value');
        $Evalaverage = $Evalevaluations->avg('value');


        #Promedio Final Cuestionario
        $FinalQuizaverage = $Quizaverage * 0.6;
        #Promedio Final Examen
        $FinalEvalaverage = $Evalaverage * 0.4;

        #RESPONSE
        return view('managementviews.subject.teacher.progress', compact('evaluations', 'student', 'Quizaverage', 'Evalaverage', 'FinalQuizaverage', 'FinalEvalaverage', 'progress', 'subject')); 
    }

	public function studentByAssignmentProgressMonitor()
    {   
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::where('status',1)->first();
        if(!$announcement){
            Toastr::warning('No existe una convocatoria ACTIVA.');
            return redirect('/home');
        }
        $id = $announcement->id;
        #ASIGNATURAS ASIGNADAS A UN PROFESOR
    	$subjects = AssignmentTeacher::with('subject','grade.academicLevels')
            ->where('announcement_id',$id)
    		->get();
        #RESPONSE
		return view('managementviews.subject.monitor.index', compact('subjects'));    
	}


    public function studentByAssignmentMyProgress($slug)
    {
        #BUSCAR ASIGNATURA POR SLUG
        $subject = Subject::findBySlug($slug);
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::with('academicTime')->where('status',1)->first();
        #BUSCAR ASIGNACION
        $assignment = AssignmentTeacher::where('subject_id', $subject->id )
            ->where('announcement_id',$announcement->id)
            ->first();
        #BUSCAR ALUMNO
        $id = Auth::user()->id;

        $studentsByAssignment = UserAssignment::with('user')
            ->where('assignment_teacher_id', $assignment->id)
            ->where('user_id', Auth::user()->id)
            ->first();

        $evaluations = Evaluation::with('userAssignment.assignmentTeacher.subject', 'evaluationType')->whereHas('userAssignment', function($query) use ($assignment, $id) {
            return $query->where('user_assignments.assignment_teacher_id', $assignment->id)->where('user_assignments.user_id', $id);
        })
        ->get();
       
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::with('academicTime')->where('status',1)->first();
        $week_num = $announcement->academicTime->number_weeks*$announcement->size_time;
        $progress = $evaluations->count()*100/$week_num;

        #Evaluaciones Cuestionarios
        $Quizevaluations = Evaluation::with('userAssignment.assignmentTeacher.subject', 'evaluationType')->whereHas('userAssignment', function($query) use ($assignment, $id) {
            return $query->where('user_assignments.assignment_teacher_id', $assignment->id)->where('user_assignments.user_id', $id);
        })
        ->where('evaluation_type_id', 1)
        ->get();

        #Evaluaciones Examenes
        $Evalevaluations = Evaluation::with('userAssignment.assignmentTeacher.subject', 'evaluationType')->whereHas('userAssignment', function($query) use ($assignment, $id) {
            return $query->where('user_assignments.assignment_teacher_id', $assignment->id)->where('user_assignments.user_id', $id);
        })
        ->where('evaluation_type_id', 2)
        ->get();

        #Evaluaciones Promedio
        $Quizaverage = round($Quizevaluations->avg('value'));
        $Evalaverage = round($Evalevaluations->avg('value'));

        #Promedio Final Cuestionario
        $FinalQuizaverage = $Quizaverage * 0.6;
        #Promedio Final Examen
        $FinalEvalaverage = $Evalaverage * 0.4;

        #RESPONSE
        return view('managementviews.subject.student.evaluations', compact('evaluations', 'student', 'Quizaverage', 'Evalaverage', 'FinalQuizaverage', 'FinalEvalaverage', 'progress')); 
    }

    public function subjectByMediaManager()
    {
        $subjects= Subject::all();
        return view('managementviews.subject.media-manager.index', compact('subjects')); 

    }

    public function getCourse($id)
    {   
        /*$courseInfo = Subject::find($id);
        if(!$courseInfo || ! json_decode($courseInfo->content)){
            Toastr::error('No existe este curso.');
            return redirect('/home');
        }*/

        return view("managementviews.subject.student.course");
    }
    public function getCourseAssigned($id)
    {   
        //Validacion que exista ese curso, sino return back con error Toast.

        $assignment = UserAssignment::where('id', $id)
            ->where('user_id', Auth::id())
            ->first();
        
        dd($assignment->content);

        return;
    }

    public function updateCourseAssigned(Request $request, $id)
    {
        $assignment = UserAssignment::find($id);    
        $assignment->update([
            'content'  =>  json_encode($request->input('content')),
        ]);

        Toastr::success('Progreso actualizado exitosamente');

        return response()->json(["ok" => true], 200);
    }

    public function reportPdfSubject($id){
        $subject = Subject::find($id);
        $content = json_decode($subject->content)->weeks;
        $view 	= \View::make('reports/subject1', compact('subject','content'))->render();
        $pdf 	= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        
        return $pdf->stream('Asignatura');
    }


}
