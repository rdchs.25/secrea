<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;

class ConstancyController extends Controller
{
    
    
    public function create()
    {
        $grades = Grade::all();
        return view('configviews.constancy.create',compact('grades'));
    }

    public function generate(Request $request)
    {
        $grades = [
            'Primero de Secundaria' => '1° grado',
            'Segundo de Secundaria' => '2° grado',
            'Tercero de Secundaria' => '3° grado',
            'Cuarto de Secundaria' => '4° grado'
        ];
        $data = [
            'type'  => ($request->input('type') == 0)? 'VACANTE':'MATRÍCULA',
            'name'  => $request->input('name'),
            'grade' => $grades[Grade::find($request->input('grade_id'))->name],
        ];
        $view 	= \View::make(($request->input('type') == 0)? 'constancy/vacante':'constancy/matricula', compact('data'))->render();
        $pdf 	= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        
        return $pdf->stream('Constancia');
    }

}
