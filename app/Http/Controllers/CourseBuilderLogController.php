<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Toastr;
use App\CourseBuilderLog;
use Carbon\Carbon;

class CourseBuilderLogController extends Controller
{
    public function startCourseEditSession(Request $request)
    {

        $log = CourseBuilderLog::where([
                                    ['subject_id', '=' ,$request->input('subject_id')],
                                    ['finished_at', '=' , null]
                                ])->orderBy('started_at', 'desc')
                                ->first();

        if($log){
            if($log->user_id != $request->input('user_id')){
                Toastr::error('Ha ocurrido un error, por favor intenta nuevamente.', 'Oops!');
                return response()->json([
                    'ok' => false,
                    'status' => 200
                ]);    
            }
            else{
                return response()->json([
                    'ok' => true,
                    'status' => 200
                ]);
            }
        }

        CourseBuilderLog::create(['user_id' => $request->input('user_id'), 'subject_id' => $request->input('subject_id'), 'started_at' => Carbon::now()]);

        return response()->json([
            'ok' => true,
            'status' => 200
        ]);
    }

    public function endCourseEditSession(Request $request, $id)
    {

        $log = CourseBuilderLog::where([
                                    ['subject_id', '=' ,$id],
                                    ['user_id', '=' ,$request->user()->id],
                                    ['finished_at', '=' , null]
                              ])->orderBy('started_at', 'desc')
                                ->first();          

        if(!$log){
            return response()->json([
                'ok' => true,
                'status' => 200
            ]);
        }

        $log->update([
            'finished_at' => Carbon::now()
        ]);

        return response()->json([
            'ok' => true,
            'status' => 200
        ]);
    }


}
