<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use App\EvaluationType;
use App\Announcement;
use App\UserAssignment;
use Auth;
use Toastr;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->input('evaluation_type') == 'quiz'){
            $evaluation_type = EvaluationType::where('name', 'Cuestionario')->first();
        }

        if($request->input('evaluation_type') == 'eval'){
            $evaluation_type = EvaluationType::where('name', 'Exámen Final')->first();
        }

        
        /*Evaluation::create([
            'value'                 =>  $request->input('grade'),
            'week'                  =>  $request->input('week'),
            'evaluation_type_id'    =>  $evaluation_type->id,
            'user_assignment_id'    =>  $request->input('user_assignment_id'),
        ]);*/

        Evaluation::updateOrCreate(
            ['week' => $request->input('week'),'evaluation_type_id' => $evaluation_type->id,'user_assignment_id' => $request->input('user_assignment_id')],
            ['value' => $request->input('grade')]
        );

        #Calcular Promedio Actual
        $asignacion = UserAssignment::find($request->input('user_assignment_id'));
        
        $evaluations = Evaluation::where('user_assignment_id', $request->input('user_assignment_id'))
        ->get();
        
        #CONVOCATORIA ACTIVA
        $announcement = Announcement::with('academicTime')->where('status',1)->first();
        $week_num = $announcement->academicTime->number_weeks*$announcement->size_time;

        #Calcular Progreso
        $progress = $evaluations->count()*100/$week_num;

        #Calcular Promedio
        $average = $evaluations->avg('value');
    
        $new_data = $request->all();
        $new_data['final_score']  = $progress;
 
        #Update subject

        $asignacion->update($new_data);

        Toastr::success('Evaluación Registrada Exitosamente');
        return response()->json(["ok" => true], 200);
    }
}
