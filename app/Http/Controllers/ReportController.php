<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Announcement;
use App\UserAssignment;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
  	public function index()
    {
        return view('reports.index');
    }

    public function enroll($grade){
        Excel::create('SECREA '.$grade.'°', function($excel) use ($grade) {
            
            $excel->sheet('Grado '.$grade.'°', function($sheet) use($grade) {
                
                $studentsByGrade = UserAssignment::select('id','assignment_student_id','user_id')
                    ->with('user.profiles','assignmentStudent')
                    ->get();

                $studentsByGrade = $studentsByGrade->unique('user_id');

                $filtered = $studentsByGrade->filter(function($value) use($grade){
                    return $value->assignmentStudent->grade_id == $grade ;
                });

                $sheet->row( 1, [ '#', 'Nombre', 'Email','Teléfono']);
                $i = 2;
                foreach($filtered as $index => $ua) { 
                    $sheet->row( $i, [$i - 1, $ua->user->name, $ua->user->email,isset($ua->user->profiles->phone)? $ua->user->profiles->phone:'No registrado']);	
                    $i++;
                }
 
            });
        })->export('xls');
    }

}
