<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EvaluationTypeRequest;
use App\EvaluationType;
use Toastr;

class EvaluationTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluationTypes= EvaluationType::orderBy('id', 'desc')->get();
        return view('configviews.evaluation-type.index', compact('evaluationTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configviews.evaluation-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EvaluationTypeRequest $request)
    {
        EvaluationType::create($request->all());
        Toastr::success('Tipo de Evaluación Creado Exitosamente');
        return redirect('/evaluation-type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $evaluationType = EvaluationType::findBySlug($slug);
         if(! $evaluationType){
            Toastr::error('Tipo de Evaluación inexistente');
            return redirect('/evaluation-type');
         }        
        return view('configviews.evaluation-type.edit', compact('evaluationType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EvaluationType $evaluationType,EvaluationTypeRequest $request)
    {
        $evaluationType->update($request->all());
        Toastr::success('Tipo de Evaluación Actualizado Exitosamente');
        return redirect('/evaluation-type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        #Buscar academic time
        $evaluationType = EvaluationType::findBySlug($slug);

        #Eliminar de la base de datos
        try{
            $evaluationType->delete();
            Toastr::success('Tipo de Evaluación Eliminado Exitosamente');
        
        }catch(\PDOException $e){
            Toastr::warning('Lo sentimos, el tipo de evaluación \"'.$evaluationType->name.'\" está siendo utilizada.');
        }

        #Response
        return redirect('/evaluation-type');
        
    }
}
