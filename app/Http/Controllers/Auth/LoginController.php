<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

     /**
     * Redes sociales
     *
     * @var array
     */
    protected $redes_sociales = ['facebook','google'];


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('verified')->only('login');
    }
    
    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider(Request $request)
    {
        #SOLO SE ADMITEN REDES SOCIALES COMO FACEBOOK Y GOOGLE
        if(in_array($request->rs,$this->redes_sociales)){
            return Socialite::driver($request->rs)->redirect();
        }else{
            return redirect('/');
        }
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $this->user = Socialite::driver($request->rs)->user();
        #Verificar email en database
        if(!$this->checkAuthDataBase()){ 
            $this->registerUser();               
        }  
        #Authenticar usuario
        Auth::attempt(['email' => $this->user->getEmail(), 'password' => 'secret']);
        
        #Redireccionar
        return redirect('/home');
    }

     /**
     * Checked database data
     *
     * @return boolean
     */
    public function checkAuthDataBase()
    {
        $user = User::where('email',$this->user->getEmail())->first();
        if(!is_null($user)){
            return true;
        }
        return false;
    }

    /**
     * Register user database
     *
     */
    public function registerUser()
    {
        $user = User::create([
            'name'      => $this->user->getName(),
            'email'     => $this->user->getEmail(),
            'password'  => bcrypt('secret'),
            'confirmed' => true,
        ]);

        $user->assignRole(10);

    }


    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
    }

    
}
