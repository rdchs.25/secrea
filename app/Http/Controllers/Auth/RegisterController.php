<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Grade;
use App\Profile;
use App\AssignmentStudent;
use App\Announcement;
use App\PreEnrollment;
use App\UserAgreement;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\Verification;
use App\Mail\Correo;
use App\Mail\CorreoAgreement;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Toastr;
use Mailgun\Mailgun;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['years'] = \Carbon\Carbon::parse( $data['birth_date'] )->age;
        return Validator::make($data, [
           'name' => 'required|string|max:255',
           'email' => 'required|string|email|max:255|unique:users',
           'password' => 'required|string|min:6|confirmed',
           'dni'=>'required|digits:8',
           'birth_date'=>'required|date',
           'years'=>'numeric|min:14|max:100',
           'phone'=>'required|digits:9',
           'grade_id'=>'required|numeric'
       ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //$announcement = Announcement::where('status',0)->first();
        $announcement = Announcement::where('status',0)->orderBy('created_at','desc')->first();
        #CODIGO DE VERIFICATION
        $code = str_random(40);
        #REGISTAR USER
        $user = User::create([
            'name'          => $data['name'],
            'email'         => $data['email'],
            'password'      => bcrypt($data['password']),
            'confirmed'     =>  1,
            'confirmed_code'=> $code
        ]);

        $mgClient = new Mailgun('pubkey-edafe85612c91af6e1070d1f93724bae');
        $result = $mgClient->get("address/validate", array('address' => $user->email,'mailbox_verification'=>true));
        # is_valid is 0 or 1
        if($result->http_response_body->mailbox_verification == "true"){
            #ENVIAR CORREO
            Mail::to($data['email'])->send(new Correo($user,$data['password'])); 
            #REGISTRAR PROFILE
            Profile::create([
                'dni'                   => $data['dni'], 
                'name'                  => $data['name'], 
                'address'               => null, 
                'phone'                 => $data['phone'],    
                'path_profile_image'    => null,
                'birth_date'            => $data['birth_date'],
                'user_id'               => $user->id
            ]);
            #ASIGNAR ROLE
            $user->assignRole(10);
            #ASIGNARLE GRADO EN LA CONVOCATORIA ACTUAL
            AssignmentStudent::create([
                'user_id'           => $user->id,
                'grade_id'          => $data['grade_id'],
                'announcement_id'   => $announcement->id,
            ]);
            #PRE INSCRIBIRLO
            PreEnrollment::create([
                'user_id'               =>  $user->id,
                'announcement_id'       =>  $announcement->id,
            ]);
        }else{
            $user->delete();
            $user = null;
        }
        #RETORNAR
        return $user;
    }
    /*
    protected function verification(Request $request){
        $user = User::where('confirmed_code',$request->code)
                ->where('confirmed','0')
                ->first();
        
        if($user){
            $user->confirmed = 1;
            $user->save();
            #ENVIAR CORREO
            $user_agreement = UserAgreement::with('agreement.partnerCompanies')
                                    ->where('user_id',$user->id)
                                    ->orderBy('created_at', 'desc')
                                    ->first();
            
            if($user_agreement){
                $partner_company = $user_agreement->agreement->partnerCompanies->name;
                Mail::to($user->email)->send(new CorreoAgreement($user,$partner_company));
            }else{
                Mail::to($user->email)->send(new Correo($user));
            }
            Auth::loginUsingId($user->id);
        }
    
        return redirect('/home');
        
    }*/
    public function showRegistrationForm()
    {
        $announcement = Announcement::where('status',0)->orderBy('created_at','DESC')->first();
        if($announcement){
            $grades = Grade::all();
            return view('auth.register',compact('grades'));
        }else{
            echo "Lo sentimos no existe una convocatoria para pre inscribirse.";
        }
        
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        
        if(is_null($user)){
            Toastr::error('!Ops¡ Verifica que tu correo esté bien escrito.');
            return redirect('/register');
        }else{
            Toastr::warning('Recuerda verificar tu correo electrónico.');
            event(new Registered($user));
            $this->guard()->login($user);

            return $this->registered($request, $user)
                                ?: redirect('/home');
        }
        //return redirect('/register');
    }
}
