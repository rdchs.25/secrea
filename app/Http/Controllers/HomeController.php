<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\UserAssignment;
use App\AssignmentTeacher;
use App\PreEnrollment;
use App\Document;
use Toastr;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->status == 0){
            Toastr::error('Usuario bloqueado por un administrador');
            
            Auth::logout();
            return redirect('/login');  
        }
        //announcement active 
        $announcement = Announcement::where('status','1')->first();

        if($announcement){

            $id = $announcement->id;
            
            if(Auth::user()->can('announcement-management')){
            
                //Count enrolled 
                //$userAssignment = UserAssignment::with('user.profiles','assignmentStudent.announcement')->get();
                $userAssignment = UserAssignment::select('id','user_id','assignment_student_id')->with('assignmentStudent:id,announcement_id')->get();
                $userAssignment = $userAssignment->unique('user_id');
                if($userAssignment){
                    $countEnrolled = $userAssignment->filter(function($value) use ($id){
                        return $value->assignmentStudent->announcement_id == $id;
                    })->count();
                }

                //count pre enrollment
                $annPreEnroll = Announcement::where('status','0')->first();

                $pre_enrollment =  PreEnrollment::select('id','announcement_id')->with('documents:path_voucher')->where('announcement_id',$annPreEnroll->id)->get();
                
                $countPreEnrolled = empty($annPreEnroll)? '0': $pre_enrollment->count();

                $pre_enrollment_with_voucher = $pre_enrollment->filter(function($value){
                    return !empty($value->documents->path_voucher) ;
                })->count();

            }

            //if(Auth::user()->can('my-subjects-management')){
            
                /* my subject by student */
                /*$userAssignment = UserAssignment::with('assignmentStudent.subject')
                    ->where('user_id',Auth::id())
                    ->get();*/
                /* filter by announcement active */
                /*$countSubjectByStudent = $userAssignment->filter(function($value) use ($id){
                    return $value->assignmentStudent->announcement->id == $id;
                })->count();*/

            //}

            if(Auth::user()->can('my-assignments-management')){
                
                /* my subject by teacher */
                $countSubjectByTeacher = AssignmentTeacher::with('user')
                    ->where('announcement_id',$id)
                    ->where('user_id',Auth::id())
                    ->count();
            }
        }

        $view =  ( Auth::user()->isRole('student') || Auth::user()->isRole('registrado') )? 'home-student' : 'home';
        
        return view($view,compact('announcement','annPreEnroll','countPreEnrolled','countEnrolled','countSubjectByStudent','countSubjectByTeacher','pre_enrollment_with_voucher'));
    }
}
