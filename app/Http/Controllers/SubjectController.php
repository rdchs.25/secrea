<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use App\Http\Requests\SubjectRequest;
use Toastr;

class SubjectController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects= Subject::orderBy('id', 'desc')->get();
        return view('configviews.subject.index',compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
               
        return view('configviews.subject.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    {
        $weeks = '[{"name" : "Nueva semana sin título", "sessions" : []}]';

        $content = '{"weeks": '. $weeks .', "completed": false}';


        Subject::create([
            'name'                  =>  $request->input('name'),
            'identify'              =>  $request->input('identify'),
            'path_image_subject'    =>  $request->file('path_image_subject')->store('image_subject', 'public'),
            'path_calendar'         =>  $request->file('path_calendar')->store('calendar', 'public'),
            'path_content'          =>  $request->file('path_content')->store('content', 'public'),
            'path_programation'     =>  $request->file('path_programation')->store('programation', 'public'),
            'url_welcome_video'     =>  $request->input('url_welcome_video'),
            'content'               =>  $content,
        ]);

        Toastr::success('Asigantura Creada Exitosamente');
        return redirect('/subject');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {  
        $subject = Subject::findBySlug($slug);
         if(! $subject){
            Toastr::error('Asigantura Inexistente');
            return redirect('/subject');
         }
              
        return view('configviews.subject.edit', compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SubjectRequest  $SubjectRequest
     * @param  Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectRequest $request, Subject $subject)
    {
        #Obtener nuevos datos
        $new_data = [
            'name'                  =>  $request->input('name'),
            'identify'              =>  $request->input('identify'),
            'url_welcome_video'     =>  $request->input('url_welcome_video'),
        ];

        if(!empty($request->file('path_image_subject'))){
            $new_data['path_image_subject']  =  $request->file('path_image_subject')->store('image_subject', 'public');
            \Storage::delete($subject->path_image_subject);
        }
        if(!empty($request->file('path_calendar'))){
            $new_data['path_calendar']   =  $request->file('path_calendar')->store('calendar', 'public');
            \Storage::delete($subject->path_calendar);
        }
        if(!empty($request->file('path_programation'))){
            $new_data['path_programation'] = $request->file('path_programacion')->store('programacion', 'public');
             \Storage::delete($subject->path_programation);
        }  

        #Update subject
        $subject->update($new_data);
        #Mensaje
        Toastr::success('Asignatura Actualizado Exitosamente');
        #Response
        return redirect('/subject');

    }

    public function updateContent(Request $request, $id)
    {
        $subject = Subject::find($id);        
        $subject->update([
            'content'  =>  $request->input('content'),
        ]);

        Toastr::success('Curso actualizado exitosamente');

        return response()->json(["ok" => true], 200);
    }

    public function updateContentPreview(Request $request, $id)
    {
        $subject = Subject::find($id);        
        
        $subject->update([
            'content_preview'  =>  $request->input('content'),
        ]);

        return response()->json(["ok" => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        #Buscar subject
        $subject = Subject::findBySlug($slug);
        #Eliminar archivos en storage
        try{
            $subject->delete();
            \Storage::delete($subject->path_image_subject);
            \Storage::delete($subject->path_calendar);
            \Storage::delete($subject->path_programation);
            Toastr::success('Asignatura Eliminada Exitosamente');
        }catch(\PDOException $e){  
            Toastr::warning('Lo sentimos, la asignatura \"'.$subject->name.'\" está siendo utilizada.');
        }
    
        #Response
        return redirect('/subject');
    }
    
}
