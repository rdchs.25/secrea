<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Toastr;

class RoleController extends Controller
{

    /**
     * Display a listing of the resource.1
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles= Role::orderBy('id', 'desc')->get();        
        return view('configviews.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configviews.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        Role::create($request->all());
        Toastr::success('Rol Creado Exitosamente');

        return redirect('/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $role = Role::find($id);
         if(! $role){
            Toastr::warning('Rol inexistente');

            return redirect('/role');
         }        
        return view('configviews.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Role $role , RoleRequest $request)
    {
        $role->update($request->all());
        Toastr::success('Rol Actualizado Exitosamente');

        return redirect('/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agreement = Role::find($id);
        $agreement->delete();
        Toastr::success('Rol Eliminado Exitosamente');

        return redirect('/role');
    }

    public function editPermissions($id)
    {
        $role = Role::where('id', $id)
         ->first();
        
         if(! $role){
            Toastr::warning('Rol inexistente');

            return redirect('/role');
         }

         $permissions = Permission::all();

        return view('configviews.role.assign', compact('role', 'permissions'));
    }

    public function addPermissions($id, Request $request)
    {
        $role=Role::find($id);

        $role->assignPermission($request->permission_id);
        Toastr::success('Permiso asignado exitosamente');

        return redirect('/role/'.$request->id.'/permissions');
    }

    public function removePermissions($id, $permission_id)
    {
        $role = Role::find($id);

        $role->revokePermission($permission_id);
        Toastr::success('Permiso revocado exitosamente');

        return redirect('/role/'.$id.'/permissions');
    }
}
