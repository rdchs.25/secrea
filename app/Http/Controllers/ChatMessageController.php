<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile; 
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\ChatMessage;
use App\AssignmentTeacher;
use App\AssignmentStudent;
use App\UserAssignment;
use App\Announcement;
use App\Events\ChatMessageWasReceived;
use DB;
use Toastr;

class ChatMessageController extends Controller
{

	public function index()
	{
	    if(Auth::user()->can('my-subjects-management')){
          /* active announcement */
          $announcement = Announcement::where('status','1')->first();
          $id = $announcement->id;
          /* CHECKED MATRICULA (verificar si el alumno ya está matriculado) */
          $userassignment = UserAssignment::with('user','assignmentStudent')->where('user_id',Auth::id())->get();
          /* FILTRAR LA MATRICULA POR CONVOCATORIA */
          $filtered = $userassignment->filter(function($value) use ($id){
                return $value->assignmentStudent->announcement->id == $id ;
          });
          if($filtered->isEmpty()){
            Toastr::warning('Usted no tiene acceso al chat, verifique su matricula.'); 
            return view('home'); 
          }
        }  
        
      return view('chat');
      
      
	}

  protected function getCountSmsChatIcon(Request $request)
  {
    if(Auth::user()->isRole('director')){
        $count = ChatMessage::where('state' , 1)
                           ->groupBy('emitter_user_id')
                           ->count(); 
    }else{
        $count = ChatMessage::Where('receiver_user_id', Auth::id())
                           ->where('state' , 1)
                           ->groupBy('emitter_user_id')
                           ->count(); 
    }
        
    return $count;

  }

  protected function getNotyChat(Request $request)
  {
    if(Auth::user()->isRole('director')){
        $notyChat = ChatMessage::select(DB::raw('count(*) as count, emitter_user_id'))
                              ->where('state' , 1)
                              ->groupBy('emitter_user_id')
                              ->with('user:id,name')
                              ->get();
    }else{
        $notyChat = ChatMessage::select(DB::raw('count(*) as count, emitter_user_id'))
                              ->where('receiver_user_id', Auth::id())
                              ->where('state' , 1)
                              ->groupBy('emitter_user_id')
                              ->with('user:id,name')
                              ->get();
    }
    return $notyChat;
  }

  public function setMessage(Request $request)
    {
        $cadena_resultante= $request->input('body');
         
        $cadena_resultante= preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blank">$0</a>', $cadena_resultante);
        
        $cadena_resultante= preg_replace("/href=\"www/", 'href="http://www', $cadena_resultante);

      /* store message*/
	    $chatMessage = ChatMessage::create([
	        'message' 			        => 	$cadena_resultante,
	        'receiver_user_id'	        => 	$request->input('receiver_user_id'),
	        'emitter_user_id'           =>	Auth::id(),
            'assignment_teacher_id'     =>  $request->input('assignment_teacher')
	    ]);

      /* launch event */
      $chat['body'                 ]       =  $cadena_resultante;
      $chat['created_at'           ]       =  $chatMessage->created_at;
      $chat['receiver_user_id'     ]       =  $request->input('receiver_user_id');
      $chat['emitter_user_id'      ]       =  Auth::id();
      $chat['assignment_teacher_id']       =  $request->input('assignment_teacher');
      $chat['user'                 ]       =  User::select('id','name')->where('id',Auth::id())->first();
      $chat['user']['path_profile_image']  =  (Profile::where('user_id',Auth::id())->first())? "storage/".Profile::where('user_id',Auth::id())->first():"/img/avatars/avatar.jpg"  ;

      event(new ChatMessageWasReceived($chat));
 		
 	  return json_encode(["true",$cadena_resultante]);
   }


   /* GET CONVERSATIONS - CHAT */
   protected function conversations(Request $request)
   {
   		/* active announcement */
   		$announcement = Announcement::where('status','1')->first();
        $announcement_id = $announcement->id;
        /**/
        $assignment_student   = AssignmentStudent::where('user_id',Auth::id())->where('announcement_id',$announcement_id)->first();  

        /* for director */

        if(Auth::user()->isRole('director')){
            $data = UserAssignment::select('id','user_id','assignment_teacher_id')
                            ->with('user.profiles')
                            ->with('assignmentTeacher.subject')
                            ->with('assignmentTeacher.grade')
                            ->get();

            foreach ($data as $key => $value) {
                
                if($value->assignmentTeacher->announcement_id == $announcement_id){
                    /* only the fields we need  */       
                    $conversations[$key]['unread'] =  ChatMessage::where('assignment_teacher_id',$value->assignmentTeacher->id)
                            ->where('emitter_user_id',$value->user_id)
                            //->where('receiver_user_id',Auth::id())
                            ->where('state',1)
                            ->count();
                    
                    $conversations[$key]['created_at'] =  ChatMessage::where('assignment_teacher_id',$value->assignmentTeacher->id)
                            ->where('emitter_user_id',$value->user_id)
                            //->where('receiver_user_id',Auth::id())
                            ->max('created_at');
                     
                    $conversations[$key]['assignmentTeacher']                       = $value->assignment_teacher_id;
                    $conversations[$key]['auth']                                    = Auth::id();
                    $conversations[$key]['user']['name']                            = $value['user']['name'];
                    $conversations[$key]['user']['id']                              = $value['user']['id'];
                    $conversations[$key]['subject']['name']                         = $value['assignmentTeacher']['subject']['name'];
                    $conversations[$key]['subject']['grade']                        = $value['assignmentTeacher']['grade']['name'];
                    $conversations[$key]['user']['profiles']['path_profile_image']  = $value->user->profiles['path_profile_image'] ? "storage/".$value->user->profiles['path_profile_image'] : "/img/avatars/avatar.jpg" ; 
                    
                    $conversations[$key]['index']                                   = 0;
                }
              
            }         
        }

        /* for student */
        if(Auth::user()->can('my-subjects-management')){
          
            $data = AssignmentTeacher::select('id','user_id','subject_id','announcement_id')
                                ->with('subject:id,name','user.profiles')
                                ->where('announcement_id', $announcement_id)
                                ->where('grade_id',$assignment_student->grade_id)
                                ->get(); 

            foreach ($data as $key => $value) {
                /* only the fields we need  */  
                $conversations[$key]['unread']    = ChatMessage::where('assignment_teacher_id' , $value->id)
                                                              ->where('emitter_user_id' , $value->user_id)
                                                              ->where('receiver_user_id', Auth::id())
                                                              ->where('state' , 1)
                                                              ->count(); 

                $conversations[$key]['created_at'] =  ChatMessage::where('assignment_teacher_id',$value->id)
                            ->where('emitter_user_id',$value->user_id)
                            ->where('receiver_user_id',Auth::id())
                            ->max('created_at');

                $conversations[$key]['assignmentTeacher']                       = $value->id;
                $conversations[$key]['subject']['name']                         = $value['subject']['name'];
                $conversations[$key]['subject']['grade']                        = "";
                $conversations[$key]['user']['name']                            = $value['user']['name'];
                $conversations[$key]['auth']                                    = Auth::id();
                $conversations[$key]['user']['id']                              = $value['user']['id'];
                $conversations[$key]['user']['profiles']['path_profile_image']  = $value->user->profiles['path_profile_image'] ? "storage/".$value->user->profiles['path_profile_image'] : "/img/avatars/avatar.jpg" ;
                $conversations[$key]['index']                                   = 0;

            }

        }
      
        /* for teacher */
        if(Auth::user()->can('my-assignments-management')){
          
            $data = UserAssignment::select('id','user_id','assignment_teacher_id')
                            ->with('user.profiles')
                            ->with('assignmentTeacher.subject')
                            ->with('assignmentTeacher.grade')
                            ->get();

            foreach ($data as $key => $value) {
                
                if($value->assignmentTeacher->announcement_id == $announcement_id and $value->assignmentTeacher->user_id == Auth::id()){
                    /* only the fields we need  */       
                    $conversations[$key]['unread'] =  ChatMessage::where('assignment_teacher_id',$value->assignmentTeacher->id)
                            ->where('emitter_user_id',$value->user_id)
                            ->where('receiver_user_id',Auth::id())
                            ->where('state',1)
                            ->count();
                    
                    $conversations[$key]['created_at'] =  ChatMessage::where('assignment_teacher_id',$value->assignmentTeacher->id)
                            ->where('emitter_user_id',$value->user_id)
                            ->where('receiver_user_id',Auth::id())
                            ->max('created_at');
                           
                    $conversations[$key]['assignmentTeacher']                       = $value->assignment_teacher_id;
                    $conversations[$key]['auth']                                    = Auth::id();
                    $conversations[$key]['user']['name']                            = $value['user']['name'];
                    $conversations[$key]['user']['id']                              = $value['user']['id'];
                    $conversations[$key]['subject']['name']                         = $value['assignmentTeacher']['subject']['name'];
                    $conversations[$key]['subject']['grade']                        = $value['assignmentTeacher']['grade']['name'];
                    $conversations[$key]['user']['profiles']['path_profile_image']  = $value->user->profiles['path_profile_image'] ? "storage/".$value->user->profiles['path_profile_image'] : "/img/avatars/avatar.jpg" ; 
                    $conversations[$key]['index']                                   = 0;
                }
              
            }

        }   
     
        $collection = collect($conversations);
        
        $sorted = $collection->sortByDesc('created_at');
        
        return $sorted->values()->all();

   }

   protected function getConversation(Request $request)
   {	
        /* get messages */
        if(Auth::user()->isRole('director') || Auth::user()->isRole('teacher') ){
            $sms = ChatMessage::with('user.profiles')
                ->where('receiver_user_id'       , $request->input('user_id') )
                ->where('assignment_teacher_id'  , $request->input('id'))
                ->orWhere('emitter_user_id'      , $request->input('user_id') )
                ->where('assignment_teacher_id'  , $request->input('id'))
                ->orderBy('id','desc')
                ->offset(0)
                ->limit(($request->input('index')+1)*10)
                ->get();

        }else{
            $sms = ChatMessage::with('user.profiles')
                //->where('emitter_user_id'        , Auth::id() )
                //->where('receiver_user_id'       , $request->input('user_id') )
                ->where('assignment_teacher_id'  , $request->input('id'))
                //->orWhere('receiver_user_id'     , Auth::id() )
                //->orWhere('emitter_user_id'        , $request->input('user_id') )
                //->where('assignment_teacher_id'  , $request->input('id'))
                ->orderBy('id','desc')
                ->offset(0)
                ->limit(($request->input('index')+1)*10)
                ->get();
        }
        
        /* change message status */
         ChatMessage::where('assignment_teacher_id', $request->input('id'))
                    ->update(['state' => 0]); 
                    //where('receiver_user_id',Auth::id())

        /* only the fields we need  */    
        $messages = [];   
        foreach ($sms as $key => $value) {
            $messages[$key]['id']               = $value->id;
            $messages[$key]['detail']           = (Auth::user()->isRole('director') || Auth::user()->isRole('teacher') )?true:false;
            $messages[$key]['body']             = $value->message;
            $messages[$key]['emitter_user_id']  = $value->emitter_user_id;
            $messages[$key]['receiver_user_id'] = $value->receiver_user_id;
            $messages[$key]['state']            = $value->state;
            $messages[$key]['user']['name']     = $value->user->name;
            $messages[$key]['user']['id']       = $value->user->id;
            $messages[$key]['created_at']       = $value->created_at;
            $messages[$key]['user']['path_profile_image']       = $value->user->profiles['path_profile_image'] ? "storage/".$value->user->profiles['path_profile_image'] : "/img/avatars/avatar.jpg" ; 
        }

        $collection = collect($messages);
        $messages = $collection->sortBy('id')->values()->all();

   		return json_encode([
            'messages'            =>  $messages,
            'index'               =>  $request->input('index'),
            'subject'             =>  $request->input('subject'),
            'user'                =>  $request->input('user'),
            'user_id'             =>  $request->input('user_id'), 
            'id'                  =>  $request->input('id'),
            'receiver'            =>  (Auth::user()->isRole('director') ) ? AssignmentTeacher::find($request->input('id'))->user_id : Auth::id(),
            'auth'                =>  Auth::id(),
            'scroll'              =>  $request->input('scroll')
        ]);
   }

}
