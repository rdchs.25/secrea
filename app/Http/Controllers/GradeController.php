<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GradeRequest;
use App\Grade;
use App\AcademicLevel;
use Toastr;

class GradeController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades= Grade::with('academicLevels')
        ->orderBy('name', 'desc')
        ->get();
        return view('configviews.grade.index',compact('grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $academicLevels = AcademicLevel::all();         
        if($academicLevels->isEmpty()){
            Toastr::error('Verificar los registros de Niveles Académicos.');
            return redirect('/grade');            
        }
        
        return view('configviews.grade.create', compact("academicLevels"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeRequest $request)
    {
        Grade::create($request->all());
        Toastr::success('Grado Creado Exitosamente');
        return redirect('/grade');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $grade = Grade::find($id);
         if(! $grade){
            Toastr::success('Grado inexistente');
            return redirect('/grade');
         }

         $academicLevels = AcademicLevel::all();         
         if($academicLevels->isEmpty()){
            Toastr::error('No existen niveles académicos registrados, registre uno para poder continuar');
            return redirect('/grade');            
         }

         return view('configviews.grade.edit', compact('grade', 'academicLevels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update(GradeRequest $request, Grade $grade)
    {
        $grade->update($request->all());
        Toastr::success('Grado Actualizado Exitosamente');
        return redirect('/grade');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grade = Grade::find($id);
        try{
            $grade->delete();
            Toastr::success('Grado Eliminado Exitosamente');
        }catch(\PDOException $e){  
            Toastr::warning('Lo sentimos, el grado \"'.$grade->name.'\" está siendo utilizado.');
        }
        return redirect('/grade');
    }
}
