<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AgreementRequest;
use App\Agreement;
use App\Company;
use Toastr;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agreements = Agreement::with('partnerCompanies','saleCompanies')
        ->orderBy('id', 'desc')
        ->get();

        return view('configviews.agreement.index', compact('agreements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $companies = Company::all();
        return view('configviews.agreement.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgreementRequest $request)
    {
        Agreement::create($request->all());
        Toastr::success('Convenio Creado Exitosamente');

        return redirect('/agreement');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $agreement = Agreement::findBySlug($slug);
         if(! $agreement){
            flash('Convenio inexistente', 'danger');
            return redirect('/agreement');
         }
   
        return view('configviews.agreement.show', compact('agreement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($slug)
    {
         $agreement = Agreement::findBySlug($slug);
         if(! $agreement){
            Toastr::warning('Convenio inexistente');

            return redirect('/agreement');
         }
        $companies = Company::all();        
        return view('configviews.agreement.edit', compact('agreement', 'companies'));
    }

    public function update(Agreement $agreement , AgreementRequest $request)
    {      
        $agreement->update($request->all());
        Toastr::success('Convenio Actualizado Exitosamente');

        return redirect('/agreement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $agreement = Agreement::findBySlug($slug);
        $agreement->delete();
        Toastr::success('Convenio Eliminado Exitosamente');

        return redirect('/agreement');
    }
}
