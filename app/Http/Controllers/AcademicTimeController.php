<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AcademicTimeRequest;
use App\AcademicTime;
use Toastr;

class AcademicTimeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $academicTimes= AcademicTime::orderBy('created_at','desc')->get();
        return view('configviews.academic-time.index', compact('academicTimes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configviews.academic-time.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(AcademicTimeRequest $request)
    {
        AcademicTime::create($request->all());
        Toastr::success('Tiempo Academico Creado Exitosamente');
        return redirect('/academic-time');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $academicTime = AcademicTime::findBySlug($slug);
         if(! $academicTime){
            Toastr::error('Tiempo Académico inexistente');
            return redirect('/academic-time');
         }        
        return view('configviews.academic-time.edit', compact('academicTime'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AcademicTimeRequest $request, AcademicTime $academicTime)
    {
        $academicTime->update($request->all());
        Toastr::success('Tiempo Académico Actualizado Exitosamente');
        return redirect('/academic-time');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        #Buscar academic time
        $academicTime = AcademicTime::findBySlug($slug);
        #Eliminar de la base de datos
        try{
            $academicTime->delete();
            Toastr::success('Tiempo Académico Eliminado Exitosamente');
        }catch(\PDOException $e){  
            Toastr::warning('Lo sentimos, el tiempo académico \"'.$academicTime->name.'\" está siendo utilizado.');
        }
        #Response
        return redirect('/academic-time');
    }
}
