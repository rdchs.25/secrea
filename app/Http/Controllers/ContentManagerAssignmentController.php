<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContentManagerAssignmentRequest;
use App\ContentManagerAssignment;
use App\User;
use App\Subject;
use Toastr;

class ContentManagerAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignments = ContentManagerAssignment::with('user','subject')
        ->orderBy('id', 'desc')
        ->get();
        return view('configviews.content-manager-assignment.index',compact('assignments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #GESTORES DE CONTENIO
        $contentManagers = User::with('roles')->get();

        #FILTRAR SOLO GESTORES DE CONTENIDO
    
        $filtered = $contentManagers->filter(function($value){
            return $value->roles[0]->slug == 'content-manager' ;
        });

        $contentManagers = $filtered;

        #ASIGNATURAS
        $subjects= Subject::all();

        if($contentManagers->isEmpty() || $subjects->isEmpty() ){
            Toastr::warning("Verificar registros de Gestores de Contenidos y/o Asignaturas.");
            return redirect('/content-manager-assignment');
        }
       
        //dd($contentManagers);
        return view('configviews.content-manager-assignment.create',compact('contentManagers','subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContentManagerAssignmentRequest $request)
    {
        ContentManagerAssignment::create($request->all());
        Toastr::success('Asignación Creada Exitosamente');

        return redirect('/content-manager-assignment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #GESTORES DE CONTENIO
        $contentManagers = User::with('roles')->get();

        $filtered = $contentManagers->filter(function($value){
            return $value->roles[0]->slug == 'content-manager' ;
        });

        $contentManagers = $filtered;

        #ASIGNATURAS
        $subjects= Subject::all();

        #ASIGNACION ELEGIDA
        $assignment = ContentManagerAssignment::find($id);
         if(! $assignment){
            Toastr::warning('Asignación inexistente');
            return redirect('/content-manager-assignment');
         }

        return view('configviews.content-manager-assignment.edit', compact('assignment','contentManagers','subjects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        #GESTORES DE CONTENIO
        $contentManagers = User::with('roles')->get();

        $filtered = $contentManagers->filter(function($value){
            return $value->roles[0]->slug == 'content-manager' ;
        });

        $contentManagers = $filtered;

        #ASIGNATURAS
        $subjects= Subject::all();

        #ASIGNACION ELEGIDA
        $assignment = ContentManagerAssignment::find($id);
         if(! $assignment){
            Toastr::warning('Asignación inexistente');
            return redirect('/content-manager-assignment');
         }

        return view('configviews.content-manager-assignment.edit', compact('assignment','contentManagers','subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContentManagerAssignmentRequest $request, ContentManagerAssignment $assignment)
    {
        $assignment->update($request->all());
        Toastr::success('Asignación Actualizada Exitosamente');
        
        return redirect('/content-manager-assignment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignment = ContentManagerAssignment::find($id);
        $assignment->delete();
        Toastr::success('Asignación Eliminada Exitosamente');

        return redirect('/content-manager-assignment');
    }
}
