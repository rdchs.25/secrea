<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$notifications = Notification::orderBy('created_at', 'desc')
    	->where('user_id', Auth::user()->id)
    	->get();

        return view('configviews.notification.index', compact('notifications'));
    }

    public function notificationsHeader()
    {
    	$notifications = Notification::orderBy('created_at', 'desc')
    	->where('user_id', Auth::user()->id)
    	->limit(3)
    	->get();
    	
        return view('configviews.notification.index', compact('notifications'));
    }

}
