<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AcademicLevelRequest;
use App\AcademicLevel;
use Toastr;

class AcademicLevelController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academicLevels= AcademicLevel::orderBy('name','desc')->get();
        return view('configviews.academic-level.index', compact('academicLevels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('configviews.academic-level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AcademicLevelRequest $request)
    {
        AcademicLevel::create($request->all());
        Toastr::success('Nivel Academico Creado Exitosamente');
        return redirect('/academic-level');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
         $academicLevel = AcademicLevel::findBySlug($slug);
         if(! $academicLevel){
            Toastr::error('Nivel Académico inexistente');
            return redirect('/academic-level');
         }        
        return view('configviews.academic-level.edit', compact('academicLevel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AcademicLevelRequest $request, AcademicLevel $academicLevel)
    {
        $academicLevel->update($request->all());
        Toastr::success('Nivel Académico Actualizado Exitosamente');
        return redirect('/academic-level');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        #Buscar academic level
        $academicLevel = AcademicLevel::findBySlug($slug);
        #Eliminar de la base de datos
        try{
            $academicLevel->delete();
            Toastr::success('Nivel Académico Eliminado Exitosamente');
        }catch(\PDOException $e){  
            Toastr::warning('Lo sentimos, el nivel académico \"'.$academicLevel->name.'\" está siendo utilizado.');
        }
        #Response
        return redirect('/academic-level');
    }
}
