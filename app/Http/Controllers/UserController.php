<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use App\Grade;
use App\Agreement;
use App\UserAgreement;
use App\Document;
use App\Profile;
use App\Comment;
use App\PayStudent;
use App\UserAssignment;
use App\Voucher;
use App\Announcement;
use App\AssignmentStudent;
use App\PreEnrollment;
use App\Company;
use App\SupportMessage;
use App\UserCompany;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use App\Mail\Correo;
use App\Mail\CorreoAgreement;
use Illuminate\Support\Facades\Mail;
use Toastr;
use Auth;
use DB;
use Mailgun\Mailgun;

class UserController extends Controller
{
 
    public function confirmed($id)
    {
        $user = User::find($id);
        $mgClient = new Mailgun('pubkey-edafe85612c91af6e1070d1f93724bae');
        $result = $mgClient->get("address/validate", array('address' => $user->email,'mailbox_verification'=>true));
        if($result->http_response_body->mailbox_verification == "true"){
            Toastr::success('Usuario confirmado');
            $user->confirmed = 1;
            $user->save();
        }else{
            Toastr::error('Al parecer el correo no existe');
        }

        return back();

    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fusers      = User::with('userAgreements.agreement')
        ->orderBy('id', 'desc')
        ->get();

        if(Auth::user()->isRole('sales-manager')){
            $company_id = UserCompany::where('user_id',Auth::id())->value('company_id');
            $users = $fusers->filter(function($value) use($company_id){
                if($value->userAgreements != null){
                    if($value->userAgreements->agreement->sales_company_id ==$company_id){
                        return true;
                    }
                }
                return false;
            });
        }else{
            if(Auth::user()->isRole('asesor-comercial')){
                $users = $fusers->filter(function($value){
                    if($value->isRole('registrado')){
                        return true;
                    }
                    return false;
                });
            }else{
                $users = $fusers;
            }
        }
        
        return view('configviews.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #GRADOS
        $grades     = Grade::with('academicLevels')->orderBy('name', 'asc')->get();
        #ROLES
        $user = User::find(Auth::id());
        $roles = Auth::user()->isRole('sales-manager') || Auth::user()->isRole('asesor-comercial') ?Role::where('id','10')->get() : Role::where('name', '!=', 'Estudiante')->get();

        #ANNOUNCEMENT
        $agreements = null;
        if(Auth::user()->isRole('sales-manager')){
            $announcements = Announcement::where('status','0')->get();
            $user_company = UserCompany::where('user_id',Auth::id())->first();
            
            $agreements = Agreement::where('sales_company_id',$user_company->company_id)->get();
        
        }else{
            $announcements = Announcement::where('status','0')->orWhere('status','1')->get();
            $agreements = Agreement::all();
        }
        #COMPANYS
        $companys = Company::all();
        #RESPONSE
        return view('configviews.user.create', compact('roles', 'agreements','grades','announcements','companys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        
        $pass = Auth::user()->isRole('sales-manager')?'secrea':$request->input('password');
        $code = str_random(40);
        //$confirmed = !(($request->input('not_send_mail'))? 1 : 0);
        $user = User::create([
                    'name'                  =>  $request->input('name'),
                    'email'                 =>  $request->input('email'),
                    'password'              =>  bcrypt($pass),
                    'confirmed'             =>  1,
                    'confirmed_code'        =>  $code
                ]);

        if( $request->input('role_id') == 10 ){    
            //if($confirmed){
            $mgClient = new Mailgun('pubkey-edafe85612c91af6e1070d1f93724bae');
            $result = $mgClient->get("address/validate", array('address' => $user->email,'mailbox_verification'=>true));
            //}
            //$val = $confirmed ? $result->http_response_body->mailbox_verification: "true"; 

            if($result->http_response_body->mailbox_verification == "true"){
                AssignmentStudent::create([
                    'user_id'           => $user->id,
                    'grade_id'          => $request->input('grade_id'),
                    'announcement_id'   => $request->input('announcement_id')
                ]); 
                PreEnrollment::create([
                        'user_id'               =>  $user->id,
                        'announcement_id'       =>  $request->input('announcement_id'),
                    ]);
                if($request->input('agreement_id')){
                    $ua = UserAgreement::create([
                        'user_id'                  =>  $user->id,
                        'agreement_id'             =>  $request->input('agreement_id'),
                    ]);
                    $user_agreement = UserAgreement::with('agreement.partnerCompanies')->find($ua->id);
                    //if($confirmed){
                    Mail::to($user->email)->send(new CorreoAgreement($user,$pass,$user_agreement->agreement->partnerCompanies->name));
                    //}
                    
                }else{
                    //if($confirmed){
                    Mail::to($user->email)->send(new Correo($user,$pass)); 
                    //}
                }

            }else{
                $user->delete();
                Toastr::error('Al parecer el correo '.$request->input('email').' no existe.');
                return redirect('/user');
            } 

        }
        if( $request->input('role_id') == 7 ){            
            UserCompany::create([
                'user_id'       => $user->id,
                'company_id'    => $request->input('company_id'),
            ]);          
        }
        try{
            $user->assignRole($request->role_id);
            Toastr::success('Usuario Creado Exitosamente');
        }catch(\Exception $e){
            $user->delete();
            Toastr::error('Eres un infiltrado.');
        }

        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if(! $user){
            Toastr::warning('Usuario inexistente');
            return redirect('/user');
        }
        $user->password = '';

        $disabled = false;
        $roles = null;
        $user['role_id'] =  DB::table('role_user')->where('user_id',$id)->value('role_id');
        /* student or pre inscrito*/
        if($user['role_id'] == 3 || $user['role_id'] == 10 ){
            $disabled = ($user['role_id'] == 3)?true:false;
            $status = ($user['role_id'] == 3)? 1 : 0 ;
            $announcement = Announcement::where('status', $status )->first();
            
            if(!$announcement){
                $status = $status? 0 : 1;
                $announcement = Announcement::where('status', $status )->first();
            }
        
            $grade_id = DB::table('assignment_students')->where('user_id',$id)->where('announcement_id',$announcement->id)->value('grade_id');
            
            if(!empty($grade_id)){
                $user['announcement_id']    = $announcement->id;
                $user['agreement_id']       = UserAgreement::where('user_id',$id)->value('agreement_id');
                $user['grade_id']           = $grade_id;
            }else{

                $status = ($status==1)? 0 : 1;                
                $announcement = Announcement::where('status', $status )->first();
                if($announcement){
                    $grade_id = DB::table('assignment_students')->where('user_id',$id)->where('announcement_id',$announcement->id)->value('grade_id');
                    if(empty($grade_id)){
                        $disabled = true;
                    }else{
                        $user['announcement_id']    = $announcement->id;
                        $user['agreement_id']       = UserAgreement::where('user_id',$id)->value('agreement_id');
                        $user['grade_id']           = $grade_id;
                    }
                }else{
                    $disabled = true;
                }
            }  
        }
        
        /* gestor de ventas */
        if($user['role_id'] == 7){
            $user_company = UserCompany::where('user_id',$id)->first();
            $user['company_id'] = $user_company->company_id;       
        }
        /* All data for form */
        if(Auth::user()->isRole('sales-manager')){
            $roles  = Role::where('id',10)->get();
        }else{
            if($user['role_id'] == 3){
                $roles  = Role::where('name','Estudiante')->get();
            }else{
                if($user['role_id'] == 10){
                    $roles  = Role::where('name','Registrado')->get();
                }else{
                    $roles  = Role::where('name','<>','Estudiante')->get();
                }
            } 
        }
        
        $grades = Grade::with('academicLevels')->orderBy('name', 'asc')->get();
        $agreements     = Agreement::all();
        $companys       = Company::all();
        $announcements  = Announcement::where('status','0')->orWhere('status','1')->get();
        
        return view('configviews.user.edit', compact('user', 'roles', 'agreements', 'grades','announcements','companys','disabled'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user , UserRequest $request)
    {
        /* Update data user, includes the student role */
        $data = [
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password)
        ];
        if(is_null($request->password)){ unset($data['password']);}

        /* revoke all rol */
        $rol_id =  DB::table('role_user')->where('user_id',$user->id)->value('role_id');
        if($rol_id != $request->role_id){
            $user->revokeAllRoles();
            $user->assignRole($request->role_id);
        }

        /* rol: registrado */
        if($rol_id == 10){
            $announcement_active    = Announcement::where('status','1')->first();
            $announcement_pi        = Announcement::where('status','0')->first();
            
            $preEnrollment = PreEnrollment::where('announcement_id', $announcement_pi->id )
                ->where('user_id', $user->id)
                ->get();

            $announcement = ($preEnrollment->isEmpty())?$announcement_active:$announcement_pi;
                    
            PreEnrollment::where('announcement_id',$announcement->id)
                ->where('user_id',$user->id)
                ->update(['announcement_id' => $request->announcement_id]);

            AssignmentStudent::where('announcement_id',$announcement->id)
                ->where('user_id',$user->id)
                ->update(['grade_id' => $request->grade_id,'announcement_id'=>$request->announcement_id]);       
        }
        /* rol: sales-manager */
        if($rol_id == 7){
            if($request->role_id == 7){
                UserCompany::where('user_id',$user->id)
                    ->update(['company_id' => $request->company_id]);
            }else{
                UserCompany::where('user_id',$user->id)->delete();
                if($request->role_id == 10){
                    AssignmentStudent::create([
                            'user_id'           => $user->id,
                            'grade_id'          => $request->input('grade_id'),
                            'announcement_id'   => $request->input('announcement_id')
                        ]); 
                    PreEnrollment::create([
                            'user_id'           =>  $user->id,
                            'announcement_id'   =>  $request->input('announcement_id'),
                        ]); 
                    UserAgreement::create([
                            'user_id'           =>  $user->id,
                            'agreement_id'      =>  $request->input('agreement_id'),
                        ]);     
                }
            }
        }

        $user->update($data);

        Toastr::success('Editado Exitosamente');
        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * View documents from user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function documents($id)
    {
        
        $user = User::find($id);

         if(! $user){
            Toastr::warning('Usuario inexistente');
            return redirect('/user');
         }

         if(! $user->documents){
            Toastr::warning('Usuario sin documentos');
            return redirect('/user');
         }

         $document = $user->documents;
         return view('configviews.document.index', compact('document'));
    }
    public function changeStatusUser($id)
    {
        $user = User::find($id);

         if(! $user){
            Toastr::warning('Usuario inexistente');
            return redirect('/user');
         }

        $user->status == 1 ? $user->status = 0 : $user->status = 1;

        $user->save();

        Toastr::success('Usuario cambiado de estado exitosamente');
        
        return redirect('/user');
    }

    /**
     * Undocumented function
     *
     * @param [type] $id pre_enrollment_id
     * @return void
     */
    public function destroyRegistrado($id){

        $preEnrollment = PreEnrollment::with('announcements')->find($id);
        $user_id = $preEnrollment->user_id;
        $slug = $preEnrollment->announcements->slug;
        //checked enroll
        $userassignment = UserAssignment::with('user','assignmentStudent')->where('user_id',$user_id)->get();
        $filtered = $userassignment->filter(function($value) use ($slug){
            return $value->assignmentStudent->announcement->slug == $slug ;
        });
        if(!$filtered->isNotEmpty()){
            PayStudent::where('pre_enrollment_id',$id)->delete();
            $preEnrollment->delete();
            AssignmentStudent::where('user_id',$user_id)->delete();
            Document::where('user_id',$user_id)->delete();
            Comment::where('user_id',$user_id)->delete();
            Voucher::where('user_id',$user_id)->delete();
            Profile::where('user_id',$user_id)->delete();
            SupportMessage::where('user_id',$user_id)->delete();
            User::where('id',$user_id)->delete();
            
            Toastr::success('Alumno eliminado');
        }else{
            Toastr::error('Solo podemos eliminar alumnos registrados');
        }
        return redirect("/announcement/$slug");

    }
}
