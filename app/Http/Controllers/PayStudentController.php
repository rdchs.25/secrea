<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreEnrollment;
use App\PayAnnouncement;
use App\PayStudent;
use App\Announcement;
use App\Voucher;
use Toastr;
use Auth;

class PayStudentController extends Controller
{
    /**
     * Show student payments - view student.
     *
     * @return \Illuminate\Http\Response
     */
    public function mypayment($slug)
    {
        if( Auth::user()->isRole('student') || Auth::user()->isRole('registrado') ){
            if($slug == "current"){
                $pe = PreEnrollment::with('announcements')->where('user_id',Auth::id())->orderBy('created_at','DESC')->first();
                $announcement = $pe->announcements;
            }else{
                $announcement = Announcement::findBySlug($slug);
            }
            //All pre enrollment
            $preenrollments = PreEnrollment::with('announcements')->where('user_id',Auth::id())->get();
            //Pre enrollment requested
            $preenrollment = $preenrollments->where('announcement_id', $announcement->id)->first();
            //payment
            $payannouncement = PayAnnouncement::with('announcement')->where('announcement_id',$announcement->id)->get();
            $paystudent = PayStudent::with('payannouncement.announcement','voucher')->where('pre_enrollment_id',$preenrollment->id)->get();
            
            $pay['enroll'] = $paystudent->contains('pay_announcement_id',$payannouncement[0]->id);
            $pay['cpension'] = $paystudent->where('pay_announcement_id', $payannouncement[1]->id)->sum('count');
            $pay['vouchers'] = Voucher::where(['announcement_id'=>$announcement->id,'user_id'=>Auth::id()])->get();
            
            return view('configviews.payment.student',compact('preenrollments','payannouncement','pay'));
        }else{
            Toastr::error('La página que intentaste ingresar no está disponible');
            return redirect('/home');
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Save in BD the student's payments
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //validate
        $this->validate($request, [
            'voucher' =>  'required',
        ]);
        $preEnrollment = PreEnrollment::find($request->input('abc'));
        $payannouncement = PayAnnouncement::with('announcement')->where('announcement_id',$preEnrollment->announcement_id)->get();
        $voucher_use = false;
        //enroll
        if($request->input('chbx_pay_ma')=="on"){
            PayStudent::create([
                'pay_announcement_id'   => $payannouncement[0]->id,
                'pre_enrollment_id'     => $request->input('abc'),
                'voucher_id'            => $request->input('voucher'),
                'count'                 => 1
            ]);
            $voucher_use = true;
        }
        //pensions
        if(sizeOf($request->input('pensiones'))>0){
            PayStudent::create([
                'pay_announcement_id'   => $payannouncement[1]->id,
                'pre_enrollment_id'     => $request->input('abc'),
                'voucher_id'            => $request->input('voucher'),
                'count'                 => sizeOf($request->input('pensiones')),
            ]);
            $voucher_use = true;
        }
        //has the voucher been used?
        if($voucher_use){
            $voucher = Voucher::find($request->input('voucher'));
            $voucher->status = false;
            $voucher->update();
        }
        
        Toastr::success('Pagos registrados con éxito.');
        return redirect("/announcement/".$payannouncement[0]->announcement->slug."/payment/".$preEnrollment->user_id);   

    }

    /**
     * Show student payments - view administrador.
     *
     * @param  string  $slug ciclo académico
     * @param  int  $id user_id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, $id)
    {   
        //Check announcement
        $announcement = Announcement::findBySlug($slug);
        if(! $announcement){
            Toastr::warning('Convocatoria inexistente');
            return redirect('/announcement');
        }
        //Check pre enrollment
        if(PreEnrollment::where('announcement_id',$announcement->id)->where('user_id',$id)->exists()){

            //User information
            $user = PreEnrollment::with('users.profiles','users.assignmentStudent.grade', 'users.documents')
                        ->where('user_id',$id)
                        ->where('announcement_id',$announcement->id)
                        ->first();
            $yourpreenrollment = PreEnrollment::with('announcements')
                        ->where('user_id',$id)
                        ->get();

            $user['vouchers'] = Voucher::where('status','1')->where('user_id',$id)->get();

            //Code y data to travel users
            $preEnrollments = PreEnrollment::select('id','user_id')
                ->where('announcement_id',$announcement->id)
                ->orderBy('created_at','desc')
                ->get();

            foreach ($preEnrollments as $key => $value) {
                if($value->user_id == $id){
                    $user['userAfter']  = $preEnrollments->has($key + 1)? $preEnrollments[$key + 1]['user_id'] : null;
                    $user['userBefore'] = $preEnrollments->has($key - 1)? $preEnrollments[$key - 1]['user_id'] : null;
                }      
            }

            //Payments
            $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $voucher = '';
            $payannouncement = PayAnnouncement::with('announcement')->where('announcement_id',$announcement->id)->get();
            $paystudent = Paystudent::with('voucher')->where('pre_enrollment_id',$user->id)->get();
            $pay['enroll'] = false;
            if($paystudent->contains('pay_announcement_id',$payannouncement[0]->id)){
                $pay['enroll'] = $paystudent->where('pay_announcement_id', $payannouncement[0]->id)[0]->voucher->path_voucher;
                $voucher = $paystudent->where('pay_announcement_id', $payannouncement[0]->id)[0]->voucher->path_voucher;
                $pay['code_enroll']= $color;
            }

            $pay['cpension'] = 0;
            $pay['pension'] = [];
            $filtered = $paystudent->where('pay_announcement_id', $payannouncement[1]->id);
            if($filtered){
                $init = 0;
                $finish = 0;
                foreach($filtered as $value){
                    
                    $pay['cpension'] += $value->count;
                    $finish += $value->count;
                
                    for ($i = $init; $i < $finish; $i++) { 
                        $pay['pension'][$i] =$value->voucher->path_voucher;
                        if($value->voucher->path_voucher != $voucher){
                            $color  = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                            $voucher = $value->voucher->path_voucher;
                        }
                        $pay['pension']['code_'.$i] = $color;
                    }
                    $init = $finish;
                }
            }
            return view('configviews.payment.show',compact('payannouncement','user','pay','yourpreenrollment'));
        }else{
            Toastr::error('Datos incorrectos');
            return redirect('/announcement');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
