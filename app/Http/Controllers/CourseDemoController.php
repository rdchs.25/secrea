<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;

class CourseDemoController extends Controller
{
    public function index()
    {
        $subjects= Subject::all();
        return view('managementviews.course-demo.index',compact('subjects'));
    }
}
