<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Comment;
use App\Document;
use App\Announcement;
use App\PreEnrollment;
use Toastr;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'body'      =>  'required',
            'status'    =>  'required'
        ]); 
        $data['body'] = $request->input('body');
        $data['author_id'] = Auth::id();
        $data['user_id'] = $request->input('abc');
        $data['status'] = $request->input('status');
        Comment::create($data);
        Toastr::success('Comentario registrado');
        return redirect('/announcement/'.$request->input('slug').'/comment/'.$request->input('abc'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug,$id)
    {
        $announcement = Announcement::findBySlug($slug);
        if(! $announcement){
            Toastr::warning('Convocatoria inexistente');
            return redirect('/announcement');
        }
        $preEnrollments = PreEnrollment::select('user_id')
            ->where('announcement_id',$announcement->id)
            ->orderBy('created_at','desc')
            ->get();

        foreach ($preEnrollments as $key => $value) {
            if($value->user_id == $id){
                $userAfter  = $preEnrollments->has($key + 1)? $preEnrollments[$key + 1]['user_id'] : null;
                $userBefore = $preEnrollments->has($key - 1)? $preEnrollments[$key - 1]['user_id'] : null;
            }      
        }
        //$user = User::with('documents','profiles')->where('id',$id)->first();

        $user = PreEnrollment::with('users.profiles','users.assignmentStudent.grade', 'users.documents')
                    ->where('user_id',$id)
                    ->where('announcement_id',$announcement->id)
                    ->first();
        
        $comments = Comment::with('author')->where('user_id',$id)->orderBy('created_at','des')->get();
        $document = Document::where('user_id',$id)->first();
        
        return view('configviews.comment.show',compact('user','comments','userAfter','userBefore','announcement','document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
