<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Document;
class VerificateEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('email',$request->email)->first();
        if(!is_null($user)){
            if(!$user->confirmed){
                return redirect('/login');
            }else{
                if(is_null(Document::where('user_id',$user->id)->first())){
                    session(['ss_document' => false]);
                }
            }
        }
        
        return $next($request);
    }
}



