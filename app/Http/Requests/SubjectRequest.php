<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
       $rules = [
           'name'               => 'required|max:255|regex:/^[a-zA-Z0-9[:space:]ÑñáéíóúÁÉÍÓÚ]+$/u',
           'identify'           => 'required',
           'url_welcome_video'  => 'required|url'
       ];

        if( $request->method() === "POST" ){
            
            $rules['path_image_subject']   =  'required|image';
            $rules['path_calendar']        =  'required|mimes:pdf';
            $rules['path_content']         =  'required|mimes:pdf';
            $rules['path_programation']    =  'required|mimes:pdf';

        }

        return $rules;
    }

     public function messages()
    {
        return [
            'name.required'                 => 'Por favor, escribe el nombre de la asignatura.',
            'name.max'                      => 'El nombre de la asignatura no puede superar los 255 caracteres.',
            'identify.required'            => 'Por favor, registrar la identificación de la asignatura.',
            'path_image_subject.required'   => 'Por favor, selecciona la imágen de asignatura.',
            'path_image_subject.image'      => 'El campo imágen de asignatura, solo admite archivo: pdf.',
            'path_calendar.required'        => 'Por favor, selecciona el archivo de calendario.',
            'path_calendar.mimes'           => 'Por favor el archivo calendario debe ser un PDF.',
            'path_content.required'         => 'Por favor, selecciona el archivo de contenido.',
            'path_content.mimes'            => 'Por favor, el archivo de contenido debe ser un PDF.',
            'path_programation.required'    => 'Por favor, selecciona el archivo de programación.',
            'path_programation.mimes'       => 'Por favor, el archivo de programación debe ser un PDF.',
            'url_welcome_video.required'    => 'Por favor, ingresa la url del video.',
            'url_welcome_video.url'         => 'La url del video no es válida.',
        ];
    }
}
