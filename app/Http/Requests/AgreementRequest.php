<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class AgreementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|max:191|regex:/^[a-zA-Z]+$/u',
            'partner_company_id'    => 'required|numeric',
            'sales_company_id'      => 'required|numeric',
            'amount'                => 'required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'name.required'                 => 'El campo nombre es requerido.',
            'name.max'                      => 'El campo nombre debe ser menor que 191 caracteres.',

            'partner_company_id.required'   => 'El campo Compañía Financiera es requerido.',

            'sales_company_id.required'     => 'El campo Compañía de Ventas es requerido.',

            'amount.required'               => 'El campo Dinero a becar es requerido.',
            'amount.numeric'                => 'El campo Dinero a becar tiene que ser numérico.',
        ];
    }
}
