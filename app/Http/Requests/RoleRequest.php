<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:191',
            'slug'          => 'required|max:191',
            'description'   => 'required|max:191',
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'El campo nombre es requerido.',
            'name.max'              => 'El campo nombre debe ser menor que 191 caracteres.',

            'slug.required'         => 'El campo slug es requerido.',
            'slug.max'              => 'El campo slug debe ser menor que 191 caracteres.',

            'description.required'  => 'El campo descripción es requerido.',            
            'description.max'       => 'El campo descripción debe ser menor que 191 caracteres.',
        ];
    }
}
