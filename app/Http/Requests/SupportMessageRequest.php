<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class SupportMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required'
        ];        
    }
    public function messages()
    {
        return [
            'message.required' => 'Por favor, escribe tu mensaje.',
            'message.max' => 'El mensaje no puede superar los 191 caracteres.'
        ];
    }
}
