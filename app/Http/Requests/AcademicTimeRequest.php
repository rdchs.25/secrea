<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AcademicTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:191|regex:/^[a-zA-Z]+$/u',
            'number_weeks'  => 'required|numeric|max:48|min:1'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor, ingresa el nombre del Tiempo Académico.',
            'number_weeks.required' => 'Por favor, ingresa la duración en semanas.',
            'number_weeks.numeric' => 'Por favor, ingresa un valor numérico en la duración de semanas.',
            'name.max' => 'El nombre del Tiempo Académico no puede superar los 191 caracteres.',
            'number_weeks.max' => 'El campo semanas debe tener un valor máximo de 48.',
            'number_weeks.min' => 'El campo semanas debe tener un valor mínimo de 4.',
        ];
    }
}
