<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentManagerAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'user_id'    =>'required|numeric',
           'subject_id' =>'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'      => 'Por favor, seleccione un gestor de contenidos.',
            'subject_id.required'   => 'Por favor, seleccione una asignatura.',
            'user_id.numeric'       => 'Error al seleccionar un gestor de contenidos',
            'subject_id.numeric'    => 'Error al seleccionar asignatura.',
        ];
    }
}
