<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
         $rules = [
            'name'      => 'required|max:191',
            'role_id'   => 'required|numeric'
        ];

        if( $request->method() === "POST"){
            if(Auth::user()->isRole('sales-manager')){
                $rules['agreement_id']= 'required';
            }else{
                $rules['password']  =  'required|string|min:6';
            }
            $rules['email']     =  'required|string|email|max:191|unique:users,email';
        } 

        if( $request->method() === "PATCH"){
            $rules['email']     =  'required|string|email|max:191|unique:users,email,' . $this->user->id;
        }

        if( $request->input('role_id') == 10 ){
            $rules['grade_id'] = 'required';
            $rules['announcement_id'] = 'required';
        }
        
        if( $request->input('role_id') == 7 ){
            $rules['company_id'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'         => 'El campo nombre es requerido.',
            'name.max'              => 'El campo nombre debe ser menor que 191 caracteres.',

            'email.required'        => 'El campo email es requerido.',
            'email.email'           => 'Ingrese un email valido.',
            'email.unique'          => 'El email ingresado ya existe.',

            'password.required'     => 'El campo contraseña es requerido.',            
            'password.min'          => 'El campo contraseña debe ser minimo de 6 caracteres.',

            'role_id.required'      => 'El campo rol es requerido.',
            'grade_id.required'     => 'El campo grado es requerido.',
            'agreement_id.required' => 'El campo convenido es requerido.',

            'company_id.required'            => 'El campo compañia es requerido.'
        ];
    }
}
