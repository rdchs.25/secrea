<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'               => 'required|max:191',
            'section'            => 'required|max:3',
            'academic_level_id'  => 'required|numeric'
        ];
    }

     public function messages()
    {
        return [
            'name.required'               => 'Por favor, escribe el nombre del grado.',
            'name.max'                    => 'El nombre del grado no puede superar los 191 caracteres.',
            'section.required'            => 'Por favor, escribe el nombre o caracter de la sección',
            'section.max'                 => 'El nombre de la sección no puede superar los 3 caracteres.',
            'academic_level_id.required'  => 'Por favor, selecciona un Nivel Académico.',
            'academic_level_id.numeric'   => 'Error al seleccionar un Nivel Académico.'
        ];
    }
}
