<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
                'name'              => 'required|max:191',
                'size_time'         => 'required',
                'status'            => 'required',
                'academic_time_id'  => 'required',
                'rode_ma'           => 'required',
                'rode_pe'           => 'required',
                'count_pe'          => 'required',
                'rode_ce'           => 'required',
            ];
        
    }
    public function messages()
    {
        return [
            'name.required'             => 'El campo nombre es requerido.',
            'name.max'                  => 'El campo nombre debe ser menor que 191 caracteres.',
            'size_time.required'        => 'El campo tiempo académico(meses) es requerido.',
            'status.required'           => 'El campo estado de la convocatoria es requerido.',
            'rode_ma.required'          => 'El campo precio de matrícula es requerido.',
            'rode_pe.required'          => 'El campo precio de pensión es requerido.',
            'rode_ce.required'          => 'El campo precio de certificado es requerido.',
            'count_pe.required'         => 'El campo cantidad de pensiones es requerido.',
            'academic_time_id.required' => 'El campo tiempo académico es requerido.',
        ];
    }


}
