<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191|regex:/^[a-zA-Z]+$/u',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es requerido.',
            'name.max'      => 'El campo nombre debe ser menor que 191 caracteres.'
        ];
    }
}
