<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        /*
         * There are only path and post methods
         */
        $rules = [
            'dni'                   =>  'required|digits:8',
            'name'                  =>  'required',
            'address'               =>  'required',
            'phone'                 =>  'required|digits:9',
            'birth_date'            =>  'required'
        ];

        if($request->method() === "POST"){
            $rules['path_profile_image']    =  'required|mimes:jpg,png,jpeg,gif,svg';
        }   
        if($request->method() === "PATCH"){
            $rules['path_profile_image']    =  'mimes:jpg,png,jpeg,gif,svg';
        } 
        
        return $rules;
    }

    public function messages()
    {
        return [
            'dni.required'                => 'Por favor,ingresa tu dni.',
            'dni.numeric'                 => 'Tu número de dni debe ser numérico.',
            //'dni.min'                     => 'El dni debe tener 8 dígitos.',
            //'dni.max'                     => 'El dni debe tener 8 dígitos',
            'name.required'               => 'Por favor, escribe tu nombre.',
            'address.required'            => 'Por favor, escribe tu dirección.',
            'phone.required'              => 'Por favor ingresa tu número de celular.',
            'phone.numeric'               => 'Tu número de celular debe ser numérico.',
            //'phone.min'                   => 'El número de celular debe tener 9 dígitos.',
            //'phone.max'                   => 'El número de celular debe tener 9 dígitos',
            'path_profile_image.required' => 'Por favor, su imágen es requerida.',
            'birth_date.required'         => 'Por favor, su fecha de nacimiento es requerida.'
        ];
    }
}
