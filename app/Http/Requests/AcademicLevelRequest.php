<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AcademicLevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191|regex:/^[a-zA-Z]+$/u'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor, ingresa el nombre del Nivel Académico.',
            'name.max' => 'El nombre del Nivel Académico no puede superar los 191 caracteres.'
        ];
    }
}
