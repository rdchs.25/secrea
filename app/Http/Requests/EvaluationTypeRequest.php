<?php
namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class EvaluationTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
         return [
            'name'          => 'required|max:191|regex:/^[a-zA-Z]+$/u'
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor, ingresa el nombre del Tipo de Evaluación.',
        ];
    }
}
