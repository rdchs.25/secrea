<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportMessage extends Model
{
    protected $fillable = [
        'message','user_id', 'attend'
    ];


    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}