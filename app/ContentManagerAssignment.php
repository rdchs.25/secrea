<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentManagerAssignment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id', 'subject_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Relationship with users
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Relationship with subjects
     *
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
