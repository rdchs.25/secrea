<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->char('concept',2);
            $table->decimal('rode', 5, 2);
            $table->tinyInteger('cuotas');
            $table->integer('announcement_id')->unsigned();
            
            $table->foreign('announcement_id')->references('id')->on('announcements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_announcements');
    }
}
