<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('identify');
            $table->string('path_image_subject');
            $table->string('path_calendar');
            $table->string('path_content');
            $table->string('path_programation');
            $table->string('url_welcome_video',100);
            $table->boolean('status')->default(true);
            $table->string('slug');
            $table->json('content');
            $table->json('content_preview')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
