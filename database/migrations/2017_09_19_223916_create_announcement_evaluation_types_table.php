<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementEvaluationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_evaluation_types', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('percent');
            $table->integer('announcement_id')->unsigned();
            $table->integer('evaluation_types_id')->unsigned();

            $table->foreign('announcement_id')->references('id')->on('announcements');
            $table->foreign('evaluation_types_id')->references('id')->on('evaluation_types');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_type_evaluations');
    }
}
