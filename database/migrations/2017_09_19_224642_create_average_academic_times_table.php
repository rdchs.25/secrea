<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAverageAcademicTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('average_academic_times', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('value');
            $table->string('current_time_academic_active');
            $table->integer('user_assignment_id')->unsigned();
            
            $table->foreign('user_assignment_id')->references('id')->on('user_assignments');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('average_academic_times');
    }
}
