<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pay_announcement_id')->unsigned();
            $table->integer('pre_enrollment_id')->unsigned();
            $table->integer('voucher_id')->unsigned();
            $table->tinyInteger('count');

            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->foreign('pay_announcement_id')->references('id')->on('pay_announcements');
            $table->foreign('pre_enrollment_id')->references('id')->on('pre_enrollments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_students');
    }
}
