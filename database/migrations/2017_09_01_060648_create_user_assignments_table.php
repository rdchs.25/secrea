<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->json('content')->nullable();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('final_score')->nullable();
            $table->integer('assignment_teacher_id')->unsigned();
            $table->integer('assignment_student_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('assignment_teacher_id')->references('id')->on('assignment_teachers');
            $table->foreign('assignment_student_id')->references('id')->on('assignment_students');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_assignments');
    }
}
