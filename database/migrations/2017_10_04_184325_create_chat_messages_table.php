<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->boolean('state')->default(true);   
            $table->integer('receiver_user_id')->unsigned();
            
            $table->integer('emitter_user_id')->unsigned();
            $table->integer('assignment_teacher_id')->unsigned();

            $table->foreign('emitter_user_id')->references('id')->on('users');
            $table->foreign('assignment_teacher_id')->references('id')->on('assignment_teachers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
