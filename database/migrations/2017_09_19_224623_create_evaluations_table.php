<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('value');
            $table->string('week'); 
            $table->integer('user_assignment_id')->unsigned();
            $table->integer('evaluation_type_id')->unsigned();

            $table->foreign('user_assignment_id')->references('id')->on('user_assignments');
            $table->foreign('evaluation_type_id')->references('id')->on('evaluation_types');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
