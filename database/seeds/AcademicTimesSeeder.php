<?php

use Illuminate\Database\Seeder;
use App\AcademicTime;

class AcademicTimesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $academicTimes =[
	       	[
	            "name"  		=> "Bimestre",
			    "slug"  		=> "bimestre",
	            "number_weeks"  => "25",
	        ],

	    ];
	    
	    foreach ($academicTimes as $academicTime)
	    {
	        AcademicTime::create($academicTime);
	    }
	    
    }
}