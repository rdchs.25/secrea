<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $users =[
	        
	        [
	            'name' 		=> 'Secundaria Creativa',
	            'email'	 	=> 'admin@secrea.net',
	            'password' 	=> bcrypt('123456'),
	            'confirmed' => true,
	        ]
	    ];
	    
	    foreach ($users as $user)
	    {
	        User::create($user);	        
	    }

	    $userAdmin = User::find(1);
	    $userAdmin->assignRole(1);
	    

    }
}
