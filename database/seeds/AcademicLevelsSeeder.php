<?php

use Illuminate\Database\Seeder;
use App\AcademicLevel;

class AcademicLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $academicLevels =[
	       	[
	            "name"  		=> "Regular",
			    "slug"  		=> "regular",
	        ]
	    ];
	    
	    foreach ($academicLevels as $academicLevel)
	    {
	        AcademicLevel::create($academicLevel);
	    }
	    
    }
}
