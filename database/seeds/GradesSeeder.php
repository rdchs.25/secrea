<?php

use Illuminate\Database\Seeder;
use App\Grade;

class GradesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $grades =[
	       	[
	            "name"  			=> "Primero de Secundaria",
			    "section"  			=> "A",
			    "academic_level_id" => "1",
	        ],
	       	[
	            "name"  			=> "Segundo de Secundaria",
			    "section"  			=> "A",
			    "academic_level_id" => "1",
	        ],
	       	[
	            "name"  			=> "Tercero de Secundaria",
			    "section"  			=> "A",
			    "academic_level_id" => "1",
	        ],
	       	[
	            "name"  			=> "Cuarto de Secundaria",
			    "section"  			=> "A",
			    "academic_level_id" => "1",
	        ],

	    ];
	    
	    foreach ($grades as $grade)
	    {
	        Grade::create($grade);
	    }
	    
    }
}