<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $roles =[
	        
	        [
	            "name"  		=> "Administrador",
	            "slug"  		=> "admin",
	            "description"  	=> "Usuario con todos los permisos",
	        ],
	       	[
	            "name"  		=> "Docente",
	            "slug"  		=> "teacher",
	            "description"  	=> "Usuario Docente",
	        ],
	       	[
	            "name"  		=> "Estudiante",
	            "slug"  		=> "student",
	            "description"  	=> "Usuario Estudiante",
	        ],
	       	[
	            "name"  		=> "Gestor de Contenido",
	            "slug"  		=> "content-manager",
	            "description"  	=> "Usuario Gestor de Contenido",
	        ],
	       	[
	            "name"  		=> "Director",
	            "slug"  		=> "director",
	            "description"  	=> "Usuario Director",
	        ],
	       	[
	            "name"  		=> "Gestor Multimedia",
	            "slug"  		=> "media-manager",
	            "description"  	=> "Usuario Gestor Multimedia",
	        ],
	       	[
	            "name"  		=> "Gestor de Ventas",
	            "slug"  		=> "sales-manager",
	            "description"  	=> "Usuario Gestor de Ventas",
	        ],
	       	[
	            "name"  		=> "Gestor Académico",
	            "slug"  		=> "academic-manager",
	            "description"  	=> "Usuario Gestor Académico",
	        ],
	       	[
	            "name"  		=> "Inversionista",
	            "slug"  		=> "investor",
	            "description"  	=> "Usuario Inversionista",
			],
			[
	            "name"  		=> "Registrado",
	            "slug"  		=> "registrado",
	            "description"  	=> "Usuario registrado",
	        ],
	        [
	            "name"  		=> "Asesor Comercial",
	            "slug"  		=> "asesor-comercial",
	            "description"  	=> "ventas de secrea",
	        ],
	    ];
	    
	    foreach ($roles as $role)
	    {
	        Role::create($role);	        
	    }

	    $roleAdmin 				= Role::find(1);
	    $roleTeacher 			= Role::find(2);
	    $roleStudent 			= Role::find(3);
	    $roleContentManager 	= Role::find(4);
	    $roleDirector 			= Role::find(5);
	    $roleMediaManager 		= Role::find(6);
	    $roleSalesManager 		= Role::find(7);
	    $roleAcademicManager 	= Role::find(8);
		$roleInvestor 			= Role::find(9);
		$roleRegistrado			= Role::find(10);
		$roleAsesorComercial	= Role::find(11);

	    $roleAdmin
	    	->syncPermissions([1, 2, 3,4,5,6,7,8,10,13,14,15,17,18,20,23,24,26]);
	    $roleTeacher
	    	->syncPermissions([12,10,19]);
	    $roleStudent
	    	->syncPermissions([9, 10, 11,19,25]);
	    $roleContentManager
	    	->syncPermissions([16,17]);	    
	    $roleDirector
	    	->syncPermissions([1,3,5,6,7,8,13,14,17,18]);	    
	    $roleMediaManager
	    	->syncPermissions([10,23]);	    
	    $roleSalesManager
	    	->syncPermissions([1,10]);	    
	    $roleAcademicManager
	    	->syncPermissions([18,21]);	    
	    $roleInvestor
	    	->syncPermissions([18,22]);
		$roleRegistrado
			->syncPermissions([9,25]);
		$roleAsesorComercial
	    	->syncPermissions([1,3,21,28]);	 

    }
}
