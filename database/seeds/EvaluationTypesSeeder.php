<?php

use Illuminate\Database\Seeder;
use App\EvaluationType;

class EvaluationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $evaluationTypes =[
	       	[
	            "name"  		=> "Cuestionario",
			    "slug"  		=> "cuestionario",
	        ],
	       	[
	            "name"  		=> "Exámen Final",
			    "slug"  		=> "examen-final",
	        ],

	    ];
	    
	    foreach ($evaluationTypes as $evaluationType)
	    {
	        EvaluationType::create($evaluationType);
	    }
	    
    }
}