<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $permissions =[
	        
	        [
	            "name"  		=> "Gestión de Usuarios",
	/*1*/       "slug"  		=> "user-management",
	            "description"  	=> "Gestión de Usuarios",
	        ],
	       	[
	            "name"  		=> "Gestión de Roles",
	/*2*/       "slug"  		=> "rol-management",
	            "description"  	=> "Gestión de Roles",
	        ],
	       	[
	            "name"  		=> "Gestión de Convocatorias",
	/*3*/       "slug"  		=> "announcement-management",
	            "description"  	=> "Gestión de Convocatorias",
	        ],
	       	[
	            "name"  		=> "Gestión de Convenios y Empresas Asociadas",
	/*4*/       "slug"  		=> "agreement-management",
	            "description"  	=> "Gestión de Convenios y Empresas Asociadas",
	        ],	       	
	        [
	            "name"  		=> "Gestión de Niveles Académicos",
	/*5*/       "slug"  		=> "academic-level-management",
	            "description"  	=> "Gestión de Niveles Académicos",
	        ],	        
	        [
	            "name"  		=> "Gestión de Asignaturas",
	/*6*/       "slug"  		=> "subject-management",
	            "description"  	=> "Gestión de Asignaturas",
	        ],
	       	[
	            "name"  		=> "Gestión de Grados",
	/*7*/       "slug"  		=> "grade-management",
	            "description"  	=> "Gestión de Grados-Secciones",
	        ],	       	
	        [
	            "name"  		=> "Gestión de Asignaciones de Docentes",
	/*8*/       "slug"  		=> "assignment-teacher-management",
	            "description"  	=> "Gestión de Asignaciones de Docentes",
	        ],	        
	        [
	            "name"  		=> "Pre Inscripciones",
	/*9*/       "slug"  		=> "pre-enrollment-management",
	            "description"  	=> "Pre Inscripciones",
	        ],	        
	        [
	            "name"  		=> "Contacto con el Soporte",
	/*10*/      "slug"  		=> "support-message",
	            "description"  	=> "Contacto con el Soporte",
	        ],
	       	[
	            "name"  		=> "Mis Asignaturas",
	/*11*/      "slug"  		=> "my-subjects-management",
	            "description"  	=> "Mis Asignaturas",
	        ],
	       	[
	            "name"  		=> "Mis Asignnaciones",
	/*12*/      "slug"  		=> "my-assignments-management",
	            "description"  	=> "Mis Asignnaciones",
	        ],
	       	[
	            "name"  		=> "Tiempos Académicos",
	/*13*/      "slug"  		=> "academic-times-management",
	            "description"  	=> "Tiempos Académicos",
	        ],
	        [
	            "name"  		=> "Tipos de Evaluaciones",
	/*14*/      "slug"  		=> "evaluation-type-management",
	            "description"  	=> "Tipos de Evaluaciones",
	        ],
	       	[
	            "name"  		=> "Gestión de Asignaciones de Gestores de Contenido",
	/*15*/      "slug"  		=> "assignment-content-manager-management",
	            "description"  	=> "Gestión de Asignaciones de Gestores de Contenido",
	        ],
	       	[
	            "name"  		=> "Generador de Formularios",
	/*16*/      "slug"  		=> "form-generator",
	            "description"  	=> "Generador de Formularios",
	        ],
	       	[
	            "name"  		=> "Gestión de Mensajes al Soporte",
	/*17*/      "slug"  		=> "support-message-management",
	            "description"  	=> "Gestión de Mensajes al Soporte",
	        ],
	       	[
	            "name"  		=> "Gestión de Reportes",
	/*18*/      "slug"  		=> "reports-management",
	            "description"  	=> "Gestión de Reportes",
	        ],
	       	[
	            "name"  		=> "Chat",
	/*19*/      "slug"  		=> "chat",
	            "description"  	=> "Chat",
	        ],	       	
	        [
	            "name"  		=> "Gestión de Notificaciones",
	/*20*/      "slug"  		=> "notifications-management",
	            "description"  	=> "Gestión de Notificaciones",
	        ],
	       	[
	            "name"  		=> "Seguimiento documental",
	/*21*/      "slug"  		=> "documentary-management",
	            "description"  	=> "Seguimiento documental",
	        ],
	       	[
	            "name"  		=> "Demo de Asignaturas",
	/*22*/      "slug"  		=> "course-demo",
	            "description"  	=> "Demo de Asignaturas",
	        ],
	       	[
	            "name"  		=> "Gestión Multimedia",
	/*23*/      "slug"  		=> "media-management",
	            "description"  	=> "Gestión Multimedia",
	        ],
	       	[
	            "name"  		=> "Respaldos de Seguridad",
	/*24*/      "slug"  		=> "backups-management",
	            "description"  	=> "Respaldos de Seguridad",
			],
			[
	            "name"  		=> "Mis documentos",
	/*25*/      "slug"  		=> "my-documents",
	            "description"  	=> "Mis documentos",
	        ],
	       	[
	            "name"  		=> "Monitor de Evaluaciones",
	/*26*/      "slug"  		=> "evaluation-monitor",
	            "description"  	=> "Monitor de Evaluaciones",
			],
			[
	            "name"  		=> "Matricular",
	/*27*/      "slug"  		=> "enroll",
	            "description"  	=> "Realizar matriculas",
			],
			[
	            "name"  		=> "Comentarios",
	/*28*/      "slug"  		=> "comment",
	            "description"  	=> "Realizar comentarios-alumnos",
			],
			[
	            "name"  		=> "Pagos",
	/*29*/      "slug"  		=> "payment",
	            "description"  	=> "Realizar pagos",
			],
			[
	            "name"  		=> "Sms ",
	/*30*/      "slug"  		=> "messages",
	            "description"  	=> "Enviar mensajes de texto",
			],
			[
	            "name"  		=> "Gestión de sms",
	/*31*/      "slug"  		=> "messages-management",
	            "description"  	=> "Gestión de mensajes",
			],

	    ];
	    
	    foreach ($permissions as $permission)
	    {
	        Permission::create($permission);
	    }
	    
    }
}
