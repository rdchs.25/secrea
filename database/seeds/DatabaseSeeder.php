<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(EvaluationTypesSeeder::class);
        $this->call(AcademicLevelsSeeder::class);
        $this->call(AcademicTimesSeeder::class);
        $this->call(GradesSeeder::class);
    }
}
