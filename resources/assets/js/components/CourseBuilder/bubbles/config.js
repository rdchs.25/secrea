//Config File for the Form Builder Component

import MediaComponentBuilder from '../../../classes/MediaComponentBuilder'
import ComponentTypes 		from '../../../utils/ComponentTypes'

const config = new MediaComponentBuilder({
	name 		: "Burbujas",
	component 	: "bubbles",
    value 		: ["title", {"img" : { "default" : "http://via.placeholder.com/1000x300" }}, {"bubbles" : { "default" : [] }}],
    options		: { "maxBubbles" : 2 }
})


export {config}