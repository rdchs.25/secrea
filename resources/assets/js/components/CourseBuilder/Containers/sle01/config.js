/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const config = new TextComponentBuilder({
	id: "S-LE-01",
	name: "IMG IZQUIERDA",
	component: "sle01",
	portrait: "/img/contenedores/S-LE-01.jpg",
	value: [
		{ "title": "" },
		{ "body": "" },
		{ "img": "http://via.placeholder.com/412x412/c9d8d6" },
		{ "imgType": "default" },
		{ "imgName": "" },
		{ "readingTime": 20 }],
	options: { "uploadable": true },
})


export { config }