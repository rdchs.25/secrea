/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const data = [
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    }
]

const config = new TextComponentBuilder({
	id			: "S-LE-10",
    name 		: " 5 PÁRRAFO + SUBTITULOS",
	portrait 	: "/img/contenedores/S-LE-10.jpg",
	component 	: "sle10",
    value 		: [
    	{"title": ""},
        {"data" : {"default" : data }},
    	{"readingTime" : 20}

    ],
    options: { "uploadable": false },
})


export {config}