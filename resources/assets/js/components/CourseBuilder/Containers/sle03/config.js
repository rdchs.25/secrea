/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const config = new TextComponentBuilder({
	id			: "S-LE-03",
	name 		: "IMG ABAJO",
	component 	: "sle03",
	portrait 	: "/img/contenedores/S-LE-03.jpg",
    value 		: [
    	{"title": ""},
    	{"body" : ""},  
    	{"img"  : "http://via.placeholder.com/895x313"},
		{ "imgType": "default" },
    	{"readingTime" :20}],
    options: { "uploadable": true },
})


export {config}