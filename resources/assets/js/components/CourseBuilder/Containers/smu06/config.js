/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const config = new MediaComponentBuilder({
	id			: "S-MU-06",
    name 		: "CITA",
	portrait 	: "/img/contenedores/S-MU-06.jpg",
	component 	: "smu06",
    value 		: ["body", "author"],
    options		: {}
})


export {config}