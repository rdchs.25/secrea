/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const config = new MediaComponentBuilder({
	id			: "S-MU-02",
    name 		: "2 BURBUJAS",
	portrait 	: "/img/contenedores/S-MU-02.jpg",
	component 	: "smu02",
    value 		: ["title", "body", {"img"  : "http://via.placeholder.com/895x313"}, { "imgType": "default" }, {"bubbles" : [] }],
    options		: { "bubbles" : 2, "uploadable" : true }
})


export {config}