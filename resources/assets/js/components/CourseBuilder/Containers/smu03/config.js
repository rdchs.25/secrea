/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const config = new MediaComponentBuilder({
	id			: "S-MU-03",
    name 		: "3 BURBUJAS",
	portrait 	: "/img/contenedores/S-MU-03.jpg",
	component 	: "smu03",
    value 		: ["title", "body", {"img"  : "http://via.placeholder.com/895x313"}, { "imgType": "default" }, {"bubbles" : [] }],
    options		: { "bubbles" : 3, uploadable: true }
})


export {config}