/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const config = new TextComponentBuilder({
	id: "S-LE-02",
	name: "IMG DERECHA",
	component: "sle02",
	portrait: "/img/contenedores/S-LE-02.jpg",
	value: [
		{ "title": "" },
		{ "body": "" },
		{ "img": "http://via.placeholder.com/412x412" },
		{ "imgType": "default" },
		{ "readingTime": 20 }],
	options: { "uploadable": true },
})


export { config }