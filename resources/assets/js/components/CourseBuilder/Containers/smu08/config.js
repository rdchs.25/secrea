/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const data = [
    {
        "audio": [],
        "img": "http://via.placeholder.com/200x200/c9d8d6",
        "imgType": "default",
        "imgName": "",
        "name": ""
    },
    {
        "audio": [],
        "img": "http://via.placeholder.com/200x200/c9d8d6",
        "imgType": "default",
        "imgName": "",
        "name": ""
    }
]

const config = new MediaComponentBuilder({
	id			: "S-MU-08",
    name 		: "2 AUDIOS 2 IMG",
	portrait 	: "/img/contenedores/S-MU-08.jpg",
	component 	: "smu08",
    value 		: ["title", {"audios" : {"default" : data }}],
    options		: { "audios" : 2, uploadable: true }
})


export {config}