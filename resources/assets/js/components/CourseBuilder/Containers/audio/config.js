//Config File for the Form Builder Component

import MediaComponentBuilder from '../../../classes/MediaComponentBuilder'
import ComponentTypes 		from '../../../utils/ComponentTypes'

const config = new MediaComponentBuilder({
	name 		: "Audio",
	component 	: "audiocontainer",
    value 		: ["title", {"img" : "http://via.placeholder.com/1000x300"}, "bubbleA", "bubbleB"],
    options		: { "maxBubbles" : 2 }
})


export {config}