/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const config = new TextComponentBuilder({
	id			: "S-LE-05",
	name 		: "PÁRRAFO LARGO",
	portrait 	: "/img/contenedores/S-LE-05.jpg",
	component 	: "sle05",
    value 		: [
    	{"title"		: ""},
    	{"subtitle"		: ""},
    	{"body" 		: ""}, 
    	{"readingTime" : 20}
    ],
    options: { "uploadable": false },
})


export {config}