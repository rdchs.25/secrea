/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const config = new MediaComponentBuilder({
	id			: "S-MU-05",
    name 		: "VIDEO",
	portrait 	: "/img/contenedores/S-MU-05.jpg",
	component 	: "smu05",
    value 		: ["title", "body", {"video" : { "default" : "" }}],
    options		: {"uploadable": true}
})


export {config}