/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | EvalComponentBuilder
 */

import EvalComponentBuilder from '../../../../classes/EvalComponentBuilder'

const config = new EvalComponentBuilder({
	id			: "S-PR-05",
    name 		: "Pregunta Libre",
	portrait 	: "/img/contenedores/S-PR-05.jpg",
	component 	: "srp05",
    value 		: ["question", "answer"],
    options		: {}
})

export {config}