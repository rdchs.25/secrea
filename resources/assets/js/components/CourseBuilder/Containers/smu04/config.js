/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const config = new MediaComponentBuilder({
	id			: "S-MU-04",
    name 		: "4 BURBUJAS",
	portrait 	: "/img/contenedores/S-MU-04.jpg",
	component 	: "smu04",
    value 		: ["title", "body", {"img"  : "http://via.placeholder.com/895x313"}, { "imgType": "default" }, {"bubbles" : [] }],
    options		: { "bubbles" : 4, "uploadable": true }
})


export {config}