/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const data = [
    {
        "audio": [],
        "img": "http://via.placeholder.com/895x313/c9d8d6",
        "imgType": "default",
        "imgName": "",
        "name": ""
    },
]

const config = new MediaComponentBuilder({
	id			: "S-MU-09",
    name 		: "1 AUDIOS 1 IMG",
	portrait 	: "/img/contenedores/S-MU-09.jpg",
	component 	: "smu09",
    value 		: ["title", {"audios" : {"default" : data }}],
    options		: { "audios" : 1, uploadable: true }
})


export {config}