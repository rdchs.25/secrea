/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const config = new TextComponentBuilder({
	id: "S-LE-04",
	name: "SOLO TEXTO",
	portrait: "/img/contenedores/S-LE-04.jpg",
	component: "sle04",
	value: [
		{ "title": "" },
		{ "body": "" },
		{ "readingTime": 20 }
	],
	options: { "uploadable": false },
})


export { config }