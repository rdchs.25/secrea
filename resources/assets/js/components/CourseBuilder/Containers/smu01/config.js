/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | MediaComponentBuilder
 */

import MediaComponentBuilder from '../../../../classes/MediaComponentBuilder'

const config = new MediaComponentBuilder({
	id			: "S-MU-01",
    name 		: "1 BURBUJA",
	portrait 	: "/img/contenedores/S-MU-01.jpg",
	component 	: "smu01",
    value 		: ["title", "body", {"img"  : "http://via.placeholder.com/895x313"}, { "imgType": "default" }, {"bubbles" : [] }],
    options		: { "bubbles" : 1, "uploadable" : true }
})


export {config}