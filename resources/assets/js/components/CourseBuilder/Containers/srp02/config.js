/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | EvalComponentBuilder
 */

import EvalComponentBuilder from '../../../../classes/EvalComponentBuilder'

const data = [
    {
        "label": "",
        "options" : {
            "correct": true,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
]


const config = new EvalComponentBuilder({
    id			: "S-PR-02",
    name 		: "Pregunta 2",
	portrait 	: "/img/contenedores/S-PR-02.jpg",
	component 	: "srp02",
    value 		: ["question", {"img" : "http://via.placeholder.com/895x313"},  { "imgType": "default" }, {"choices" : {"default" : data } }],
    options		: { numberOfChoices : 4, uploadable: true }
})

export {config}