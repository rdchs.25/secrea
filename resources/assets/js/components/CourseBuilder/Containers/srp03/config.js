/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | EvalComponentBuilder
 */

import EvalComponentBuilder from '../../../../classes/EvalComponentBuilder'

const data = [
    {
        "label": "",
        "options" : {
            "correct": true,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
]

const config = new EvalComponentBuilder({
    id			: "S-PR-03",
    name 		: "Pregunta 2",
	portrait 	: "/img/contenedores/S-PR-03.jpg",
	component 	: "srp03",
    value 		: ["question", "body", {"choices" : {"default" : data } }],
    options		: { numberOfChoices : 4 }
})

export {config}