/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | EvalComponentBuilder
 */

import EvalComponentBuilder from '../../../../classes/EvalComponentBuilder'

const data = [
    {
        "label": "",
        "options" : {
            "correct": true,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
    {
        "label": "",
        "options" : {
            "correct": false,
            "style": "btn-default",
            "border": "",
        }
    },
]

const config = new EvalComponentBuilder({
    id			: "S-PR-04",
    name 		: "Pregunta 4",
	portrait 	: "/img/contenedores/S-PR-04.jpg",
	component 	: "srp04",
    value 		: ["question", {"choices" : {"default" : data } }],
    options		: { numberOfChoices : 4 }
})

export {config}