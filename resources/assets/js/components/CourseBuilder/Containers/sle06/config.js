/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const config = new TextComponentBuilder({
	id: "S-LE-06",
	name: "PÁRRAFO LARGO + IMAGEN",
	portrait: "/img/contenedores/S-LE-06.jpg",
	component: "sle06",
	value: [
		{ "title": "" },
		{ "subtitle": "" },
		{ "body": "" },
		{ "img": "http://via.placeholder.com/412x412" },
		{ "imgType": "default" },
		{ "readingTime": 20 }
	],
	options: { "uploadable": true},
})


export { config }