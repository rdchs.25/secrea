/**
 * Archivo de configuración del componente.
 * -----------------------------------------------------
 * Config File for the component
 *        
 * @export ES6 class | TextComponentBuilder
 */

import TextComponentBuilder from '../../../../classes/TextComponentBuilder'

const data = [
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    },
    {
        "subtitle" :"",
        "body"     :""
    }
]

const config = new TextComponentBuilder({
	id			: "S-LE-07",
    name 		: "4 PÁRRAFO + SUBTITULOS",
	portrait 	: "/img/contenedores/S-LE-07.jpg",
	component 	: "sle07",
    value 		: [
    	{"title": ""},
        {"data" : {"default" : data }},
    	{"readingTime" : 20}

    ],
    options: { "uploadable": false },
})


export {config}