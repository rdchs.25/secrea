require('./bootstrap');

import Vue from 'vue'
import VeeValidate from 'vee-validate';
import store from './store'
import Viewer from 'v-viewer'
import VModal from 'vue-js-modal'

const moment = 	require('moment');
				require('moment/locale/es');

import VueYouTubeEmbed from 'vue-youtube-embed'
Vue.use(VueYouTubeEmbed)

Vue.use(require('vue-moment'), {
    moment
})

Vue.use(Viewer, {
	defaultOptions: {
		"navbar": false, 
		"title": false, 
		"toolbar": false
	}
  })

Vue.use(VeeValidate);

Vue.use(VModal);

Vue.component('video-modal', require('./components/video-modal.vue'));
Vue.component('json', require('./components/json.vue'));
Vue.component('chat-icon', require('./components/Navbar/Chat-icon.vue'));
Vue.component('notification-icon', require('./components/Navbar/Notification-icon.vue'));

Vue.component('example', require('./components/Example.vue'));
Vue.component('drag', require('./components/Drag.vue'));
Vue.component('course-builder', require('./components/CourseBuilder/index.vue'));
Vue.component('course', require('./components/Course/index.vue'));
Vue.component('course-preview', require('./components/Course/preview.vue'));
Vue.component('chat', require('./components/Chat/Index.vue'));
Vue.component('chat-conversations', require('./components/Chat/chat-conversations.vue'));
Vue.component('chat-log', require('./components/Chat/chat-log.vue'));
Vue.component('chat-message', require('./components/Chat/chat-message.vue'));
Vue.component('chat-composer', require('./components/Chat/chat-composer.vue'));

if ( $("#navbar") .length > 0 ) {
  	const navbar = new Vue({
	    el: '#navbar',
	    store
	});
}


if ( $("#app") .length > 0 ) {
	 const app = new Vue({
	    el: '#app',
	    store
	});

}




