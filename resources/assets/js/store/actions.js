/**
 * Cómoda abstracción para ejecutar consultas y las mutaciones
 * a la data del store directamente desde
 * nuestros componentes
 * 
 * Simple abstraction to execute queries and mutations
 * to the the store data directly 
 * from our components.
 *
 * @export ES6 arrow functions 
 */

import api from '../api'
import * as types from './mutations'
import moment from 'moment';


/**
 * Acciones para los dialogos emergentes
 * 
 * Modal Popups Actions
 */


export const displayModal = ({commit}, type) => {
    commit(types.DISPLAY_MODAL, type)
}

export const closeStandarModal = ({commit}, data) => {
    commit(types.CLOSE_MODAL)
}


/**
 * Acciones para los cursos
 * 
 * Course Actions
 */

export const getCourse = ({ commit }, id) => {
    return new Promise( (resolve, reject) => {
        api.getCourse(id,  course => {
            commit(types.RECEIVE_COURSE, course)
            resolve()
        })
    })
}

export const getCoursePreview = ({ commit }, id) => {
    return new Promise( (resolve, reject) => {
        api.getCoursePreview(id,  course => {
            commit(types.RECEIVE_COURSE, course)
            resolve()
        })
    })
}

export const closeModal = ({commit}, data) => {
    commit(types.CLOSE_MODAL)
    commit(types.RESET_SESSION_SLIDE_INDEX)
    this.finishSession({commit}, data)

}

export const changeCurrentSession = ({ commit }, indexes) => {
    commit(types.CHANGE_CURRENT_COURSE_INDEXES, indexes)
    commit(types.CHANGE_CURRENT_SESSION)
}

export const incrementIndex = ({ commit }) => {    
    commit(types.INCREMENT_SESSION_SLIDE_INDEX)    
}

export const decrementIndex = ({ commit }) => {    
    commit(types.DECREMENT_SESSION_SLIDE_INDEX)    
}

export const resetIndex = ({ commit }) => {    
    commit(types.RESET_SESSION_SLIDE_INDEX)    
}

export const resetQuiz = ({ commit }) => {    
    commit(types.RESET_QUIZ)    
}

export const toggleSidebar = ({ commit }) => {    
    commit(types.TOGGLE_SIDEBAR)    
}

export const toggleFavorite = ({ commit }) => {    
    commit(types.TOGGLE_FAVORITE)    
}

export const evaluate = ({ commit }, result) => {    
    commit(types.EVALUATE_CURRENT_SLIDE, result)    
}

export const evaluateQuiz = ({ commit }) => {    
    return new Promise((resolve, reject) => {
        commit(types.EVALUATE_CURRENT_SESSION)
        resolve()
    })
}

export const toggleSessionStatus = ({ commit }) => {    
    commit(types.TOGGLE_SESSION_STATUS)    
}

export const toggleComponentStatus = ({ commit }) => {    
    commit(types.TOGGLE_COMPONENT_STATUS)    
}


export const togglePreview = ({ commit }) => {    
    commit(types.TOGGLE_PREVIEW)    
}


export const updateCourseAssigned = ({ commit }, course) => {
    return new Promise((resolve, reject) => {
        api.updateCourseAssigned(course, ({ data }) => {
            resolve(data)
        })
    })
}

export const finishSession = ({ commit }, data) => {    

    commit(types.TOGGLE_SESSION_STATUS)    
    this.nextSession({commit}, data)
}


export const nextSession = ({ commit }, data) => {    

    commit(types.COMPLETE_SESSION)

 let nextsession
 let nextweek = false
 let last_week = -1
 let last_session = -1

    data.weeks.forEach(({sessions}, week_index) => {
        if(nextweek){
            nextsession = sessions[0]
            last_session = 0
            last_week = week_index
            nextweek    = false
        }
        let index = sessions.findIndex(session => session == data.session)
          if( index > -1){
            nextsession = sessions[index + 1]
            last_week = week_index
            last_session = index + 1
            if(! sessions[index + 1]) nextweek = true
          }          
    })

     if(nextsession){
        let indexes = { week: last_week, session: last_session }
        commit(types.CHANGE_CURRENT_COURSE_INDEXES, indexes)
        commit(types.CHANGE_CURRENT_SESSION)
     } 
     else
        commit(types.COMPLETE_COURSE)        


    
}

/**
 * Acciones para el chat
 * 
 * Chat Actions
 */

export const getChats = ({ commit }) => {
    api.getChats( chats => {
        commit(types.RECEIVE_CHATS, chats)
    });
}

export const changeCurrentChat = ({ commit }, chat) => {
    api.changeCurrentChat( chat ,chat => {
        commit(types.CHANGE_CURRENT_CHAT, chat);
        
        if(chat['scroll']=="end"){
            /* hide span red (new message)*/
            $("#"+chat.user_id+"_conv_span").hide();
            /* scroll div conversation*/
            var altura = chat.messages.length * 400;
            $(".m-chat").animate({ scrollTop: altura+"px" }, 1000);
        }
        
    })    
}

export const filterChatList = ({ commit }, name) => {
    commit(types.FILTER_CHAT_LIST, name) 
}

export const restartCurrentChat = ({ commit }) => {
    commit(types.RESTART_CURRENT_CHAT) 
}

    
export const sendMessage = ({ commit }, sms ) => {    
    
    
    let msg = { 
        body : sms.body, 
        state : 0,
        receiver_user_id : sms.receiver_user_id, 
        created_at : new moment(new Date).format("YYYY-MM-DD HH mm"),
        assignment_teacher : sms.assignment_teacher
    };

    api.sendMessage(msg, message =>{
        commit(types.SEND_MESSAGE, message);
        $(".m-chat").animate({ scrollTop: $('.m-chat')[0].scrollHeight}, 500);
    });
     
}

/**
 * Acciones para el course-builder
 * 
 * Course-Builder Actions
 */

export const toggleSidebarResponsive = ({ commit }) => {
    commit(types.TOGGLE_SIDEBAR_RESPONSIVE)     
}
export const toggleResponsiveSchemes = ({ commit }) => {
    commit(types.TOGGLE_RESPONSIVE_SCHEMES)     
}
export const updateCourseBuilderList = ({ commit }, list) => {
    commit(types.UPDATE_COURSE_BUILDER_LIST, list)     
}

export const updateCourseBuilderWeek = ({ commit }, list) => {
    commit(types.UPDATE_COURSE_BUILDER_WEEK, list)
}

export const changeCurrentSessionWeek = ({ commit }, index) => {
    commit(types.CHANGE_CURRENT_SESSION_WEEK, index)     
}

export const changeTempSessionWeek = ({ commit }, index) => {
    commit(types.CHANGE_TEMP_SESSION_WEEK, index)     
}

export const changeCurrentSessionName = ({ commit }, name) => {
    commit(types.CHANGE_CURRENT_SESSION_NAME, name)     
}

export const changeCurrentSessionType = ({ commit }, type) => {
    commit(types.CHANGE_CURRENT_SESSION_TYPE, type)     
}

export const changeCurrentEvalTime = ({ commit }, time) => {
    commit(types.CHANGE_CURRENT_EVAL_TIME, time)     
}

export const changeTempSessionName = ({ commit }, name) => {
    commit(types.CHANGE_TEMP_SESSION_NAME, name)     
}

export const changeTempSessionType = ({ commit }, type) => {
    commit(types.CHANGE_TEMP_SESSION_TYPE, type)     
}

export const changeTempEvalTime = ({ commit }, time) => {
    commit(types.CHANGE_TEMP_EVAL_TIME, time)     
}

export const startDragging = ({ commit }) => {
    commit(types.START_DRAGGING)     
}

export const endDragging = ({ commit }) => {
    commit(types.END_DRAGGING)     
}

export const startBuilding = ({ commit }) => {
    commit(types.START_SESSION_BUILDER)     
}

export const endBuilding = ({ commit }) => {
    commit(types.END_SESSION_BUILDER)     
}

export const startEditing = ({ commit }) => {
    commit(types.START_EDITING)     
}

export const endEditing = ({ commit }) => {
    commit(types.END_EDITING)     
}

export const addNewSession = ({ commit }) => {
    commit(types.ADD_NEW_SESSION)     
}

export const addNewWeek = ({ commit }, name) => {    
    commit(types.ADD_NEW_WEEK, name)     
}

export const deleteCurrentSession = ({ commit }) => {
    commit(types.DELETE_CURRENT_SESSION)     
}

export const deleteEditingSession = ({ commit }) => {
    commit(types.DELETE_EDITING_SESSION)     
}

export const deleteWeek = ({ commit }, index) => {
    commit(types.DELETE_WEEK, index)     
}

export const changeCurrentBuilderSession = ({ commit }, indexes) => {
    commit(types.CHANGE_CURRENT_INDEXES, indexes)     
    commit(types.CHANGE_CURRENT_BUILDER_SESSION)     
    commit(types.CHANGE_TEMP_SESSION_NAME)     
    commit(types.CHANGE_TEMP_SESSION_TYPE)     
    commit(types.START_SESSION_BUILDER)     
    commit(types.START_EDITING)     
}

export const pushCurrentSession = ({ commit }) => {
    commit(types.PUSH_CURRENT_SESSION)     
    commit(types.DELETE_CURRENT_SESSION)     
}

export const updateCurrentSession = ({ commit }, week_index) => {
    commit(types.UPDATE_EDITING_SESSION, week_index)     
}

export const updateBuiltPercentage = ({ commit }, builtPercentage) => {
    commit(types.UPDATE_SESSION_BUILT_PERCENTAGE, builtPercentage)     
}

export const storeCourse = ({ commit }, data) => {
    return new Promise((resolve, reject) => {
        api.storeCourse(data, ({ data }) => {
            resolve(data)
        })
    })
   
}

export const storeCoursePreview = ({ commit }, data) => {
    return new Promise((resolve, reject) => {
        api.storeCoursePreview(data, ({ data }) => {
            resolve(data)
        })
    })
   
}

export const getCourseInfo = ({ commit }, id) => {
    return new Promise( (resolve, reject) => {
        api.getCourseInfo(id, info => {
            commit(types.RECEIVE_COURSE_INFO, info)
            resolve(info)
        })
    })
}

export const getUser = ({ commit }, id) => {
    return new Promise( (resolve, reject) => {
        api.getUser(info => {
            commit(types.RECEIVE_USER, info)
            resolve(info)
        })
    })
}