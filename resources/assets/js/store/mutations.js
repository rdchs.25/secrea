/**
 * Lista de mutaciones permitidas para
 * modificar la data del store
 * 
 * List of mutations allowed to
 * modify the store data.
 *
 * @export const | ES6 consts (variables)
 */

/**
 * Mutaciones para los cursos
 * 
 * Course Mutations
 */

export const RECEIVE_COURSE 				= 'RECEIVE_COURSE'
export const CHANGE_CURRENT_SESSION 		= 'CHANGE_CURRENT_SESSION'
export const INCREMENT_SESSION_SLIDE_INDEX 	= 'INCREMENT_SESSION_SLIDE_INDEX'
export const DECREMENT_SESSION_SLIDE_INDEX 	= 'DECREMENT_SESSION_SLIDE_INDEX'
export const RESET_SESSION_SLIDE_INDEX 		= 'RESET_SESSION_SLIDE_INDEX'
export const TOGGLE_FAVORITE 				= 'TOGGLE_FAVORITE'
export const TOGGLE_SIDEBAR 				= 'TOGGLE_SIDEBAR'
export const TOGGLE_SESSION_STATUS 			= 'TOGGLE_SESSION_STATUS'
export const FINISH_SESSION 				= 'FINISH_SESSION'
export const NEXT_SESSION 					= 'NEXT_SESSION'
export const COMPLETE_COURSE				= 'COMPLETE_COURSE'
export const COMPLETE_SESSION				= 'COMPLETE_SESSION'
export const EVALUATE_CURRENT_SLIDE			= 'EVALUATE_CURRENT_SLIDE'
export const EVALUATE_CURRENT_SESSION		= 'EVALUATE_CURRENT_SESSION'
export const RESET_QUIZ						= 'RESET_QUIZ'
export const DISPLAY_MODAL					= 'DISPLAY_MODAL'
export const CLOSE_MODAL					= 'CLOSE_MODAL'
export const TOGGLE_COMPONENT_STATUS		= 'TOGGLE_COMPONENT_STATUS'
export const CHANGE_LAST_WEEK_SEEN  		= 'CHANGE_LAST_WEEK_SEEN'
export const TOGGLE_PREVIEW           		= 'TOGGLE_PREVIEW'
export const CHANGE_CURRENT_COURSE_INDEXES	= 'CHANGE_CURRENT_COURSE_INDEXES'

/**
 * Mutaciones para los chats
 * 
 * Chat Mutations
 */

export const RECEIVE_CHATS	 				= 'RECEIVE_CHATS'
export const CHANGE_CURRENT_CHAT	 		= 'CHANGE_CURRENT_CHAT'
export const RESTART_CURRENT_CHAT	 		= 'RESTART_CURRENT_CHAT'
export const FILTER_CHAT_LIST	 			= 'FILTER_CHAT_LIST'
export const SEND_MESSAGE	 				= 'SEND_MESSAGE'

/**
 * Mutaciones para el course builder
 * 
 * Course Builder Mutations
 */

export const UPDATE_COURSE_BUILDER_LIST		    = 'UPDATE_COURSE_BUILDER_LIST'
export const UPDATE_COURSE_BUILDER_WEEK		    = 'UPDATE_COURSE_BUILDER_WEEK'
export const START_SESSION_BUILDER              = 'START_SESSION_BUILDER'
export const END_SESSION_BUILDER                = 'END_SESSION_BUILDER'
export const START_DRAGGING                     = 'START_DRAGGING'
export const END_DRAGGING                       = 'END_DRAGGING'
export const START_EDITING                      = 'START_EDITING'
export const END_EDITING                        = 'END_EDITING'
export const ADD_NEW_SESSION                    = 'ADD_NEW_SESSION'
export const ADD_NEW_WEEK                       = 'ADD_NEW_WEEK'
export const DELETE_WEEK                        = 'DELETE_WEEK'
export const DELETE_CURRENT_SESSION             = 'DELETE_CURRENT_SESSION'
export const DELETE_EDITING_SESSION             = 'DELETE_EDITING_SESSION'
export const UPDATE_EDITING_SESSION             = 'UPDATE_EDITING_SESSION'
export const STORE_COURSE                       = 'STORE_COURSE'
export const CHANGE_CURRENT_INDEXES             = 'CHANGE_CURRENT_INDEXES'
export const CHANGE_CURRENT_BUILDER_SESSION     = 'CHANGE_CURRENT_BUILDER_SESSION'
export const CHANGE_TEMP_SESSION_NAME           = 'CHANGE_TEMP_SESSION_NAME'
export const CHANGE_TEMP_SESSION_TYPE           = 'CHANGE_TEMP_SESSION_TYPE'
export const CHANGE_TEMP_EVAL_TIME              = 'CHANGE_TEMP_EVAL_TIME'
export const CHANGE_CURRENT_SESSION_NAME        = 'CHANGE_CURRENT_SESSION_NAME'
export const CHANGE_CURRENT_SESSION_TYPE        = 'CHANGE_CURRENT_SESSION_TYPE'
export const CHANGE_CURRENT_EVAL_TIME           = 'CHANGE_CURRENT_EVAL_TIME'
export const CHANGE_CURRENT_SESSION_WEEK        = 'CHANGE_CURRENT_SESSION_WEEK'
export const CHANGE_TEMP_SESSION_WEEK           = 'CHANGE_TEMP_SESSION_WEEK'
export const PUSH_CURRENT_SESSION               = 'PUSH_CURRENT_SESSION'
export const RECEIVE_COURSE_INFO			    = 'RECEIVE_COURSE_INFO'
export const UPDATE_SESSION_BUILT_PERCENTAGE	= 'UPDATE_SESSION_BUILT_PERCENTAGE'
export const RECEIVE_USER                   	= 'RECEIVE_USER'
export const TOGGLE_SIDEBAR_RESPONSIVE        	= 'TOGGLE_SIDEBAR_RESPONSIVE'
export const TOGGLE_RESPONSIVE_SCHEMES        	= 'TOGGLE_RESPONSIVE_SCHEMES'
