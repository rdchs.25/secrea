/**
 * Modulo del manejador de data central para el chat,
 * contiene toda la logica para su funcionamiento.
 * 
 * Vuex Module for the chat, contains all the logic
 * for its functionability
 *        
 * @export ES6 arrow functions 
 */

import {
    RECEIVE_CHATS,
    CHANGE_CURRENT_CHAT,
    RESTART_CURRENT_CHAT,
    FILTER_CHAT_LIST,
    SEND_MESSAGE,
} from '../mutations'

const _initial_chat_data = {
       /*user : { name : '', image : ''},
        teacher : { name : '', course : '', image: ''},
        messages : [],
        selected : false,
        unread : 0,
        last_message_time : '',
        last_message_content : ''*/
        user:'Selecciona ...',
        subject:''
    
}

const state = {
    all : [],
    filtered : [],
    current_chat : _initial_chat_data,
}


const mutations = {
    [RECEIVE_CHATS] (state, chats) {
        state.all = chats
        state.filtered = chats       
    },
    [CHANGE_CURRENT_CHAT] (state, chat, index) {
        state.current_chat.selected = false
        state.current_chat = chat
        state.current_chat.selected = true
        state.current_chat.unread = 0
    },
    [RESTART_CURRENT_CHAT] (state, chat) {
        state.current_chat.selected = false
        state.current_chat = _initial_chat_data
    },
    [FILTER_CHAT_LIST] (state, name) {
         state.filtered = state.all.filter( chat => chat.user.name.toUpperCase().includes(name.toUpperCase()) )       
    },
    [SEND_MESSAGE] (state, message) {
        state.current_chat.messages.push(message)
    }
}

export default {
    state,
    mutations
}