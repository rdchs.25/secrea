/**
 * Modulo del manejador de data central para los los 
 * mensajes emergentes, contiene toda la 
 * logica para su funcionamiento.
 * 
 * Vuex Module for the modal popups, contains all 
 * the logic for its functionability
 *        
 * @export ES6 arrow functions 
 */

import {
    DISPLAY_MODAL,
    CLOSE_MODAL,
} from '../mutations'

import ResultTypes from '../../utils/ResultTypes'

const state = {
    displayModal: false,
    modaltype: 'begin',
}

const mutations = {
    [DISPLAY_MODAL](state, type) {
        state.modaltype = type
        state.displayModal = true
    },
    [CLOSE_MODAL](state, type) {
        state.displayModal = false
    },
}

export default {
    state,
    mutations
}