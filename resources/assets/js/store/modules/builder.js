/**
 * Modulo del manejador de data central para el course-builder,
 * contiene toda la logica para su funcionamiento.
 * 
 * Vuex Module for the course-builder, contains all the logic
 * for its functionability
 *        
 * @export ES6 arrow functions 
 */

import Vue from 'vue'

import {
    RECEIVE_USER,
    UPDATE_COURSE_BUILDER_LIST,
    UPDATE_SESSION_BUILT_PERCENTAGE,
    UPDATE_COURSE_BUILDER_WEEK,
    START_SESSION_BUILDER,
    END_SESSION_BUILDER,
    START_DRAGGING,
    END_DRAGGING,
    START_EDITING,
    END_EDITING,
    STORE_COURSE,
    ADD_NEW_SESSION,
    ADD_NEW_WEEK,
    CHANGE_CURRENT_INDEXES,
    CHANGE_CURRENT_BUILDER_SESSION,
    CHANGE_CURRENT_SESSION_NAME,
    CHANGE_CURRENT_SESSION_TYPE,
    CHANGE_CURRENT_EVAL_TIME,
    CHANGE_TEMP_EVAL_TIME,
    CHANGE_TEMP_SESSION_NAME,
    CHANGE_TEMP_SESSION_TYPE,
    CHANGE_CURRENT_SESSION_WEEK,
    CHANGE_TEMP_SESSION_WEEK,
    DELETE_CURRENT_SESSION,
    DELETE_EDITING_SESSION,
    UPDATE_EDITING_SESSION,
    DELETE_WEEK,
    PUSH_CURRENT_SESSION,
    RECEIVE_COURSE_INFO,
    TOGGLE_SIDEBAR_RESPONSIVE,
    TOGGLE_RESPONSIVE_SCHEMES,
} from '../mutations'

let _empty = {
    "name": "",
    "type": "content",
    "evalTime": -1,
    "slide_index": 0,
    "slides": [],
    "favorite": false,
    "completed": false,
    "builtPercentage": 0,
}

const state = {
    current_user : {},
    current_session_index : -1,
    current_week_index : 0,
    temp_week_index : 0,
    current_session : {},
    session_name: '',
    session_type: 'content',
    evalTime: -1,
    isDragging : false,
    isEditing : false,
    isBuilding : false,
    sidebarResponsive : false,
    responsiveSchemes : false,
    course : {
        "weeks": [
                {
                    "name": "", 
                    "sessions": []
                }
            ],
        "completed": false
    },
    courseBuilderInfo : {name: '', grade: '', id: -1},
}

const mutations = {
    [RECEIVE_USER] (state, user) {
        state.current_user = user
    },
    [UPDATE_COURSE_BUILDER_LIST] (state, slides) {
        Vue.set(state.current_session, 'slides', slides)
    },
    [UPDATE_COURSE_BUILDER_WEEK] (state, sessions) {
        Vue.set(state.course.weeks, 'slides', sessions)
    },
    [START_DRAGGING] (state) {
        state.isDragging = true
    },
    [END_DRAGGING] (state) {
        state.isDragging = false
    },
    [START_EDITING] (state) {
        state.isEditing = true
    },
    [END_EDITING] (state) {
        state.isEditing = false
    },
    [TOGGLE_SIDEBAR_RESPONSIVE] (state) {
        if(! state.sidebarResponsive){
            state.sidebarResponsive = true
            state.responsiveSchemes = false
        }else{
            state.sidebarResponsive = false
        }
    },
    [TOGGLE_RESPONSIVE_SCHEMES] (state) {
        if(! state.responsiveSchemes){
            state.responsiveSchemes = true
            state.sidebarResponsive = false
        }else{
            state.responsiveSchemes = false
        }
    },
    [START_SESSION_BUILDER] (state) {
        state.isBuilding = true
    },
    [END_SESSION_BUILDER] (state) {
        state.isBuilding = false
    },
    [STORE_COURSE] (state, course) {
        state.course = course
    },
    [CHANGE_CURRENT_BUILDER_SESSION] (state) {        
        state.current_session = Object.assign({}, state.current_session, state.course.weeks[state.current_week_index].sessions[state.current_session_index]) 
    },
    [CHANGE_CURRENT_SESSION_NAME] (state, name) {        
        state.current_session.name = name
    },
    [CHANGE_CURRENT_SESSION_TYPE] (state, type) {   
        state.current_session.type = type
        if(type == 'content'){
            state.current_session.evalTime = -1
         }
    },
    [CHANGE_CURRENT_EVAL_TIME] (state, evalTime) {        
        state.current_session.evalTime = Number.parseInt(evalTime)
    },
    [CHANGE_TEMP_SESSION_NAME] (state) {        
        state.session_name = state.current_session.name
    },
    [CHANGE_TEMP_EVAL_TIME] (state) {        
        state.evalTime = state.current_session.evalTime
    },
    [CHANGE_TEMP_SESSION_TYPE] (state) {        
        state.session_type = state.current_session.type
        if(state.current_session.type == 'session'){
            state.evalTime = -1
         }
    },
    [CHANGE_CURRENT_SESSION_WEEK] (state, index) {    
        state.current_week_index = index
    },
    [CHANGE_TEMP_SESSION_WEEK] (state, index) {    
        state.temp_week_index = index
    },
    [CHANGE_CURRENT_INDEXES] (state, indexes) {    
        state.current_week_index = indexes.week
        state.current_session_index = indexes.session
        state.temp_week_index = indexes.week
    },
    [RECEIVE_COURSE_INFO] (state, course) {
        state.courseBuilderInfo.name  = course.title
        state.courseBuilderInfo.grade = course.grade
        state.courseBuilderInfo.id = course.id
        state.course = course.content

    },
    [ADD_NEW_SESSION] (state) {
        state.current_session_index = state.course.weeks[state.current_week_index].sessions.length
        state.current_session = Object.assign({}, state.current_session, _empty)   
    },
    [ADD_NEW_WEEK] (state, name) {
        state.course.weeks.push({
            "name" : name,
            "sessions" : []
        })
    },
    [DELETE_CURRENT_SESSION] (state) {
        state.current_session_index = -1
        state.current_week_index = 0
        state.current_session = {}
    },
    [DELETE_EDITING_SESSION] (state) {
        state.course.weeks[state.current_week_index].sessions.splice(state.current_session_index, 1)        
    },
    [DELETE_WEEK] (state, index) {
        state.current_week_index = 0
        state.course.weeks.splice(index, 1)        
    },
    [PUSH_CURRENT_SESSION] (state) {
        state.course.weeks[state.current_week_index].sessions.push(state.current_session)       
    },
    [UPDATE_SESSION_BUILT_PERCENTAGE] (state, builtPercentage) {
        Vue.set(state.current_session, 'builtPercentage', builtPercentage)
    },
    [UPDATE_EDITING_SESSION] (state) {
        if(state.temp_week_index == state.current_week_index){
            Vue.set(state.course.weeks[state.temp_week_index].sessions, state.current_session_index, state.current_session)
        }
        else{
            state.course.weeks[state.current_week_index].sessions.splice(state.current_session_index, 1)        
            state.course.weeks[state.temp_week_index].sessions.push(state.current_session)   
        }
        state.current_session_index = -1
        state.current_session = {}
        state.session_name = ''
        state.session_type = 'content'
    },
}

export default {
    state,
    mutations
}