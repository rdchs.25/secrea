/**
 * Modulo del manejador de data central para los cursos,
 * contiene toda la logica para su funcionamiento.
 * 
 * Vuex Module for the courses, contains all the logic
 * for its functionability
 *        
 * @export ES6 arrow functions 
 */

import {
    RECEIVE_COURSE,
    CHANGE_CURRENT_SESSION,
    INCREMENT_SESSION_SLIDE_INDEX,
    DECREMENT_SESSION_SLIDE_INDEX,
    RESET_SESSION_SLIDE_INDEX,
    TOGGLE_FAVORITE,
    TOGGLE_SIDEBAR,
    TOGGLE_SESSION_STATUS,
    FINISH_SESSION,
    NEXT_SESSION,
    COMPLETE_COURSE,
    COMPLETE_SESSION,
    EVALUATE_CURRENT_SLIDE,
    EVALUATE_CURRENT_SESSION,
    RESET_QUIZ,
    DISPLAY_MODAL,
    CLOSE_MODAL,
    TOGGLE_COMPONENT_STATUS,
    TOGGLE_PREVIEW,
    CHANGE_CURRENT_COURSE_INDEXES,
} from '../mutations'

import ResultTypes from '../../utils/ResultTypes'

const _initial_course_data = {
    grade       : '',
    title       : '',
    content     : {
        weeks       : [],
        completed   : false,
    }
}

const _initial_session_data = {
        name        : '',
        slide_index : 0,
        slides      : [],
        favorite    : false,
        completed   : false
}

const state = {
    data                        : _initial_course_data,
    current_session             : _initial_session_data,
    current_empty_session       : _initial_session_data,
    current_course              : { week : '', name : '' },
    current_quiz_result         : 0,
    hasStartedSession           : false,
    hasCompletedSession         : false,
    sidebar                     : false,
    displayModal                : true,    
    modaltype                   : 'begin',
    isPreview                   : false,
    current_session_index : -1,
    current_week_index : 0,
}


const mutations = {
    [CHANGE_CURRENT_COURSE_INDEXES] (state, indexes) {
        state.current_week_index = indexes.week
        state.current_session_index = indexes.session
    },
    [RECEIVE_COURSE] (state, course) {
        state.data = course
    },
    [CHANGE_CURRENT_SESSION] (state) {
        state.displayModal = true
        state.modaltype = 'begin'
        let session = state.data.content.weeks[state.current_week_index].sessions[state.current_session_index]
        let week = state.data.content.weeks.findIndex( week => week.sessions.includes(session) ) + 1
        state.current_course        = { week : `Semana ${week}`, sessionName : session.name}
        state.current_session       = session
        state.current_empty_session = Object.assign({}, session)
    },
    [INCREMENT_SESSION_SLIDE_INDEX] (state, session_length) {
        state.current_session.slide_index++
    },
    [DECREMENT_SESSION_SLIDE_INDEX] (state, session_length) {
        state.current_session.slide_index--
    },
    [RESET_SESSION_SLIDE_INDEX] (state) {
        state.current_session.slide_index = 0
    },
    [RESET_QUIZ] (state) {
        state.current_session = state.current_empty_session
    },
    [DISPLAY_MODAL] (state, type) {
        state.modaltype = type
        state.displayModal = true
    },
    [CLOSE_MODAL] (state, type) {
        state.displayModal = false
    },
    [TOGGLE_FAVORITE] (state) {
        state.current_session.favorite = ! state.current_session.favorite
    },
    [TOGGLE_SIDEBAR] (state) {
        state.sidebar = ! state.sidebar
    },
    [TOGGLE_SESSION_STATUS] (state) {
        state.hasStartedSession = ! state.hasStartedSession
    },
    [TOGGLE_COMPONENT_STATUS] (state) {
        state.hasCompletedSession = ! state.hasCompletedSession
    },
    [TOGGLE_PREVIEW] (state) {
        state.isPreview = ! state.isPreview
    },
    [COMPLETE_COURSE] (state) {
        state.data.content.completed = true
        window.alert(`Curso de ${state.data.title} completo`)
    },
    [COMPLETE_SESSION] (state) {
        state.current_session.completed = true
        state.current_session = _initial_session_data        
    },
    [EVALUATE_CURRENT_SLIDE] (state, result) {
        let index = state.current_session.slide_index
        state.current_session.slides[index].result = result
    },
    [EVALUATE_CURRENT_SESSION] (state) {
        let count  = 0;
        let slides = state.current_session.slides
            slides.forEach(( slide )=> {
                if(slide.result == ResultTypes.PASSED) count++
            })
        state.current_quiz_result = count / slides.length * 100    
    },
}

export default {
    state,
    mutations
}