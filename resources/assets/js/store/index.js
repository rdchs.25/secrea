/**
 * Punto de entrada para el store de data central
 * de nuestra apliación.
 * 
 * Entry point for the centralized state store
 * of our app.
 */ 

import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import course from './modules/course'
import chats from './modules/chats'
import modal from './modules/modal'
import builder from './modules/builder'


Vue.use(Vuex)

export default new Vuex.Store({
	actions,
	getters,
    modules: {
        course,
        chats,
        builder,
        modal
    }
})