/**
 * Cómoda abstraccion para acceder a la data del store
 * directamente desde nuestros componentes
 * 
 * Simple abstraction to access all the store data
 * directly from our components.
 *
 * @export ES6 arrow functions 
 */

import ResultTypes	from '../utils/ResultTypes'

/**
 * Accesores para los dialogos emergentes
 * 
 * Modal Popups Getters
 */


export const modal = state => state.modal.displayModal
export const modaltype = state => state.modal.modaltype

/**
 * Accesores para los cursos
 * 
 * Course Getters
 */

export const session = state => state.course.current_session
export const hasStartedSession = state => state.course.hasStartedSession
export const hasCompletedSession = state => state.course.hasCompletedSession
export const courseInfo = state => state.course.current_course
export const course = state => state.course.data
export const sidebar = state => state.course.sidebar
export const isPreview = state => state.course.isPreview
export const quizResult = state => state.course.current_quiz_result
export const approveCurrentTest = state => state.course.quizResult > state.course.current_session.minimum_approval
export const pass = state => {

    let index 	= state.course.current_session.slide_index        
    let session = state.course.current_session
    return session.name == '' ? false :  session.slides[index].result != ResultTypes.PENDING
} 
export const sessionsCompleted = state => {
	return state.course.data.content.weeks.filter( ({sessions}) => sessions.filter( session => session.completed ))
}

/**
 * Accesores para el chat
 * 
 * Chat Getters
 */

export const chats = state => state.chats.filtered
export const chat = state => state.chats.current_chat
/**
 * Accesores para el course-builder
 * 
 * Course-Builder Getters
 */

export const sidebarResponsive = state => state.builder.sidebarResponsive
export const responsiveSchemes = state => state.builder.responsiveSchemes
export const current_user = state => state.builder.current_user
export const courseBuilderList = state => state.builder.current_session_index != -1 ? state.builder.current_session.slides : []
export const courseBuilderWeeks = state => state.builder.course.weeks ? state.builder.course.weeks : []
export const currentSessionBuilderWeek = state => state.builder.current_week_index
export const tempSessionBuilderWeek = state => state.builder.temp_week_index
export const isDragging = state => state.builder.isDragging
export const hasStartedSessionBuilder = state => state.builder.isBuilding
export const isEditing = state => state.builder.isEditing
export const tempSessionName = state => state.builder.current_session.name ? state.builder.current_session.name : ''
export const tempSessionType = state => state.builder.current_session.type ? state.builder.current_session.type : ''
export const tempEvalTime = state => state.builder.evalTime ? state.builder.evalTime : -1
export const courseBuilderInfo = state => state.builder.courseBuilderInfo
export const courseBuilder = state => state.builder.course
export const currentSessionIndexes = ({builder}) => {
	 return { 
		 "session" : builder.current_session_index,
		 "week" : builder.current_week_index
		}
}
export const currentCourseSessionIndexes = ({course}) => {
	 return { 
		 "session" : course.current_session_index,
		 "week" : course.current_week_index
		}
}
export const uploadableComponentsInCurrentSession = state => {
		if(state.builder.current_session_index != -1 ){
			const finalArray = []
			if(state.builder.current_session.slides){
				state.builder.current_session.slides.forEach( component => {
					if(component.options.uploadable){
						let count = component.options.audios ? component.options.audios : 1
						for(let i = 0; i < count; i++){
							finalArray.push(component)
						}
					}
				})
			}
			return finalArray
		}
		return []
}
export const uploadableComponentsUntilCurrentSession = state => {
		if(state.builder.current_session_index != -1 ){
			const finalArray = []
			let count = 0
			let i = 0
			state.builder.course.weeks[state.builder.temp_week_index].sessions.forEach( session =>{
				session.slides.forEach( component => {
					if(component.options.uploadable){
						count = component.options.audios ? component.options.audios : 1
						for(i = 0; i < count; i++){
							finalArray.push(component)
						}
					}
				})
			})

			if(state.builder.current_session.slides){
				state.builder.current_session.slides.forEach( component => {
					if(component.options.uploadable){
						let count = component.options.audios ? component.options.audios : 1
						for(let i = 0; i < count; i++){
							finalArray.push(component)
						}
					}
				})
			}
			return finalArray
		}
		return []
}
