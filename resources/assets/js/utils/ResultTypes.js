/**
 * Exporta un objeto con los tipos de resultados usados
 * para evaluar los componenetes cursos.
 *                                   
 * Exports an object with the result types used
 * for evaluate the component courses.
 *
 * @export {} : JS Object
 */

export default Object.freeze({
    PASSED      : "PASSED",    
    FAILED      : "FAILED",    
    PENDING     : "PENDING",    
});