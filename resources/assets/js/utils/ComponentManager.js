/**
 * Importa y organiza los objetos de configuracion
 * de los componentes para el 
 * Generador de Cursos
 *                                   
 * Imports and organizes the config objects from
 * the components for the CourseBuilder
 *
 */


/** 
 * Contenedores de Lectura
 *                                   
 * Lecture Containers
 */

import {config as sle01} from '../components/CourseBuilder/Containers/sle01/config'
import {config as sle02} from '../components/CourseBuilder/Containers/sle02/config'
import {config as sle03} from '../components/CourseBuilder/Containers/sle03/config'
import {config as sle04} from '../components/CourseBuilder/Containers/sle04/config'
import {config as sle05} from '../components/CourseBuilder/Containers/sle05/config'
import {config as sle06} from '../components/CourseBuilder/Containers/sle06/config'
import {config as sle07} from '../components/CourseBuilder/Containers/sle07/config'
import {config as sle08} from '../components/CourseBuilder/Containers/sle08/config'
import {config as sle09} from '../components/CourseBuilder/Containers/sle09/config'
import {config as sle10} from '../components/CourseBuilder/Containers/sle10/config'

/** 
 * Contenedores Multimedia
 *                                   
 * Multimedia Containers
 */

import {config as smu01} from '../components/CourseBuilder/Containers/smu01/config'
import {config as smu02} from '../components/CourseBuilder/Containers/smu02/config'
import {config as smu03} from '../components/CourseBuilder/Containers/smu03/config'
import {config as smu04} from '../components/CourseBuilder/Containers/smu04/config'
import {config as smu05} from '../components/CourseBuilder/Containers/smu05/config'
import {config as smu06} from '../components/CourseBuilder/Containers/smu06/config'
import {config as smu07} from '../components/CourseBuilder/Containers/smu07/config'
import {config as smu08} from '../components/CourseBuilder/Containers/smu08/config'
import {config as smu09} from '../components/CourseBuilder/Containers/smu09/config'

/** 
 * Contenedores Evaluativos
 *                                   
 * Eval Containers
 */

import {config as srp01} from '../components/CourseBuilder/Containers/srp01/config'
import {config as srp02} from '../components/CourseBuilder/Containers/srp02/config'
import {config as srp03} from '../components/CourseBuilder/Containers/srp03/config'
import {config as srp04} from '../components/CourseBuilder/Containers/srp04/config'
import {config as srp05} from '../components/CourseBuilder/Containers/srp05/config'


/** 
 * Exporta la data en el formato adecuado
 *                                   
 * Exports the data in the correct format
 * 
 * @export [] : JS Array
 */


export default [
	sle01.getComponentInfo(), sle02.getComponentInfo(), sle03.getComponentInfo(), sle04.getComponentInfo(),	sle05.getComponentInfo(),
	sle06.getComponentInfo(), sle07.getComponentInfo(), sle08.getComponentInfo(), sle09.getComponentInfo(), sle10.getComponentInfo(),

	smu01.getComponentInfo(), smu02.getComponentInfo(), smu03.getComponentInfo(), smu04.getComponentInfo(),	smu05.getComponentInfo(),
	smu06.getComponentInfo(), smu07.getComponentInfo(), smu08.getComponentInfo(), smu09.getComponentInfo(),

	srp01.getComponentInfo(), srp02.getComponentInfo(), srp03.getComponentInfo(), srp04.getComponentInfo(),  srp05.getComponentInfo(),
]
