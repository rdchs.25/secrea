/**
 * Exporta un objeto con los tipos de componentes
 * usados en los cursos.
 * ------------------------------------------------
 * Exports an object with the component types
 * used in the courses
 *
 * @export {} : JS Object
 */

export default Object.freeze({
    TEXT 	: "TEXT",    
    MEDIA 	: "MEDIA",    
    EVAL 	: "EVAL",    
});