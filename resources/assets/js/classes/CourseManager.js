/* INCOMPLETE */

/**
 * Maneja las responsabilidades del curso
 *                                   
 * Manage all the course responsabilities
 */

export default class CourseManager{
	constructor( {getters, dispatch} ){

		/**
		 * Constructor de la clase, usando los metodos
		 * de acceso y mutacion del store.
		 *                                   
		 * Class Constructor, using the access and
		 * mutator methods from the store
		 *
		 * @var getters		| {} 		: JS Objects
		 * @var dispatch	| function	: JS Function
		 */

		this.getters  = getters
		this.dispatch = dispatch
	}

	static changeTab(){
		
		/**
		 * Cambia la pestaña de la sesión y la termina si es necesario.
		 *                                         
		 * Changes the session tab and finished if needed.
		 */

		if (this.getters.isLastStep)  
			this.finishSession()
		else 
			this.incrementIndex()
	}

	static finishSession(){

		/**
		 * Finaliza la sesion y ejecuta los procesos necesarios 
		 * dependiendo el tipo de session
		 *                                              
		 * Finish the session and executes the needed process
		 * based upon the session type
		 */

		if( this.getters.session.type == 'quiz')
		{
			this.dispatch('evaluateQuiz')

			this.displayQuizResult()

			this.dispatch('finishSession',
			{
				weeks 	: this.course.weeks,
				session : this.session
			})
		}
		else
		{

			this.dispatch('resetIndex')
			this.dispatch('finishSession',
			{
				weeks 	: this.course.weeks,
				session : this.session
			})      		 				
		}
	}


	displayQuizResult(){

		/**
		 * Muestra el resultado de la evaluación
		 *                                  
		 * Displays the evaluation result.
		 */

			if(! this.getters.approveCurrentTest)
			{
				alert(`Prueba fallida con: ${this.getters.quizResult} pts. Minimo aprovatorio ${this.getters.session.minimum_approval}`)
				//this.resetQuiz()
			}
			else
			{
				alert(`Pasaste con: ${this.getters.quizResult} pts. Minimo aprovatorio ${this.getters.session.minimum_approval}`)
			}
	}

	incrementIndex(){

		/**
		 * Aumenta en 1 el index de la sesión, cambiando 
		 * el container actual del curso
		 *                                            
		 * Increments by 1 the session index, changing 
		 * the actual course container
		 */

		this.store.dispatch('incrementIndex')
	}


		 methods: {
		    ...mapActions([
		      'resetIndex',
		      'toggleFavorite',
		      'toggleSessionStatus',
		      'finishSession',
		      'evaluateQuiz',
		      'resetQuiz',
		    ]),


 		    complete(){
	 			if(this.session.type == 'quiz'){
	 				this.evaluateQuiz()
	 				if(this.quizResult < this.session.minimum_approval){
	 					alert(`Prueba fallida con: ${this.quizResult} pts. Minimo aprovatorio ${this.session.minimum_approval}`)
	 					//this.resetQuiz()
	 				}else{
	 					alert(`Pasaste con: ${this.quizResult} pts. Minimo aprovatorio ${this.session.minimum_approval}`)
	 				}
		 		      	this.finishSession({
		 		      			weeks 	: this.course.weeks,
		 		      			session : this.session
		 		      		}) 		      	
	 			}
	 			else{
 		      	this.resetIndex()
 		      	this.finishSession({
 		      			weeks 	: this.course.weeks,
 		      			session : this.session
 		      			}) 		      		 				
	 				}
		     	}	
	   		}

}