/**
 * Construye la estructura de la data necesaria
 * para los componentes.
 *                                   
 * Builds the data structure needed for components-
 *
 */



import ResultTypes from '../utils/ResultTypes'
import Vue from 'vue'


export default class ComponentBuilder {
	constructor({ id, name, portrait, component, type, value, options = {} }) {
		this.id = id
		this.name = name
		this.portrait = portrait
		this.component = component
		this.type = type
		this.value = {}
		this.props = { value: {} }
		this.options = options
		this.initValues(value)
		this.getComponentInfo = this._getComponentInfo
		this.completed = false
		this.result = ResultTypes.PENDING

	}

	/**
	 * Devuelve la estructura usada por el generador
	 * de cursos al leer los componentes.
	 *                                   
	 * Returns the structure used for the CourseBuilder 
	 * at the moment of reading the components.
	 *
	 * @return {} : JS Object
	 */

	_getComponentInfo() {
		return Object.assign({},
			{
				"id": this.id,
				"name": this.name,
				"portrait": this.portrait,
				"component": this.component,
				"type": this.type,
				"value": this.value,
				"result": this.result,
				"options": this.options
			})
	}


	/**
	 * Inicializa los valores usados por el generador
	 * de cursos y los props usados para crear
	 * cada componente.
	 *                                   
	 * Inits the values used for the CourseBuilder
	 * and the props used for build every
	 * component.
	 *
	 * @var {} : JS Object
	 */


	initValues(args) {

		/**
		 *  Dependiendo del valor que reciba, construira
		 *  el objeto a ser exportado con el
		 *  formato correcto.
		 * 
		 *  Depending of the value they recieve they will
		 *  construct the exporting objects in 
		 *  the correct format.* 
		 */

		this.value['notes'] = ""
		this.props.value['notes'] = ""
		for (let e of args) {

			if (typeof (e) == "string") {
				this.value[e] = ""
				this.props.value[e] = ""
			}
			else if (typeof (e) == "object") {
				for (let pn in e) {
					if (typeof (e[pn]) == "string" || typeof (e[pn]) == "number") {
						this.value[pn] = e[pn]
						this.props.value[pn] = { "default": e[pn] }
					}
					else if (Array.isArray(e[pn])) {
						Vue.set(this.value, pn, e[pn])
						Vue.set(this.props.value, pn, Object.assign({}, { "default": e[pn] }))
					}
					else if (typeof (e[pn]) == "object") {
						for (let ipn in e[pn]) {
							this.props.value[pn] = { [ipn]: e[pn][ipn] }
							this.value[pn] = e[pn][ipn]
						}
					}
				}
			}
		}
	}
}