/**
 * Construye la estructura de la data necesaria para los componentes
 * de tipo TEXT, extiende y hereda las funcionabilidades
 * de la clase ComponentBuilder
 *                                    
 * Builds the data structure needed for TEXT components,
 * extends and inherits the ComponentBuilder class
 * functionabilities
 */

import ComponentBuilder from './ComponentBuilder'
import ComponentTypes 	from '../utils/ComponentTypes'

export default class TextComponentBuilder extends ComponentBuilder{
	constructor({  id, name, portrait, component, value, options = {} }){
		super({ 
			"id"		: id,
			"name"		: name,
			"component"	: component, 
			"portrait"	: portrait, 
			"type"		: ComponentTypes.TEXT, 
			"value"		: value, 
			"options"	: options
		})
	}
}