/**
 * Construye la estructura de la data necesaria para los componentes
 * de tipo EVAL, extiende y hereda las funcionabilidades
 * de la clase ComponentBuilder
 * ------------------------------------------------
 * Builds the data structure needed for EVAL components,
 * extends and inherits the ComponentBuilder class
 * functionabilities
 */

import ComponentBuilder from './ComponentBuilder'
import ComponentTypes 	from '../utils/ComponentTypes'

export default class EvalComponentBuilder extends ComponentBuilder{
	constructor({ id, name, portrait, component, value, options = {}}){
		super({ 
			"id" 		: id,
			"name" 		: name,
			"portrait"	: portrait,
			"component"	: component, 
			"type" 		: ComponentTypes.EVAL, 
			"value"		: value, 
			"options" 	: options
		})

	}
}