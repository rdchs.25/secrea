/**
 * Maneja todas las consultas a la API REST y retorna 
 * la data usando un callback.
 *
 * Manage all the queries to the API REST  and returns
 * the data using a callback.
 */

import ResultTypes from '../utils/ResultTypes'


const _empty = {
	"grade": "",
	"title": "",
	"weeks": [
		[
			{
				"name": "",
				"type": "",
				"slide_index": 0,
				"slides": [
					{
						"name": "",
						"component": "",
						"miniatura": "",
						"category": "",
						"value": {},
						"icon": "",
						"result": ResultTypes.PENDING
					},
				],
				"favorite": false,
				"completed": false
			},
		],
	],
	"completed": false
}


const courses = {

	matematica:
	{
		"grade": "1ro Secundaria",
		"title": "Matemática",
		"weeks": [
			[
				{
					"name": "Conjuntos",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{  //hecho
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Empecemos",
								"body": "Hablemos de conjuntos",
								"video": "VWs7lhlAYK8"
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "img-abajo ",
							"component": "sle03",
							"miniatura": "sle03.jpg",
							"category": "lectura",
							"value": {
								"title": "Conjuntos",
								"body": "Un conjunto es la colección o agrupación de objetos con características en común, por ejemplo, cuando alguien colecciona algún objeto, siempre los colecciona bajo un parámetro, es decir una característica establecida. Un requisito indispensable para que a una agrupación de objetos se llame conjunto es que el parámetro con el que se agrupan se pueda determinar.",
								"img": "/img/cursos/matematica/SEM-01-MAT-02.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Característica demostrable ",
								"body": "Determinación de los elementos del conjunto.",
								"video": "iggK1nAhMAQ"
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "img-abajo ",
							"component": "sle03",
							"miniatura": "sle03.jpg",
							"category": "lectura",
							"value": {
								"title": "Representación gráfica",
								"body": "Para representar los conjuntos gráficamente, se pueden usar los diagramas de Venn, estos consisten en representar los conjuntos mediante círculos y colocar en su interior los elementos que lo conforman, como lo hicimos con los conjuntos de frutas que vimos en el video.",
								"img": "/img/cursos/matematica/SEM-01-MAT-04.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "3audios3img",
							"component": "smu07",
							"miniatura": "smu07.jpg",
							"category": "multimedia",
							"value": {
								"title": "Veamos algunos ejemplos",
								"readingTime": 10,
								"audios": [
									{
										"audio": ['/audio/cursos/matematica/SEM-01-MAT-05.mp3'],
										"img": "/img/cursos/matematica/SEM-01-MAT-05.jpg",
										"name": "Por comprensión"
									},
									{
										"audio": ['/audio/cursos/matematica/SEM-01-MAT-06.mp3'],
										"img": "/img/cursos/matematica/SEM-01-MAT-06.jpg",
										"name": "Por extensión"
									},
									{
										"audio": ['/audio/cursos/matematica/SEM-01-MAT-07.mp3'],
										"img": "/img/cursos/matematica/SEM-01-MAT-07.jpg",
										"name": "Representación gráfica "
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "2-parrafo-subtitulos",
							"component": "sle09",
							"miniatura": "sle09.jpg",
							"category": "lectura",
							"value": {
								"title": "Relación entre elementos de conjuntos",
								"data": [
									{
										"subtitle": "Pertenencia y No pertenencia",
										"body": "Cuando un elemento pertenece a un conjunto, se escribe: Є Cuando un elemento no pertenece a un conjunto, se escribe:  A = {1,2,3,4,5,6}  1 ϵ A, 2 ϵ A, 6 ϵ A. 0 ∉ A, 7 ∉ A "
									},
									{
										"subtitle": "Inclusión de conjuntos",
										"body": "Se da entre conjuntos y subconjuntos. Se dice que está incluido (⊂) cuando un subconjunto está dentro de un conjunto mayor. Por otro lado se dice que un subconjunto no está incluido en un conjunto mayor cuando por lo menos uno de sus elementos no pertenece a este."
									}

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "img-izquierda",
							"component": "sle01",
							"miniatura": "sle01.jpg",
							"category": "lectura",
							"value": {
								"title": "Ejemplo de pertenencia y No pertenencia",
								"body": "Entonces: A ⊂ B ⇒ Se lee: 'A está incluido en B. B ⊄ A ⇒ Se lee: 'B no está incluido en A.  A ⊄ C ⇒ Se lee: 'A no está incluido en C. ' ",
								"img": "/img/cursos/matematica/SEM-01-MAT-08.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
					],
					"favorite": false,
					"completed": false
				},
			],
		],
		"completed": false,
	},
	comunicacion:
	{
		"grade": "1ro Secundaria",
		"title": "Comunicación",
		"weeks": [
			[
				{
					"name": "Lenguaje",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Bienvenido",
								"body": "Comencemos imaginando",
								"video": "fiD5-Mtn_6g"
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "img-abajo ",
							"component": "sle03",
							"miniatura": "sle03.jpg",
							"category": "lectura",
							"value": {
								"title": "Lenguaje",
								"body": "Se define el lenguaje como un rasgo único de la humanidad, una facultad con la que nacemos y que nos permite conocer y usar una o más lenguas. Es decir, todos los seres humanos contamos con la facultad general del lenguaje, pero distintas comunidades han desarrollado distintas lenguas.",
								"img": "/img/cursos/comunicacion/SEM-01-COM-02.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Manifestaciones del lenguaje",
								"body": "Veamos de qué forma se presenta el lenguaje.",
								"video": "9uY4iT64te8"
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
				{
					"name": "Lengua",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Motivación",
								"body": "Sigamos utilizando la imaginación.",
								"video": "-whA3OJwUnE",
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "solo-texto ",
							"component": "sle04",
							"miniatura": "sle04.jpg",
							"category": "lectura",
							"value": {
								"title": "Lengua",
								"body": "Se llama lengua al conjunto o sistema de formas o signos orales y escritos que sirven para la comunicación entre las personas de una misma comunidad lingüística. La lengua es un inventario que los hablantes emplean a través del habla pero que no pueden modificar. Por ejemplo, el español es la lengua hablada por más de 500 millones de personas en todo el mundo.",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "4-parrafo-subtitulos",
							"component": "sle07",
							"miniatura": "sle07.jpg",
							"category": "lectura",
							"value": {
								"title": "Características de la lengua",
								"data": [
									{
										"subtitle": "Social",
										"body": "Porque se manifiesta es una comunidad."
									},
									{
										"subtitle": "Abstracta",
										"body": "Porque existe en la mente de cada individuo."
									},
									{
										"subtitle": "Lineal",
										"body": "Porque los elementos lingüísticos se suceden uno tras otro, formando una cadena."
									},
									{
										"subtitle": "Convencional",
										"body": "Porque es la sociedad quien decide el significado de cada significante."
									},

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
				{
					"name": "El Habla",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{
							"name": "img-abajo ",
							"component": "sle03",
							"miniatura": "sle03.jpg",
							"category": "lectura",
							"value": {
								"title": "El habla",
								"body": "Es la expresión individual de la lengua. Es decir, es la forma particular en que cada persona se comunica. Viene a ser la concretización de la lengua.",
								"img": "/img/cursos/comunicacion/SEM-01-COM-05.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "citaja1",
							"component": "smu06",
							"miniatura": "smu06.jpg",
							"category": "multimedia",
							"value": {
								"body": "Un pensamiento o sentimiento puede expresarse de diferentes maneras : deseo satisfacer mi apetito, Desearía algo para calmar mi hambre, Me gustaría comer porque tengo hambre.",
								"author": "Ejemplo",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "5-parrafo-subtitulos",
							"component": "sle10",
							"miniatura": "sle10.jpg",
							"category": "lectura",
							"value": {
								"title": "Tipo de Habla",

								"data": [
									{
										"subtitle": "ARGOT",
										"body": "Lenguaje formal de los profesionistas en sus diferentes especialidades."
									},
									{
										"subtitle": "JERGA",
										"body": "Lenguaje informal de las personas que desempeñan determinados oficios o actividades."
									},
									{
										"subtitle": "CALÓ",
										"body": "Lenguaje popular que se basa en modismos. Se usa principalmente en los estratos sociales más bajos."
									},
									{
										"subtitle": "DIALECTO",
										"body": "Constituye una variedad regional de la lengua."
									},
									{
										"subtitle": "NORMA",
										"body": "Es el conjunto de reglas establecidas por el grupo social de lo que se considera como la expresión más adecuada."
									},

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
				{
					"name": "Dialecto",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{
							"name": "img-abajo",
							"component": "sle03",
							"miniatura": "sle03.jpg",
							"category": "lectura",
							"value": {
								"title": "Dialecto",
								"body": "Los dialectos son las variantes o modalidades regionales de una lengua, puede ser: Interno y Externo",
								"img": "/img/cursos/comunicacion/SEM-01-COM-06.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "3-parrafo-subtitulos",
							"component": "sle08",
							"miniatura": "sle08.jpg",
							"category": "lectura",
							"value": {
								"title": "Características",
								"data": [
									{
										"subtitle": "Escasa nivelación",
										"body": "Los rasgos dialectales no son unitarios, no están normalizados."
									},
									{
										"subtitle": "Sin tradición literaria",
										"body": "Los hablantes, al escribir, utilizan la lengua general."
									},
									{
										"subtitle": "Subordinación a otra lengua",
										"body": "El hablante de un dialecto tiene conciencia de pertenecer a otra lengua de rango superior."
									}

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "2-parrafo-subtitulos",
							"component": "sle09",
							"miniatura": "sle09.jpg",
							"category": "lectura",
							"value": {
								"title": "Ejemplos",
								"data": [
									{
										"subtitle": "Interno",
										"body": "Castellano capitalino: “Tengo dolor de espalda”. Castellano andino: “De mi espalda dolor sintiendo estoy, pues”"
									},
									{
										"subtitle": "Externo",
										"body": "Castellano de Perú: Muchacho. Castellano de México: Chamaco. Castellano de Argentina: Pibe. Castellano de Chile: Cabro. Castellano de Venezuela: Chamo "
									}

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
				{
					"name": "Cuestionario",
					"type": "quiz",
					"minimum_approval": 70,
					"slide_index": 0,
					"slides": [
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Es el lenguaje popular que se basa en modismos:",
								"choices": [
									{
										"label": "Caló",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Argot",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "dialectos",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Jerga",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Es un rasgo único de la humanidad con lo que nacemos",
								"choices": [
									{
										"label": "Lenguaje",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Lengua",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Habla",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Dialecto",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta2",
							"component": "srp02",
							"miniatura": "srp02.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Es el conjunto de formas o signos orales o escritos",
								"img": "/img/cursos/comunicacion/SEM-01-COM-07.jpg",
								"choices": [
									{
										"label": "Lengua",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Lenguaje",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Habla",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Dialecto",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Puede ser verbal o no verbal:",
								"choices": [
									{
										"label": "Lenguaje",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Lengua",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Habla",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Dialecto",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Es social, abstracto, lineal y convencional:",
								"choices": [
									{
										"label": "Lengua",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Lenguaje",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Dialecto",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Norma",
										"correct": false,
										"style": "btn-default",
										"border": "Argot",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Son variantes o modalidades regionales de una lengua:",
								"choices": [
									{
										"label": "Dialecto",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Lenguaje",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Norma",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Argot",
										"correct": false,
										"style": "btn-default",
										"border": "Argot",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Es el lenguaje formal de los profesionistas en sus diferentes especialidades:",
								"choices": [
									{
										"label": "Argot",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Caló",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Dialecto",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Normal",
										"correct": false,
										"style": "btn-default",
										"border": "Argot",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
			],
			[
				{
					"name": "La palabra",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Palabras",
								"body": "Iniciemos la sesión",
								"video": "ep9Tn1TcDy0"
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "solo-texto ",
							"component": "sle04",
							"miniatura": "sle04.jpg",
							"category": "lectura",
							"value": {
								"title": "La palabra",
								"body": "La palabra es un conjunto o secuencia de sonidos articulados, que se pueden representar gráficamente con letras, y por lo general, asocian un significado.",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "img-izquierda",
							"component": "sle01",
							"miniatura": "sle01.jpg",
							"category": "lectura",
							"value": {
								"title": "La raíz",
								"body": "Son la parte fundamental de una palabra. Tienen un significado léxico, es decir, definido en el diccionario. Significan conceptos y entidades (seres, cualidades, acciones, estados) reales o imaginados.  En ocasiones solo la raíz constituye una palabra entera, pero otras veces aparece algún morfema.",
								"img": "/img/cursos/comunicacion/SEM-02-COM-09.jpg",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "2-parrafo-subtitulos",
							"component": "sle09",
							"miniatura": "sle09.jpg",
							"category": "lectura",
							"value": {
								"title": "Morfema",
								"data": [
									{
										"subtitle": "Derivativo",
										"body": "Sirve para formar palabras derivadas. Los prefijos, los infijos y los sufijos son morfemas derivativos."
									},
									{
										"subtitle": "Gramatical ",
										"body": "Morfema que expresa la información gramatical de una palabra: caso, género, número, persona, tiempo, aspecto y modo verbal."
									}

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "videoa1",
							"component": "smu05",
							"miniatura": "smu05.jpg",
							"category": "multimedia",
							"value": {
								"title": "Repaso",
								"body": "Repasemos hasta aquí",
								"video": "IcbIvg_5e5Y"
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
				{
					"name": "Clasificación por estructura",
					"type": "content",
					"slide_index": 0,
					"slides": [
						{
							"name": "solo-texto ",
							"component": "sle04",
							"miniatura": "sle04.jpg",
							"category": "lectura",
							"value": {
								"title": "Clasificación de las palabras según su estructura",
								"body": "Las palabras según su estructura tienen diversas formas de clasificarse, aquí te mostramos cuáles son:",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "2-parrafo-subtitulos",
							"component": "sle09",
							"miniatura": "sle09.jpg",
							"category": "lectura",
							"value": {
								"title": "Variable e invariable",
								"data": [
									{
										"subtitle": "Variable",
										"body": "Admiten variaciones al añadir distintos morfemas al lexema: Libro, libros, librero, libreros, librería, librerías. Rojo, roja, rojos, rojas, rojizo, rojizas."
									},
									{
										"subtitle": "Invariable ",
										"body": "No admiten variaciones porque no existe morfema que se pueda añadir al lexema: Y, o, u, mas, en, desde, ¡ah!, ¡oh!."
									}

								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "2-parrafo-subtitulos",
							"component": "sle08",
							"miniatura": "sle09.jpg",
							"category": "lectura",
							"value": {
								"title": "Simples y compuestas",
								"data": [
									{
										"subtitle": "Simples",
										"body": "Son las palabras formadas por un lexema: Casa, mano, niño, silla, libro."
									},
									{
										"subtitle": "Compuestas ",
										"body": "Se forman con dos o más palabras simples: Contraorden, telaraña, cortaplumas, barbiblanco, rascacielos."
									}
								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "solo-texto ",
							"component": "sle04",
							"miniatura": "sle04.jpg",
							"category": "lectura",
							"value": {
								"title": "Parasintéticas",
								"body": "Se forman por composición y derivación a la vez: Norteamericanos, radiotelefonista, pisapapeles, multipartidista, retroexcavadora, unipersonalmente.",
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "2-parrafo-subtitulos",
							"component": "sle09",
							"miniatura": "sle09.jpg",
							"category": "lectura",
							"value": {
								"title": "Primitivas y derivadas",
								"data": [
									{
										"subtitle": "Primitivas",
										"body": "Son las que no derivan de ninguna otra palabra: Casa, mano, comer, libro, hombre, mesa, árbol, trueno."
									},
									{
										"subtitle": "Derivadas ",
										"body": "Son las que derivan de las primitivas: Casas, manos, comieron, librero, hombres, mesita, arboles, truenos."
									}
								],
								"readingTime": 10
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
					],
					"favorite": false,
					"completed": false
				},
				{
					"name": "Cuestionario",
					"type": "quiz",
					"minimum_approval": 70,
					"slide_index": 0,
					"slides": [
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Es un conjunto o secuencia de sonidos que se puede representar gráficamente:",
								"choices": [
									{
										"label": "La palabra",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "La raíz",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Los morfemas",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Palabras simples",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Tiene un significado léxico:",
								"choices": [
									{
										"label": "La raíz",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Los morfemas",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "La palabra",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Las palabras primitivas",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Puede ser derivativo o gramatical",
								"choices": [
									{
										"label": "Morfema",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Raíz",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Palabra",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Palabra primitiva",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Expresa la información gramatical de una palabra:",
								"choices": [
									{
										"label": "Morfema gramatical",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Morfema derivativo",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Raíz",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Todas la anteriores",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Son las que no derivan de ninguna otra palabra:",
								"choices": [
									{
										"label": "Primitivas",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Compuestas",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Simples",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Parasintéticas",
										"correct": false,
										"style": "btn-default",
										"border": "Argot",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta1",
							"component": "srp01",
							"miniatura": "srp01.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "Se forman por composición y derivación:",
								"choices": [
									{
										"label": "Parasintéticas",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Compuestas",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Simples",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "Primitivas",
										"correct": false,
										"style": "btn-default",
										"border": "Argot",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						},
						{
							"name": "pregunta2",
							"component": "srp02",
							"miniatura": "srp02.jpg",
							"category": "question",
							"type": "EVAL",
							"value": {
								"question": "De la siguiente imagen: ¿Cuántas palabras simples hay?",
								"img": "/img/cursos/comunicacion/SEM-02-COM-11.jpg",
								"choices": [
									{
										"label": "3 simples",
										"correct": true,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "1 simple ",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "4 simples",
										"correct": false,
										"style": "btn-default",
										"border": "",
									},
									{
										"label": "2 simples",
										"correct": false,
										"style": "btn-default",
										"border": "",
									}
								],
							},
							"icon": "ti-layout-media-center-alt",
							"result": ResultTypes.PENDING
						}
					],
					"favorite": false,
					"completed": false
				},
			]
		],
		"completed": false
	}

}




const _chats = [
	{
		user: { name: 'Jon Doe', image: '/images/student.jpg' },
		teacher: { name: 'Alex', course: 'Ciencias', image: '/images/teacher.jpg' },
		messages: [
			{ body: 'Ipsum', user: 'teacher', date: '2017-08-21 08 42' },
			{ body: 'Dolor', user: 'student', date: '2017-08-28 22 52' },
		],
		selected: false,
		unread: 1,
		last_message_time: 'hace 5 minutos'
	},
	{
		user: { name: 'Jon Doe', image: '/images/student.jpg' },
		teacher: { name: 'María', course: 'Comunicación', image: '/images/teacher-2.jpg' },
		messages: [
			{ body: 'Ipsum', user: 'teacher', date: '2017-08-21 16 42' },
			{ body: 'Ipsum', user: 'teacher', date: '2017-08-21 16 42' },
		],
		selected: false,
		unread: 0,
		last_message_time: 'hace 10 minutos',
		last_message_content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla consequuntur ipsam voluptatibus eius vero, id nostrum iure non neque provident dolore deserunt minus, voluptates doloremque odit dignissimos ipsum quos aliquid.'
	},]

const _chat = [
	{
		user: { name: 'Jon Doe', image: '/images/student.jpg' },
		subject: { name: 'Comunicación', image: '/images/teacher-2.jpg' },
		messages: [
			{ body: 'Ipsum', user: 'teacher', date: '2017-08-21 16 42' },
			{ body: 'Ipsum', user: 'teacher', date: '2017-08-21 16 42' },
		],
		selected: false,
		unread: 0,
		last_message_time: 'hace 10 minutos',
		last_message_content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla consequuntur ipsam voluptatibus eius vero, id nostrum iure non neque provident dolore deserunt minus, voluptates doloremque odit dignissimos ipsum quos aliquid.'
	}

];


export default {

	getCourse(id, success) {
		axios.get(`/api/course/${id}`).then(({data}) => success(data))	
	},

	getUser(success) {
		axios.get(`/get-user`).then(({data}) => success(data))	
	},

	getCoursePreview(id, success) {
		axios.get(`/api/course-preview/${id}`).then(({data}) => success(data))	
	},

	getCourseInfo(id, success) {

		axios.get(`/api/course-builder/${id}`, {
		}).then((response) => {
				success(response.data)
			});
	},

	storeCourse(course, success) {
		let id = course.get('subject_id')
		axios.post(`/course-builder/${id}`, course)
			.then((response) => {
				success(response)
			});

	},

	updateCourseAssigned(course, success) {
		axios.post(`/api/my-subjects/${course.id}`, course)
			.then((response) => {
				success(response)
			});

	},

	storeEvaluationGrade(evaluation, success) {
		axios.post(`/api/evaluation/`, evaluation)
			.then((response) => {
				success(response)
			});

	},

	storeCoursePreview(course, success) {
		let id = course.get('subject_id')
		axios.post(`/course-preview/${id}`, course)
			.then((response) => {
				success(response)
		});
	},

	storeCourseLog(data) {
		return new Promise((resolve, reject) => {
			axios.post(`/course-log/`, data)
				.then(({data}) => {
					resolve(data)
			});
		});
	},

	endCourseLog(data) {
		return new Promise((resolve, reject) => {
			axios.post(`/course-log/${data.id}`, data)
				.then((response) => {
					resolve(response)
			});
		});

	},

	getChats(success) {

		axios.get('/conversations')
			.then((response) => {
				success(Array.isArray(response.data) ? response.data : Object.values(response.data));
			})
			.catch((err) => {});

	},

	changeCurrentChat(chat, success) {
		
		let obj = {
			subject: chat.subject.name,
			user: chat.user.name,
			user_id: chat.user.id,
			id: chat.assignmentTeacher ? chat.assignmentTeacher : chat.userAssignment,
			index : chat.index,
			scroll :chat.scroll
		}
		
		axios.post('/getConversation', obj)
			.then(({ data }) => {
				success(data)
			});
		
	},

	sendMessage(message, success) {
		axios.post('/setMessage', message)
			.then((response) => {
				message.body = response['data'][1];
				success(message)
			});
	},

	uploadFile(formData) {
		return new Promise((resolve, reject) => {
			axios.post('/api/file-upload', formData, {
				headers: {
					'Content-Type': 'multipart/form-data'
				}
			}).then((response) => {
				resolve(response.data.path)
			})
		})
	},

	uploadAudio(formData) {
		return new Promise((resolve, reject) => {
			axios.post('/api/audio-upload', formData, {
				headers: {
					'Content-Type': 'multipart/form-data'
				}
			}).then((response) => {
				resolve(response.data.path)
			})
		})
	}


}