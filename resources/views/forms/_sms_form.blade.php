<div id="campos">

	<div class="form-group row">
	    {!! Form::label('tag', 'Tag del mensaje', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('tag', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Número de cuentas']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('content', 'Mensaje', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
            {!! Form::textarea('content',null,['size'=>'100x5','class' => 'form-control input-md','maxlength'=>'230','placeholder'=>'Este mensaje tiene un límite de 230 caracteres.']) !!}
			
	    </div>
	</div>


	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
