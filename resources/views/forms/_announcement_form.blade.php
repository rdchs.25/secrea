<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre de la Convocatoria', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Ejemplo']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('size_time', 'Tiempo Académico', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('size_time', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 3']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('academic_time_id', 'Tiempo Académico', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $itiempoacademico = array();
	            foreach($tiemposacademicos as $tiempoacademico) {
	              $itiempoacademico[$tiempoacademico->id] = $tiempoacademico->name;
	            }    
	        ?>
	         {!! Form::select('academic_time_id', $itiempoacademico, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un tiempo académico']); !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('status', 'Estado la Convocatoria', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
				{!! Form::radio('status', 0, true),'&nbsp', 'En Inscripción' !!} 
	            {!! Form::radio('status', 1, true),'&nbsp', 'Activa' !!}                    
	            {!! Form::radio('status', 2, true),'&nbsp', 'Inactiva' !!} 		                   
	    </div>
	</div>

	<div class="form-group row">

	    {!! Form::label('pagos', 'Pagos', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	    
	    	<div class="form-group row">	
	       		{!! Form::label('rode_ma', 'Matrícula (s/.)', ['class' => 'col-sm-2 col-form-label']) !!}
	       		<div class="col-md-8">
				   {!! Form::number('rode_ma', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 40','min'=>10 ]) !!}
	       		</div>
	       	</div>

			<div class="form-group row">	
	       		{!! Form::label('asd', 'Pensión', ['class' => 'col-sm-2 col-form-label']) !!}
				<div class="col-md-8">
					<div class="form-group row">	
						{!! Form::label('rode_pe', 'Monto (s/.)', ['class' => 'col-sm-2 col-form-label']) !!}
						<div class="col-md-8">
							{!! Form::number('rode_pe', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 40','min'=>10]) !!}
						</div>
					</div>
					<div class="form-group row">	
						{!! Form::label('count_pe', 'Cantidad', ['class' => 'col-sm-2 col-form-label']) !!}
						<div class="col-md-8">
							{!! Form::number('count_pe', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 6','min'=>1]) !!}
						</div>
					</div>
				</div>
	       	</div>

			<div class="form-group row">	
	       		{!! Form::label('rode_ce', 'Certificado (s/.)', ['class' => 'col-sm-2 col-form-label']) !!}
	       		<div class="col-md-8">
				   {!! Form::number('rode_ce', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 100','min'=>10]) !!}
	       		</div>
	       	</div>

	    </div>
	</div>

	<div class="form-group row">

	    {!! Form::label('evaluation_type_id', 'Tipo de Evaluación', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8 slidecontainer">
	    @php 
	        $ievaluationtypes = array();
	    	foreach($evaluationTypes as $evaluationType) {
	    @endphp	
	    	<div class="form-group row">	
	       		{!! Form::label('percent_eval_type_'.$evaluationType->id, $evaluationType->name, ['class' => 'col-sm-2 col-form-label']) !!}
	       		<div class="col-md-8">
	       			<input type="range" min="0" max="100" value="{{$evaluationType->percent or 0}}" class="slider" name="percent_eval_type_{{$evaluationType->id}}" id="{{$evaluationType->id}}">
					
					<div id="range_value_{{$evaluationType->id}}"></div>
	       		</div>
	       	</div>
	    @php 
	       }    
	    @endphp
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){

		let sum_percent = 0;
	    $(".slidecontainer input.slider").each(function(){

	    	let tag_id 	= $(this).prop('id');

	    	let slider 	= document.getElementById(tag_id);
	    	let output 	= document.getElementById("range_value_"+tag_id);
	    	

	    	output.innerHTML = slider.value;
	    	sum_percent += parseInt($(this).val());

			slider.oninput = function() {

			    output.innerHTML = this.value;

			    let sum_percent = 0;
				$(".slidecontainer input.slider").each(function(){
			    	
			    	sum_percent += parseInt($(this).val());

			    });

			    if(sum_percent == 100){
			    	$(':submit').prop('disabled',false);
			    }else{
			    	$(':submit').prop('disabled',true);
			    }

			}
		    
	    });

	    if(sum_percent == 100){
		  	$(':submit').prop('disabled',false);
		}else{
		  	$(':submit').prop('disabled',true);
		}

	});
</script>
<style type="text/css" media="screen">
	.slider {
	    -webkit-appearance: none;
	    width: 100%;
	    height: 15px;
	    border-radius: 5px;   
	    background: #d3d3d3;
	    outline: none;
	    opacity: 0.7;
	    -webkit-transition: .2s;
	    transition: opacity .2s;
	}

	.slider::-webkit-slider-thumb {
	    -webkit-appearance: none;
	    appearance: none;
	    width: 25px;
	    height: 25px;
	    border-radius: 50%; 
	    background: #4CAF50;
	    cursor: pointer;
	}

	.slider::-moz-range-thumb {
	    width: 25px;
	    height: 25px;
	    border-radius: 50%;
	    background: #4CAF50;
	    cursor: pointer;
	}
</style>