<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre de la Sección', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Sección A']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('size', 'Capacidad de la Sección', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('size', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 50']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('grade_id', 'Grado', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $items = array();
	            foreach($grades as $grade) {
	              $items[$grade->id] = $grade->name;
	            }    
	        ?>
	        {!! Form::select('grade_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Grado']); !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
