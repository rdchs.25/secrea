<div id="campos">
    
	<div class="form-group row">
	    {!! Form::label('name', 'Alumno', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Nombre completo del alumno']) !!}
	    </div>
	</div>

    <div class="form-group row">
	    {!! Form::label('type', 'Tipo de constancia', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8" style="padding:10px;">
				{!! Form::radio('type', 0, true),'&nbsp', 'Vacante' !!} 
	            {!! Form::radio('type', 1, true),'&nbsp', 'Matrícula' !!}       	                   
	    </div>
	</div>

    <div class="form-group row">
        {!! Form::label('grade_id', 'Grado', ['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-md-8">
            <?php 
                $igrades = array();
                foreach($grades as $grade) {
                $igrades[$grade->id] = $grade->name;
                }    
            ?>
            {!! Form::select('grade_id', $igrades, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un grado académico']); !!}
            @if ($errors->has('grade_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('grade_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
