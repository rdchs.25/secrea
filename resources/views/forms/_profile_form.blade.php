
<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre y Apellido', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Juan Quispe']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('dni', 'DNI', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('dni', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 12345678-9']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('address', 'Dirección', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('address', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Lima, San Borja 123']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('phone', 'Teléfono', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('phone', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: +51 123 456 789']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('birth_date', 'Fecha de nacimiento', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::date('birth_date', null, ['class' => 'form-control input-md']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('path_profile_image', 'Foto de Perfil', ['class' => 'col-sm-2 col-form-label']) !!}
		 <div class="col-md-8">
		        {!! Form::file('path_profile_image', null, ['class' => 'form-control input-md']) !!}
		    	@if(!empty($profile) && $profile->path_profile_image!=null)
					 <a href='{{ asset("storage/$profile->path_profile_image")}}' target="_blank"class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
	                    <span class="btn-label"><i class="fa fa-file-image-o"></i></span>
	                    Ver Imagen
	                </a>
		    	@endif
		    </div>

		</div>
	</div>
	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
