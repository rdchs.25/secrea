
<div id="campos">

    <div class="form-group row">
        {!! Form::label('path_dni', 'Dni', ['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-md-8">    
        {!! Form::file('path_dni', null, ['class' => 'form-control input-md']) !!}
               @if(!empty($document->path_dni))
                   <a href='{{ asset("storage/$document->path_dni")}}' target="_blank"class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                       <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>
                       Ver DNI
                   </a>
               @endif
           </div>
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('path_study_certificate', 'Certificado', ['class' => 'col-sm-2 col-form-label']) !!}
            <div class="col-md-8">
             {!! Form::file('path_study_certificate', null, ['class' => 'form-control input-md']) !!}
                @if(!empty($document->path_study_certificate))
                    <a href='{{ asset("storage/$document->path_study_certificate")}}' target="_blank"class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                        <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>
                        Ver Certificado
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('path_voucher', 'Voucher de Matrícula', ['class' => 'col-sm-2 col-form-label']) !!}
            <div class="col-md-8">
             {!! Form::file('path_voucher', null, ['class' => 'form-control input-md']) !!}
                @if(!empty($document->path_voucher))
                    <a href='{{ asset("storage/$document->path_voucher")}}' target="_blank"class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                        <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>
                        Ver Voucher
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
    <br>
        {!! Form::submit("Actualizar Documentos", ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
    </div>
</div>