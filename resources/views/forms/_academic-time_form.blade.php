<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre del Tiempo Académico', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Semestre']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('number_weeks', 'Semanas', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('number_weeks', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 6']) !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
