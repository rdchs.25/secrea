<div id="campos">

	<div class="form-group row">
	    {!! Form::label('message', 'Mensaje al Soporte técnico:', ['class' => 'col-sm-12 col-form-label']) !!}
	    <div class="col-md-12">
	        {!! Form::textarea('message', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Presento un problema en ...']) !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
