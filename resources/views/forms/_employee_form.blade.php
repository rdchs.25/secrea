<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre de la Compañia', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'disabled' => 'disabled'	]) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre del Usuario', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-5">
	        <?php 
	            $items = array();
	            foreach($users as $user) {
	              $items[$user->id] = $user->name;
	            }    
	        ?>
	        {!! Form::select('user_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Usuario']); !!}
	    </div>
		<div class="col-md-3">
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
		</div>
	</div>

</div>
