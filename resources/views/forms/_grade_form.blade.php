<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre del Grado', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 1ro de Secundaria']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre de la Sección', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('section', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: A']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('academic_level_id', 'Nivel Académico', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $items = array();
	            foreach($academicLevels as $academicLevel) {
	              $items[$academicLevel->id] = $academicLevel->name;
	            }    
	        ?>
	        {!! Form::select('academic_level_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Nivel Académico']); !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
