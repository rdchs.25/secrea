
<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre de la asignatura', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Matemática']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('identify', 'Identificación de la asignatura', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('identify', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Matemática para 1 grado']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('path_image_subject', 'Imágen de asignatura', ['class' => 'col-sm-2 col-form-label']) !!}

	    <div class="col-md-5">
	        {!! Form::file('path_image_subject', null, ['class' => 'form-control input-md']) !!}
	    </div>
	    <div class="col-md-5">
	    	@if(!empty($subject))
				 <a href='{{ asset("storage/$subject->path_image_subject")}}' target="_blank"class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="fa fa-file-image-o"></i></span>
                    Ver Imagen
                </a>
	    	@endif
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('path_calendar', 'Calendario', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-5">
	        {!! Form::file('path_calendar', null, ['class' => 'form-control input-md']) !!}
	    </div>
	    <div class="col-md-5">
	    	@if(!empty($subject))
	    		 <a href='{{ asset("storage/$subject->path_calendar")}}' target="_blank" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>
                    Ver Documento
                </a>
	    	@endif
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('path_content', 'Contenido', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-5">
	        {!! Form::file('path_content', null, ['class' => 'form-control input-md']) !!}
	    </div>
	    <div class="col-md-5">
	    	@if(!empty($subject))
	    		<a href='{{ asset("storage/$subject->path_content")}}' target="_blank" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>
                    Ver Documento
                </a>
	    	@endif
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('path_programation', 'Programación', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-5">
	         {!! Form::file('path_programation', null, ['class' => 'form-control input-md']) !!}
	    </div>
	    <div class="col-md-5">
	    	@if(!empty($subject))
	    		<a href='{{ asset("storage/$subject->path_programation")}}' target="_blank" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span>
                    Ver Documento
                </a>
	    	@endif
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('url_welcome_video', 'URL video', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('url_welcome_video', null, ['class' => 'form-control input-md', 'placeholder' => '']) !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
