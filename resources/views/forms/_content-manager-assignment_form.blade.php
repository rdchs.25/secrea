<div id="campos">

	<div class="form-group row">
	    {!! Form::label('user_id', 'Gestor de Contenido', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $isContentManager = array();
	            foreach($contentManagers as $contentManager) {
	              $isContentManager[$contentManager->id] = $contentManager->name;
	            }    
	        ?>
	         {!! Form::select('user_id', $isContentManager, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Gestor de Contenido']); !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('subject_id', 'Asignatura', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $isubject = array();
	            foreach($subjects as $subject) {
	              $isubject[$subject->id] = $subject->name." | ".$subject->identify;
	            }    
	        ?>
	         {!! Form::select('subject_id', $isubject, null, ['class' => 'form-control', 'placeholder' => 'Selecciona una Asignatura']); !!}
	    </div>
	</div>


	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
