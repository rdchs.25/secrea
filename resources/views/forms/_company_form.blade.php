<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre de la Empresa', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Ejemplo']) !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
