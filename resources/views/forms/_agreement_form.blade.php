<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre del Convenio', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Ejemplo']) !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('partner_company_id', 'Compañía Financiera', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $items = array();
	            foreach($companies as $company) {
	              $items[$company->id] = $company->name;
	            }    
	        ?>
	        {!! Form::select('partner_company_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona la Compañía Financiera']); !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('sales_company_id', 'Compañía de Ventas', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $items = array();
	            foreach($companies as $company) {
	              $items[$company->id] = $company->name;
	            }    
	        ?>
	        {!! Form::select('sales_company_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona una Compañía de Ventas']); !!}
	    </div>
	</div>

	<div class="form-group row">
	    {!! Form::label('amount', 'Dinero a becar', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('amount', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: 100.000 S/']) !!}
	    </div>
	</div>

	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
