<div id="campos">

	<div class="form-group row">
	    {!! Form::label('user_id', 'Docente', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $isdocentes = array();
	            foreach($docentes as $docente) {
	              $isdocentes[$docente->id] = $docente->name;
	            }    
	        ?>
	         {!! Form::select('user_id', $isdocentes, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Docente']); !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('grade_id', 'Grado', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $igrade = array();
	            foreach($grades as $grade) {
	              $igrade[$grade->id] = $grade->name."|".$grade->section."|".$grade->academicLevels->name;
	            }    
	        ?>
	         {!! Form::select('grade_id', $igrade, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Grado']); !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('subject_id', 'Asignatura', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $isubject = array();
	            foreach($subjects as $subject) {
	              $isubject[$subject->id] = $subject->identify;
	            }    
	        ?>
	         {!! Form::select('subject_id', $isubject, null, ['class' => 'form-control', 'placeholder' => 'Selecciona una Asignatura']); !!}
	    </div>
	</div>


	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>
