<div id="campos">

	<div class="form-group row">
	    {!! Form::label('name', 'Nombre del Usuario', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('name', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: Pedro Perez']) !!}
	    </div>
	</div>
	<div class="form-group row">
	    {!! Form::label('email', 'Correo Electrónico', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('email', null, ['class' => 'form-control input-md', 'placeholder' => 'Ej: email@email.com']) !!}
	    </div>
	</div>
	@role('sales-manager')
	@else
	<div class="form-group row">
	    {!! Form::label('password', 'Contraseña', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('password', null, ['class' => 'form-control input-md']) !!}
	    </div>
	</div>
	@endrole

	<?php
		$estado = (!empty($disabled))?$disabled:false; 
	?>

	<div class="form-group row">
	    {!! Form::label('role_id', 'Rol del Usuario', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $items = array();
	            foreach($roles as $role) {
	            	$items[$role->id] = $role->name;
	            }   
	        ?>
	        {!! Form::select('role_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Rol']); !!}
	    </div>
	</div>

	<div class="form-group row" style="display: none;" id="div_grade">
	    {!! Form::label('grade_id', 'Grado', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $igrade = array();
	            foreach($grades as $grade) {
	            	$igrade[$grade->id] = $grade->name."|".$grade->section."|".$grade->academicLevels->name;
	            }    
	        ?>
	        {!! Form::select('grade_id', $igrade, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Grado','disabled'=>$estado]); !!}
	    </div>
	</div>

	<div class="form-group row" style="display: none;" id="div_agreement">
	    {!! Form::label('agreement_id', 'Convenio', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $items = array();
	            foreach($agreements as $agreement) {
	              $items[$agreement->id] = $agreement->name;
	            }    
	        ?>
	        {!! Form::select('agreement_id', $items, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un Convenio de poseer uno','disabled'=>$estado]); !!}
	    </div>
	</div>

	<div class="form-group row" style="display: none;" id="div_announcement">
	    {!! Form::label('announcement_id', 'Ciclo Académico', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $iannouncement = array();
	            foreach($announcements as $announcement) {
					$status = ($announcement->status)?'Activa':'Pre Inscripción';
	              	$iannouncement[$announcement->id] = $status.': '.$announcement->name ;
	            }    
	        ?>
	        {!! Form::select('announcement_id', $iannouncement, null, ['class' => 'form-control', 'placeholder' => 'Seleccione un ciclo académico','disabled'=>$estado]); !!}
	    </div>
	</div>

	<div class="form-group row" style="display: none;" id="div_company">
	    {!! Form::label('company_id', 'Compañia', ['class' => 'col-sm-2 col-form-label']) !!}
	    <div class="col-md-8">
	        <?php 
	            $icompanys = array();
	            foreach($companys as $company) {
	              	$icompanys[$company->id] = $company->name ;
	            }    
	        ?>
	        {!! Form::select('company_id', $icompanys, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una compañia']); !!}
	    </div>
	</div>

	<!--<div class="form-group row" style="display: none;" id="div_confirmed">
	<div class="col-md-8">
		{!! Form::checkbox('not_send_mail')!!}
	    <strong>{!! Form::label('not_send_mail', 'No enviar mail', ['class' => 'col-form-label']) !!}</strong>
	</div>
	</div>-->



	<div class="form-group row">
	<br>
		{!! Form::submit($submitButtonText, ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
	</div>

</div>

<script>

$(document).ready(function(){
	$("#role_id").change(function(){
        if( $("#role_id").val() == 10 || $("#role_id").val() == 3 ){
        	$("#div_grade").css('display','block');
        	$("#div_agreement").css('display','block');
			$("#div_announcement").css('display','block');
			//$("#div_confirmed").css('display','block');
		
        }else{
        	$("#div_grade").css('display','none');
        	$("#div_agreement").css('display','none');
			$("#div_announcement").css('display','none');
			//$("#div_confirmed").css('display','none');
        }
		if( $("#role_id").val() == 7){
        	$("#div_company").css('display','block');
        }else{
        	$("#div_company").css('display','none');
        }
    });
	    if( $("#role_id").val() == 10 || $("#role_id").val() == 3){
        	$("#div_grade").css('display','block');
        	$("#div_agreement").css('display','block');
			$("#div_announcement").css('display','block');
			//$("#div_confirmed").css('display','block');
        }else{
        	$("#div_grade").css('display','none');
        	$("#div_agreement").css('display','none');
			$("#div_announcement").css('display','none');
			//$("#div_confirmed").css('display','none');
        }
		if( $("#role_id").val() == 7){
        	$("#div_company").css('display','block');
        }else{
        	$("#div_company").css('display','none');
        }
});

</script>
