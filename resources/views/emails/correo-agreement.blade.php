<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Secrea</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#70bbd9" style="padding: 0px 0 0px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<img src="{{  $message->embed('images/h11.gif') }}" alt="Creating Email Magic" width="598" height="328" style="display: block;" />
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
                                    <td style="color: #153643; font-family: Arial, sans-serif; font-size: 18px;">
                                        <h2> <b>¡Ya eres parte de SECREA!</b> </h2>
                                        <b>Hola {{$user->name}}, </b> A continuación te recordamos tu usuario para actualizar tus datos personales.
                                    
                                        <span style="background-color:#eee; text-align:center;  padding:16px; display: block; color: #153643; font-size: 18px; color: #444; margin: 20px; border-radius: 10px;"> 
                                            <b>Usuario:  </b> {{$user->email}} <br>
                                        </span>
										<span style="background-color:#eee; text-align:center;  padding:16px; display: block; color: #153643; font-size: 18px; color: #444; margin: 20px; border-radius: 10px;"> 
                                            <b>Usuario:  </b> {{$pass}} <br>
                                        </span>
                                        <span style="background-color:#eee; text-align:center;  padding:16px; display: block; color: #153643; font-size: 18px; color: #444; margin: 20px; border-radius: 10px;"> 
                                            Tu beca es otrorgada por : {{$partner_company}}
                                        </span>
                                        <span style="background-color:#46927a; text-align:center;   padding:16px; display: block; color: #153643; font-size: 18px; margin: 20px; border-radius: 10px;"> 
                                            <a  style=" color: #fff; text-decoration: none;" href="{{url('login')}}">Ingresar a la plataforma</a> 
                                        </span>
                                        
                                        <p>Mantente al tanto del inicio de clases</p>
                                                                                                                
                                    </td>
                                </tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#46927a" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
										&reg; SECREA, Secundaria creativa 2017<br/>
										Termina tu secundaria con nosotros
									</td>
									<td align="right" width="25%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													<a href="https://twitter.com/secreavirtual" style="color: #ffffff;">
														<img src="{{$message->embed('images/tw3.gif')}}" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
													</a>
												</td>
												<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													<a href="https://www.facebook.com/Secrea-Secundaria-Creativa-1612558169053650/?fref=ts" style="color: #ffffff;">
														<img src="{{$message->embed('images/fb3.gif')}}" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
													</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>


