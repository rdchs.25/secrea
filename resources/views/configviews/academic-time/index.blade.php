@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Tiempos Académicos</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Tiempos Académicos </a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/academic-time/create" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class=" ti-plus"></i></span>
                    Nuevo Tiempo Académico
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Tiempos Académicos</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás editar, visualizar y eliminar información de los Tiempos Académicos. </p>
                             </div>
                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Nombre</th>
                                   <th>Semanas</th>
                                   <th>Opciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($academicTimes as $id => $academicTime)
                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $academicTime->name}}
                                        </td>
                                        <td>
                                            {{ $academicTime->number_weeks}}
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Opciones
                                              </button>
                                              <div class="dropdown-menu">
                                                <!-- <a class="dropdown-item" href="/academic-level/{{ $academicTime->slug }}">Ver</a> -->
                                                <a class="dropdown-item" href="/academic-time/{{ $academicTime->slug }}/edit">Editar</a>
                                                <a class="dropdown-item" href="/academic-time/{{ $academicTime->slug }}/delete">Eliminar</a>
                                              </div>
                                            </div>
                                       </td>
                                    </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
