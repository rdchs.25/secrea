@extends('layouts.app')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <h4>Pagos:</h4>
        <nav class="navbar navbar-light bg-white b-a ">
            <ol class="breadcrumb no-bg mb-0">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active"> Pagos </li>
            </ol>
        </nav>
        <div class="mt-2 col-md-4 col-sm-12">
            <div class="box bg-white user-5">
                @can('payment')
                    @if(!empty($user->userBefore))
                    <div class="float-md-left mb-2">
                        <button type="button" class="btn  btn-success btn-circle  waves-light">
                            <a href='{{ asset("announcement/".$payannouncement[0]->announcement->slug."/payment/".$user->userBefore) }}' style="color:white"><i class="ti-arrow-left"></i></a>
                        </button>
                    </div>
                    @endif
                    @if(!empty($user->userAfter))
                    <div class="float-md-right mb-2">
                        <button type="button" class="btn2 btn-success btn-circle  waves-light ">
                            <a href='{{ asset("announcement/".$payannouncement[0]->announcement->slug."/payment/".$user->userAfter) }}' style="color:white"><i class="ti-arrow-right"></i></a>
                        </button>
                    </div>
                    @endif
                @endcan
                <div class="u-content">
                    <div class="mb-2 text-sm-center text-md-center">
                        <div class="img-div2">
                            @if(empty($user->users->profiles->path_profile_image))
                                <img src="/img/avatars/avatar.jpg" alt="">
                            @else   
                                <img src="/storage/{{ $user->users->profiles->path_profile_image}}" alt="">
                            @endif
                        </div>
                        <i class="status bg-success bottom right"></i>
                    </div>
                    <h2><a class="text-black" href="#">{{$user->users->name}}</a></h2>
                    <p class="text-muted mb-1">Registrado</p>
                    
                    @if(empty($user->users->documents))
                        <span class="tag tag-pill tag-danger">Sin documentos</span>
                    @else
                        @if(empty($user->users->documents->path_dni))
                            <span class="tag tag-pill tag-danger">DNI</span>
                        @else 
                            <a href='{{ asset("storage/".$user->users->documents->path_dni) }}' target="_blank">
                            <span class="tag tag-pill tag-success">DNI</span>
                            </a> 
                        @endif 
                        @if(empty($user->users->documents->path_study_certificate))
                            <span class="tag tag-pill tag-danger">CERT</span>
                        @else 
                            <a href='{{ asset("storage/".$user->users->documents->path_study_certificate)}}' target="_blank">
                            <span class="tag tag-pill tag-success">CERT</span>
                            </a>   
                        @endif                                                                                                    
                    @endif
                    
                    <table class="table  mb-0" style="margin-top:10px">
                        <tbody>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Grado:</td>
                                <td style="text-align: left;" >{{ $user->users->assignmentStudent[0]->grade->name }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Edad:</td>
                                <td style="text-align: left;">{{ empty($user->users->profiles->birth_date)? '--': \Carbon\Carbon::parse( $user->users->profiles->birth_date )->age  }}</td>
                            </tr>
                            @if(!empty($user->users->profiles))
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Teléfono:</td>
                                <td style="text-align: left;">{{$user->users->profiles->phone}}</td>
                            </tr>
                            @endif
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Correo:</td>
                                <td style="text-align: left;">{{$user->users->email}}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Último login:</td>
                                <td style="text-align: left;">{{ $user->last_login != null ? \Carbon\Carbon::parse($user->users->last_login)->diffForHumans() : 'Ninguna' }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Confirmación e-mail:</td>
                                <td style="text-align: left;">{{ $user->users->confirmed ? 'SI' : 'NO' }}</td>
                            </tr>
                        </tbody>
                    </table>  
                </div>
            </div>
            @can('payment')
            <div class="box bg-white user-5">
                <h4>Subir voucher</h4>
                {!! Form::open(
                        array(
                            'url' => '/voucher', 
                            'class' => 't-3 form-material material-primary', 
                            'files' => true)) !!}
                
                    <div id="campos">
                        <div class="form-group row">
                            <div class="col-md-12">  
                            <br>  
                            {!! Form::file('path_voucher', null, ['class' => 'form-control input-md']) !!}
                            </div>
                        </div>
                        {!! Form::submit("Subir", ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            @endcan
        </div>
        
        <div class="mt-2 col-md-8 col-sm-12">
            <form action="/payment" method="POST">
            <div class="b-a b-a-seccess mt-1 b-a-dashed mb-0-5 p-2 b-a-radius-0-5 " >  
                @can('payment')
                <div class=" mt-0 mb-2">
                <h5>Vouchers sin asignar</h5>
                @if ($errors->has('voucher'))
                    <span class="help-block" style="color:#f44236">
                        <strong>Para realizar pagos, debes seleccionar un voucher</strong>
                    </span>
                @endif
                <div class="voucherList">
                    <ul>
                        @if(!$user->vouchers->isEmpty())
                            @foreach($user->vouchers as $id=>$voucher)
                                <li>
                                    <div class="imgContent">
                                        <img src="/img/payment/pago1.jpg">
                                    </div>
                                    
                                    <div class="infoContent">
                                        <input type="radio" class="voucher" name="voucher" value="{{$voucher->id}}"><br>
                                        <span class="tag tag-default">Voucher {{$id+1}}</span>
                                        <p>{{\Carbon\Carbon::parse($voucher->created_at)->toFormattedDateString()}}</p>
                                        <a href='{{ asset("storage/".$voucher->path_voucher)}}' class="btn btn-info" target="_blank">
                                            <span >Ver PDF</span>
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li>
                                <div class="sinVoucher">
                                    <img src="/img/payment/pago3.jpg">
                                </div>
                            </li>
                        @endif
                    </ul>
                    
                </div>
                </div>
                @endcan
                <ul class="nav nav-tabs " role="tablist">
                    @foreach($yourpreenrollment as $your)
                        <li class="nav-item">
                            <a class="nav-link {{ $payannouncement[0]->announcement_id == $your->announcement_id? 'active': ''  }}" href='{{ asset("/announcement/".$your->announcements->slug."/payment/".$user->user_id) }}'  >{{$your->announcements->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="false">
                        <div class="box bg-white cart-mini">
                            <div class="cm-title">LISTA DE PAGOS </div>
                            <table class="positions table mb-0">
                                <thead>
                                    <tr>
                                        <th>Select</th>
                                        <th>Concepto</th>
                                        <th> Monto (s/.) </th>
                                        <th>Voucher</th>
                                        <th>Estado</th>
                                        <th>Elim</th>
                                    </tr>
                                </thead>
                                <tbody>
                            
                                    <tr>
                                        <td>
                                            @if(!$pay['enroll'])
                                                <input type="checkbox" name="chbx_pay_ma">
                                            @else
                                                <span class="fa fa-check-circle" style="color:{{$pay['code_enroll']}}">
                                            @endif
                                        </td>
                                        <td><a class="text-black" href="#"><span>Matrícula</span></a></td>
                                        <td>{{$payannouncement[0]->rode}}</td>
                                        @if($pay['enroll'])
                                            <td>
                                                <a href='{{ asset("storage/" . $pay["enroll"]) }}' class="btn btn-info btn-sm" target="_blank">Ver imagen</a>
                                            </td>
                                            <td><span class="tag tag-pill tag-info">Pagado</span></td>
                                        @else
                                            <td><a href='#' class="btn btn-danger btn-sm" > Ver imagen</a></td>
                                            <td><span class="tag tag-pill tag-danger">No Pagado</span></td>
                                        @endif
                                            <td>
                                                <a class="text-grey"  href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                    </tr>

                                    @for ($i = 0; $i < $payannouncement[1]->cuotas; $i++)
                                        <tr>
                                            <td>
                                                @if(!array_key_exists($i,$pay['pension']))
                                                    <input type="checkbox"  name="pensiones[]">
                                                @else
                                                    <span class="fa fa-check-circle" style='color:{{$pay['pension']["code_$i"]}}'>
                                                @endif
                                            </td>
                                            <td><a class="text-black" href="#"><span>Cuota {{$i + 1}}</span></a></td>
                                            <td>{{$payannouncement[1]->rode}}</td>
                                            @if(array_key_exists($i,$pay['pension']))
                                                <td>
                                                    <a href='{{ asset("storage/" . $pay["pension"][$i]) }}' class="btn btn-info btn-sm" target="_blank">Ver imagen</a>
                                                </td>
                                                <td><span class="tag tag-pill tag-info">Pagado</span></td>
                                            @else
                                                <td><a href='#' class="btn btn-danger btn-sm">Ver imagen</a></td>
                                                <td><span class="tag tag-pill tag-danger">No Pagado</span></td>
                                            @endif 
                                            <td><a class="text-grey" href="/#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                            
                            <div class="card-block">
                                <div class="clearfix">
                                    <div class="cm-total float-xs-left"><span class="text-muted">Total Matrícula:</span> S/.{{ $payannouncement[0]['cuotas'] * $payannouncement[0]['rode']  }}</div>
                                    <div class="cm-total float-xs-left"><span> | </span></div>
                                    <div class="cm-total float-xs-left"><span class="text-muted">Total Pensiones:</span> S/.{{ $payannouncement[1]['cuotas'] * $payannouncement[1]['rode']  }}</div>
                                    @can('payment')
                                        @if(!$user->vouchers->isEmpty())
                                        <button type="submit" class="btn btn-outline-success label-right float-xs-right">Pagar <span class="btn-label"><i class="ti-arrow-right"></i></span></button>
                                        @endif
                                    @endcan
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="profile" role="tabpanel" aria-expanded="true">
                        
                    </div>
                </div>
            </div>
            <input type="hidden" name="abc" value="{{$user->id}}">
            </form>
        </div>

        <!---    <form action="/payment" method="POST">
                <div class="box box-block bg-white">
                    <table>
                        <thead>
                            <th colspan="2">Lista vouchers</th>
                        </thead>
                        <tbody>
                            <tr>
                                @foreach($user->vouchers as $id=>$voucher)
                                    <td><input type="radio" name="voucher" value="{{$voucher->id}}"></td>
                                    <td>
                                        <a href='{{ asset("storage/".$voucher->path_voucher)}}' target="_blank">
                                            <span class="tag tag-pill tag-success">Voucher</span>
                                        </a> 
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td colspan="2">
                                    @if ($errors->has('voucher'))
                                        <span class="help-block">
                                            <strong>Para realizar pagos, debes seleccioanr un voucher</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>                        
                        </tbody>
                    </table>
                    
                </div>
                <div class="box box-block bg-white">
                    <table>
                        <thead>
                            <th colspan="4">Matrícula</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @if($pay['enroll'])
                                        Pagado
                                    @else
                                        <input type="checkbox" name="chbx_pay_ma"></td>
                                    @endif
                                <td>{{$payannouncement[0]->announcement->name}}</td>
                                <td>{{$payannouncement[0]->rode}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <thead>
                            <tr>
                            <th colspan="3">Pensiones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for ($i = 0; $i < $payannouncement[1]->cuotas; $i++)
                                <tr>
                                    <td>Cuota {{$i + 1}}</td>
                                    <td>{{$payannouncement[1]->rode}}</td>
                                    <td>
                                    @if($i+1 <= $pay['cpension'])
                                        Pagado
                                    @else
                                        sin pagar
                                    @endif 
                                    </td>
                                </tr>
                            @endfor
                            <tr>
                                <td colspan="2">Pagar</td>
                                <td><input type="number" name="inp_pat_pe" min="0" max="{{$payannouncement[1]->cuotas - $pay['cpension'] }}" ></td>
                            </tr>
                            <tr>
                                @if(!empty($user->vouchers))
                                    <td colspan="3"><input type="submit" value="cancelar"></td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </form>-->
        </div>
    </div>
</div>

@endsection