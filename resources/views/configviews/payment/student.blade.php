@extends('layouts.app')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="mt-2 col-md-7 col-sm-12 b-a b-a-seccess  b-a-dashed mb-0-5 p-2 b-a-radius-0-5 ">
                <div class="col-md-12">
                    {!! Form::open(
                            array(
                                'url' => '/voucher', 
                                'files' => true)) !!}
                    <h5>Subir nuevo Voucher</h5>
                    <button type="submit" class="btn btn-success mb-1 label-right float-xs-right">Guardar <span class="btn-label"><i class="ti-arrow-up"></i></span></button>
                    <input type="file" id="input-file-now-custom-1" name="path_voucher" class="dropify"  />
                    {!! Form::close() !!}
                </div>
                
                <div class="voucherList col-md-12 mt-2">
                    <h5>Mis Vouchers({{$payannouncement[0]->announcement->name}})</h5>
                    <ul>
                        @if(!$pay['vouchers']->isEmpty())
                            @foreach($pay['vouchers'] as $id=>$val)
                                <li>
                                    <div class="imgContent">
                                        <img src="/img/payment/pago2.jpg">
                                    </div>
                                    
                                    <div class="infoContent">
                                        <span class="tag tag-default"> {{ $val->status == 1? 'Nuevo':''}} Voucher {{$id+1}}</span>
                                        <p>{{\Carbon\Carbon::parse($val->created_at)->toFormattedDateString()}}</p>
                                        <a href='{{ asset("storage/".$val->path_voucher)}}' class="btn btn-info" target="_blank">
                                            <span >Ver PDF</span>
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li>
                                <div class="sinVoucher">
                                    <img src="/img/payment/pago3.jpg">
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>

                

        </div>
        <div class="col-md-5">                         
            <div class="" >
                <div class=" mt-0 mb-2">

                </div>

                <ul class="nav nav-tabs " role="tablist">
                    @foreach($preenrollments as $pre)
                    <li class="nav-item">
                        <a class="nav-link {{ $payannouncement[0]->announcement_id == $pre->announcement_id? 'active': ''  }}" href='{{ asset("/my-payment/".$pre->announcements->slug) }}'  >{{$pre->announcements->name}}</a>
                    </li>
                    @endforeach
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="false">
                        <div class="box bg-white cart-mini">
                            <div class="cm-title">LISTA DE PAGOS </div>
                            <table class="positions table mb-0">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Concepto</th>
                                        <th>Monto </th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td><a class="text-black" href="#"><span>Matrícula</span></a></td>
                                        <td>{{$payannouncement[0]->rode}}</td>
                                        <td>
                                        @if($pay['enroll'])
                                            <span class="tag tag-pill tag-info">Pagado</span>
                                        @else
                                            <span class="tag tag-pill tag-danger">No Pagado</span>
                                        @endif
                                        </td>
                                    </tr>
                                @for ($i = 1; $i <= $payannouncement[1]->cuotas; $i++)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td><a class="text-black" href="#"><span>Cuota {{$i}}</span></a></td>
                                        <td>{{ $payannouncement[1]->rode }}</td>
                                        <td>
                                        @if($i<=$pay['cpension'])
                                            <span class="tag tag-pill tag-info">Pagado</span>
                                        @else
                                            <span class="tag tag-pill tag-danger">No Pagado</span>
                                        @endif
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                            
                            <div class="card-block">
                                <div class="clearfix">
                                    <div class="cm-total float-xs-left"><span class="text-muted">Total Matrícula:</span> S/.{{ $payannouncement[0]['cuotas'] * $payannouncement[0]['rode']  }}</div>
                                    <div class="cm-total float-xs-left"><span> | </span></div>
                                    <div class="cm-total float-xs-left"><span class="text-muted">Total Pensiones:</span> S/.{{ $payannouncement[1]['cuotas'] * $payannouncement[1]['rode']  }}</div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="profile" role="tabpanel" aria-expanded="true">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-2 col-md-8 col-sm-12">
        </div>
    </div>
</div>

@endsection