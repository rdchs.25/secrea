@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Asignaciones de Docentes</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Asignaciones de Docentes</a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/assignment-teacher/create" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class=" ti-plus"></i></span>
                    Nueva Asignación
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Asignaciones de Docentes</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás asignar cursos o materias a los docentes </p>
                             </div>
                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Docente</th>
                                   <th>Grado</th>
                                   <th>Sección</th>
                                   <th>Asignatura</th>
                                   <th>Nivel Académico</th>
                                   <th>Opciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($assignments as $id => $assignment)
                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $assignment->user->name}}
                                        </td>                                       
                                        <td>
                                            {{ $assignment->grade->name}}
                                        </td> 
                                        <td>
                                            {{ $assignment->grade->section}}
                                       </td>
                                        <td>
                                            {{ $assignment->subject->name}}
                                       </td>
                                       <td>
                                            {{ $assignment->grade->academicLevels->name}}
                                       </td>
                                       <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Opciones
                                              </button>
                                              <div class="dropdown-menu">
                                                <!-- <a class="dropdown-item" href="/announcement/{{ $assignment->id }}">Ver</a> -->
                                                <a class="dropdown-item" href="/assignment-teacher/{{ $assignment->id }}/edit">Editar</a>
                                                <a class="dropdown-item" href="/assignment-teacher/{{ $assignment->id }}/delete">Eliminar</a>
                                              </div>
                                            </div>
                                       </td>
                                    </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
