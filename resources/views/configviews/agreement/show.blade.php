@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">

            <div id="cabeceraFlotante">
                <h4>Convenios</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Convenios </a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/agreement" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="ti-search"></i></span>
                    Ver Convenios
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Convenios</h4>
                             </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre del Convenio</label>
                                <div class="col-md-4">
                                    <input class="form-control input-md" type="text" value="{{ $agreement->name }}" disabled>
                                </div>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Opciones
                                  </button>
                                  <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/agreement/{{ $agreement->slug }}/edit">Editar</a>
                                    <a class="dropdown-item" href="/agreement/{{ $agreement->slug }}/delete">Eliminar</a>
                                  </div>
                                </div>
                            </div>

                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
