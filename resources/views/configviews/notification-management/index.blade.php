@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Notificaciones</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Notificaciones </a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2"></div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Notificaciones</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás enviar notificaciones generales a los usuarios. </p>
                             </div>
                        </div>
                   </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="fa fa-envelope-o"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Enviar Correo a Inactivos</h6>
                        <h1 class="mb-1">
                            <a href="/notification-management/inactivity-mail" title="Enviar Correo">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
