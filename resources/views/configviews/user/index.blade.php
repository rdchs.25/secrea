@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Usuarios</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Usuarios </a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/user/create" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class=" ti-plus"></i></span>
                    Nuevo Usuario
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Usuarios</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás editar, visualizar y eliminar información de los Usuarios. </p>
                             </div>
                        </div>
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Nombre</th>
                                   <th>Rol</th>
                                   <th>Convenio</th>
                                   <th>Última Conexión</th>
                                   @can('documentary-management')
                                   <th>Documentos</th>
                                   @endcan
                                   <th>Opciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($users as $id => $user)
                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $user->name}}
                                        </td>                                        
                                        <td>
                                          @foreach($user->getRoles() as $roles)
                                            <span class="tag tag-success">{{ $roles }}</span>
                                          @endforeach        
                                        </td>
                                        <td>
                                            {{ $user->userAgreements->agreement->name or 'Ninguno'}}
                                        </td>
                                        <td>
                                            {{ $user->last_login != null ? \Carbon\Carbon::parse($user->last_login)->diffForHumans() : 'Ninguna' }}
                                        </td>
                                        @can('documentary-management')
                                        <td>
                                            @if($user->documents)
                                                <a href='{{ "/user/" . $user->id . "/documents" }}'>Ver Documentos</a>
                                            @else
                                              Ninguno
                                            @endif
                                        </td>
                                        @endcan
                                        <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Opciones
                                              </button>
                                              <div class="dropdown-menu">
                                                <a class="dropdown-item" href="/user/{{ $user->id }}/edit">Editar</a>
                                                <a class="dropdown-item" href="/user/change-status/{{ $user->id }}">{{ $user->status == 1 ? 'Bloquear' : 'Desbloquear' }}</a>                      
                                              </div>
                                            </div>
                                       </td>
                                    </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
