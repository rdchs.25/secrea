@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Mensajes con Soporte técnico</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Mensajes con Soporte técnico</a></li>
                    </ol>
                </nav>
            </div> 

             <div id="mFloatBox2"></div>
    
            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Mensajes con Soporte técnico</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás visualizar los mensaje de los usuario que presente un problema relacionado a la plataforma. </p>
                             </div>
                        </div>
                        @foreach($messages as $message)
						<div class="{{ $message->attend == false ? 'Message Message--green' : 'Message Message--light-green' }}">
							<div class="Message-icon">
								<i class="{{ $message->attend == false ? 'fa fa-envelope-o' : 'fa fa-envelope-open-o' }}"></i>
							</div>
							<div class="Message-body">
								<p>
                                    <i class="ti-user" aria-hidden="true"></i>
                                    {{ $message->users->name }}
                                    @foreach($message->users->getRoles() as $roles)
                                        ({{ $roles }})
                                    @endforeach
                                </p>
								<p>
                                    <i class="ti-mobile" aria-hidden="true"></i> 
                                    {{ $message->users->profiles->phone or 'No registrado' }}
                                </p>
                                <p>
                                    {{ \Carbon\Carbon::parse($message->created_at)->diffForHumans() }} - 
                                    {{ $message->message }}
                                </p>
							</div>
							@if($message->attend == false)
								<a class="Message-close js-messageClose" title="Marcar como atendido" href="/support-message/{{$message->id}}/attend"><i class="fa fa-check"></i></a>
							@endif
						</div>
						@endforeach


                   </div>

                </div>
            </div>

        </div>
    </div>
@endsection
