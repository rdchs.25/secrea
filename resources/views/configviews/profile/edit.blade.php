@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">

            <div id="cabeceraFlotante">
                <h4>Mi Perfil</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Perfil </a></li>
                    </ol>
                </nav>
            </div> 
            
            <div id="mFloatBox2"></div>

            

            <div class="mt-2 col-md-12 col-sm-12">
                @if(is_null($profile))
                    <div class="alert alert-warning-fill alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>¡Recuerda actualizar tus datos!</strong> Eso nos ayudará a mantenernos en contacto contigo.
                    </div>
                @endif
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Mi Perfil</h4>
                             </div>
                            @if(is_null($profile))
                                {!! Form::open(
                                    array(
                                        'url' => '/profile', 
                                        'class' => 't-3 form-material material-primary', 
                                        'files' => true)) !!}
                            @else
                                {!! Form::model($profile ,['method' => 'PATCH' ,'action' => ['ProfileController@update', $profile->id], 'class' => 'mt-3 form-material material-primary','files'=>true]) !!}

                                {!! Form::model($profile ,['method' => 'PATCH' ,'action' => ['ProfileController@update', $profile->id], 'class' => 'mt-3 form-material material-primary', 'files' => true]) !!}

                            @endif
                                  @include('forms._profile_form', ['submitButtonText' => 'Actualizar Perfil'])
                                {!! Form::close() !!}

                                @include('errors.list')
                            

                        </div>

                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
