@extends('layouts.app')

@section('content')
<div class="content-area pb-1">
    <div class="profile-header mb-1">
        <div class="profile-header-cover img-cover" style="background-image: url(img/photos-1/1.jpg);"></div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-3 ">
                <div class="card profile-card">
                    <div class="profile-avatar">
                        <span class="avatar box-32">
                            @if(empty(Auth::user()->profiles->path_profile_image))
                                <img src="/img/avatars/avatar.jpg" alt="">
                            @else
                                <img src="/storage/{{ Auth::user()->profiles->path_profile_image }}" alt="">
                            @endif
                        </span>
                    </div>
                    <div class="card-block">
                        <h4 class="mb-0-25">{{ Auth::user()->name }}</h4>
                            @foreach(Auth::user()->getRoles() as $roles)
                                <h6 class="text-uppercase">{{ $roles }}</h6>
                            @endforeach                        
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <a href="/profile/{{Auth::user()->id}}/edit" class="btn btn-info btn-lg label-right b-a-0 waves-effect waves-light" role="button" aria-disabled="true">
                            <span class="btn-label"><i class=" ti-plus"></i></span>
                            Editar Perfil
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-8 col-md-9 ">
                <div class="card mb-0">
                    
                    <div class="tab-content">

                        <div class="tab-pane active card-block pt-2" id="miPerfil" role="tabpanel" aria-expanded="true">
                           
                            <div class="col-md-12 pt-2">                            
                                <div class="box box-block bg-white tile tile-4 ">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="t-content text-xs-left">
                                            @foreach(Auth::user()->getRoles() as $roles)
                                                <h6 class="text-uppercase">{{ $roles }}</h6>
                                            @endforeach
                                            <h3 class="mb-0">{{ Auth::user()->name }}</h3>
                                        </div>
                                    </div>   
                                </div>
                            </div>

                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@stop