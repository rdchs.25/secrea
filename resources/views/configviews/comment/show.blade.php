@extends('layouts.app')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <h4>Historial de Atención: {{$announcement->name}}</h4>
        <nav class="navbar navbar-light bg-white b-a ">
            <ol class="breadcrumb no-bg mb-0">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item"><a href="cursosEstudiante.html">Mis cursos</a></li>
                <li class="breadcrumb-item active">Historial de Atención </li>
            </ol>
        </nav>
        <div class="mt-2 col-md-4 col-sm-12">
            <div class="box bg-white user-5">
                @if(!empty($userBefore))
                <div class="float-md-left mb-2">
                    <button type="button" class="btn  btn-success btn-circle  waves-light">
                        <a href='{{ asset("announcement/".$announcement->slug."/comment/".$userBefore) }}' style="color:white"><i class="ti-arrow-left"></i></a>
                    </button>
                </div>
                @endif
                @if(!empty($userAfter))
                <div class="float-md-right mb-2">
                    <button type="button" class="btn2 btn-success btn-circle  waves-light ">
                        <a href='{{ asset("announcement/".$announcement->slug."/comment/".$userAfter) }}' style="color:white"><i class="ti-arrow-right"></i></a>
                    </button>
                </div>
                @endif
                <div class="u-content">
                    <div class="mb-2 text-sm-center text-md-center">
                        <div class="img-div2">
                        @if(empty($user->users->profiles->path_profile_image))
                            <img src="/img/avatars/avatar.jpg" alt="">
                        @else   
                            <img src="/storage/{{ $user->users->profiles->path_profile_image}}" alt="">
                        @endif
                        </div>
                        <i class="status bg-success bottom right"></i>
                    </div>
                    <h2><a class="text-black" href="#">{{$user->users->name}}</a></h2>
                    <p class="text-muted mb-1">Registrado</p>
                    @if(empty($user->users->documents))
                        <span class="tag tag-pill tag-danger">Sin documentos</span>
                    @else
                        @if(empty($user->users->documents->path_dni))
                            <span class="tag tag-pill tag-danger">DNI</span>
                        @else 
                            <a href='{{ asset("storage/".$user->users->documents->path_dni) }}' target="_blank">
                            <span class="tag tag-pill tag-success">DNI</span>
                            </a> 
                        @endif 
                        @if(empty($user->users->documents->path_study_certificate))
                            <span class="tag tag-pill tag-danger">CERT</span>
                        @else 
                            <a href='{{ asset("storage/".$user->users->documents->path_study_certificate)}}' target="_blank">
                            <span class="tag tag-pill tag-success">CERT</span>
                            </a>   
                        @endif 
                        @if(empty($user->users->documents->path_voucher))
                            <span class="tag tag-pill tag-danger">VOUCHER</span>
                        @else 
                            <a href='{{ asset("storage/".$user->users->documents->path_voucher)}}' target="_blank">
                            <span class="tag tag-pill tag-success">VOUCHER</span>
                            </a>
                        @endif                                                                                                     
                    @endif
                    <table class="table  mb-0" style="margin-top:10px">
                        <tbody>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Grado:</td>
                                <td style="text-align: left;" >{{ $user->users->assignmentStudent[0]->grade->name }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Edad:</td>
                                <td style="text-align: left;">{{ empty($user->users->profiles->birth_date)? '--': \Carbon\Carbon::parse( $user->users->profiles->birth_date )->age  }}</td>
                            </tr>
                            @if(!empty($user->users->profiles))
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Teléfono:</td>
                                <td style="text-align: left;">{{$user->users->profiles->phone}}</td>
                            </tr>
                            @endif
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Correo:</td>
                                <td style="text-align: left;">{{$user->users->email}}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Último login:</td>
                                <td style="text-align: left;">{{ $user->last_login != null ? \Carbon\Carbon::parse($user->users->last_login)->diffForHumans() : 'Ninguna' }}</td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight:bold;">Confirmación e-mail:</td>
                                <td style="text-align: left;">{{ $user->users->confirmed ? 'SI' : 'NO' }}</td>
                            </tr>
                        </tbody>
                    </table> 
                </div>
            </div>
            
            <div class="box bg-white user-5">
                @if(is_null($document))
                    {!! Form::open(
                        array(
                            'url' => '/document', 
                            'class' => 't-3 form-material material-primary', 
                            'files' => true)) !!}
                @else
                    {!! Form::model($document ,['method' => 'PATCH' ,'action' => ['DocumentController@update', $document->id], 'class' => ' form-material material-primary','files' => true]) !!}
                @endif
                    
                    <div id="campos">
                        @if(empty($user->users->documents))
                            <span class="tag tag-pill tag-danger">Sin documentos</span>
                        @else
                            @if(empty($user->users->documents->path_dni))
                                <span class="tag tag-pill tag-danger">DNI</span>
                            @else 
                                <a href='{{ asset("storage/".$user->users->documents->path_dni) }}' target="_blank">
                                <span class="tag tag-pill tag-success">DNI</span>
                                </a> 
                            @endif 
                            @if(empty($user->users->documents->path_study_certificate))
                                <span class="tag tag-pill tag-danger">CERT</span>
                            @else 
                                <a href='{{ asset("storage/".$user->users->documents->path_study_certificate)}}' target="_blank">
                                <span class="tag tag-pill tag-success">CERT</span>
                                </a>   
                            @endif 
                            @if(empty($user->users->documents->path_voucher))
                                <span class="tag tag-pill tag-danger">VOUCHER</span>
                            @else 
                                <a href='{{ asset("storage/".$user->users->documents->path_voucher)}}' target="_blank">
                                <span class="tag tag-pill tag-success">VOUCHER</span>
                                </a>
                            @endif                                                                                                     
                        @endif
                        <hr>
                        <div class="form-group row">
                            <h5>Subir DNI</h5>
                            <div class="col-md-12">  
                                {!! Form::file('path_dni', null, ['class' => 'form-control input-md']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-md-12">
                            <h5>Subir Certificado</h5> 
                                {!! Form::file('path_study_certificate', null, ['class' => 'form-control input-md']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-md-12">
                            <h5>Subir Voucher</h5>
                            {!! Form::file('path_voucher', null, ['class' => 'form-control input-md']) !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <br>
                            {!! Form::submit("Actualizar Documentos", ['class' => 'ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-light', 'onclick' => 'this.disabled=true;this.form.submit();']) !!}
                        </div>

                    </div>

                {!! Form::close() !!}

            </div>
            
        </div>
        <div class="mt-2 col-md-8 col-sm-12">
            <form class="card write-something" method="post" action="/comment">
                <textarea name="body" id="body" placeholder="Ingresa tu comentario"></textarea>
                <input type="hidden" name="abc" value="{{$user->user_id}}">
                <input type="hidden" name="slug" value="{{$announcement->slug}}">
                <div class="card-footer">
                    <div class="clearfix">
                        <div class="float-xs-left">
                            <div class="btn-group" role="group">
                                <select name="status" id="status" class="btn btn-outline-default btn-rounded waves-effect dropdown-toggle">
                                    <option value="Otro">Otro</option>
                                    <option value="Soporte">Soporte</option>
                                    <option value="Fuera de servicio">Fuera de servicio</option>
                                    <option value="No interesado">No interesado</option>
                                    <option value="Faltan documentos">Faltan documentos</option>
                                    <option value="Confirmará correo">Confirmará correo</option>
                                    <option value="Apagado">Número equivocado</option>
                                </select>
                                <!--<button type="button" class="btn btn-outline-success btn-rounded waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Selecciona un tag
                                </button>
                                <div class="dropdown-menu">
                                    <a class="pl-1 dropdown-item active" href="#">Soporte</a>
                                    <a class="pl-1 dropdown-item" href="#">Otro</a>
                                    <a class="pl-1 dropdown-item" href="#">Apagado</a>
                                    <a class="pl-1 dropdown-item" href="#">Sí contesto</a>
                                    <a class="pl-1 dropdown-item" href="#">No interesado</a>
                                    <a class="pl-1 dropdown-item" href="#">Fuera de servicio</a>
                                    <a class="pl-1 dropdown-item" href="#" >Volver a  llamar</a>
                                </div>-->
                            </div>
                        </div>
                        <div class="float-xs-right">
                            <button type="submit" class="btn btn-success btn-rounded">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="box box-block bg-white">
                <div class="timeline timeline-left">
                    @foreach($comments as $id => $comment)
                        <div class="tl-item">
                            <div class="tl-wrap b-a-success">
                                <div class="tl-content box box-block bg-white">
                                    <span class="arrow left b-a-white"></span>
                                    {{$comment->body}}
                                    <h6><span class="ti-user"></span> {{$comment->author->name}}</h6>
                                    <span class="tag tag-pill tag-info"> {{$comment->status}}</span>
                                </div>
                                <div class="tl-date text-muted">{{\Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</div>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection