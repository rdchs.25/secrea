@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Control de Documentos</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Control de Documentos</a></li>
                    </ol>
                </nav>
            </div> 


            <div id="mFloatBox2"></div>
    
            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Ciclos Académicos</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás visualizar los Ciclos Académicos y gestionar los documentos de los estudiantes matriculados. </p>
                             </div>
                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Nombre</th>
                                   <th>Estatus</th>
                                   <th>Duración</th>
                                   <th>Opciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($announcements as $id => $announcement)
                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $announcement->name}}
                                        </td>
                                        <td>
                                            {{ ($announcement->status==1) ? "Activa" : ($announcement->status == 0? "En Inscripción":"Inactiva")}}
                                        </td>
                                        <td>
                                            {{ $announcement->size_time." ".$announcement->academicTime->name.'\'s'}}
                                        </td>
                                        <td>
	                                        <div class="btn-group">
	                                          <a href='/documentary-management/{{ $announcement->slug }}' type="button" class="btn btn-info">
	                                            Matriculados
	                                          </a>
	                                        </div>
                                       </td>
                                    </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
