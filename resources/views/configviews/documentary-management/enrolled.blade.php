@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Convocatorias (Matriculados)</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Matriculados </a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2">

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                	<div class="tab-content">
                    	<div class="mt-2  pv-content">
                        	<div class="pv-title">
                            	<h4 class="">Convocatoria: {{ $announcement->name }}</h4>
                            	<p class="font-90 text-muted mb-1"> Lista de matriculados</p>
                        	</div>
                    	</div>
                    	<div class="table-responsive">
	                        <table class="table table-striped table-bordered dataTable" id="example">
	                            <thead>
	                                <tr>
	                                   <th>#</th>
	                                   <th>Nombre</th>
	                                   <th>Email</th>
	                                   <th>Grado</th>
	                                   <th>Teléfono</th>
	                                   <th>Documentos</th>
	                                   <th>Opciones</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                @foreach($filtered as $id => $userassignment)
	                                    <tr>
	                                        <td>
	                                            {{ $id + 1}}
	                                        </td>
	                                        <td>
	                                            {{ $userassignment->user->name }}
	                                        </td>
	                                        <td>
	                                            {{ $userassignment->user->email }}
	                                        </td>
	                                        <td>
	                                        	{{ $userassignment->assignmentStudent->grade->name }}
	                                        </td>
	                                        <td>
	                                            {{ $userassignment->user->profiles->phone or 'No registrado' }}
	                                        </td>
	                                        <td>
		                                       	@if($userassignment->user->documents)
	                                                <a href='{{ "/user/" . $userassignment->user->id . "/documents/" }}'>Ver Documentos</a>
	                                            @else
	                                              No registrados
	                                            @endif
	                                        </td>
	                                        <td>
		                                        <div class="btn-group">
		                                          <a href='/documentary-management/change-status/{{$userassignment->user->id}}' type="button" class="btn btn-info">
		                                            {{ $userassignment->user->status == 1 ? 'Bloquear' : 'Desbloquear' }}
		                                          </a>
		                                        </div>
	                                        </td>
	                                    </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
