@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">

            <div id="cabeceraFlotante">
                <h4>Roles de Usuarios</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Roles </a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/role" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="ti-search"></i></span>
                    Ver Roles
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Roles de Usuarios</h4>
                             </div>

                            {!! Form::model($role ,['method' => 'post' ,'action' => ['RoleController@addPermissions', $role->id], 'class' => 'mt-3 form-material material-primary']) !!}
                              @include('forms._assign-role_form', ['submitButtonText' => 'Asignar Permiso'])
                            {!! Form::close() !!}

                        </div>
                        <div class="pv-title">
                            <h4 class="">Permisos Asignados</h4>
                            <p class="font-90 text-muted mb-1">A continuación podrás editar, visualizar y eliminar información de los Roles de Usuarios. </p>
                        </div>
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Nombre</th>
                                   <th>Slug</th>
                                   <th>Descripción</th>
                                   <th>Quitar Permiso</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($role->permissions as $id => $permission)
                                <tr>
                                    <td>
                                        {{ $id+1 }}
                                    </td>
                                    <td>
                                        {{ $permission->name }}
                                    </td>
                                    <td>
                                        {{ $permission->slug }} 
                                    </td>
                                    <td>
                                        {{ $permission->description }} 
                                    </td>
                                    <td>
                                        <a href="{{ '/role/remove/'.$role->id.'/'.$permission->id }}" class="ml-2 btn btn-info btn-lg b-a-0 waves-effect waves-lightt" role="button" aria-disabled="true">
                                            Quitar Permiso
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>

                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
