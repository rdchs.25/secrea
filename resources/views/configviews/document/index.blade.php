@extends('layouts.app')

@section('content')  

    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Documentos</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Documentos</a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2"></div>

            <div class="mt-2 col-md-12 col-sm-12">
                @if(is_null($document))
                    <div class="alert alert-warning-fill alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>¡Recuerda subir tus documentos!</strong>
                    </div>
                @endif
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                            <div class="pv-title">
                                <h4 class="">Gestión de Documentos</h4>
                            </div>
                            @if(is_null($document))
                                {!! Form::open(
                                    array(
                                        'url' => '/document', 
                                        'class' => 't-3 form-material material-primary', 
                                        'files' => true)) !!}
                            @else
                                {!! Form::model($document ,['method' => 'PATCH' ,'action' => ['DocumentController@update', $document->id], 'class' => 'mt-3 form-material material-primary','files' => true]) !!}
                            @endif
                                
                            @include('forms._document_form', ['submitButtonText' => 'Actualizar Perfil'])
                            {!! Form::close() !!}

                            @include('errors.list')

                        </div>

                   </div>
                </div>
            
            </div>

        </div>
    </div>
@endsection