@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Mensajes al Soporte</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Notificaciones </a></li>
                    </ol>
                </nav>
            </div> 

             <div id="mFloatBox2"></div>
    
            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Notificaciones</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás visualizar sus notificaciones. </p>
                             </div>
                        </div>
                        @foreach($notifications as $id => $notification)
						<div class="Message Message--green">
							<div class="Message-icon">
								<i class="ti-announcement"></i>
							</div>
							<div class="Message-body">
								<p>
                                    <i class="ti-pin-alt" aria-hidden="true"></i>
                                    {{ $notification->title }}
                                </p>
                                <p>
                                    {{ $notification->message }}
                                </p>
							</div>
						</div>
                        @endforeach
                   </div>

                </div>
            </div>

        </div>
    </div>
@endsection
