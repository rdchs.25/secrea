@extends('layouts.app')

@section('content')  
    <div class="content-area py-1" id="app">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Analizando curso por alumno</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2"></div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                    <div class="tab-content">
                        <div class="mt-2  pv-content">
                            <div class="pv-title">
                                <h4 class="">Json del curso de alumno</h4>
                                <p class="font-90 text-muted mb-1"></p>
                            </div>
                        </div>

                        <json data="{{$user_assignement->content}}" id="{{$user_assignement->id}}"></json>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
