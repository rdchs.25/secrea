@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Backups</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Backups</a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/backup-export" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="fa fa-floppy-o"></i></span>
                    Realizar Backup
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Backups</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás visualizar los backups realizados anteriormente</p>
                             </div>
                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Nombre</th>
                                   <th>Descargar</th>
                                </tr>
                             </thead>
                             <tbody>
                             @foreach($backups as $id => $backup)
                            
                                @php
                                    $path='/storage/backup/'. $backup['basename'];
                                @endphp

                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $backup['basename'] }}
                                        </td>
                                        <td>
                                            <a href="{{ $path }}" download="{{ $backup['basename'] }}">Descargar</a>
                                        </td>
                                    </tr>
                            @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
