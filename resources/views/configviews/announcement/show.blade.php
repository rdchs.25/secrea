@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4> Ciclo académico (Pre Inscritos)</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Pre Inscripciones </a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2">
            <form action="/sms/send" method="POST">
            @if(Auth::user()->can('messages')  )
            <div class="col-md-12">
                <div class="box box-block bg-white">
                    <div>
                        <div class="pv-title">
                            <h4 class=""> Enviar mensajes</h4>
                            <p class="font-90 text-muted mb-1"> Opciones para SMS </p>
                        </div>
                        @if($sms->isEmpty())
                            <p class="font-90 text-muted mb-1"> Registra tus mensajes personalizados. </p>
                        @else 
                            @can('messages-management')
                            <select class="custom-select" name="users_sms" id="users_sms">
                                <option value="0">Enviar a: </option>
                                <option value="1">Usuarios sin dni</option>
                                <option value="2">Usuarios sin matricula</option>
                                <option value="3">Usuarios sin certificado</option>
                                <option value="4">Todos</option>
                            </select>
                            @endcan
                            <select class="custom-select" name="action_sms" id="action_sms">
                            @foreach($sms as $val)
                                    <option value="{{$val->id}}">{{$val->tag}}</option>
                            @endforeach
                            </select>
                            <input type="submit" value="Enviar sms" class="btn btn-success btn-rounded">
                        @endif
                        
                    </div>
                </div>
            </div>
            @endif
            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                      <div class="mt-2  pv-content">
                           <div class="pv-title">
                              <h4 class=""> Ciclo académico: {{ $announcement->name }}</h4>
                              <p class="font-90 text-muted mb-1"> Lista de pre inscritos </p>
                           </div>
                      </div>
                      	<div class="table-responsive">
	                        <table class="table table-striped table-bordered dataTable" id="example">
	                            <thead>
	                                <tr>
                                        @if( Auth::user()->can('messages')  )
                                        <th></th>
                                        @endif 
	                                   <th>#</th>
	                                   <th>Nombre</th>
	                                   <th>Email</th>
	                                   <th>Teléfono</th>
	                                   <th>Confirmación de Email</th>
	                                   <th>Edad</th>
									   <th>Grado</th>
									   <th>Tag - reciente</th>
	                                   <th>Fecha</th>
	                                   <th>Documentos</th>
	                                   @can('comment')
                                       <th>Historial Atención</th>
                                       @endcan
                                       @can('payment')
                                       <th>Pagos</th>
                                       @endcan
									   @can('enroll')
									   <th>Matricular</th>
									   @endcan
                                       @role('admin')
                                       <th>Opciones</th>
                                        @endrole
                                    </tr>
	                            </thead>
	                            <tbody>
	                                @foreach($preEnrollments as $id => $preEnrollment)
	                                    <tr>
                                            @if(Auth::user()->can('messages')  )
                                            <td>
                                                @if(!empty($preEnrollment->users->profiles->phone))
                                                    <input type="checkbox" 
                                                            data-dni="{{ empty($preEnrollment->users->documents->path_dni)?'false':'true' }}" 
                                                            data-vou="{{ empty($preEnrollment->voucher->path_voucher)?'false':'true' }}" 
                                                            data-cer="{{ empty($preEnrollment->users->documents->path_study_certificate)?'false':'true' }}" 
                                                            class="chbxuser" 
                                                            id="chbxuser_{{$preEnrollment->users->id}}"
                                                            name="users[]" 
                                                            value="{{$preEnrollment->users->id}}" >
                                                @endif
                                            </td>
	                                        @endif
                                            <td>
	                                            {{ $id + 1}}
	                                        </td>
	                                        <td>
	                                            {{ $preEnrollment->users->name }}
	                                        </td>
	                                        <td>
	                                            {{ $preEnrollment->users->email }}
	                                        </td>
	                                        <td>
	                                            {{ $preEnrollment->users->profiles->phone or 'No registrado'}}
	                                        </td>
	                                        <td>
                                                @if($preEnrollment->users->confirmed)
                                                    SI 
                                                @else
                                                    <a href='/user/{{$preEnrollment->users->id}}/confirmed'>NO</a>
                                                @endif
	                                        </td>
	                                        <td>
                                                {{ empty($preEnrollment->users->profiles->birth_date)? '--': \Carbon\Carbon::parse( $preEnrollment->users->profiles->birth_date )->age  }}
                                            </td>
											<td>
												{{ $preEnrollment->users->assignmentStudent[0]->grade->name }}
											</td>
											<td>
												{{ empty($preEnrollment->tag['status'])? '--':  $preEnrollment->tag['status']}}
											</td>
	                                        <td>
	                                        	 {{\Carbon\Carbon::parse($preEnrollment->created_at)->diffForHumans()}}
	                                        </td>  
	                                        <td>
                                                @if(!empty($preEnrollment->users->documents))
                                                    @if(!empty($preEnrollment->users->documents->path_dni))
                                                        <a href='{{ asset("storage/" . $preEnrollment->users->documents->path_dni) }}' target="_blank">
                                                            <span class="tag tag-pill tag-success">DNI</span>
                                                        </a>
                                                    @endif
                                                    @if(!empty($preEnrollment->users->documents->path_study_certificate))
                                                    <a href='{{ asset("storage/" . $preEnrollment->users->documents->path_study_certificate) }}' target="_blank">
                                                        <span class="tag tag-pill tag-success">CER</span>
                                                    </a>
                                                    @endif
                                                    @if(!empty($preEnrollment->voucher))
                                                    <a href='{{ asset("storage/" . $preEnrollment->voucher->path_voucher) }}' target="_blank">
                                                        <span class="tag tag-pill tag-{{$preEnrollment->voucher->status?'warning':'success'}}">VOU</span>
                                                    </a>
                                                    @endif
                                                @endif
                                            </td>   
	                                        @can('comment')
											<td>
                                                <div class="btn-group">
                                                <a href='{{ asset("announcement/".$announcement->slug."/comment/". $preEnrollment->users->id) }}' type="button" class="btn btn-info">
                                                    Seguimiento
                                                </a>
                                                </div>
                                            </td>
                                            @endcan
                                            @can('payment')
                                            <td>
                                                <div class="btn-group">
                                                <a href='{{ asset("announcement/".$announcement->slug."/payment/". $preEnrollment->users->id) }}' type="button" class="btn btn-info">
                                                    Pagos
                                                </a>
                                                </div>
                                            </td>
                                            @endcan
											@can('enroll')
                                            <td>
                                                <div class="btn-group">
                                                <a href='{{ asset("userassignment/".$preEnrollment->users->id."/announcement/". $announcement->slug) }}' type="button" class="btn btn-info">
                                                    @if($preEnrollment->enrollment)
                                                        Matriculado
                                                    @else    
                                                        Matricular
                                                    @endif
                                                </a>
                                                </div>
                                            </td>
                                            @endcan
                                            @role('admin')
                                            <td>
                                                <div class="btn-group">
                                                <a href='{{ asset("user-registrado/".$preEnrollment->id."/delete") }}' type="button" class="btn btn-info">
                                                    Eliminar
                                                </a>
                                                </div>
                                            </td>
                                            @endrole
	                                    </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
                   </div>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection

