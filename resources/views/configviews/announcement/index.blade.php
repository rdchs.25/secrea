@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Ciclos académicos</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active"> Ciclos académicos</a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/announcement/create" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class=" ti-plus"></i></span>
                    Nuevo Ciclo académico
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Ciclo académico</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás editar, visualizar y eliminar información de los Ciclos académicos. </p>
                             </div>
                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Nombre</th>
                                   <th>Estatus</th>
                                   <th>Duración</th>
                                   <th>Opciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($announcements as $id => $announcement)
                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $announcement->name}}
                                        </td>
                                        <td>
                                            {{ ($announcement->status==1) ? "Activa" : ($announcement->status == 0? "En Inscripción":"Inactiva")}}
                                        </td>
                                        <td>
                                            {{ $announcement->size_time." ".$announcement->academicTime->name.'\'s'}}
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Opciones
                                              </button>
                                              <div class="dropdown-menu">
                                                <!-- <a class="dropdown-item" href="/announcement/{{ $announcement->slug }}">Ver</a> -->
                                                <a class="dropdown-item" href="/announcement/{{ $announcement->slug }}">Ver Pre Inscritos</a>
                                                <a class="dropdown-item" href="/announcement/{{ $announcement->slug }}/enrolled">Ver Matriculados</a>
                                                <a class="dropdown-item" href="/announcement/{{ $announcement->slug }}/edit">Editar</a>
                                                <a class="dropdown-item" href="/announcement/{{ $announcement->slug }}/delete">Eliminar</a>
                                              </div>
                                            </div>
                                       </td>
                                    </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
