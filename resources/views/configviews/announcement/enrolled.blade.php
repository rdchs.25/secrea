@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4> Ciclos académicos (Matriculados)</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Matriculados </a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2">

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                      <div class="mt-2  pv-content">
                           <div class="pv-title">
                              <h4 class=""> Ciclo académico: {{ $announcement->name }}</h4>
                              <p class="font-90 text-muted mb-1"> Lista de matriculados</p>
                           </div>
                      </div>
                      	<div class="table-responsive">
	                        <table class="table table-striped table-bordered dataTable" id="example">
	                            <thead>
	                                <tr>
	                                   <th>#</th>
	                                   <th>Nombre</th>
	                                   <th>Email</th>
	                                   <th>Grado</th>
									   <th>Teléfono</th>
									   <th>Confirmación</th>
									   @role('admin')
									   <th>Cursos</th>
									   @endrole
	                                   <th>Opciones</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                @foreach($filtered as $id => $userassignment)
	                                    <tr>
	                                        <td>
	                                            {{ $id + 1}}
	                                        </td>
	                                        <td>
	                                            {{ $userassignment->user->name }}
	                                        </td>
	                                        <td>
	                                            {{ $userassignment->user->email }}
	                                        </td>
	                                        <td>
	                                        	{{ $userassignment->assignmentStudent->grade->name }}
	                                        </td>
	                                        <td>
	                                            {{ $userassignment->user->profiles->phone or 'No registrado' }}
											</td>
											<td>
												{{ $userassignment->user->confirmed ? 'YES' : 'NO' }}
											</td>
											@role('admin')
											<td>
												<a href="/userassignment/{{$userassignment->id}}/content/" target="_blank">1</a>
												<a href="/userassignment/{{$userassignment->id +1}}/content/" target="_blank">2</a>
												<a href="/userassignment/{{$userassignment->id +2}}/content/" target="_blank">3</a>
												<a href="/userassignment/{{$userassignment->id +3}}/content/" target="_blank">4</a>
												<a href="/userassignment/{{$userassignment->id +4}}/content/" target="_blank">5</a>
												<a href="/userassignment/{{$userassignment->id +5}}/content/" target="_blank">6</a>
												<a href="/userassignment/{{$userassignment->id +6}}/content/" target="_blank">7</a>
											</td>
											@endrole
	                                        <td>
		                                        <div class="btn-group">
	                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                                                Opciones
	                                              </button>
	                                              <div class="dropdown-menu">
												  	@role('admin')
	                                                 <a class="dropdown-item" href="/userassignment/{{ $userassignment->user->id }}/announcement/{{$announcement->slug}}/analyze">Analizar matrícula</a>
	                                                <a class="dropdown-item" href="/userassignment/{{ $userassignment->user->id }}/announcement/{{$announcement->slug}}/delete">Eliminar matrícula</a>
	                                            	@endrole
													<a class="dropdown-item" target="_blank" href='{{ asset("announcement/".$announcement->slug."/payment/". $userassignment->user_id) }}'>
														Ver Pagos
													</a> 
												  </div>
	                                            </div>
	                                        </td>
	                                    </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
