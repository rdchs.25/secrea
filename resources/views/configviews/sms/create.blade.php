@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de mensajes</h4>
                             </div>

                            {!! Form::open(['url' => '/sms', 'class' => 'mt-3 form-material material-primary']) !!}
                                @include('forms._sms_form', ['submitButtonText' => 'Registrar mensaje'])
                            {!! Form::close() !!}

                            @include('errors.list')

                        </div>

                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
