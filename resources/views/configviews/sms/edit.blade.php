@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">

            <div id="cabeceraFlotante">
                <h4>Mensajes</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Mensajes </a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2" class=" col-md-12 col-sm-12 float-right">
                <a href="/sms" class="ml-2 btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" role="button" aria-disabled="true">
                    <span class="btn-label"><i class="ti-search"></i></span>
                    Ver Mensajes
                </a>
            </div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Gestión de Mensajes</h4>
                             </div>
     
                            {!! Form::model($sms ,['method' => 'PATCH' ,'action' => ['SmsController@update', $sms->id], 'class' => 'mt-3 form-material material-primary']) !!}
                              @include('forms._sms_form', ['submitButtonText' => 'Actualizar Mensaje'])
                            {!! Form::close() !!}

                            @include('errors.list')

                        </div>

                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
