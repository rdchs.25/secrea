<!DOCTYPE html>
	<html lang="en">
		<head>
			<title>Secrea - Gestor de Contenidos</title>


			<meta name="csrf-token" content="{{ csrf_token() }}">
			
			<!-- Vendor CSS -->
			<link rel="stylesheet" href="/vendor/bootstrap4/css/bootstrap.min.css">
			<link rel="stylesheet" href="/vendor/themify-icons/themify-icons.css">
			<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="/vendor/animate.css/animate.min.css">
			<link rel="stylesheet" href="/vendor/jscrollpane/jquery.jscrollpane.css">
			<link rel="stylesheet" href="/vendor/waves/waves.min.css">
			<link rel="stylesheet" href="/vendor/switchery/dist/switchery.min.css">
			<link rel="stylesheet" href="/vendor/dropify/dist/css/dropify.min.css">
			<link rel="stylesheet" href="/vendor/SpinKit/css/spinkit.css">
			<!--  CSS -->
			<link rel="stylesheet" href="/css/core.css">
			<link rel="stylesheet" href="/css/style.css">

			<script type="text/javascript" src="/vendor/jquery/jquery-1.12.3.min.js"></script>

		</head>
		<body class="material-design fixed-sidebar fixed-header skin-default ">
			<div class="wrapper">

				<!-- Sidebar -->
				<div class="site-overlay"></div>
				<div class="site-sidebar">
					<div class="custom-scroll custom-scroll-light">
						<ul class="sidebar-menu">
							<li class="menu-title">Principal</li>
							<li class="active">
								<a href="index.html" class=" waves-effect  waves-light">
									<span class="s-icon"><i class="ti-home"></i></span>
									<span class="s-text">Home</span>
								</a>
							</li>
							<li class="">
								<a href="perfilEstudiante.html" class="waves-effect  waves-light">
									<span class="s-icon"><i class=" ti-user "></i></span>
									<span class="s-text">Mi Perfil</span>
								</a>
							</li>
							<li>
								<a href="cursosEstudiante.html" class="waves-effect  waves-light">
									<span class="s-icon"><i class="ti-layout-tab"></i></span>
									<span class="s-text">Mis cursos</span>
								</a>
							</li>
							

							<!-- <li class="menu-title">Otros</li> -->
							<li class="">
								<a href="notificacionesEstudiante.html" class="waves-effect  waves-light">
									<span class="tag tag-success">1</span>
									<span class="s-icon"><i class=" ti-bell"></i></span>
									<span class="s-text">Notificaciones</span>
								</a>
								
							</li>
							<li class=" active  ">
								<a href="mensajesEstudiante.html" class="waves-effect  waves-light">
									<span class="tag tag-success">2</span>
									<span class="s-icon"><i class="ti-comments"></i></span>
									<span class="s-text">Mensajes</span>
								</a>
							</li>
	                     	<li class="with-sub">
			                     <a href="demo.html" class="waves-effect  waves-light">
			                        <span class="s-icon"><i class="ti-widgetized"></i></span>
			                        <span class="s-text">Demo</span>
			                     </a>
		                 	</li>
						</ul>
					</div>
				</div>
				
				
		
				<!-- Header -->
				<div class="site-header">
					<nav class="navbar navbar-dark gradient gradient-success">
						<div class="navbar-left">
							<a class="navbar-brand" href="index.html">
								<div class="logo"></div>
							</a>
							<div class="toggle-button sidebar-toggle-first float-xs-left hidden-md-up light ">
								<span class="hamburger"></span>
							</div>
							<div class="toggle-button float-xs-right hidden-md-up light" data-toggle="collapse" data-target="#collapse-1">
								<span class="more"></span>
							</div>
						</div>
						<div class="navbar-right navbar-toggleable-sm collapse" id="collapse-0">
							<div class="toggle-button sidebar-toggle-second float-xs-left hidden-sm-down dark">
								<span class="hamburger"></span>
							</div>
							
							<ul class="nav navbar-nav float-md-right">
								<li class="nav-item dropdown">
									<a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
										<i class="ti-bell"></i>
										<span class="hidden-md-up ml-1">Alertas</span>
										<span class="tag tag-alert top">1</span>
									</a>
									<div class="dropdown-messages dropdown-tasks dropdown-menu dropdown-menu-right animated fadeInUp">
										<div class="m-item">
											<div class="mi-icon bg-success"><i class="ti-bell"></i></div>
											<div class="mi-text"><a class="text-black" href="#">Alex Muro </a> <span class="text-muted">(Docente Ciencias)</span> <a class="text-black" href="#">Subió nuevo contenido en tu curso "Ciencia y Ambiente"</a></div>
											<div class="mi-time">Hace 5 minutos</div>
										</div>
										<a class="dropdown-more" href="notificacionessEstudiante">
											<strong>Ver todas las notificaciones</strong>
										</a>
									</div>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
										<i class="ti-comments"></i>
										<span class="hidden-md-up ml-1">Alertas</span>
										<span class="tag tag-alert top">2</span>
									</a>
									<div class="dropdown-messages dropdown-tasks dropdown-menu dropdown-menu-right animated fadeInUp">
										<div class="m-item">
											<div class="mi-icon bg-info"><i class="ti-comment"></i></div>
											<div class="mi-text"><a class="text-black" href="#">Alex Muro </a> <span class="text-muted">(Docente Ciencias)</span> <a class="text-black" href="#">respondió tu pregunta en el chat</a></div>
											<div class="mi-time">Hace 5 minutos</div>
										</div>
										<div class="m-item">
											<div class="mi-icon bg-info"><i class="ti-comment"></i></div>
											<div class="mi-text"><a class="text-black" href="#">Sara</a> <span class="text-muted">Respodió</span> <a class="text-black" href="#">tu pregunta en el chat</a></div>
											<div class="mi-time">Hace 5 minutos</div>
										</div>
										<a class="dropdown-more" href="mensajesEstudiante.html">
											<strong>Ver todos los mensajes</strong>
										</a>
									</div>
								</li>
								<li class="nav-item dropdown hidden-sm-down">
									<a href="#" data-toggle="dropdown" aria-expanded="false">
										<span class="avatar box-32">
											<img src="img/avatars/1.jpg" alt="">
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right animated fadeInUp">
										<a class="dropdown-item" href="perfilEstudiante.html">
											<i class="ti-user mr-0-5"></i> Mi Perfil
										</a>
										<a class="dropdown-item" href="#">
											<i class="ti-settings mr-0-5"></i> Configuración
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="login.html"><i class="ti-power-off mr-0-5"></i> Salir del sistema</a>
									</div>
								</li>
							</ul>
							<ul class="nav navbar-nav">
								<li class="nav-item hidden-sm-down">
									<a class="nav-link toggle-fullscreen" href="#">
										<i class="ti-fullscreen"></i>
									</a>
								</li>
							</ul>
						</div>
					</nav>
				</div>


				<!-- ////////////////////////////////////////// CONTENIDO /////////////////////////////////////////// -->

				<div id="app">
					<course-builder></course-builder>
				</div>		
		</body>
		<script type="text/javascript" src="/vendor/tether/js/tether.min.js"></script>
		<script type="text/javascript" src="/vendor/bootstrap4/js/bootstrap.min.js"></script>        
        <script type="text/javascript" src="/vendor/detectmobilebrowser/detectmobilebrowser.js"></script>
		<script type="text/javascript" src="/vendor/jscrollpane/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="/vendor/jscrollpane/mwheelIntent.js"></script>
		<script type="text/javascript" src="/vendor/jscrollpane/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript" src="/vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js"></script>
		<script type="text/javascript" src="/vendor/dropify/dist/js/dropify.min.js"></script>
		<script type="text/javascript" src="/js/course.js"></script>
		<script type="text/javascript" src="/js/app.js"></script>
	</html>