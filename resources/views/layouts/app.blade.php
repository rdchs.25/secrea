<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('partials.head')
        @yield('header')
    </head>
    <body class="material-design fixed-sidebar fixed-header skin-default">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader"></div>
            {!! Toastr::render() !!}
            
            @if( Auth::user()->isRole('registrado') || Auth::user()->isRole('student') )
            @else
                @include('partials.sidebar')
            @endif

            @include('partials.header')
            
            <div class="site-content" style="{{ (Auth::user()->isRole('registrado') || Auth::user()->isRole('student'))? 'margin-left:0':'' }}" >
 
                @yield('content')
        
            </div> 
            
        </div>
    @include('partials.scripts')
    @yield('scripts')
    </body>
</html>