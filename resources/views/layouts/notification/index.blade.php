@extends('layouts.app')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            <h4>Notificaciones recibidas</h4>
            <nav class="navbar navbar-light bg-white b-a ">

                <ol class="breadcrumb no-bg mb-0">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">Notificaciones</li>
                </ol>
            </nav>

            <div class="mt-2 col-md-8 col-sm-12 offset-md-2">
                <div class="mt-2 col-md-12 col-sm-12 float-right  mb-2 ">
                    <button type="button" class=" btn btn-info btn-lg label-right b-a-0 waves-effect waves-light float-md-right" href="mensajesEstudiante.html">
                        <span class="btn-label"><i class="ti-trash"></i></span>
                        Eliminar seleccionados
                    </button>
                </div>
                <div class=" box box-block bg-white tile tile-1 mt-2 mb-2">
                    <div class="management mb-1">

                        <div class="m-item">
                            <div class="mi-checkbox">
                                <label class=" ">
                                    <input type="checkbox"> 
                                </label>
                            </div>
                            <div class="mi-title">
                                <a class="text-black">Estudiante 001</a> 1ro Secundaria | Comunicación <a class="text-black">subió un nuevo trabajo </a>
                            </div>
                            <div class="mi-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam sem, imperdiet at mollis vestibulum, bibendum id purus. Aliquam molestie, leo sed molestie condimentum, massa enim lobortis massa, in vulputate diam lorem quis justo.</div>
                            <div class="clearfix">
                                <div class="float-md-left">
                                    <a class="btn btn-success btn-sm" href="#"><i class="ti-check mr-0-5"></i>OK</a>
                                    <a class="btn btn-danger btn-sm" href="#"><i class="ti-trash mr-0-5"></i>Eliminar</a>
                                </div>
                                <div class="float-md-right">
                                    <span class="font-90 text-muted">5 minutes ago</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>  
            </div>

        </div>

@stop