@extends('layouts.app')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">

        <div id="cabeceraFlotante" style="width: 1590px;">
            <h4>Cursos</h4>
            <nav class="navbar navbar-light bg-white b-a ">
                
                <ol class="breadcrumb no-bg mb-0">
                    <li class="breadcrumb-item"><a href="home.php">Inicio</a></li>
                    <li class="breadcrumb-item active">Mis cursos</li>
                </ol>
            </nav>         
        </div>
        
        <div id="mFloatBox2" class="row  mb-0 mb-md-1">
            
            <div class="col-md-3">
                <div class="banner">
                    <div class="b-img img-cover" style="background-image: url(img/photos-1/4.jpg);"></div>
                    <div class="gradient gradient-success"></div>
                    <div class="b-content">
                        <div class="b-title"><span id="grado">1ro de Secundaria</span></div>
                        <div class="b-text">Recuerda que los contenidos de tus cursos se encuentran adaptados a tu grado de estudio</div>
                    </div>
                </div>
                <div class="mt-2 box box-block  tile tile-2 bg-info mb-2">
                    <div class="t-icon right"><i class="ti-comments"></i></div>
                    <div class="t-content">
                        <h4 class="mb-1 ">" Recuerda que puedes realizar consultas a tus docentes en todo momento desde el chat "  </h4>
                    </div>
                </div>
            </div>

            <div id="listacursos" class="col-lg-9 col-md-9 col-sm-12 col-xs12">

                <div class="col-md-3 col-md-4  col-sm-6  col-xs-12">
                    <div class="box bg-white user-1">
                        <div onclick="curso(1)" class="fondocurso">
                            <div class="u-img img-cover" style="background-image: url(/img/photos-1/1curso.png);"></div>
                        </div>
                        <div class="u-content mt-3">
                            <h5><a class="text-black" href="#"> Idioma Extranjero</a></h5>
                            <p class="text-muted"> <b>Docente:</b> Marcos Joaquín Peña Vega</p>
                            <div class="text-xs-center">
                                <button type="submit" class="btn btn-outline-success btn-rounded">Ver curso 
                                    <i class="ti-plus ml-0-5"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@stop