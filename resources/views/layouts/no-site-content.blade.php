<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('partials.head')
    </head>
    <body class="material-design fixed-sidebar fixed-header skin-default">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader"></div>
            {!! Toastr::render() !!}
            
            @include('partials.sidebar')
            @include('partials.header')

            
                
            @yield('content')
            
        </div>
    @include('partials.scripts')
    @yield('scripts')
    </body>
</html>