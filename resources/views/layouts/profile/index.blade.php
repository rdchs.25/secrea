@extends('layouts.app')

@section('content')
<div class="content-area pb-1">
    <div class="profile-header mb-1">
        <div class="profile-header-cover img-cover" style="background-image: url(img/photos-1/1.jpg);"></div>
        <div class="profile-header-counters clearfix">
            <div class="container-fluid">
                
                <div class="float-xs-right">
                    <a href="#" class="text-black">
                        <h5 class="font-weight-bold">6</h5>
                        <span class="text-muted">Cursos</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-3 ">
                <div class="card profile-card">
                    <div class="profile-avatar">
                        <img src="img/avatars/5.jpg" alt="">
                    </div>
                    <div class="card-block">
                        <h4 class="mb-0-25">{{ Auth::user()->name }}</h4>
                        <div class="text-muted mb-1">Especialidad</div>
                        
                    </div>
                    <ul class="list-group" role="tablist">
                        <li class="nav-item">
                            <a class=" active list-group-item " data-toggle="tab" href="#miPerfil" role="tab" aria-expanded="true"  href="#">
                                <i class="ti ti-id-badge mr-0-5"></i> Mi perfil
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card">
                    <div class="card-block">
                        <button type="submit" onclick="window.location.href='cursosDocente.html'" class="btn btn-success btn-block">Ver cursos a cargo</button>
                    </div>
                </div>
            </div>

            <div class="col-sm-8 col-md-9 ">
                <div class="card mb-0">
                    
                    <div class="tab-content">

                        <div class="tab-pane active card-block pt-2" id="miPerfil" role="tabpanel" aria-expanded="true">
                            
                            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                                <div class="box box-block  tile tile-2  bg-success">
                                    <div class="t-icon right"><span class="bg-success"></span><i class="ti-pencil-alt"></i></div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Cursos a cargo</h6>
                                        <h1 class="mb-1">6</h1>
                                        <span class=" font-90">6 cursos</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-2">
                                <div class="box box-block  tile tile-2 bg-success ">
                                    <div class="t-icon right"><span class="bg-success"></span><i class=" ti-close"></i></div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Pendientes de revisión</h6>
                                        <h1 class="mb-1">7</h1>
                                        <span class=" font-90">Revisar cursos</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                                <div class="box box-block  tile tile-2 bg-success ">
                                    <div class="t-icon right"><span class="bg-success"></span><i class="ti-check"></i></div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Revisadas</h6>
                                        <h1 class="mb-1">12</h1>
                                        <span class=" font-90">Durante la semana</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pt-2">
                                
                                <div class="box box-block bg-white tile tile-4 ">
                                    
                                    
                                    <div class="col-lg-12 col-md-12 ">
                                        <div class="t-content text-xs-left">
                                            <h6 class="text-uppercase">Especialidad</h6>
                                            <h3 class="mb-0">{{ Auth::user()->name }}</h3>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 mt-1">
                                        <div class="t-content text-xs-left">
                                            <button type="button" class="btn btn-info btn-rounded btn-md label-right b-a-0 waves-effect waves-light">
                                                <span class="btn-label"><i class="ti-settings"></i></span>
                                                Editar Información
                                            </button>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="col-md-6 pt-2">
                                <div class="card">
                                    <div class="card-header text-uppercase"><b>Notificaciones recientes <span class="tag tag-alert top">1</span></b>
                                    </div>
                                    <div class="management mb-1">
                                        <div class="m-item">
                                            <div class="mi-checkbox">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </div>
                                            <div class="mi-title">
                                                <a class="text-black">Estudiante 001</a> 1ro Secundaria | Comunicación <a class="text-black">subió un nuevo trabajo </a>
                                            </div>
                                            <div class="mi-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam sem, imperdiet at mollis vestibulum, bibendum id purus. Aliquam molestie, leo sed molestie condimentum, massa enim lobortis massa, in vulputate diam lorem quis justo.</div>
                                            <div class="clearfix">
                                                <div class="float-md-right">
                                                    <span class="font-90 text-muted">hace 5 minutos</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="col-md-6 pt-2">
                                <div class="card">
                                    <div class="card-header text-uppercase"><b>Mensajes recientes <span class="tag tag-alert top">2</span></b>
                                    </div>
                                    <div style="padding:10px; border-bottom:#eee solid 1px;" class="mc-item">
                                        <a class="text-black" href="#">
                                            <div class="media">
                                                <div class="media-left">
                                                    <div class="avatar box-48">
                                                        <img class="b-a-radius-circle" src="img/avatars/1.jpg" alt="">
                                                        <span class="status bg-success top right"></span>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <div class="clearfix">
                                                        <h6 class="media-heading float-xs-left">Estudiante 002 <span class="tag tag-danger">2</span></h6>
                                                        <span class="font-90 text-muted float-xs-right">hace 5 minutos</span>
                                                    </div>
                                                    <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod...</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div style="padding:10px; border-bottom:#eee solid 1px;" class="mc-item">
                                        <a class="text-black" href="#">
                                            <div class="media">
                                                <div class="media-left">
                                                    <div class="avatar box-48">
                                                        <img class="b-a-radius-circle" src="img/avatars/1.jpg" alt="">
                                                        <span class="status bg-success top right"></span>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <div class="clearfix">
                                                        <h6 class="media-heading float-xs-left">Estudiante 001 <span class="tag tag-danger">2</span></h6>
                                                        <span class="font-90 text-muted float-xs-right">hace 5 minutos</span>
                                                    </div>
                                                    <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod...</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>


                        </div>

                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@stop