<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Title -->
    <title>SECREA</title>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/vendor/bootstrap4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/themify-icons/themify-icons.css">

    <!-- Neptune CSS -->
    <link rel="stylesheet" href="/css/core.css">

  </head>

  <body class="gradient gradient-success">    
    <div class="error-message text-xs-center">
      <h1 class="mb-3" style="color:#fff;">Alerta<span><i class="ti-na"></i></span></h1>
      <h5 class="text-uppercase">Para continuar con tus clases, Necesitas cargar tus documentos, en la sección <a href="/profile">Mi Perfil</a></div>

      <a href="/home" class="btn btn-outline-white w-min-mdt" role="button" aria-disabled="true">
                Ir al Inicio
            </a>

    </div>
  </body>

</html>