@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Reportes</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Reportes </a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2"></div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Reportes</h4>
                                <p class="font-90 text-muted mb-1">A continuación podrás visualizar la información de los Reportes. </p>
                             </div>
                        </div>
                   </div>
                </div>
            </div>

            <!--<div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="ti-ruler-pencil"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Usuarios</h6>
                        <h1 class="mb-1">
                            <a href="/report/users/excel" title="Listado en Excel">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                            <a href="/report/users/pdf" title="Listado en PDF" target="_blank">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="ti-user"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Matriculados a la fecha</h6>
                        <h1 class="mb-1">
                            <a href="#" title="Listado en Excel">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                            <a href="/report/enrolled-users/pdf" title="Listado en PDF" target="_blank">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="ti-user"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Pre-Inscritos a la fecha</h6>
                        <h1 class="mb-1">
                            <a href="#" title="Listado en Excel">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                            <a href="/report/pre-enrolled-users/pdf" title="Listado en PDF" target="_blank">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="ti-id-badge"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Usuarios - Documentos</h6>
                        <h1 class="mb-1">
                            <a href="#" title="Listado en Excel">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                            <a href="/report/document-users/pdf" title="Listado en PDF" target="_blank">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="fa fa-handshake-o"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Usuarios - Convenios</h6>
                        <h1 class="mb-1">
                            <a href="#" title="Listado en Excel">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                            <a href="/report/agreement-users/pdf" title="Listado en PDF" target="_blank">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>
            -->

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="fa fa-handshake-o"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Matriculados 1°</h6>
                        <h1 class="mb-1">
                            <a href="/report/enroll/1" title="Listado en Excel" target="_blank">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="fa fa-handshake-o"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Matriculados 2°</h6>
                        <h1 class="mb-1">
                            <a href="/report/enroll/2" title="Listado en Excel" target="_blank">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="fa fa-handshake-o"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Matriculados 3°</h6>
                        <h1 class="mb-1">
                            <a href="/report/agreement/3" title="Listado en Excel" target="_blank">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                <div class="box box-block  tile tile-2  bg-success">
                    <div class="t-icon right">
                        <span class="bg-success"></span><i class="fa fa-handshake-o"></i>
                    </div>
                    <div class="t-content">
                        <h6 class="text-uppercase mb-1">Matriculados 4°</h6>
                        <h1 class="mb-1">
                            <a href="/report/enroll/4" title="Listado en Excel" target="_blank">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                        </h1>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
