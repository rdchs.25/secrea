<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte de asignatura: {{$subject->name}}</title>
    <style>
    body,html{overflow-x:hidden;text.align:c;position:relative;margin-top:90px;width:725px!important}body{background-position:bottom right;background-repeat:no-repeat}.top,.bottom{width:100%;text-align:center;position:fixed}.top img{width:200px}.top{top:-30px;text-align:left}.bottom{bottom:5px}.pagination{position:fixed;bottom:-10px;right:-600px}.pagenum:before{content:counter(page);font-size:11px}.page-break{page-break-after:always}hr{height:2px;width:50%;color:#ed1d61;background:#43B675;font-size:0;border:0}.datagrid table{border-collapse:collapse;text-align:left;width:100%}.datagrid{font:normal 12px/150% Verdana,Arial,Helvetica,sans-serif;background:#fff;overflow:hidden;border:1px solid #000}.datagrid table td,.datagrid table th{padding:3px 3px}.datagrid table thead th{background:-webkit-gradient(linear,left top,left bottom,color-stop(.05,#43B675),color-stop(1,#43B675));background:-moz-linear-gradient(center top,#43B675 5%,#43B675 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#43B675',endColorstr='#43B675');background-color:#43B675;color:#FFF;font-size:15px;font-weight:700}.datagrid table thead th:first-child{border:none}.datagrid table tbody td{color:#000;font-size:12px;border-bottom:1px solid #000;font-weight:400}.datagrid table tbody .alt td{background:#43B675;color:#000}.datagrid table tbody td:first-child{border-left:none}.datagrid table tbody tr:last-child td{border-bottom:none}
    
    </style>
</head>
<body>
<div class="all">
  <div class="top">
    <img src="images/logo.png" >
  </div>

    <b><center>Reporte de asignatura - {{$subject->name}} - {{  $subject->identify}}</center></b>
    <br>
    <hr>
    <br>
    <div class="datagrid">

        <table>
            <thead>
                <tr>
                    <th>Body</th>
                    <th>Notes</th>
                    <th>Title</th>
                    <th>Video</th>
                    <th>Código de imagen</th>
                </tr>
            </thead>
            <tbody>
                @foreach($content as $week)
                    @foreach($week->sessions as $session)

                        @foreach($session->slides as $slide)

                            @if( $slide->id === 'S-MU-05' && $slide->value->video === "")
                                <tr>
                                    <td>{{$slide->value->body}}</td>
                                    <td>{{substr($slide->value->notes,0,40)}}</td>
                                    <td>{{$slide->value->title}}</td>
                                    <td>{{$slide->value->video}}</td>
                                    <td>{{$slide->value->imgName}}</td>
                                </tr>  
                            @endif

                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
        </table>

    </div>
    
    <br>
    <hr>
    <br>
    <div class="datagrid">
        <table>
            <thead>
                <tr>
                    <th>Semana</th>
                    <th>Sesión</th>
                    <th>Estado imágen</th>
                    <th>Código imágen</th>
                </tr>
            </thead>
            <tbody>
                @foreach($content as $week)
                    @foreach($week->sessions as $session)

                        @foreach($session->slides as $slide)
                            @if(isset($slide->value->imgType))
                                @if($slide->value->imgType != 'final')
                                <tr>
                                    <td>{{$week->name}}</td>
                                    <td>{{$session->name}}</td>
                                    <td>{{$slide->value->imgType}}</td>
                                    <td>{{$slide->value->imgName}}</td>
                                </tr>
                                @endif
                            @else

                                @if(isset($slide->value->audios)){
                                    @for ($i = 0; $i < sizeof($slide->value->audios); $i+=1)
                                        @if($slide->value->audios[$i]->imgType != 'final')
                                        <tr>
                                            <td>{{$week->name}}</td>
                                            <td>{{$session->name}}</td>
                                            <td>{{$slide->value->audios[$i]->imgType}}</td>
                                            <td>{{$slide->value->audios[$i]->imgName}}</td>
                                        </tr>  
                                        @endif
                                    @endfor         
                                @endif 
                            
                            @endif  

                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
        </table>    
    </div>
    <br>
    <hr>
    <br>

    <div class="datagrid">

        <table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Notes</th>
                    <th>Total Audios</th>
                    <th>Código imagen</th>
                </tr>
            </thead>
            <tbody>
                @foreach($content as $week)
                    @foreach($week->sessions as $session)

                        @foreach($session->slides as $slide)

                            @if ( $slide->id === 'S-MU-09' ||  $slide->id === 'S-MU-08' || $slide->id === 'S-MU-07' ) 
                                @for ($i = 0; $i < sizeof($slide->value->audios); $i+=1) 
                                    @if (sizeof($slide->value->audios[$i]->audio) === 0) 
                                    <tr>
                                        <td>{{$slide->value->title}}</td>
                                        <td>{{substr($slide->value->notes,0,40)}}</td>
                                        <td>{{sizeof($slide->value->audios)}}</td>
                                        <td>{{$slide->value->audios[$i]->imgName}}</td>
                                    </tr>  
                                    @endif
                                @endfor
                            @endif

                        @endforeach

                    @endforeach
                @endforeach
            </tbody>
        </table>

    </div>

    

    <div class="bottom">
      <div class="pagination">Página <span class="pagenum"></span></div>
    </div>
</div>
</body>
</html>