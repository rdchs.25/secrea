<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte de Usuario</title>
    <style>
    body,html{overflow-x:hidden;text.align:c;position:relative;margin-top:90px;width:725px!important}body{background-image:url(/images/bg-hoja.png);background-position:bottom right;background-repeat:no-repeat}.top,.bottom{width:100%;text-align:center;position:fixed}.top img{width:200px}.top{top:-30px;text-align:left}.bottom{bottom:5px}.pagination{position:fixed;bottom:-10px;right:-600px}.pagenum:before{content:counter(page);font-size:11px}.page-break{page-break-after:always}hr{height:2px;width:50%;color:#ed1d61;background:#43B675;font-size:0;border:0}.datagrid table{border-collapse:collapse;text-align:left;width:100%}.datagrid{font:normal 12px/150% Verdana,Arial,Helvetica,sans-serif;background:#fff;overflow:hidden;border:1px solid #000}.datagrid table td,.datagrid table th{padding:3px 3px}.datagrid table thead th{background:-webkit-gradient(linear,left top,left bottom,color-stop(.05,#43B675),color-stop(1,#43B675));background:-moz-linear-gradient(center top,#43B675 5%,#43B675 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#43B675',endColorstr='#43B675');background-color:#43B675;color:#FFF;font-size:15px;font-weight:700}.datagrid table thead th:first-child{border:none}.datagrid table tbody td{color:#000;font-size:12px;border-bottom:1px solid #000;font-weight:400}.datagrid table tbody .alt td{background:#43B675;color:#000}.datagrid table tbody td:first-child{border-left:none}.datagrid table tbody tr:last-child td{border-bottom:none}
    </style>
</head>
<body>
<div class="all">
  <div class="top">
    <img src="images/logo.png" >
  </div>

    <b><center>LISTADO DE USUARIOS A LA FECHA {{ $date  }}</center></b>
    <br>
    <hr>
    <br>

    <div class="datagrid">
      <table>
        <thead>
          <tr>
            <th>Nombre y Apellido</th>
            <th>Correo Electronico</th>
            <th>Fecha de registro</th>
          </tr>
        </thead>
        <tbody>
            @foreach($data as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at }}</td>
          </tr>
            @endforeach
        </tbody>
      </table>
    </div>

    <div class="bottom">
      <div class="pagination">Página <span class="pagenum"></span></div>
    </div>
</div>
</body>
</html>