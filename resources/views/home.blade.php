@extends('layouts.app')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="header text-xs-center img-cover mb-2" style="background-image: url(img/photos-1/4.jpg); min-height: 90vh">
                <div class="gradient gradient-success"></div>
                <div class="h-content">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="h-title">Bienvenido a Secrea, tu plataforma educativa</div>
                            <div class="h-text">Somos el primer Colegio Virtual del Perú creado para que millones de jóvenes y adultos puedan terminar su secundaria a través de internet.</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 ">
                    
                    @can('announcement-management')
                        <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                            <a href="#">
                                <div class="box box-block  tile tile-2  bg-black">
                                    <div class="t-icon right">
                                        <span class="bg-success"></span><i class="ti-ruler-pencil"></i>
                                    </div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Convocatoria Activa</h6>
                                        <h1 class="mb-1">{{ $announcement->name or '-' }}</h1>
                                        <span class=" font-90">{{ isset($announcement->name)? 'Iniciamos clases' : 'No hay convocatoria activa' }}</span>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                            <a href="#">
                                <div class="box box-block  tile tile-2  bg-black">
                                    <div class="t-icon right">
                                        <span class="bg-success"></span><i class="ti-ruler-pencil"></i>
                                    </div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Matriculados {{ $announcement->name or '-' }}</h6>
                                        <h1 class="mb-1">{{ $countEnrolled or '0' }}</h1>
                                        <span class=" font-90">{{ $countEnrolled or '0'}} alumno(s) matriculado(s)</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        @if(!empty($annPreEnroll))
                            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                                <a href="#">
                                    <div class="box box-block  tile tile-2  bg-black">
                                        <div class="t-icon right">
                                            <span class="bg-success"></span><i class="ti-ruler-pencil"></i>
                                        </div>
                                        <div class="t-content">
                                            <h6 class="text-uppercase mb-1">Pre inscritos {{$annPreEnroll->name}}</h6>
                                            <h1 class="mb-1">{{ $countPreEnrolled or '0' }}</h1>
                                            <span class=" font-90">{{ $countPreEnrolled or '0'}} alumno(s) pre inscritos</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                                <a href="#">
                                    <div class="box box-block  tile tile-2  bg-black">
                                        <div class="t-icon right">
                                            <span class="bg-success"></span><i class="ti-ruler-pencil"></i>
                                        </div>
                                        <div class="t-content">
                                            <h6 class="text-uppercase mb-1">Matriculados {{$annPreEnroll->name}}</h6>
                                            <h1 class="mb-1">{{ $pre_enrollment_with_voucher or '0' }}</h1>
                                            <span class=" font-90">{{ $pre_enrollment_with_voucher or '0'}} alumno(s) que pagaron su matrícula</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif

                        
                    @endcan
                    -->



                    @can('my-subjects-management')
                        <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                            <a href="/my-subjects">
                                <div class="box box-block  tile tile-2  bg-success">
                                    <div class="t-icon right">
                                        <span class="bg-success"></span><i class="ti-pencil-alt"></i>
                                    </div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Mis Asignaturas</h6>
                                        <h1 class="mb-1">{{ $countSubjectByStudent or '0'}}</h1>
                                        <span class=" font-90">
                                            {{ $countSubjectByStudent or '0'}} asignaturas
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcan
                    @can('my-assignments-management')
                       <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                            <a href="/my-assignment">
                                <div class="box box-block  tile tile-2  bg-success">
                                    <div class="t-icon right">
                                        <span class="bg-success"></span><i class="ti-ruler-pencil"></i>
                                    </div>
                                    <div class="t-content">
                                        <h6 class="text-uppercase mb-1">Mis Asignaciones</h6>
                                        <h1 class="mb-1">{{ $countSubjectByTeacher or '0' }}</h1>
                                        <span class=" font-90">
                                            {{ $countSubjectByTeacher or '0' }} asignaciones
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endcan
                           <!-- <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-2">
                                <a href="#">
                                    <div class="box box-block  tile tile-2 bg-success ">
                                        <div class="t-icon right"><span class="bg-success"></span><i class=" ti-close"></i></div>
                                        <div class="t-content">
                                            <h6 class="text-uppercase mb-1">Pendientes de revisión</h6>
                                            <h1 class="mb-1">0</h1>
                                            <span class=" font-90"><span class="tag tag-warning">En Construcción</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-xs-12 mt-2 mb-1">
                                <a href="#">
                                    <div class="box box-block  tile tile-2 bg-success ">
                                        <div class="t-icon right"><span class="bg-success"></span><i class="ti-check"></i></div>
                                        <div class="t-content">
                                            <h6 class="text-uppercase mb-1">Revisadas</h6>
                                            <h1 class="mb-1">0</h1>
                                            <span class=" font-90"><span class="tag tag-warning">En Construcción</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>-->
                        
                </div>
            </div>
        </div>
    </div>
    
@stop