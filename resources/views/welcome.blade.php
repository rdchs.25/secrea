<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TLZKGPM');</script>
    <!-- End Google Tag Manager -->

    <meta name="google-site-verification" content="6i0dBAQbF7LLIs83B7c02NslDyvnBnoSM1TLf-4ubpM" />
    <meta charset="utf-8">


    <!-- ========== VIEWPORT META ========== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- ========== PAGE TITLE ========== -->
    <title>SECREA</title>

    <!-- ========== META TAGS ========== -->
    <meta name="description" content="Somos SECREA (Secundaria Creativa), la plataforma educativa virtual que ofrece a jóvenes y adultos la oportunidad de completar sus estudios secundarios, con la misión de mejorar sus condiciones personales, familiares y laborales. ">
    
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Secrea - Secundaria Creativa" />
    <meta property="og:description" content="Terminar tu secundaria nunca más será complicado" />
    <meta property="og:image" content="www.secundariacreativa.com/storage/inscripcion.png" />

    <meta name="keywords" content="Secrea, secrea, secundaria virtual,educacion virtual, secundaria ">
    <meta name="author" content="secrea">

    <link rel="alternate" hreflang="es" href="http://www.secundariacreativa.com/" />
    <!-- ========== FAVICON ========== -->
    <link rel="shortcut icon" href="webSecrea/img/favicon.png">

    <!-- ========== MINIFIED VENDOR CSS ========== -->
    <link rel="stylesheet" href="webSecrea/css/plugins.css">

    <!-- ========== MAIN CSS ========== -->
    <link href="webSecrea/css/style.css?v={{time()}}" rel="stylesheet">
    <link href="webSecrea/css/presets/preset-gradient.css" rel="stylesheet">
    <link href="webSecrea/css/responsive.css" rel="stylesheet">

    <!-- === Your own custom css file === -->
    <link id="preset" href="webSecrea/css/custom.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        function checkSubmit() {
            document.getElementById("form-submit").value = "Enviando...";
            document.getElementById("form-submit").disabled = true;
            return true;
        }
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://secundariacreativa.com",
      "@type": "Secrea",
      "url": "http://www.secundariacreativa.com",
      "contactPoint": {
        "@type": "ContactPoint",
        "telephone": "+1-401-555-1212",
        "contactType": "customer service"
      }
    }
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113868199-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-113868199-1');
    </script>

</head><!--/head-->


<body id="page-top">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLZKGPM"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- PRELOADER -->
    <div class="preloader">
        <div class="loading-wrap">
            <img src="webSecrea/img/logo-gradient.png" alt="logo">
            <div class="p-loading ft-fm-2">TERMINA TU SECUNDARIA  </div>
        </div>
    </div>
    <!-- END PRELOADER -->


    <div id="page-wrap" class="animsition">
        <!-- Overlay display while mobile navigation will open  -->
        <div class="offcanvas-overlay"></div>

        <!-- ========== HEADER ========== -->
        <header id="header" class="box-header sticky">
            <div class='container'>
                <div class="row box-header-wrap">
                    <!-- Start logo -->
                    <div class="col-sm-2 col-xs-4">
                        <div id="logo" class="logos">
                            <a href="/" class="standard-logo pull-left">
                                <img class="logo" src="webSecrea/img/logo.png" alt="logo">
                            </a>
                            <a href="/" class="retina-logo pull-left">
                                <img class="logo" src="webSecrea/img/logo%402x.png" alt="logo">
                            </a>
                        </div>
                    </div> <!-- //.col-sm-2 -->
                    <!-- End logo -->
                
                    <!-- Start desktop nav -->
                    <div class="col-sm-10 col-xs-8">
                        <nav class="main-nav pull-right">
                            <ul>
                                <li>
                                    <a href="webSecrea/nosotros.html">Sobre Nosotros</a>
                                </li>
                                <!-- <li class="">
                                    <a href="docentes.html">Docentes</a>
                                </li> -->
                                <li class="">
                                    <a href="webSecrea/contact.html">Contáctanos</a>    
                                </li>
                                <li>
                                    <a href="http://secrea.net/virtual/">Iniciar Sesión - 2017</a>
                                </li>
                                <a id="btn-preinscripcion" href="http://secundariacreativa.com/login" class="ripple btn btn-primary menuButton" style="position: relative; overflow: hidden;">Alumnos 2018</a>
                                <a id="btn-preinscripcion" href="http://secundariacreativa.com/register" class="ripple btn btn-success menuButton" style="position: relative; overflow: hidden;">Inscribirme</a>
                                
                            </ul>
                        </nav>

                        <!-- toogle icons, which are responsible for display and hide menu in small layout -->

                        <div class="offcanvas-toggler pull-right">
                            <i id="offcanvas-opener" class="icon-menu"></i>
                            <i id="offcanvas-closer" class="icon-times"></i>
                        </div>
                    </div> <!-- //.col-sm-10 -->
                    <!-- End desktop nav -->
                </div> <!-- //.row -->
             </div> <!-- //.container -->
        </header>
        <!-- //End Header -->


        <!-- ========== START MOBILE NAV ========== -->
        <nav class="mobile-nav">
            <ul>
                <li>
                    <a href="/">Inicio</a>
                </li>
                <li>
                    <a href="webSecrea/nosotros.html">Sobre nosotros</a>
                </li>
                <!-- <li>
                    <a href="docentes.html">Docentes</a>
                </li> -->
                <li class="">
                    <a href="webSecrea/contact.html">Contáctanos</a>
                </li>

                <a href="http://secundariacreativa.com/login" class="ripple btn btn-primary menuButton2" style="position: relative; overflow: hidden;">Alumnos 2018</a>
                <a id="btn-preinscripcion" href="http://secundariacreativa.com/register" class="ripple btn btn-success menuButton btn-iniciar" style="position: relative; overflow: hidden;">Inscribirme</a>
                <a id="btn-preinscripcion" href="http://secrea.net/virtual/" class="ripple btn btn-success menuButton btn-iniciar" style="position: relative; overflow: hidden;">Iniciar Sesión - 2017</a>
                
            </ul>
        </nav>
        <!-- end mobile nav -->


        <!-- ========== TEXT SLIDER SECTION ========== -->
        <section class="advanced-slider-wrapper">
            <div class="simple-slider advanced-slider owl-carousel">
                <div class="item">
                    <div class="item-content" style="background-image: url(webSecrea/img/slider/5.jpg);">
                        <div class="bgcolor-major-gradient-overlay"></div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="slider-details">
                                        <h1 class="title zim-fadeInDown">TERMINA TU SECUNDARIA EN  </br>2 AÑOS</h1>
                                        <p class="details zim-fadeInDown-delay">
                                            Con una nueva forma de estudiar.<br>  
                                        <p class="more-btn zim-fadeInDown-delay2">
                                            <a href="http://secundariacreativa.com/register" class="ripple btn next-section">Quiero pre-inscribirme</a>
                                            <a class="popup-video" href="https://www.youtube.com/watch?v=Lj69CiTctFk?showinfo=0"><i class="fa fa-play"></i> Ver Video</a>
                                            <!-- <a class="popup-video" href="https://www.youtube.com/watch?v=A1CoJKju-YI"><i class="fa fa-play"></i> Play Video</a> -->
                                        </p>
                                    </div> <!--  //slider-details -->
                                </div> <!-- //.col-sm-6 -->
                                <!-- Slider Images -->
                                
                            </div>
                        </div> <!-- //.container -->
                    </div> <!-- //.item-content -->
                </div> <!-- //.item -->

                <div class="item">
                    <div class="item-content" style="background-image: url(webSecrea/img/slider/6.jpg);">
                        <div class="bgcolor-major-gradient-overlay"></div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="slider-details">
                                        <h1 class="title zim-fadeInLeft">RENOVAMOS NUESTRA PLATAFORMA  </h1>
                                        <p class="details zim-fadeInLeft-delay">
                                            Creamos una plataforma especializada que nos permite visualizar en todo momento tu progreso de lectura, así cómo tu progreso en tus evaluaciones.<br>  
                                        </p>
                                        <p class="more-btn zim-fadeInLeft-delay2">
                                            <a href="#price" class="ripple btn next-section">VER PRECIOS</a>
                                        </p>
                                    </div> <!--  //slider-details -->
                                </div> <!-- //.col-sm-6 -->
                                <!-- Slider Images -->
                            
                            </div>
                        </div> <!-- //.container -->
                    </div> <!-- //.item-content -->
                </div> <!-- //.item -->
            </div> <!-- //#simple-slider -->
        </section>
        <!-- //End simple slider -- >


        <!-- ========== PRICING TABLE V2  ========== -->
        <section class="section-common-space bgcolor-major-gradient">
            <div class="container">
                <!-- ========== SECTION HEADER ========== -->
                <div class="section-header section-header-v2 text-center">
                <h2 class="section-title">¿Cómo ser parte de Secrea?</h2>
    
                </div> <!-- //section-header -->

                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <!-- pricing-table -->
                        <div class="pricing-table pricing-table-v2 text-center">
                            <div class="price-heading">
                                <h4 class="title">Paso 1</h4>
                                <p class="icon"><i class="fa fa-columns"><span class="bgcolor-major-gradient-overlay"></span></i></p>
                                <div class="price-group text-center">
                                    <span class="dollar">PRE-INSCRIPCIÓN</span>
                                </div>
                            </div> <!-- //price-heading -->
                            <ul class="price-feature ft-fm-2">
                                <li> Llena el formuario de pre-inscripción, luego te enviaremos un correo de confirmación con tu usuario y contraseña.</li>
                                <br>
                            </ul> <!-- //price-feature -->
                            
                        </div> <!-- //pricing-table -->
                    </div> <!-- //.col-sm-6 col-md-3 -->

    
                    <div class="col-sm-6 col-md-4">
                        <!-- pricing-table -->
                        <div class="pricing-table pricing-table-v2 text-center">
                            <div class="price-heading">
                                <h4 class="title">Paso 2</h4>
                                <p class="icon"><i class="fa fa-paste"><span class="bgcolor-major-gradient-overlay"></span></i></p>
                                <div class="price-group text-center">
                                    <span class="dollar">REQUISITOS</span>
                                </div>
                            </div> <!-- //price-heading -->
                            <ul class="price-feature ft-fm-2">
                                <li>Ingresa a la plataforma con tu usuario y contraseña. Actualiza tus datos personales y sube los documentos requeridos (DNI, Certifiado de estudios y voucher de matrícula)  a la plataforma.</li>
                            
                            </ul> <!-- //price-feature -->
                            
                        </div> <!-- //pricing-table -->
                    </div> <!-- //.col-sm-6 col-md-3 -->

                    <div class="col-sm-6 col-md-4">
                        <!-- pricing-table -->
                        <div class="pricing-table pricing-table-v2 text-center">
                            <div class="price-heading">
                                <h4 class="title">Paso 3</h4>
                                <p class="icon"><i class="fa fa-list-alt"><span class="bgcolor-major-gradient-overlay"></span></i></p>
                                <div class="price-group text-center">
                                    <span class="dollar">MATRÍCULA</span>
                                </div>
                            </div> <!-- //price-heading -->
                            <ul class="price-feature ft-fm-2">
                                <li>Luego de la entrega de los requisitos procederemos a hacer tu matrícula y será momento de iniciar tus clases con nosotros. </li>
                                <br>
                            </ul> <!-- //price-feature -->
                            
                        </div> <!-- //pricing-table -->
                    </div> <!-- //.col-sm-6 col-md-3 -->
            
                </div> <!-- //row -->
            </div>  <!-- //container -->
        </section>
        <!-- //End pricing-table v2  -->


                <!-- ========== PRICING TABLE SECTION  ========== -->
        <section class="bgcolor-gray section-common-space" style="background-color:#e9e9e9 ;">
            <div class="container">
                <!-- ========== SECTION HEADER ========== -->
                <div id="price" class="section-header section-header-v2 text-center">
                <h2 class="section-title">Nuestros precios</h2>
                    <p class="section-subtitle ft-fm-2">
                        Terminar la secundaria nunca más será complicado, CEBA Virtual Bruning y SECREA.
                    </p>
                </div> <!-- //section-header -->

                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        

                        <div class=" col-sm-6 col-md-6 col-lg-6">
                            <!-- pricing-table -->
                            <article class="single-post-v2 ">
                            <div class="img-wrapper relative">
                                <!-- <div class="bgcolor-major-gradient-overlay"></div> -->
                                <!-- <img src="img/blog/list2/2.jpg" alt="img"> -->
                                <iframe style="width:100%; height:250px;" src="https://www.youtube.com/embed/09mrDI8pYlA?autoplay=0&amp;showinfo=0&amp;autohide=0&amp;frameborder=0&amp;color=white&amp;modestbranding=0&amp;rel=0" frameborder="0"  encrypted-media"allowfullscreen"></iframe>
                                <!-- <a data-rippleria-color="#bdbdbd" class="ripple btn btn-lg btn-info hori-vert-center" href="single.html" style="overflow: hidden;">View more</a> -->
                            </div>
                            <div class="info pricing-table text-center">
                                <span class="tag">Precios accesibles para nuestros alumnos</span>
                                <h3 class="title">
                                    <!-- <a href="single.html">
                                        SECREA & CEBA BRUNING
                                    </a> -->
                                </h3>
                                <p class="intro">
                                    Plataforma SECREA de mano de su aliado CEBA Virtual Bruning  
                                </p>
                            </div> <!-- //.info -->
                        </article>
                        </div> <!-- //.col-sm-6 col-md-4 col-lg-3 -->
                        <div class=" col-sm-6 col-md-6 col-lg-6">
                            <!-- pricing-table -->
                            <div class="pricing-table text-center featured-price">
                                <div class="price-heading">
                                    <h3 class="title">Pensión</h3>
                                    <div class="price-group text-center">
                                        <span class="dollar">S/.</span>
                                        <span class="price">40</span>
                                        <span class="price-more">.00</span>
                                        <span class="time">/Mes</span>
                                    </div>
                                </div> <!-- //price-heading -->
                                <ul class="price-feature">
                                    <li>Contenido Multimedia</li>
                                    <li>Tutores 24/7</li>
                                    <li>Curso 100% virtual</li>
                                    <li></li>
                                </ul> <!-- //price-feature -->
                                <div class="price-footer">
                                    <a href="http://secundariacreativa.com/register" class="ripple btn btn-info">¡Quiero empezar ya!</a>
                                </div>
                            </div> <!-- //pricing-table -->
                        </div> <!-- //.col-sm-6 col-md-4 col-lg-3 -->
                    </div>
                </div> <!-- //row -->
            </div>  <!-- //container -->
        </section>
        <!-- //End pricing-table  -->   

        <!-- Features box -->
        <section id="services" class="services bgcolor-gray section-common-space">
            <div class="container">
                <!-- ========== SECTION HEADER ========== -->
                <div class="section-header section-header-v2 text-center">
                <h3 class="section-title">Somos un Colegio Virtual creado para que millones de jóvenes y adultos puedan terminar su secundaria a través de internet.</h3>
                <!--    <p class="section-subtitle ft-fm-3">
                        SECREA está conformado por profesionales en el campo de la Educación, con formación en entornos virtuales de aprendizaje.

                    </p> -->
                </div> <!-- //section-header -->

                <div class="text-center m-t-120">
                    <div class="row clear-problem">
                        <!-- EACH FEATURE -->
                        <div class="col-sm-6 col-md-4">
                            <div class="features-item">
                                <div class="features-icon">
                                    <i class="icon-bulb "></i>
                                </div>
                                <div class="features-info">
                                    <h4 class="title">VALOR OFICIAL</h4>
                                    <p>Te ofrecemos un certificado de estudios con valor oficial respaldado por el ministerio de educación.</p>
                                </div>
                            </div> <!-- //each-feature -->
                        </div> <!-- //.col-sm-6 col-md-4 -->


                        <!-- EACH SERVICE -->
                        <div class="col-sm-6 col-md-4">
                            <div class="features-item m-0 res-m-b-100">
                                <div class="features-icon">
                                    <i class="icon-cpu"></i>
                                </div>
                                <div class="features-info">
                                    <h4 class="title">INSERCIÓN LABORAL  </h4>
                                    <p>Es nuestro objetivo conectar a nuestros egresados con posibles empleadores.</p>
                                </div>
                            </div> <!-- //features-item -->
                        </div> <!-- //.col-sm-6 col-md-4 -->

                        <div class="col-sm-6 col-md-4">
                            <div class="features-item m-0 res-m-b-100">
                                <div class="features-icon">
                                    <i class="icon-layout"></i>
                                </div>
                                <div class="features-info">
                                    <h4 class="title">ACCESIBILIDAD</h4>
                                    <p>Nuestros estudiantes podrán ingresar a la plataforma desde cualquier disposiitivo.</p>
                                </div>
                            </div> <!-- //features-item -->
                        </div> <!-- //.col-sm-6 col-md-4 -->

                    </div> <!-- //row -->
                </div>
            </div> <!-- //container -->
        </section>
        <!-- //.Features box -->





        <!-- ========== ABOUT US SECTION ========== -->
        <!-- <section id="about-us" class="about-us-bg section-common-space"  style="background-image: url(img/about-us/about-bg.jpg);"> -->
        <section id="about-us" class="about-us-bg section-common-space"  >
            <div class="container">
                <!-- ========== SECTION HEADER ========== -->
                <div class="section-header section-header-v2 text-center">
                    <h2 class="section-title">¿Por qué elegirnos?</h2>
                    <p class="section-subtitle ft-fm-2">
                        Se parte de nuestra familia.
                    </p>
                </div> <!-- //section-header -->

                <div class="tabbed-about-us tabbed-about-us-v2">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- ========== NAV TABS ========== -->
                            <ul class="tabs-nav" role="tablist">
                                <li  class="active"  role="presentation">
                                    <a href="#EduDecalidad" aria-controls="EduDecalidad" role="tab" data-toggle="tab">
                                        <span class="icon">
                                            <i class="fa fa-shield"></i>
                                        </span>
                                        calidad
                                    </a>
                                    <span class="bgcolor-major-gradient-overlay"></span>
                                </li>

                                <li role="presentation">
                                    <a href="#creemosEnTi" aria-controls="creemosEnTi" role="tab" data-toggle="tab">
                                        <span class="icon">
                                            <i class="fa fa-thumbs-o-up "></i>
                                        </span>
                                        Creemos en tí
                                    </a>
                                    <span class="bgcolor-major-gradient-overlay"></span>
                                </li>


                                <li role="presentation">
                                    <a href="#calidadVida" aria-controls="calidadVida" role="tab" data-toggle="tab">
                                        <span class="icon">
                                            <i class="icon-layout"></i>
                                        </span>
                                        Calidad de vida
                                    </a>
                                    <span class="bgcolor-major-gradient-overlay"></span>
                                </li>
                                


                                <li role="presentation">
                                    <a href="#educVirtual" aria-controls="educVirtual" role="tab" data-toggle="tab">
                                        <span class="icon"><i class="fa fa-mixcloud "></i>
                                        </span>
                                        virtual
                                    </a>
                                    <span class="bgcolor-major-gradient-overlay"></span>
                                </li>

                                <li role="presentation">
                                    <a href="#oportunidades" aria-controls="oportunidades" role="tab" data-toggle="tab">
                                        <span class="icon"><i class="icon-building"></i>
                                        </span>
                                        OPORTUNIDADES
                                    </a>
                                        <span class="bgcolor-major-gradient-overlay"></span>
                                    </li>

                                <li role="presentation">
                                    <a href="#metodologia" aria-controls="metodologia" role="tab" data-toggle="tab">
                                        <span class="icon">
                                            <i class="fa fa-paperclip"></i>
                                        </span>
                                        METODOLOGÍA
                                    </a>
                                    <span class="bgcolor-major-gradient-overlay"></span>
                                </li>
                        
                            </ul>
                        </div> <!-- //.col-sm-6 -->

                        <div class="col-sm-6">
                            <div class="tab-content">
                                <!-- ========== TAB PANE ========== -->
                                <div role="tabpanel" class="tab-pane active" id="EduDecalidad">
                                    <div class="details-wrapper">
                                        <div class="details">
                                            <p>
                                                <h3>Educación de calidad</h3>
                                                
                                                <ul>
                                                    <li>Certificado de estudios con valor oficial.</li>
                                                    <li>Plana docente calificada.</li>
                                                    <li>Contar en todo momento con ayuda del docente. </li>
                                                    <li>Contar con un material didáctico y adecuado a sus requerimientos de aprendizaje.</li>
                                                </ul>
                                            
                                            
                                            </p>
                                        
                                        </div> <!-- //details -->
                                        
                                    </div> <!-- //.details-wrapper -->
                                </div> <!-- //tab-pane -->

                                <!-- ========== TAB PANE ========== -->
                                <div role="tabpanel" class="tab-pane" id="creemosEnTi">
                                    <div class="details-wrapper">
                                        <div class="details">
                                            <p>
                                                <h3>Creemos en ti</h3>
                                                
                                                <ul>
                                                    <li>La posibilidad de terminar los estudios mejora la autoestima de la persona que demuestra que puede seguir estudiando y concluir sus estudios.</li>
                                                    <li>Poder continuar estudios superiores en universidades e institutos tecnológicos, instituciones militares o a la Policía Nacional del Perú.</li>
                                                    
                                                </ul>
                                            </p>
                                        </div> <!-- //details -->
                                        
                                    </div> <!-- //.details-wrapper -->
                                </div> <!-- //tab-pane -->

                                <!-- ========== TAB PANE ========== -->
                                <div role="tabpanel" class="tab-pane" id="calidadVida">
                                    <div class="details-wrapper">
                                        <div class="details">
                                            <p>
                                                <h3>Calidad de vida</h3>
                                                
                                                <ul>
                                                    <li>Mejora sus ingresos a nivel personal, familiar, etc.</li>
                                                    <li>Contribuye a mejorar la calidad de prestación en el trabajo que desarrolla.</li>
                                                    
                                                </ul>
                                            </p>
                                        </div> <!-- //details -->
                                        
                                    </div> <!-- //.details-wrapper -->
                                </div> <!-- //tab-pane -->

                                <!-- ========== TAB PANE ========== -->
                                <div role="tabpanel" class="tab-pane" id="educVirtual">
                                    <div class="details-wrapper">
                                        <div class="details">
                                            <p>
                                                <h3>Educación virtual</h3>
                                                
                                                <ul>
                                                    <li>Poder acceder a un servicio educativo de calidad desde cualquier lugar y manejando sus propios horarios.</li>
                                                    <li>Reducción de costos de transporte a un centro de estudios.</li>
                                                    
                                                </ul>
                                            </p>
                                        </div> <!-- //details -->
                                        
                                    </div> <!-- //.details-wrapper -->
                                </div> <!-- //tab-pane -->

                                <!-- ========== TAB PANE ========== -->
                                <div role="tabpanel" class="tab-pane" id="oportunidades">
                                    <div class="details-wrapper">
                                        <div class="details">
                                            <p>
                                                <h3>Oportunidades</h3>
                                                
                                                <ul>
                                                    <li>Acceso a oportunidades laborales debido a que obtendría un plus salarial por el certificado.</li>
                                                    <li>Te orientamos para que puedas obtener becas de estudio en instituciones de nivel superior.</li>
                                                    
                                                </ul>
                                            </p>
                                        </div> <!-- //details -->
                                        
                                    </div> <!-- //.details-wrapper -->
                                </div> <!-- //tab-pane -->

                                <!-- ========== TAB PANE ========== -->
                                <div role="tabpanel" class="tab-pane" id="metodologia">
                                    <div class="details-wrapper">
                                        <div class="details">
                                            <p>
                                                <h3>Metodología</h3>
                                                
                                                <ul>
                                                    <li>Clases prácticas con presentaciones en diversos formatos pdf, word, ppt y videos. </li>
                                                    <li>Tutorías a través de herramientas como chat, foros y videoconferencias lo cual facilita el contacto de acuerdo a las necesidades del estudiante.</li>
                                                    <li>Planificación para que el estudiante pueda conocer con antelación los objetivos de la asignatura, la programación o calendario, el método de evaluación, actividades.</li>
                                                    
                                                </ul>
                                            </p>
                                        </div> <!-- //details -->
                                        
                                    </div> <!-- //.details-wrapper -->
                                </div> <!-- //tab-pane -->
                            </div> <!-- //tab-content -->
                        </div>
                    </div><!-- //.row -->
                </div> <!-- //tabbed-about-us -->
            </div> <!-- //container -->
        </section>
        <!-- //End About us -->


        <!-- ========== NEWSLETTER SECTION ========== -->
        <section class="newsletter bgcolor-major-gradient">
            <div class="section-header section-header-v2 text-center">
                <h2 class="section-title">Recibe noticias de nosotros</h2>
                <p class="section-subtitle ft-fm-2">
                    Déjanos tu e-mail y recibirás notificaciones de nosotros.
                </p>
            </div> <!-- //section-header -->

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mailchimpForm-wrapper text-center">
                            <form action="#" class="mailchimpForm m-t-20 text-left">
                                <span class="input">
                                    <input class="input-field" type="text" id="nl_email" name="EMAIL" />
                                    <label class="input-label" for="nl_email">
                                        <span class="input-label-content">Tu email</span>
                                    </label>
                                </span>
                                <button type="submit" class="btn ripple">Suscríbete</button>
                            </form> <!-- //form -->
                         </div> <!-- //mailchimpForm-wrapper -->
                    </div> <!-- //.col-md-12 col-lg-7 -->
                </div> <!-- //row -->
            </div> <!-- //container -->
        </section>
        <!-- //End newsletter -->


        <!-- ========== FOOTER ========== -->
        <footer class="bgcolor-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p class="footer-logo bgcolor-major-gradient m-b-30"><img src="webSecrea/img/logo-white.png" alt="logo"></p>
                        <p class="copyright m-b-10">© Copyright 2018 <b>SECREA</b>. Todos los derechos Reservados</p>
                        <p class="copyright m-b-10"><a href="webSecrea/politicas.html">Politicas de Privacidad</a></p>
                        <p class="made-by">Secundaria Creativa  </i></p>
                    </div> <!-- //col-sm-12 -->
                </div> <!-- //row -->
            </div> <!-- //container -->
        </footer>
        <!-- //End footer -->


        <!-- Go to top button -->
        <button data-rippleria-color="#fff" class="go-top ripple"><i class="icon-angle-up"></i></button>

    </div>
    <!-- //End page-wrap -->


    <!-- ==================   SCRIPT PART   ================= -->
    <script type="text/javascript" src="webSecrea/js/vendor.js"></script>
    <script type="text/javascript" src="webSecrea/js/plugins.js"></script>
    <script type="text/javascript" src="webSecrea/js/app.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){

            $('.menuButton2').on('click', function(event){
                event.preventDefault();
                $('body').removeClass('offcanvas');
                $('.offcanvas-toggler').removeClass('active');
            });
                
        });

        function myFunction2() {

            var cadena = $("#email").val();

            if(cadena.indexOf('@') != -1){
                document.getElementById('txtcorreo').innerHTML=''; 

            }else{

                if (cadena.length < 1) {
                    $('.txtcorreo').css("color","#a94442");
                    document.getElementById('txtcorreo').innerHTML='Completa este campo'; 
                }
                else{
                    $('.txtcorreo').css("color","#a94442");
                    document.getElementById('txtcorreo').innerHTML='Incluye un signo @ en el correo'; 
                };
            }

                
        }
        


    </script>
</body>


</html>
