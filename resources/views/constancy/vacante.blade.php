<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Constancia de {{$data['type']}}</title>
    <style>
        .all{
            padding-right:40px;
            padding-left:40px;
            font-size:22px;
        }
        #fecha{
            text-align:right;
            padding-top:30px;
            padding-bottom:50px;
        }
        #anho{
            text-align:center;
            padding-right:50px;
            padding-left:50px;
        }
        #constancy{
            padding:40px;
            font-size:35px;
        }
        .top{
            padding:20px;
        }
        .body{
            padding-top:100px;
        }
    </style>
</head>
<body>
@php
    $mes = [
        '1' => 'Enero',
        '2' => 'Febrero',
        '3' => 'Marzo',
        '4' => 'Abril',
        '5' => 'Mayo',
        '6' => 'Junio',
        '7' => 'Julio',
        '8' => 'Agosto',
        '9' => 'Septiembre',
        '10'=> 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre'
    ];
@endphp
<div class="all">
  
    <div class="top">
        <div style="float:left;margin-top:-20px;">
            <img width="220px" src="images/brunning.png" >
        </div>
        <div style="float:right">
            <img width="180px;" src="images/logo.png" >
        </div>
    </div>

    <div class="body">
        <p id="anho"><strong>“AÑO DEL DIÁLOGO Y LA RECONCILIACIÓN NACIONAL”</strong></p>
        <p style="text-align:justify;">
            &nbsp;&nbsp;&nbsp;&nbsp; El que suscribe, en representación de la  Institución Educativa Básica Alternativa “Bruning College”, identificado con R. D Nº 3065 – 2014.GR.LAMB/GRED-UGEL-CHIC; otorga la presente 
        </p>
        <p id="constancy"><strong><center>CONSTANCIA DE {{$data['type']}}</center></strong></p>
        <p style="text-align:justify">
            Al estudiante {{ strtoupper($data['name'])}}, para cursar el {{$data['grade']}} de Educación Secundaria en nuestra Institución Educativa, en el presente Año Escolar {{\Carbon\Carbon::now()->formatLocalized('%Y')}}.
            <br>
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;Se expide la presente a petición del interesado para los fines correspondientes.
        </p>
        
        <p id="fecha">            
            {{ "Chiclayo, ". $mes[\Carbon\Carbon::now('America/Lima')->month].' '. \Carbon\Carbon::now('America/Lima')->formatLocalized('%Y') }} 
        </p>
        
        <div style="width:100%;text-align:center">
            
                __________________________________
                <br>
                Lic. Carlos Eduar Ramírez Carrasco
            
        </div>
        
        
    </div>

    
    
</div>
</body>
</html>