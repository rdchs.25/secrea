<!-- Meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="description" content="">
<meta name="author" content="">

<!-- Title -->
<title>Secrea</title>

<!-- Favicon -->
<link rel="shortcut icon" href="{{{ asset('img/logo-small.png') }}}">

<!-- Vendor CSS -->
<link rel="stylesheet" href="/vendor/bootstrap4/css/bootstrap.min.css">
<link rel="stylesheet" href="/vendor/themify-icons/themify-icons.css">
<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/vendor/animate.css/animate.min.css">
<link rel="stylesheet" href="/vendor/jscrollpane/jquery.jscrollpane.css">
<link rel="stylesheet" href="/vendor/waves/waves.min.css">
<link rel="stylesheet" href="/vendor/switchery/dist/switchery.min.css">
<link rel="stylesheet" href="/vendor/DataTables/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="/vendor/toastr/toastr.min.css">
<link rel="stylesheet" href="/vendor/spinkit/css/spinkit.css">
<link rel="stylesheet" href="/vendor/dropify/dist/css/dropify.min.css">

<script type="text/javascript" src="/vendor/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/vendor/toastr/toastr.min.js"></script>


<!--  CSS -->
<link rel="stylesheet" href="/css/core.css">
<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/css/pagination.css">

<style>
    #content-comunicado {
      background-color: rgba(250, 240, 245,0.9);
      height: 100%;
      width: 100%;
      position:fixed;
      -webkit-transition: all 1s ease;
      -o-transition: all 1s ease;
      transition: all 1s ease;
      z-index:1000;
      padding-top:100px;
    }
    #comunicado {
        font-size:15px;
        text-align: justify;
    }
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113868199-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113868199-1');
</script>
