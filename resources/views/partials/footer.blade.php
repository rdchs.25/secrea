<footer class="footer">
    <div class="container-fluid">
        <div class="row text-xs-center">
            <div class="col-sm-4 text-sm-left mb-0-5 mb-sm-0">
                © Secrea 2017
            </div>
            <div class="col-sm-8 text-sm-right">
                <ul class="nav nav-inline l-h-2">
                    <li class="nav-item"><a class="nav-link text-black" href="www.secrea.net">Web de secrea</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>