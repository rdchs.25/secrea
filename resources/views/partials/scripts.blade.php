
<script type="text/javascript" src="/vendor/tether/js/tether.min.js"></script>
<script type="text/javascript" src="/vendor/bootstrap4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/vendor/detectmobilebrowser/detectmobilebrowser.js"></script>
<script type="text/javascript" src="/vendor/jscrollpane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/vendor/jscrollpane/mwheelIntent.js"></script>
<script type="text/javascript" src="/vendor/jscrollpane/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="/vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js"></script>
<script type="text/javascript" src="/vendor/waves/waves.min.js"></script>
<script type="text/javascript" src="/vendor/switchery/dist/switchery.min.js"></script>
<script type="text/javascript" src="/vendor/DataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/vendor/DataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/demo.js"></script>
<script type="text/javascript" src="/js/sidebar.js"></script>

<script type="text/javascript" src="/vendor/dropify/dist/js/dropify.min.js"></script>
<script type="text/javascript" src="/js/forms-upload.js"></script>
<script type="text/javascript">
    // IMPORTANTE -> TRADUCCION DE DROPIFY
        $('.dropify').dropify({
            messages: {
                default: 'Click para subir tu voucher',
                replace: 'Click para subir tu voucher nuevamente',
                remove:  'Eliminar',
                error:   'No pudimos subir la imágen'
            }
        });

</script>

    <script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
        $('#users_sms').change(()=>{
            if($('#users_sms').val() == 4){
                $(".chbxuser").prop('checked',true);
            }else{
                $("input:checkbox").each(function() {
                    var user = document.getElementById('chbxuser_'+$(this).val());
                    
                    if($('#users_sms').val() == 1){
                        val = user.dataset.dni;
                    }
                    if($('#users_sms').val() == 2){
                        val = user.dataset.vou;
                    }
                    if($('#users_sms').val() == 3){
                        val = user.dataset.cer;
                    }
                    $("#chbxuser_"+$(this).val()).prop('checked',(val == "true")? false : true);
                });
            } 
        });
    } );

    $('#example').DataTable( {
        "language": {
            "url": "{{ asset('/js/Spanish.json') }}"
        },
        "pageLength": 10,
    } );



</script>


