<div class="site-overlay"></div>
<div class="site-sidebar">
    <div class="custom-scroll custom-scroll-light">
        <ul class="sidebar-menu">
            <li class="menu-title">Menú</li>
            <li class="active">
                <a href="/home" class=" waves-effect  waves-light">
                    <span class="s-icon"><i class="ti-home"></i></span>
                    <span class="s-text">Inicio</span>
                </a>
            </li>
            @if(Auth::user()->isRole('registrado') || Auth::user()->isRole('student') )
            @else
            <li class="active">
                <a href="/profile" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="ti-user"></i></span>
                    <span class="s-text">Mi Perfil</span>
                </a>
            </li>
            @endif

            @can('my-documents')
            <li class="active">
                <a href="/document/{{Auth::user()->id}}/edit" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-file-text-o"></i></span>
                    <span class="s-text">Mis Documentos</span>
                </a>
            </li>
            @endcan
            
            @if(Auth::user()->isRole('registrado') || Auth::user()->isRole('student') )
            <li class="active">
                <a href="/my-payment/current" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-dollar"></i></span>
                    <span class="s-text"> Mis pagos</span>
                </a>
            </li>
            @endif
            
            @can('chat')
                <li class="active">
                    <a href="/chat" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-comment-o"></i></span>
                        <span class="s-text">Chat</span>
                    </a>
                </li>
            @endcan
            @can('my-subjects-management')
                <li class="active">
                    <a href="/my-subjects" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="ti-ruler-pencil"></i></span>
                        <span class="s-text">Mis Asignaturas</span>
                    </a>
                </li>
            @endcan
            @role('director')
            <li class="active">
                <a href="/assignment" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-book"></i></span>
                    <span class="s-text">Asignaciones</span>
                </a>
            </li>
            @endrole
            @can('my-assignments-management')
            <li class="active">
                <a href="/my-assignment" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-book"></i></span>
                    <span class="s-text">Mis Asignaciones</span>
                </a>
            </li>
            @endcan
            @can('user-management')
            <li class="menu-title">Configuración</li>
                <li class="active">
                    <a href="/user" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-users"></i></span>
                        <span class="s-text">Usuarios</span>
                    </a>
                </li>  
            @endcan
            @can('rol-management')          
                <li class="active">
                    <a href="/role" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-check-square-o"></i></span>
                        <span class="s-text">Roles</span>
                    </a>
                </li>
            @endcan 
            @can('academic-times-management') 
            <li class="menu-title">Configuración Académica</li>
                <li class="active">
                    <a href="/academic-time" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-calendar"></i></span>
                        <span class="s-text">Tiempos Académicos</span>
                    </a>
                </li>
            @endcan 
            @can('academic-level-management')  
            <li class="active">
                <a href="/academic-level" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-graduation-cap"></i></span>
                    <span class="s-text">Niveles Académicos</span>
                </a>
            </li> 
            @endcan
             @can('grade-management')  
                <li class="active">
                    <a href="/grade" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-bars"></i></span>
                        <span class="s-text">Grados</span>
                    </a>
                </li>            
            @endcan
             @can('subject-management')  
                <li class="with-sub">
                    <a href="/subject" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-book"></i></span>
                        <span class="s-text">Asignaturas</span>
                    </a>
                </li>
            @endcan

            @can('assignment-teacher-management')  
                <li class="active">
                    <a href="/assignment-teacher" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-list-alt"></i></span>
                        <span class="s-text">Asignaciones de Docentes</span>
                    </a>
                </li>
            @endcan
            @can('assignment-content-manager-management')  
                <li class="active">
                    <a href="/content-manager-assignment" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-list-alt"></i></span>
                        <span class="s-text">Asignaciones de Gestores de Contenido</span>
                    </a>
                </li>
            @endcan

            @can('evaluation-type-management')
                <li class="active">
                    <a href="/evaluation-type" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-sticky-note"></i></span>
                        <span class="s-text">Gestión Tipos de Evaluación</span>
                    </a>
                </li>
            @endcan

            @can('assignment-management')  
                <li class="active">
                    <a href="/assignment-teacher" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-list-alt"></i></span>
                        <span class="s-text">Asignaciones a Docentes</span>
                    </a>
                </li>
            @endcan
            @can('announcement-management')
                <li class="active">
                    <a href="/announcement" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-file-text-o"></i></span>
                        <span class="s-text"> Ciclos académicos</span>
                    </a>
                </li>            
            @endcan
            @can('pre-enrollment-management')  
                <li class="active">
                    <a href="/pre-enrollment" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-check"></i></span>
                        <span class="s-text">Pre-Inscripciones de Ciclos Académicos</span>
                    </a>
                </li>
            @endcan
            @can('documentary-management')  
            <li class="menu-title">Gestión de Académica</li>               
                <li class="active">
                    <a href="/documentary-management" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-search"></i></span>
                        <span class="s-text">Control de Documentos</span>
                    </a>
                </li>
            @endcan
            @can('evaluation-monitor')  
            <li class="active">
                <a href="/evaluation-monitor" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-search"></i></span>
                    <span class="s-text">Monitoreo general de asignaturas</span>
                </a>
            </li>
            @endcan
            @can('agreement-management')  
            <li class="menu-title">Gestión de Ventas</li>               
                <li class="active">
                    <a href="/agreement" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-handshake-o"></i></span>
                        <span class="s-text">Convenios</span>
                    </a>
                </li>
            @endcan
            @can('agreement-management')                    
                <li class="active">
                    <a href="/company" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-building-o"></i></span>
                        <span class="s-text">Empresas Asociadas</span>
                    </a>
                </li>
            @endcan
            <li class="menu-title">Comunicación</li>              
                <li class="active">
                    <a href="/support-message/create" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="ti-headphone-alt"></i></span>
                        <span class="s-text">Contacto con Soporte técnico</span>
                    </a>
                </li>
            @can('support-message-management')           
                <li class="active">
                    <a href="/support-message/" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="ti-headphone-alt"></i></span>
                        <span class="s-text">Gestión Mensajes con Soporte técnico</span>
                    </a>
                </li>
            @endcan
            @can('messages-management')        
                <li class="active">
                    <a href="/sms" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-mobile"></i></span>
                        <span class="s-text">Mensajes</span>
                    </a>
                </li>
            @endcan  
            @can('form-generator')
            <li class="menu-title">Creación de Contenido</li>              
                <li class="active">
                    <a href="/form-generator" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-table"></i></span>
                        <span class="s-text">Generador de Formularios</span>
                    </a>
                </li>
            @endcan 
            @can('media-management')        
                <li class="active">
                    <a href="/form-generator-media-management" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-table"></i></span>
                        <span class="s-text">Gestión Multimedia</span>
                    </a>
                </li>
            @endcan 
            @can('notifications-management')
            <li class="menu-title">Gestión de Notificaciones</li>              
                <li class="active">
                    <a href="/notification-management" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-bell"></i></span>
                        <span class="s-text">Notificaciones</span>
                    </a>
                </li>
            @endcan              
            @can('reports-management')
            <li class="menu-title">Gestión de Reportes y Constancias</li>              
            <li class="active">
                <a href="/report" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-file-o"></i></span>
                    <span class="s-text">Reportes</span>
                </a>
            </li>
            <li class="active">
                <a href="/constancy" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="fa fa-file-o"></i></span>
                    <span class="s-text">Constancias</span>
                </a>
            </li>
            <li class="menu-title">Demo</li>              
                <li class="active">
                    <a href="/course-demo" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-book"></i></span>
                        <span class="s-text">Demo de Asignaturas</span>
                    </a>
                </li>
            @endcan  
            @can('backups-management')        
                <li class="active">
                    <a href="/backup" class="waves-effect  waves-light">
                        <span class="s-icon"><i class="fa fa-table"></i></span>
                        <span class="s-text">Respaldos de Seguridad</span>
                    </a>
                </li>
            @endcan  
        </ul>
    </div>
</div>