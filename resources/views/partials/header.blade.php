<div class="site-header">
    <nav class="navbar navbar-dark gradient gradient-success">
        <div class="navbar-left">
            <a class="navbar-brand" href="/home">
                <div class="logo"></div>
            </a>
            
            @if( Auth::user()->isRole('registrado') || Auth::user()->isRole('student') )
                <a href="/home">
                    <div class="toggle-button float-xs-left hidden-md-up light MenuInicio">
                        <i class="fa fa-home" style="font-size:2.5rem"></i> <div class="textMenu">Inicio</div>
                    </div>
                </a>
            @else
                <div class="toggle-button sidebar-toggle-first float-xs-left hidden-md-up light">
                    <span class="hamburger"></span>
                </div>
            @endif

            
            <!--<div class="toggle-button-second float-xs-right hidden-md-up light">
                <i class="ti-arrow-left"></i>
            </div>-->
            <div class="toggle-button float-xs-right hidden-md-up light" data-toggle="collapse" data-target="#collapse-1">
                <span class="more"></span>
            </div>
        </div>
        
        <div class="navbar-right navbar-toggleable-sm collapse" id="collapse-1">
            
            @if( Auth::user()->isRole('registrado') || Auth::user()->isRole('student') )
                <ul class="nav navbar-nav">
                    <!--<li class="nav-item hidden-sm-down">
                        <a class="nav-link toggle-fullscreen" href="#">
                            <i class="ti-fullscreen"></i>
                        </a>
                    </li>-->
                    <li class="nav-item hidden-sm-down ">
                        <a class="nav-link " href="{{ URL::previous() }} " >
                        <div class="MenuInicio">
                            <i class="ti-arrow-left" style="font-size:1.5rem"></i> <div class="textMenu">atrás</div>
                        </div>
                        </a>
                    </li>
                    
                </ul>
                <a href="/home">
                    <div class="toggle-button float-xs-left hidden-sm-down dark MenuInicio">
                        <i class="fa fa-home" style="font-size:2rem"></i><div class="textMenu">inicio</div>
                    </div>
                </a>
            @else
                <div class="toggle-button sidebar-toggle-second float-xs-left hidden-sm-down dark ">
                    <span class="hamburger"></span>
                </div>
            @endif
            
            <ul class="nav navbar-nav float-md-right" id="navbar">
                @can('chat')
                <chat-icon></chat-icon>
                @endcan
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">
                        <i class="ti-bell"></i>
                        <span class="hidden-md-up ml-1">Notificaciones</span>
                        <span class="tag tag-alert top"></span>
                    </a>

                    <div class="dropdown-messages dropdown-tasks dropdown-menu dropdown-menu-right animated fadeInUp">

                        @for($i = 0; $i < 3; $i++)
                            @if(isset(Auth::user()->notifications[$i]))
                            <div class="m-item">
                                <div class="mi-icon bg-success"><i class="ti-bell"></i></div>
                                <div class="mi-text"><a class="text-black" href="#">{{ Auth::user()->notifications[$i]->title }}</a> 
                                <br>
                                <a class="text-black" href="#">{{ Auth::user()->notifications[$i]->message }}</a></div>
                                <div class="mi-time">{{ \Carbon\Carbon::parse(Auth::user()->notifications[$i]->created_at)->diffForHumans() }}</div>
                            </div>
                            @endif
                        @endfor
                        
                        <a class="dropdown-more" href="/notification">
                            <strong>Ver todas las notificaciones</strong>
                        </a>
                    </div>
                </li>
                
                <li class="nav-item dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                        <span class="avatar box-32">
                            @if(empty(Auth::user()->profiles->path_profile_image))
                            <div class="img-div">    
                                <img src="/img/avatars/avatar.jpg" alt="">
                            </div>
                            @else
                            <div class="img-div">    
                                <img src="/storage/{{ Auth::user()->profiles->path_profile_image}}" alt="">
                            </div>
                            @endif
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right animated fadeInUp">
                        
                        <a class="dropdown-item" href="/profile">
                            <i class="ti-user mr-0-5"></i> Mi Perfil
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="ti-power-off mr-0-5"></i>
                            Cerrar Sesión
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </li>
            </ul>
            

        </div>
    </nav>
</div>

@section('scripts')
    @if( Auth::user()->isRole('student') )
        <script type="text/javascript" src="/js/course.js?v={{time()}}"></script>
    @else
        <script type="text/javascript" src="/js/course.js"></script>
    @endif
    
@endsection