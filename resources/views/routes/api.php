<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/academic-level', function (Request $request) {
    return \App\AcademicLevel::orderBy('name','desc')->get();
});

Route::post('/file-upload', 'GalleryController@storeFile');
Route::post('/audio-upload', 'GalleryController@storeAudio');
Route::get('/course-builder/{id}', 'CourseBuilderController@getCourseInfo');
Route::get('/course/{id}', 'CourseBuilderController@getCourseInfo');
Route::get('/course-preview/{id}', 'CourseBuilderController@getCoursePreview');
