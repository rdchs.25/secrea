@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">

            <div id="cabeceraFlotante">
                <h4>Contacto con el Soporte</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Soporte</a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2">

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Contacto con el Soporte</h4>
                                <p class="font-90 text-muted mb-1">Describe aquí tu problema o inconveniente técnico con la aplicación, y en breve sera atendido y solucionado.</p>
                             </div>

                            {!! Form::open(['url' => '/support-message', 'class' => 'mt-3 form-material material-primary']) !!}
                                @include('forms._support-message_form', ['submitButtonText' => 'Enviar Mensaje'])
                            {!! Form::close() !!}

                            @include('errors.list')

                        </div>

                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
