@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Demo de Asignaturas</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Demo de Asignaturas</a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2">
              <div class="col-md-12 col-sm-12  col-xs-12">
                <div class="banner">
                <div class="b-img img-cover" style="background-image: url(img/photos-1/1.jpg);"></div>
                  <div class="gradient gradient-success"></div>
                  <div class="b-content">
                    <div class="b-title"><span id="grado">Demo de Asignaturas</span></div>
                    <div class="b-text">Lista de cursos asignados</div>
                  </div>
                </div>
                <br>
              </div>
              
              {{--  Condición temporal usuario Virgilio para DEMO  --}}
              @if(Auth::user() && Auth::user()->name == "Virgilio Acuna")
              
              @foreach($subjects as $item)
                @if($item->id == 1 || $item->id == 9)
                  <div class="col-md-3 col-md-4  col-sm-6  col-xs-12">
                    <div class="box bg-white user-1">
                      <div class="fondocurso">
                        <a href="/course-preview/{{ $item->id }}">
                          <div class="u-img img-cover" style="background-image: url(/storage/{{ $item->path_image_subject }});"></div>
                        </a>
                      </div>
                      <div class="u-content mt-3">
                        <h5><a class="text-black">{{ $item->name }}</a></h5>
                        <h5><a class="text-black">{{ $item->identify }}</a></h5>
                      </div>
                    </div>
                  </div>
                @endif
              @endforeach          

              @else

              @foreach($subjects as $item)
              <div class="col-md-3 col-md-4  col-sm-6  col-xs-12">
                <div class="box bg-white user-1">
                  <div class="fondocurso">
                    <a href="/course-preview/{{ $item->id }}">
                    	<div class="u-img img-cover" style="background-image: url(/storage/{{ $item->path_image_subject }});"></div>
                    </a>
                  </div>
                  <div class="u-content mt-3">
                    <h5><a class="text-black">{{ $item->name }}</a></h5>
                    <h5><a class="text-black">{{ $item->identify }}</a></h5>
                  </div>
                </div>
              </div>
              @endforeach

              @endif

        </div>
    </div>
@endsection
