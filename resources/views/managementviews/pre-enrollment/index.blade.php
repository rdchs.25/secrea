@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Ciclos Academicos</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Ciclos Académicos</a></li>
                    </ol>
                </nav>
            </div> 

            <div id="mFloatBox2">

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                      <div class="mt-2  pv-content">
                         <div class="pv-title">
                            <h4 class="">Ciclos Académicos Abiertos</h4>
                            <p class="font-90 text-muted mb-1">Selecciona un ciclos academicos disponible para pre inscribirte a ella </p>
                         </div>
                      </div>
                      <div class="row">
                        @foreach($announcements as $id => $announcement)
                        <div class="col-md-3 col-md-4  col-sm-6  col-xs-12">
                          <a href="/pre-enrollment/{{ $announcement->id }}" >
                            <div class="box bg-white user-1">
                              <div class="fondocurso">
                                <div class="u-img img-cover" style="background-image: url(http://www.abuelosmodernos.com/example/slider03.jpg);"></div>
                              </div>
                              <div class="u-content mt-3">
                                <h5><a class="text-black" href="/pre-enrollment/{{ $announcement->id }}">{{ $announcement->name }}</a></h5>
                                <p>Click aqui para pre-inscribirte</p>
                                @foreach($announcementsByUser as $id => $announcementByUser)
                                  @if($announcementByUser->announcements->name == $announcement->name)
                                  PRE-INSCRITO
                                  <i data-toggle="tooltip" data-placement="right" data-original-title="Preinscrito" class="sin-tarea fa fa-check-circle"></i>
                                  @endif
                                @endforeach
                              </div>
                            </div>
                          </a>
                        </div>
                        @endforeach
                      </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
