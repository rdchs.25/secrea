@extends('layouts.no-site-content')

@section('content')  
		<div id="app">
			<course-preview></course-preview>
		</div>	
@endsection

@section('scripts')
	<script type="text/javascript" src="/js/course.js?v={{time()}}"></script>
	<script>
		$(document).ready(function(){
			$('body').addClass('compact-sidebar');
		})
	</script>
@endsection
	