@extends('layouts.app')

@section('header')
        <link rel="stylesheet" href="/vendor/dropify/dist/css/dropify.min.css">
        <link rel="stylesheet" href="/vendor/SpinKit/css/spinkit.css">    
@endsection

@section('content')  
<div id="app">
    <course-builder></course-builder>
</div>

<script type="text/javascript" src="/vendor/dropify/dist/js/dropify.min.js"></script>
@endsection
