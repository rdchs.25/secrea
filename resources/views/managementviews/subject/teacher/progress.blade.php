@extends('layouts.app')

@section('content')  
            <div class="content-area py-1">
               <div class="container-fluid">
                  <div id="cabeceraFlotante">
                     <h4>Ver Progeso</h4>
                     <nav class="navbar navbar-light bg-white b-a ">
                        <ol class="breadcrumb no-bg mb-0">
                           <li class="breadcrumb-item"><a href="#">Home</a></li>
                           <li class="breadcrumb-item active"><a href="#"> Progeso del Estudiante </a></li>
                        </ol>
                     </nav>
                  </div>



                  <div class="mt-2 col-md-12 col-sm-12">
                     <div class="card" id="mFloatBox">
                        <div class="card-header clearfix">
                           <h5 class="float-xs-left mb-0">{{ $student->name }} <small>| {{ $subject->name }} - {{ $subject->identify }}</small> </h5>
                           <div class="float-xs-right">{{ \Carbon\Carbon::now()->format('d/m/Y') }}</div>
                        </div>
                        <div class="card-block">
                           <div class="row mb-2">
                              <div class="col-sm-12 col-xs-12">
                                 <p class="mb-0"><b> Teléfono:</b> {{ $student->profiles->phone or 'No registrado'}}</p>
                                 <p class="mb-0"><b> Correo Electrónico:</b> {{ $student->email or 'No registrado'}} </p>
                                 <p class="mb-0"><b> Último ingreso al sistema: </b> {{ $student->last_login != null ? \Carbon\Carbon::parse($student->last_login)->diffForHumans() : 'Ninguna' }} </p>
                                <p class="mb-0"><b>Avance del curso:</b> {{ $progress }}% </p>
                                 <p>
                                    <progress class="progress progress-success progress-md d-inline-block mb-0" value="{{ $progress }}" max="100">{{ $progress }}%</progress> 
                                 </p>
                                @if($evaluations->isEmpty())
                                    <h5>No hay notas registradas para este estudiante</h5>
                                @endif
                              </div>

                           </div>

                           <table class="table table-bordered table-striped mb-2">
                              <thead>
                                 <tr>
                                    <th>Tipo</th>
                                    <th>Semana</th>
                                    <th>fecha</th>
                                    <th>Nota</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($evaluations as $id => $evaluation)
                                 <tr>
                                    <td>{{ $evaluation->evaluationType->name}}</td>
                                    <td>{{ $evaluation->week }}</td>
                                    <td>{{ $evaluation->created_at }}</td>
                                    <td>
                                        {{ $evaluation->value }}%
                                        ({{ ($evaluation->value*20)/100}})
                                    </td>
                                 </tr>
                                 @endforeach
                              </tbody>
                           </table>
                           <div class="row">
                              <div class="col-lg-6">
                                 <strong>Términos</strong>
                                 <p class="text-muted mb-0">En la tabla se detalla el progreso del estudiante a la fecha, así como su infromación personal con el fin exclusivo de analizar su avance en la plataforma.</p>
                              </div>
                              <div class="col-lg-6">
                                 <div class="text-xs-right">
                                    <div class="mb-0-5">Nota Cuestionarios:     <b>{{ $Quizaverage }}% ({{ ($Quizaverage*20)/100 }})</b></div>
                                    <div class="mb-0-5">Nota Examenes Finales:  <b>{{ $Evalaverage }}% ({{ ($Evalaverage*20)/100 }})</b></div>
                                    <br>
                                    <div class="mb-0-5">Promedio Cuestionarios (60%): <b>{{ $FinalQuizaverage }}% ({{ ($FinalQuizaverage*20)/100 }})</b></div>
                                    <div class="mb-0-5">Promedio Examenes (40%):      <b>{{ $FinalEvalaverage }}% ({{ ($FinalEvalaverage*20)/100 }})</b></div>

                                    <div class="mb-0-5">Nota promedio Final: <b>{{ (($FinalQuizaverage+$FinalEvalaverage)*20)/100}}</b></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="card-footer clearfix">
                           <button type="button" class="btn btn-info label-left float-xs-right">
                              <span class="btn-label"><i class="ti-download"></i></span>
                              Download
                           </button>
                           
                        </div>
                     </div>

                  </div>


@endsection
