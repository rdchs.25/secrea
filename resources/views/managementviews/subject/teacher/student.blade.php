@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            <div id="cabeceraFlotante">
                <h4>Alumnos</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Alumnos</a></li>
                    </ol>
                </nav>
            </div> 
    
            <div id="mFloatBox2"></div>

            <div class="mt-2 col-md-12 col-sm-12">
                <div  class="box box-block bg-white">
                   <div class="tab-content">
                        <div class="mt-2  pv-content">
                             <div class="pv-title">
                                <h4 class="">Mis alumnos</h4>
                                <p class="font-90 text-muted mb-1"> {{$subject->name}} ({{$subject->identify}}) </p>
                             </div>
                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped table-bordered dataTable" id="example">
                             <thead>
                                <tr>
                                   <th>#</th>
                                   <th>Alumno</th>
                                   <th>Teléfono</th>
                                   <th>Email</th>
                                   <th>Progreso</th>
                                   <th>Opciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($studentsByAssignment as $id => $student)
                                    <tr>
                                        <td>
                                            {{ $id + 1}}
                                        </td>
                                        <td>
                                            {{ $student->user->name }}
                                        </td>
                                        <td>
                                            {{ $student->user->profiles->phone or 'No registrado'}}
                                        </td>
                                        <td>
                                            {{ $student->user->email }}
                                        </td>
                                        <td>
                                            <div class="col-md-2 pull-right">
                                                <p class="mb-0-5"><span class="float-xs-right">{{ $student->final_score }}%</span></p>
                                            </div> 
                                            @if($student->final_score >= '0' && $student->final_score <= '33')
                                                <div class="col-md-10">
                                                    <progress class="progress progress-danger progress-sm" value="{{ $student->final_score }}" max="100">{{ $student->final_score }}%</progress>
                                                </div> 
                                            @endif
                                            @if($student->final_score >= '34' && $student->final_score <= '63')
                                                <div class="col-md-10">
                                                    <progress class="progress progress-warning progress-sm" value="{{ $student->final_score }}" max="100">{{ $student->final_score }}%</progress>
                                                </div>
                                            @endif
                                            @if($student->final_score >= '65' && $student->final_score <= '99')
                                                <div class="col-md-10">
                                                    <progress class="progress progress-success progress-sm" value="{{ $student->final_score }}" max="100">{{ $student->final_score }}%</progress>
                                                </div>
                                            @endif
                                            @if($student->final_score == '100')
                                                <div class="col-md-10">
                                                    <progress class="progress progress-info progress-sm" value="{{ $student->final_score }}" max="100">{{ $student->final_score }}%</progress>
                                                </div>
                                            @endif

							
                                        </td>
                                        
                                        <td>
                                            <div class="btn-group">
                                                <a href="/my-student-progress/{{$subject->slug}}/{{$student->user->id}}" type="button" class="btn btn-info">
                                                Ver Progreso
                                                </a>
                                            </div>
                                        </td>
                                       
                                    </tr>
                                @endforeach
                             </tbody>
                          </table>
                        </div>
                   </div>
                </div>
            </div>

        </div>
    </div>
@endsection
