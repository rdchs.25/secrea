@extends('layouts.app')

@section('content')  
            <div class="content-area py-1">
               <div class="container-fluid">
                  <div id="cabeceraFlotante">
                     <h4>Monitoreo general de asignaturas</h4>
                     <nav class="navbar navbar-light bg-white b-a ">
                        <ol class="breadcrumb no-bg mb-0">
                           <li class="breadcrumb-item"><a href="#">Home</a></li>
                           <li class="breadcrumb-item active"><a href="#"> Cursos Asignados </a></li>
                        </ol>
                     </nav>
                  </div>


                  <div  id="mFloatBox2"  >

                  </div>

                  <div  class="row mt-2 mb-0 mb-md-1">

                     <div class="col-md-12">
                        
                      @foreach($subjects as $item)
                        <div class="col-xl-4 col-lg-3 col-md-3 col-sm-6 col-xs-12">
                         <div class="box bg-white user-1 mt-2">
                           <div class="fondocurso">
                             <a href="/my-student/{{$item->subject->slug}}">
                              <div class="u-img img-cover" style="background-image: url(/storage/{{ $item->subject->path_image_subject }});"></div>
                             </a>
                           </div>
                           <div class="u-content mt-3">
                             <h5><a class="text-black">{{ $item->subject->name }}</a></h5>
                             <h6><a class="text-small">{{ $item->grade->name." \"".$item->grade->section."\""}}</a></h6>
                           </div>
                           <div class="u-counters">
                              <div class="row no-gutter icon-ourse">
                                 
                                 <div class="col-xs-6 uc-item ">
                                    <a class="text-black" href="/course-preview/{{$item->subject->id}}" >
                                       <strong><i class=" ti-bookmark ml-0-5"></i></strong>
                                       <span>Ver Curso</span>
                                    </a>
                                 </div>
                                 <div class="col-xs-6 uc-item">
                                    <a class="text-black" href="/my-student/{{$item->subject->slug}}">
                                       <strong><i class="  ti-pulse ml-0-5"></i></strong>
                                       <span>Monitor de progreso</span>
                                    </a>
                                 </div>
                                 
                                 
                              </div>
                           </div>
                         </div>
                       </div>
                      @endforeach
                     </div>
                  </div>

@endsection
