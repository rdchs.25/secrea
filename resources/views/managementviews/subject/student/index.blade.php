@extends('layouts.app')

@section('content')  
    <div class="content-area py-1">
        <div class="container-fluid">
            @if( Auth::user()->isRole('registrado') || Auth::user()->isRole('student') )
            @else
            <div id="cabeceraFlotante">
                <h4>Asignaturas</h4>
                <nav class="navbar navbar-light bg-white b-a ">
                    <ol class="breadcrumb no-bg mb-0">
                        <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
                        <li class="breadcrumb-item active">Mis Asignaturas </a></li>
                    </ol>
                </nav>
            </div>
            @endif
            <div id="mFloatBox2">
              <div class="col-md-12 col-sm-12  col-xs-12">
                <div class="banner">
                <div class="b-img img-cover" style="background-image: url(img/photos-1/1.jpg);"></div>
                  <div class="gradient gradient-success"></div>
                  <div class="b-content">
                    <div class="b-title"><span id="grado">Mis Asignaturas</span></div>
                    <div class="b-text">Recuerda que los contenidos de tus cursos se encuentran adaptados a tu grado de estudio</div>
                  </div>
                </div>
                <br>
              </div>
              @foreach($filtered as $index => $item)
              <a href="/course/{{$item->id}}">
                <div class="col-md-3 col-md-4  col-sm-6  col-xs-12">
                  <div class="box bg-white user-1">
                    <div class="fondocurso">
                      <div class="u-img img-cover" style="background-image: url(/storage/{{ $item->assignmentTeacher->subject->path_image_subject }});"></div>
                    </div>
                    <div class="u-content mt-3">
                      <h5><a class="text-black" href="/course/{{$item->id}}">{{ $item->assignmentTeacher->subject->name }}</a></h5>
                      <p class="text-muted">
                        <b>Docente:</b> {{ $item->assignmentTeacher->user->name }}
                      </p>
                    </div>
                    <div class="u-counters">
                      <div class="row no-gutter icon-ourse">
                          
                          <div class="col-xs-6 uc-item ">
                            <a class="text-black" href="/course/{{$item->id}}">
                                <strong><i class="ti-bookmark ml-0-5"></i></strong>
                                <span>Ver Curso</span>
                            </a>
                          </div>
                          <div class="col-xs-6 uc-item">
                            <a class="text-black" href="/my-evaluations/{{$item->assignmentTeacher->subject->slug}}">
                                <strong><i class=" ti-pulse ml-0-5"></i></strong>
                                <span>Mis Evaluaciones</span>
                            </a>
                          </div>
                          
                          
                      </div>
                    </div>
                  </div>
                </div>
              </a>
              @endforeach
        </div>
    </div>
@endsection
