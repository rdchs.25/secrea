@extends('layouts.app')
@section('content')
    @php
        $tips = [
            'Estudiar por obligación es una de las actividades más aburridas de esta vida.',
            'Proporciónale a tu cerebro el tiempo que necesita para procesar la información.',
            'Explica lo que estás estudiando a alguien.',
            'Busca una aplicación real a lo que estás estudiando',
            'Usa internet para buscar algo que no conoces en tu día a día',
            'Aprende con tus propias palabras.',
            'Guárdate una recompensa para cuando acabes tu tiempo de estudio.',
            'Aprendizaje de texto escrito.',
            'Motívate antes de empezar.',
            'Toda información se resume en una gran idea.',
            'Puede mejorar nuestro estado de ánimo e, incluso, mejorar nuestros hábitos',
            'Aprovecha las horas en las que eres más productivo mentalmente.',
            'El estrés no es nada bueno para nuestra mente.',
            'Es importante descansar un par de horas cada cierto tiempo de estudio.',
            'El mundo cambia, la forma de estudiar evoluciona escoge la mejor forma de parender.',
            'Focaliza completamente en el estudio dejando aisladas el resto de distracciones.',
            'Aprenderemos mucho más si sabemos conectar conceptos en vez de memorizar.',
            'Intenta convertir la información abstracta en una imagen. ',
            'Aprender a controlar nuestros pensamientos ayuda a mejorar la curva del aprendizaje.',
            'Estudia con siglas. C.L.O.N. (Litio, Carbono, Nitrógeno, Oxígeno, Neón)',
            'Un cambio de habitación puede ayudarte a retener mejor la información ',
            'Varía las asignaturas de estudio.',
            'Coge una agenda y vete marcando cada día pequeños objetivos'
        ];
    @endphp
    <!--<div id='content-comunicado'>        
        <div class='container'>
            <div class='row'>
                <button class='btn btn-sm btn-danger pull-right' id="close-comunicado">Cerrar</button>
                <br>
                <br>
                <div id='comunicado' class='col-12 col-md-6 offset-md-3'>
                    <img src="img/logo2.png" width='250px'>
                    <br>
                    Estimado estudiante, se le solicita con carácter de urgencia presentar su documentación respectiva,
                    la cual consiste en certificado de estudios visado por la ugel, copia de dni por ambos lados.
                    Por favor esta documentación debe ser enviada a la oficina ubicada en la calle TACNA N° 065 –
                    CHICLAYO –REFERENCIA EN EL SEGUNDO PISO DE LA UNIVERSIDAD DE LAMBAYEQUE. Además no se olviden de 
                    realizar el pago de sus pensiones respectivas.
                    <br>
                    <br>
                    Los documentos deben ser enviados a la señorita ANGHI MARJORIE DIAZ DIAZ.
                    <br>
                    <br>
                    <b>CONSULTAS: 914521751</b>
                </div>
            </div>
        </div>
    </div>-->

    <div class="content-area py-1" id="app">
        <div class="container-fluid m-2">
        <!--<h4>Página en Blanco</h4>
            <ol class="breadcrumb no-bg mb-1">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Ubicación 1</a></li>
                <li class="breadcrumb-item active">Ubicación 2</li>
            </ol>
        </br>-->
            <div class="row">

                <div class="col-lg-5 col-md-3">
                    <div class="card cardHome pt-2 pb-2 pl-2 pr-2" style="color:#3e4e5c;background-color: #fff; border-color: #fff;">
                        <div class="card-block">
                            <img class="imgMenu " src="/img/other/rocket.png">
                            <h1 class="card-title nameUser">Hola {{ explode( ' ',Auth::user()->name )[0] }},</h1>
                            <h4 class="card-title">¿Qué quieres hacer hoy?</h4>
                            <!-- <p class="card-text">¿Qué quieres hacer hoy?</p> -->
                            <!-- <a href="#" class="btn btn-primary"></a> -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card card-inverse cardHome mb-1 pt-1 pb-2 pl-2 pr-2" style="background-color: #43b291; border-color: #43b291;">
                        <div class="card-block">
                            <img class="imgMenu " src="/img/other/001-youtube.png">
                            <h3 class="card-title">Quiero ser un experto en SECREA </h3>
                            <!-- <p class="card-text">Aquí puedes ver tu lista de cursos.</p> -->
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            <video-modal></video-modal>
                        </div>
                    </div>
                </div>
                
                @role('student')
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="{{ asset('/my-subjects')}}">
                    <div class="card card-inverse cardHome mb-1 pt-1 pb-2 pl-2 pr-2" style="background-color: #43afa7; border-color: #43afa7;">
                        <div class="card-block">
                            <img class="imgMenu " src="/img/other/web.png">
                            <h3 class="card-title">Quiero empezar a leer mis cursos.</h3>
                            <p class="card-title">Aquí puedes ver tu lista de cursos </p>
                            <!-- <p class="card-text">Aquí puedes ver tu lista de cursos.</p> -->
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            <a href="{{ asset('/my-subjects')}}" class="btn btn-black btn-circle waves-effect waves-light">
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    </a>
                </div>
                @endrole
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="/document/{{Auth::user()->id}}/edit"> 
                    <div class="card card-inverse cardHome mb-1 pt-1 pb-2 pl-2 pr-2" style="background-color: #43b67e; border-color: #43b67e;">
                        <div class="card-block">
                            <img class="imgMenu " src="/img/other/013-interface.png">
                            <h3 class="card-title">Subir mi voucher, certificado y DNI</h3>
                            <p class="card-title">Tengo el archivo escaneado </p>
                            <!-- <p class="card-text">Aquí puedes ver tu lista de cursos.</p> -->
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            <button type="button" class=" btn btn-black btn-circle waves-effect waves-light">
                            <i class="ti-arrow-right"></i>
                        </button>
                        </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <a href="/my-payment/current">
                    <div class="card card-inverse cardHome mb-1 pt-1 pb-2 pl-2 pr-2" style="background-color: #3e4e5c; border-color: #3e4e5c;">
                        <div class="card-block">
                            <img class="imgMenu " src="/img/other/006-money.png">
                            <h3 class="card-title">Ver mi lista de pagos. </h3>
                            <p class="card-title">Aquí puedes ver tus pagos </p>
                            <!-- <p class="card-text">Aquí puedes ver tu lista de cursos.</p> -->
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            <button type="button" class="btn btn-black btn-circle waves-effect waves-light">
                            <i class="ti-arrow-right"></i>
                        </button>
                        </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card card-inverse cardHome mb-1 pt-1 pb-2 pl-1 " style="background-color: #2e767d; border-color: #2e767d;">
                        <div class="card-block">
                            <h1 class="card-title">"</h1>
                            <h3 class="card-title">{{ $tips[rand(0, sizeOf($tips)-1)] }}</h3>
                            <p class="card-title"> Tip de estudio </p>
                            <!-- <p class="card-text">Aquí puedes ver tu lista de cursos.</p> -->
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <a href="/profile/{{Auth::user()->id}}/edit">
                    <div class="card card-inverse cardHome mb-1 pt-1 pb-2 pl-2 pr-2" style="background-color: #3e4e5c; border-color: #3e4e5c;">
                        <div class="card-block">
                            <img class="imgMenu " src="/img/other/003-draw.png">
                            <h3 class="card-title">Quiero editar mi información personal </h3>
                            <p class="card-title">Nombres, celular, etc </p>
                            <!-- <p class="card-text">Aquí puedes ver tu lista de cursos.</p> -->
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            <button type="button" class="btn btn-black btn-circle waves-effect waves-light">
                            <i class="ti-arrow-right"></i>
                        </button>
                        </div>
                    </div>
                    </a>
                </div>
               
                
                
                
            </div>

            <div id="comunicacionBtn">
                @role('registrado')
                <div class="labelOp">
                    Soporte
                </div>

                <a href="/support-message/create" class="btn-comment btn btn-lg pt-1 btn-success btn-circle waves-effect waves-light">
                    <i class="ti-headphone-alt"></i>
                </a>
                <!--<div class="fb-customerchat" page_id="1612558169053650">-->
                </div>
                @endrole
                @role('student')
                <div class="labelOp">
                    Chat
                </div>

                <a href="/chat" class="btn-comment btn btn-lg btn-success pt-1 btn-circle waves-effect waves-light">
                    <i class="mt-1 ti-comment-alt"></i>
                </a>
                @endrole
            </div>

            
            

        </div>
    </div>

    <!--<script>
        window.fbAsyncInit = function(){
            FB.init({
                appId : '740724009452798',
                autoLogAppEvents:true,
                xfbml:true,
                version:'v2.11'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if(d.getElementById(id)){return;}
            js = d.createElement(s);js.id = id;
            js.src="https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js,fjs);
        }(document,'script','facebook-jssdk'));
    </script>-->
    
    <!--<script> 
        $( "#close-comunicado" ).click(function() {
            let contenedor = document.getElementById('content-comunicado')
            contenedor.style.visibility = 'hidden';
            contenedor.style.opacity = '0';
        });
    </script>-->
@endsection