<!DOCTYPE html>
<html lang="es">

    <head>
        @include('partials.head')
    </head>
    
    <body class="img-cover" style="background-image: url(img/photos-1/1.jpg);">
        <div class="container-fluid">
        {!! Toastr::render() !!}
            <div class="sign-form">
                <div class="row">
                    <div class="col-md-4 offset-md-4 px-3">
                        <div class="box b-a-0">
                            <div class="p-2 text-xs-center">
                                <div class="logoLogin">
                                    <img src="img/logo2.png">
                                </div>
                            </div>

                            <form class="form-material" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nombre y Apellido" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
 
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Crear contraseña" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                   
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="px-2 form-group mb-0">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar contraseña" required>

                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('dni') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="dni" type="text" class="form-control" name="dni" placeholder="Documento Nacional de Identidad (DNI)" required>
                                        @if ($errors->has('dni'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dni') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('birth_date') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <div class="col hidden-xs-down">
                                            
                                        </div>
                                        <input id="birth_date" type="date" class="form-control" name="birth_date" required >
                                        <label style="font-size:10px" > <span class="fa fa-birthday-cake"></span> Fecha de nacimiento (dd = día, mm = mes, aaaa = año)</label>
                                        @if ($errors->has('birth_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('birth_date') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('years'))
                                            <span class="help-block">
                                                <strong>Ingresa tu fecha de nacimiento</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="phone" type="tel" class="form-control" name="phone" placeholder="Número de contacto" required>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('grade_id') ? 'has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <?php 
                                            $igrades = array();
                                            foreach($grades as $grade) {
                                            $igrades[$grade->id] = $grade->name;
                                            }    
                                        ?>
                                        {!! Form::select('grade_id', $igrades, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un grado académico']); !!}
                                        @if ($errors->has('grade_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('grade_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="px-2 form-group mb-0">
                                        <button type="submit" class="btn btn-success btn-block waves-effect waves-light">
                                            Registrar
                                        </button>
                                    </div>
                                </div>

                                <div class="p-2 text-xs-center text-muted">
                                    ¿Ya tienes una cuenta? <a class="text-black" href="{{ url('/login') }}"><span class="underline">Entrar</span></a>
                                </div>

                            </form>

                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.scripts')
    </body>
</html>