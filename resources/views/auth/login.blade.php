<!DOCTYPE html>
<html lang="es">

    <head>
        @include('partials.head')
    </head>

    <body class="img-cover" style="background-image: url(img/photos-1/1.jpg);">
        <div class="container-fluid">
        {!! Toastr::render() !!}
            <div class="sign-form">
                <div class="row">
                    <div class="col-md-4 offset-md-4 px-3">
                        <div class="box b-a-0">
                            <div class="p-2 text-xs-center">
                                <div class="logoLogin">
                                    <img src="img/logo2.png">
                                </div>
                            </div>

                            <form class="form-material" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo Electrónico" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="px-2 form-group mb-0">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif               

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="px-2 form-group mb-0">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuérdame
                                            </label>
                                        </div>                                
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="px-2 form-group mb-0">
                                        <button type="submit" class="btn btn-success btn-block waves-effect waves-light"><i class=" ti-angle-right float-xs-right"></i>Entrar</button>
<!--                                         <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Olvidaste tu contraseña?
                                        </a> -->
                                    </div>
                                </div>

                            </form>

                            <div class="p-2 text-xs-center text-muted">
                                <!--<a class="btn btn-primary btn-block waves-effect waves-light" href="{{ route('login.redsocial', ['rs' => 'facebook']) }}">
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    Facebook
                                </a>
                                <a class="btn btn-warning btn-block waves-effect waves-light" href="{{ route('login.redsocial', ['rs' => 'google']) }}">
                                    <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                    Google
                                </a>
                                -->
                                <div class="p-2 text-xs-center text-muted">
                                    ¿Todavía no tienes una cuenta? <a class="text-black" href="{{ url('/register') }}"><span class="underline">Regístrate</span></a>
                                </div>

                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.scripts')
    </body>
</html>